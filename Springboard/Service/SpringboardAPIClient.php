<?php declare(strict_types=1);

namespace C38\Springboard\Service;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class SpringboardAPIClient
 * Class using for communications with Springboard API
 */
class SpringboardAPIClient
{
    private const USER_AGENT = 'springboard-sdk-php/1.0';

    private const VERSION_PREFIX = 'api';

    private const RESULTS_PER_PAGE = 100;

    /**
     * @var Curl
     */
    private $curl;

    /**
     * @var Json
     */
    private $json;

    /**
     * The oauth access token granted to the client application. This key
     * will be used in most service method calls.
     *
     * @var string
     */
    private $accessToken;

    /**
     * The service url the client will connect to.
     *
     * @var string
     */
    private $url;

    /**
     * Flag to toggle debug mode.
     *
     * @var boolean
     */
    private $debugMode;

    /**
     * Array to capture debug information.
     *
     * @var array
     */
    private $debugInfo = [];

    /**
     * Array to store http post data.
     *
     * @var array
     */
    private $postFields = [];

    /**
     * SpringboardAPIClient constructor.
     *
     * @param Curl $curl
     * @param Json $json
     */
    public function __construct(
        Curl $curl,
        Json $json
    ) {
        $this->json = $json;
        $this->curl = $curl;
        $this->debugMode = false;
    }

    /**
     * Setup springboard API
     *
     * @param string $url
     * @param string $apiToken
     * @param bool $debugMode
     *
     * @return SpringboardAPIClient
     */
    public function setupAPI(
        string $url,
        string $apiToken,
        bool $debugMode = true
    ): SpringboardAPIClient {
        $this->accessToken = $apiToken;
        $this->url = $url;
        $this->debugMode = $debugMode;

        return $this;
    }

    /**
     * Public method to enable SDK debug mode.
     * @noinspection PhpUnused
     */
    public function enableDebugMode(): void
    {
        $this->debugMode = true;
    }

    /**
     * Public method to disable SDK debug mode.
     * @noinspection PhpUnused
     */
    public function disableDebugMode(): void
    {
        $this->debugMode = false;
    }

    /**
     * Public method to indicate if the SDK is in debug mode.
     *
     * @return boolean The current debug mode state.
     */
    public function isDebugMode(): bool
    {
        return $this->debugMode;
    }

    /**
     * Public method to return the latest SDK debug info.
     *
     * @return array Structured array of debug info with the following keys:
     *         - http_method
     *         - request_path
     *         - url
     *         - access_token
     *         - params
     *         - post_fields
     *         - response
     */
    public function getDebugInfo(): array
    {
        return $this->debugInfo;
    }

    /**
     * Public method to return all locations
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of Locations objects.
     * @throws Exception
     */
    public function getLocations()
    {
        $response = $this->doRequest('GET', 'locations');
        return $response['response'];
    }

    /**
     * Public method to return all items
     *
     * @param int $page
     * @param int $limit
     * @param string $filterDate
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of Items objects.
     * @throws Exception
     */
    public function getItems(int $page = 1, int $limit = self::RESULTS_PER_PAGE, string $filterDate = '')
    {
        $param = '';
        if (!empty($filterDate)) {
            $param .= '~[updated_at][$gt]=' . date('Y-m-d', strtotime($filterDate));
        }
        $response = $this->doRequest(
            'GET',
            'items',
            [
                $param,
                'per_page' => $limit,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * Public method to create an item.
     *
     * @param array $item An array of required item field values.
     *
     * @return int|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function createItem(array $item)
    {
        $this->postFields = $item;
        $response = $this->doRequest('POST', 'items');
        if (empty($response['location'])) {
            return null;
        } else {
            return filter_var($response['location'], FILTER_SANITIZE_NUMBER_INT);
        }
    }

    /**
     * Public method to create an item.
     *
     * @param array $item An array of required item field values.
     *
     * @return object A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     */
    public function createRealItem(array $item)
    {
        $this->postFields = $item;
        $response = $this->doRequest('POST', 'items');
        if (!empty($response['location'])) {
            return $response['location'];
        }

        return $response['response'];
    }

    /**
     * Public method to update an item.
     *
     * @param array $item An array of required item field values.
     * @param string $id An item ID.
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     */
    public function updateItem(array $item, string $id)
    {
        $this->postFields = $item;
        $response = $this->doRequest('PUT', 'items/' . $id);
        return $response['response'];
    }

    /**
     * Public method to return inventory values
     *
     * @param string $itemId The Item ID.
     * @param int $page Page number.
     * @param int $perPage
     * @param string $filterDate
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of metrics values.
     * @throws Exception
     */
    public function getInventoryValues(
        string $itemId,
        int $page = 1,
        int $perPage = 200,
        string $filterDate = ''
    ) {
        $param = 'group[]=location_id&group[]=item_id';
        if (!empty($filterDate)) {
            $param .= '&~[updated_at][$gt]=' . date('Y-m-d', strtotime($filterDate));
        }

        $response = $this->doRequest(
            'GET',
            'inventory/values',
            [
                $param,
                'sort' => 'item_id',
                'location_id' => $itemId,
                'per_page' => $perPage,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * Public method to update online store item qty.
     *
     * @param string $itemId Springboard item ID.
     * @param string $qty An item qty in online store.
     * @param string $cost An item cost in online store.
     * @param string $locationId Springboard location ID.
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     */
    public function setOnlineStoreQty(
        string $itemId,
        string $qty,
        string $cost,
        string $locationId
    ) {
        $this->postFields = [
            'adjustment_reason_id' => 100003,
            'item_id' => $itemId,
            'location_id' => $locationId,
            'qty' => $qty,
            'unit_cost' => $cost,
        ];
        $response = $this->doRequest(
            'POST',
            'inventory/adjustments'
        );
        return $response['response'];
    }

    /**
     * Public method to return all orders.
     *
     * @param int $page Page number.
     * @param string $startDate
     * @param string $startFromLocationId
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of districts keyed by legislative chamber.
     * @throws Exception
     */
    public function getOrders(
        int $page = 1,
        string $startDate = '2018-06-28',
        string $startFromLocationId = '100042'
    ) {
        $startDate = strtotime($startDate);
        $startDate = gmdate("Y-m-d", $startDate);

        $response = $this->doRequest(
            'GET',
            'sales/orders',
            [
                '~[source_location_id][$gte]=' . $startFromLocationId
                    . '&~[created_at][$gt]=' . $startDate
                    . '&_include[]=lines&_include[]=customer&_include[]=billing_address&_include[]=shipping_address',
                'per_page' => self::RESULTS_PER_PAGE,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * Public method to return one order.
     *
     * @param string $orderId Springboard order id.
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of districts keyed by legislative chamber.
     * @throws Exception
     */
    public function getOrder(string $orderId)
    {
        $response = $this->doRequest(
            'GET',
            'sales/orders/' . $orderId,
            [
                '_include[]=lines&_include[]=customer&_include[]=billing_address&_include[]=shipping_address'
            ]
        );
        return $response['response'];
    }

    /**
     * Public method to return all Tickets.
     *
     * @param int $page Page number.
     * @param string $startFromLocationId
     * @param string $filterDate
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of districts keyed by legislative chamber.
     * @throws Exception
     */
    public function getTickets(
        int $page = 1,
        string $startFromLocationId = '100042',
        string $filterDate = ''
    ) {
        $param = '~[source_location_id][$gte]='
            . $startFromLocationId . '&~[type][$neq]=Invoice&_include[]=lines&_include[]=customer';
        if (!empty($filterDate)) {
            $param .= '&~[created_at][$gt]=' . date('Y-m-d', strtotime($filterDate));
        }
        $response = $this->doRequest(
            'GET',
            'sales/tickets',
            [
                $param,
                'per_page' => self::RESULTS_PER_PAGE,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * Public method to return Complete Tickets.
     *
     * @param int $page Page number.
     * @param string $startFromLocationId
     * @param string $filterDate
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of districts keyed by legislative chamber.
     * @throws Exception
     */
    public function getCompleteTickets(
        int $page = 1,
        string $startFromLocationId = '100042',
        string $filterDate = ''
    ) {
        $param = '~[source_location_id][$gte]=' . $startFromLocationId .
            '&~[status][$eq]=complete&_include[]=lines&_include[]=customer' .
            '&_include[]=billing_address&_include[]=shipping_address';
        if (!empty($filterDate)) {
            $param .= '&~[updated_at][$gt]=' . date('Y-m-d', strtotime($filterDate));
        }
        $response = $this->doRequest(
            'GET',
            'sales/tickets',
            [
                $param,
                'per_page' => self::RESULTS_PER_PAGE,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * Public method to get all transfers.
     *
     * @param int $page Page number.
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of districts keyed by legislative chamber.
     * @throws Exception
     */
    public function getTransfers(int $page = 1)
    {
        $response = $this->doRequest(
            'GET',
            'inventory/transfers',
            [
                '~[created_at][$gt]=2018-05-17',
                'per_page' => self::RESULTS_PER_PAGE,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * Public method to get all transfer lines.
     *
     * @param string $transferId Transfer Id.
     * @param int $page Page number.
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of districts keyed by legislative chamber.
     * @throws Exception
     */
    public function getTransferLines(string $transferId, int $page = 1)
    {
        $response = $this->doRequest(
            'GET',
            'inventory/transfers/' . $transferId . '/lines',
            [
                'per_page' => self::RESULTS_PER_PAGE,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * Public method to update transfer.
     *
     * @param array $transfer
     * @param string $id An item ID.
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     */
    public function updateTransfer(array $transfer, string $id)
    {
        $this->postFields = $transfer;
        $response = $this->doRequest('PUT', 'inventory/transfers/' . $id);
        return $response['response'];
    }

    /**
     * Public method to get all purchasing vendors.
     *
     * @param int $page Page number.
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of districts keyed by legislative chamber.
     * @throws Exception
     */
    public function getVendors(int $page = 1)
    {
        $response = $this->doRequest(
            'GET',
            'purchasing/vendors',
            [
                'per_page' => self::RESULTS_PER_PAGE,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * Public method to return all customers.
     *
     * @param int $page Page number.
     * @param int $perPage
     * @param string $filterDate
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of districts keyed by legislative chamber.
     * @throws Exception
     */
    public function getCustomers(
        int $page = 1,
        int $perPage = self::RESULTS_PER_PAGE,
        string $filterDate = ''
    ) {
        $param = '';
        if (!empty($filterDate)) {
            $param .= '&~[updated_at][$gt]=' . date('Y-m-d', strtotime($filterDate));
        }

        $response = $this->doRequest(
            'GET',
            'customers',
            [
                $param,
                'per_page' => $perPage,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * Public method to retrieve a customer.
     *
     * @param string $id A customer ID.
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of districts keyed by legislative chamber.
     * @throws Exception
     */
    public function retrieveCustomer(string $id)
    {
        $response = $this->doRequest('GET', 'customers/' . $id);
        return $response['response'];
    }

    /**
     * Public method to create a customer.
     *
     * @param array $data An array of required customer field values.
     *
     * @return string[]|null
     * @throws Exception
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function createCustomer(array $data)
    {
        $this->postFields = $data;
        return $this->doRequest('POST', 'customers');
    }

    /**
     * Public method to update a customer.
     *
     * @param array $data An array of required item field values.
     * @param string $id A customer ID.
     *
     * @return array|string[] A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function updateCustomer(array $data, string $id)
    {
        $this->postFields = $data;
        return $this->doRequest('PUT', 'customers/' . $id);
    }

    /**
     * Public method to create an address.
     *
     * @param array $data An array of required address field values.
     * @param string $customerId A customer ID.
     *
     * @return int|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function createAddress(array $data, string $customerId)
    {
        $this->postFields = $data;
        $response = $this->doRequest('POST', 'customers/' . $customerId . '/addresses');
        if (empty($response['location'])) {
            return null;
        } else {
            $resultId = str_replace('customers/' . $customerId . '/addresses', '', $response['location']);
            return filter_var($resultId, FILTER_SANITIZE_NUMBER_INT);
        }
    }

    /**
     * Public method to create a purchase order.
     *
     * @param array $order An array of required order field values.
     *
     * @return int|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function createPurchaseOrder(array $order)
    {
        $this->postFields = $order;
        $response = $this->doRequest('POST', 'purchasing/orders');
        if (empty($response['location'])) {
            return null;
        } else {
            return filter_var($response['location'], FILTER_SANITIZE_NUMBER_INT);
        }
    }

    /**
     * Public method to create an invoice.
     *
     * @param array $invoice An array of required invoice field values.
     *
     * @return array|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function createInvoice(array $invoice)
    {
        $this->postFields = $invoice;
        return $this->doRequest('POST', 'sales/invoices');
    }

    /**
     * Public method to add an item to an purchase order.
     *
     * @param array $line An array of required line field values.
     * @param string $invoiceId
     *
     * @return array|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function addLineToInvoice(array $line, string $invoiceId)
    {
        $this->postFields = $line;
        return $this->doRequest(
            'POST',
            'sales/invoices/' . $invoiceId . '/lines',
            [
                '_include[]=item'
            ]
        );
    }

    /**
     * Public method to update an invoice.
     *
     * @param array $invoice An array of required invoice field values.
     * @param string $invoiceId Id of invoice on springboard
     *
     * @return array|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function updateInvoice(array $invoice, string $invoiceId)
    {
        $this->postFields = $invoice;
        return $this->doRequest('PUT', 'sales/invoices/' . $invoiceId);
    }

    /**
     * Public method to return all Invoices.
     *
     * @param int $page Page number.
     * @param string $startFromLocationId
     * @param string $filterDate
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array of districts keyed by legislative chamber.
     * @throws Exception
     */
    public function getInvoices(int $page = 1, string $startFromLocationId = '100042', string $filterDate = '')
    {
        $param = '~[source_location_id][$gte]=' . $startFromLocationId . '&_include[]=lines';
        if (!empty($filterDate)) {
            $param .= '&~[created_at][$gt]=' . date('Y-m-d', strtotime($filterDate));
        }

        $response = $this->doRequest(
            'GET',
            'sales/invoices',
            [
                $param,
                'per_page' => self::RESULTS_PER_PAGE,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * Public method to open a purchase order.
     *
     * @param string $poId Purchase Order Springboard ID.
     *
     * @return object|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     */
    public function openPurchaseOrder(string $poId)
    {
        $this->postFields = ['status' => 'open'];
        $response = $this->doRequest('PUT', 'purchasing/orders/' . $poId);
        return $response['response'];
    }

    /**
     * Public method to add an item to an purchase order.
     *
     * @param array $line An array of required line field values.
     * @param string $orderId A purchase order ID.
     *
     * @return int|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function addLineToPurchaseOrder(array $line, string $orderId)
    {
        $this->postFields = $line;
        $response = $this->doRequest('POST', 'purchasing/orders/' . $orderId . '/lines');
        if (empty($response['location'])) {
            return null;
        } else {
            return filter_var($response['location'], FILTER_SANITIZE_NUMBER_INT);
        }
    }

    /**
     * Public method to create a vendor.
     *
     * @param array $vendor An array of required vendor field values.
     *
     * @return int|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function createVendor(array $vendor)
    {
        $this->postFields = $vendor;
        $response = $this->doRequest('POST', 'purchasing/vendors');
        if (empty($response['location'])) {
            return null;
        } else {
            return filter_var($response['location'], FILTER_SANITIZE_NUMBER_INT);
        }
    }

    /**
     * @param int $page
     *
     * @return object|null
     * @throws Exception
     */
    public function getSalesPlans(int $page = 1)
    {
        $response = $this->doRequest(
            'GET',
            'sales/plans',
            [
                'per_page' => self::RESULTS_PER_PAGE,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * @return object|null
     * @throws Exception
     */
    public function getShippingMethods()
    {
        $response = $this->doRequest(
            'GET',
            'shipping/methods',
            [
                'per_page' => self::RESULTS_PER_PAGE,
                'page' => 1,
            ]
        );
        return $response['response'];
    }

    /**
     * @param int $page
     *
     * @return object|null
     * @throws Exception
     */
    public function getWebhooks(int $page = 1)
    {
        $response = $this->doRequest(
            'GET',
            'webhooks',
            [
                'per_page' => self::RESULTS_PER_PAGE,
                'page' => $page,
            ]
        );
        return $response['response'];
    }

    /**
     * @param string $webhookUrl
     * @param array $events
     *
     * @return object|bool
     * @throws Exception
     */
    public function createWebhook(string $webhookUrl, array $events)
    {
        if (!$webhookUrl || !$events) {
            return false;
        }
        $this->postFields = [
            'url' => $webhookUrl,
            'events' => $events
        ];
        $response = $this->doRequest(
            'POST',
            'webhooks'
        );
        return $response['response'];
    }

    /**
     * @param string $webhookId
     *
     * @return array|bool
     * @throws Exception
     */
    public function deleteWebhook(string $webhookId)
    {
        if (!$webhookId) {
            return false;
        }

        return $this->doRequest(
            'DELETE',
            'webhooks/' . $webhookId,
            []
        );
    }

    /**
     * Public method to assign image to an item.
     *
     * @param string $id An item ID.
     * @param string $url An image url.
     *
     * @return int|null A response object with an 'error' property containing a message
     * or a 'data' property containing an array with keys/values:
     * @throws Exception
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function uploadImage(string $id, string $url)
    {
        $this->postFields = [
            'source' => 'url',
            'url' => $url,
        ];
        $response = $this->doRequest('POST', 'items/' . $id . '/images');
        if (empty($response['location'])) {
            return null;
        } else {
            $resultId = str_replace('items/' . $id . '/images', '', $response['location']);
            return filter_var($resultId, FILTER_SANITIZE_NUMBER_INT);
        }
    }

    /**
     * @param string $itemId
     * @param string $imageId
     *
     * @return object|null
     * @throws Exception
     */
    public function setPrimaryImage(string $itemId, string $imageId)
    {
        $this->postFields = [
            'primary' => true,
        ];
        $response = $this->doRequest('PUT', 'items/' . $itemId . '/images/' . $imageId);
        return $response['response'];
    }

    /**
     * Private method for making the actual call to the API.
     *
     * @param string $httpMethod The HTTP verb to use for the call.
     * @param string $requestPath
     * @param array|null $params The parameters required by the service method.
     *
     * @return array
     * @throws Exception
     */
    private function doRequest(string $httpMethod, string $requestPath, $params = null): array
    {
        $this->validHttpVerb($httpMethod);
        $this->validApiMethod($requestPath, $httpMethod);

        $url = $this->buildRequestUrl($requestPath, $params);
        $curl = $this->prepareCurl($url, $httpMethod);

        // Send off the request.
        $response = $this->sendCurlRequest($curl, $httpMethod);

        // Stash some debug information if debug mode is on.
        if ($this->isDebugMode()) {
            $this->addDebugInfo('http_method', $httpMethod);
            $this->addDebugInfo('request_path', $requestPath);
            $this->addDebugInfo('url', $url);
            $this->addDebugInfo('access_token', $this->accessToken);
            $this->addDebugInfo('params', $params);
            $this->addDebugInfo('post_fields', $this->postFields);
            $this->addDebugInfo('response', $response);
        }

        $this->postFields = [];
        return $response;
    }

    /**
     * Method to describe the available service methods.
     *
     * @return array
     */
    private function getApiMethods(): array
    {
        return [
            'GET' => [
                'customers',
                'customers/%d',
                'locations',
                'items?~[id][$gt]=101033',
                'items',
                'items/%d',
                'inventory/adjustment_sets',
                'inventory/values',
                'sales/orders',
                'sales/orders/%d',
                'sales/tickets',
                'sales/invoices',
                'purchasing/vendors',
                'inventory/transfers',
                'inventory/transfers/%d/lines',
                'sales/invoices/%d',
                'sales/invoices/%d/lines',
                'sales/plans',
                'webhooks',
                'shipping/methods'
            ],
            'POST' => [
                'items',
                'items/%d/images',
                'inventory/adjustments',
                'customers',
                'customers/%d/addresses',
                'sales/invoices',
                'purchasing/vendors',
                'purchasing/orders',
                'purchasing/orders/%d/lines',
                'sales/invoices/%d/lines',
                'webhooks'
            ],
            'PUT' => [
                'customers/%d',
                'items/%d',
                'purchasing/orders/%d',
                'items/%d/images/%d',
                'inventory/transfers/%d',
                'sales/invoices/%d',
                'sales/orders/%d/lines/%d'
            ],
            'DELETE' => [
                'webhooks/%d',
            ],
        ];
    }

    /**
     * Private function to validate that the request path being
     * called actually exists.
     *
     * @param string $requestPath The name of the service method to call.
     * @param string $httpMethod The HTTP verb to use for the call.
     *
     * @return boolean True if the method exists.
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    private function validApiMethod(string $requestPath, string $httpMethod): bool
    {
        $methods = $this->getApiMethods();

        $paths = explode('/', $requestPath);

        // remove ids from request path for validation.
        foreach ($paths as $key => $val) {
            if ($key >= 0 && ($val > 0)) {
                $paths[$key] = '%d';
            }
        }

        $requestPath = implode('/', $paths);

        if (!in_array($requestPath, $methods[$httpMethod])) {
            throw new LocalizedException(__('That api endpoint does not exist. ' . $requestPath));
        }
        return true;
    }

    /**
     * Private function to validate that the service method being
     * called actually exists.
     *
     * @param string $httpMethod The HTTP verb to use for the call.
     *
     * @return boolean True if the method exists.
     * @throws LocalizedException
     */
    private function validHttpVerb(string $httpMethod): bool
    {
        $validVerbs = ['GET', 'POST', 'PUT', 'DELETE'];
        if (!in_array($httpMethod, $validVerbs)) {
            throw new LocalizedException(__('Method does not exist.'));
        }
        return true;
    }

    /**
     * Private method to generate the path to a service method endpoint.
     *
     * @param string $requestPath
     * @param array|null $params
     *
     * @return string Path to service endpoint with api key and query string params.
     */
    private function buildRequestUrl(string $requestPath, ?array $params): string
    {
        $url = sprintf(
            '%s/%s/%s',
            $this->url,
            self::VERSION_PREFIX,
            $requestPath
        );

        // Add query params if available.
        if (!empty($params)) {
            $query = [];
            foreach ($params as $key => $value) {
                if (empty($key)) {
                    $query[] = $value;
                } else {
                    $query[] = rawurlencode("" . $key) . '=' . rawurlencode("" . $value);
                }
            }
            $url .= '?' . implode('&', $query);
        }
        return $url;
    }

    /**
     * Private method for setting CURL options.
     *
     * @param string $url The request url.
     * @param string $httpMethod The HTTP verb to use for the call.
     *
     * @return array Curl options.
     * @noinspection PhpComposerExtensionStubsInspection
     */
    private function prepareCurl(string $url, string $httpMethod): array
    {
        // Set curl options.
        $options = [
            CURLOPT_USERAGENT => self::USER_AGENT,
            CURLOPT_HEADER => true,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30
        ];

        if (!empty($this->postFields) && ($httpMethod == 'PUT' || $httpMethod == 'POST')) {
            $options[CURLOPT_POSTFIELDS] = $this->json->serialize($this->postFields);
        }

        if ($httpMethod == 'DELETE' || $httpMethod == 'PUT') {
            $options[CURLOPT_CUSTOMREQUEST] = $httpMethod;
        }

        if (!empty($this->accessToken)) {
            $options[CURLOPT_HTTPHEADER] = [
                'Authorization: Bearer ' . $this->accessToken,
                'Content-Type: application/json'
            ];
        }

        return $options;
    }

    /**
     * Private method for sending the Curl request.
     *
     * @param array $curlOptions
     * @param string $httpMethod
     *
     * @return array
     * @noinspection PhpComposerExtensionStubsInspection
     */
    private function sendCurlRequest(array $curlOptions, string $httpMethod): array
    {
        // set cURL options
        $this->curl->setOptions($curlOptions);

        // execute cURL command
        switch ($httpMethod) {
            case "POST":
                $this->curl->post($curlOptions[CURLOPT_URL], []);
                break;

            case "PUT":
                $this->curl->put($curlOptions[CURLOPT_URL], []);
                break;

            default:
                $this->curl->get($curlOptions[CURLOPT_URL]);
                break;
        }

        // get cURL response
        $resp = $this->curl->getBody();

        // prepare response
        if (count(explode("\r\n\r\n", $resp, 2)) != 2) {
            return [
                'location' => '',
                'response' => '',
            ];
        }
        list($headers, $json) = explode("\r\n\r\n", $resp, 2);

        // get response json as object
        $response = json_decode($json);

        // get headers
        $headers = explode("\n", $headers);
        $location = '';
        foreach ($headers as $header) {
            if (stripos($header, 'Location:') !== false) {
                $location = $header;
            }
        }

        // return response
        return [
            'location' => $location,
            'response' => $response,
        ];
    }

    /**
     * Private method to add debug info to a structure array.
     *
     * @param string $key The under which to store the debug value.
     * @param string|array $value The debug info's value.
     */
    private function addDebugInfo(string $key, $value): void
    {
        $this->debugInfo[$key] = $value;
    }
}
