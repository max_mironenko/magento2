<?php declare(strict_types=1);

namespace C38\Springboard\Cron;

use C38\Springboard\Api\Data\InvoiceInterfaceFactory;
use C38\Springboard\Api\Data\InvoiceLineInterfaceFactory;
use C38\Springboard\Api\InvoiceLineRepositoryInterface;
use C38\Springboard\Api\InvoiceRepositoryInterface;
use C38\Springboard\Helper\Data as SpringboardHelper;
use C38\Springboard\Helper\Logs;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Throwable;

/**
 * Class ImportInvoices
 * Import invoices from the Springboard to the local storage
 */
class ImportInvoices
{
    const SPRINGBOARD_CRON_NAME = "ImportInvoices";

    /**
     * @var InvoiceInterfaceFactory
     */
    private $invoiceFactory;

    /**
     * @var InvoiceLineInterfaceFactory
     */
    private $invoiceLineFactory;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var InvoiceLineRepositoryInterface
     */
    private $invoiceLineRepository;

    /**
     * @var SpringboardHelper
     */
    private $springboardHelper;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var Logs
     */
    private $logs;

    /**
     * ImportInvoices constructor.
     *
     * @param InvoiceInterfaceFactory $invoiceFactory
     * @param InvoiceLineInterfaceFactory $invoiceLineFactory
     * @param InvoiceRepositoryInterface $invoiceRepository
     * @param InvoiceLineRepositoryInterface $invoiceLineRepository
     * @param SpringboardHelper $springboardHelper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param Logs $logs
     */
    public function __construct(
        InvoiceInterfaceFactory $invoiceFactory,
        InvoiceLineInterfaceFactory $invoiceLineFactory,
        InvoiceRepositoryInterface $invoiceRepository,
        InvoiceLineRepositoryInterface $invoiceLineRepository,
        SpringboardHelper $springboardHelper,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        Logs $logs
    ) {
        $this->invoiceFactory = $invoiceFactory;
        $this->invoiceLineFactory = $invoiceLineFactory;
        $this->invoiceRepository = $invoiceRepository;
        $this->invoiceLineRepository = $invoiceLineRepository;
        $this->springboardHelper = $springboardHelper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->logs = $logs;
    }

    /**
     * Import Springboard Invoices
     */
    public function execute()
    {
        // start
        $totalInvoices = 0;
        $successInvoices = 0;
        $startDate = $this->springboardHelper->getInvoicesSyncDate();
        $this->logs->info("Import Invoices started...", self::SPRINGBOARD_CRON_NAME);

        // get first retail location
        $startFromLocationId = $this->springboardHelper->getHamptonLocationId();
        if ($startFromLocationId === null) {
            $this->logs->error("Springboard location id not found", '', self::SPRINGBOARD_CRON_NAME);
            return;
        }

        // load data before order update
        try {

            // load all invoices from magento database
            $allSpringboardInvoices = $this->loadAllSpringboardInvoices($startDate);

            // load all invoice items from magento database
            $allSpringboardInvoiceItems = $this->loadAllSpringboardInvoiceItems($allSpringboardInvoices);

            // load all magento products
            $allMagentoProducts = $this->springboardHelper->loadMagentoProducts($allSpringboardInvoiceItems);
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
            $this->logs->info("Update Invoices finished", self::SPRINGBOARD_CRON_NAME);
            return;
        }

        // load all tickets from Springboard
        try {
            // get springboard client
            $client = $this->springboardHelper->getClient();

            // load all pages with tickets from SpringBoards
            $allPages = null;
            for ($page = 1; $allPages >= $page || $allPages === null; $page++) {
                // load all invoices for current page
                $invoices = $client->getInvoices($page, $startFromLocationId, $startDate);
                if (!is_object($invoices)) {
                    continue;
                }
                if ($allPages === null) {
                    $allPages = $invoices->pages;
                    $this->logs->info(
                        "From {$startDate} found {$invoices->total} invoices",
                        self::SPRINGBOARD_CRON_NAME
                    );
                }

                // update each ticket for current page
                foreach ($invoices->results as $invoice) {
                    try {
                        $totalInvoices++;

                        // update ticket in the springboard table
                        $this->updateInvoice($invoice, $allSpringboardInvoices);
                        // update ticket lines in the springboard table
                        $this->updateInvoiceLines($invoice, $allSpringboardInvoiceItems, $allMagentoProducts);

                        $successInvoices++;
                    } catch (Throwable $ex) {
                        $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                    }
                }
            }
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // finish
        $this->logs->info(
            "Successfully processed {$successInvoices} of {$totalInvoices} invoices",
            self::SPRINGBOARD_CRON_NAME
        );
        $this->logs->info("Import Invoices finished", self::SPRINGBOARD_CRON_NAME);
    }

    /**
     * Update Invoice in the Springboard table
     *
     * @param $invoice
     * @param array $allSpringboardInvoices
     * @throws LocalizedException
     * @noinspection DuplicatedCode
     */
    private function updateInvoice($invoice, array $allSpringboardInvoices)
    {
        // check data
        $total_item_qty = 0;
        if ($invoice->total_item_qty) {
            $total_item_qty = $invoice->total_item_qty;
        }
        $total = 0;
        if ($invoice->total) {
            $total = $invoice->total;
        }
        $total_paid = 0;
        if ($invoice->total_paid) {
            $total_paid = $invoice->total_paid;
        }
        $original_subtotal = 0;
        if ($invoice->original_subtotal) {
            $original_subtotal = $invoice->original_subtotal;
        }
        $total_discounts = 0;
        if ($invoice->total_discounts) {
            $total_discounts = $invoice->total_discounts;
        }

        // set data from springboard to the magento object
        $magentoInvoice = $this->invoiceFactory->create()
            ->setInvoiceId($invoice->id)
            ->setOrderId($invoice->order_id)
            ->setType($invoice->type)
            ->setStatus($invoice->status)
            ->setCreatedDate(date("Y-m-d H:i:s", strtotime($invoice->created_at)))
            ->setUpdatedAt(date("Y-m-d H:i:s", strtotime($invoice->updated_at)))
            ->setCustomerId($invoice->customer_id)
            ->setSalesRep($invoice->sales_rep)
            ->setStationId($invoice->station_id)
            ->setSourceLocationId($invoice->source_location_id)
            ->setTotalItemQty(number_format($total_item_qty, 4, ".", ""))
            ->setTotal(number_format($total, 4, ".", ""))
            ->setTotalPaid(number_format($total_paid, 4, ".", ""))
            ->setOriginalSubtotal(number_format($original_subtotal, 4, ".", ""))
            ->setTotalDiscounts(number_format($total_discounts, 4, ".", ""));

        // get springboard invoice data from the magento database
        $magentoInvoiceData = [];
        if (!empty($allSpringboardInvoices[$invoice->id])) {
            $magentoInvoiceData = $this->springboardHelper->getData($allSpringboardInvoices[$invoice->id]);
        }

        // compare magento and springboard invoice data
        $diff = array_diff_assoc($this->springboardHelper->getData($magentoInvoice), $magentoInvoiceData);
        // need to update invoice in the magento
        if (!empty($diff)) {
            $magentoInvoice->setSpringboardSyncDt(null);
            if (empty($magentoInvoiceData) && empty($magentoInvoiceData['id'])) {
                // create new ticket in the table
                $this->logs->info(
                    "Invoice with ID: {$invoice->id} was created",
                    self::SPRINGBOARD_CRON_NAME
                );
            } else {
                // update ticket
                $magentoInvoice->setId($magentoInvoiceData['id']);
                $this->logs->info(
                    "Invoice with ID: {$invoice->id} was updated",
                    self::SPRINGBOARD_CRON_NAME
                );
            }
            // create / update object
            $this->invoiceRepository->save($magentoInvoice);
        }
    }

    /**
     * Update Invoice Lines in the SpringBoard table
     *
     * @param $invoice
     * @param array $allSpringboardInvoiceItems
     * @param array $allMagentoProducts
     * @noinspection DuplicatedCode
     */
    private function updateInvoiceLines($invoice, array $allSpringboardInvoiceItems, array $allMagentoProducts)
    {
        // get all items from invoice
        foreach ($invoice->{'lines'} as $line) {
            try {
                // check data
                $value = 0;
                if ($line->value) {
                    $value = $line->value;
                }
                $qty = 0;
                if ($line->qty) {
                    $qty = $line->qty;
                }
                $original_unit_price = 0;
                if ($line->original_unit_price) {
                    $original_unit_price = $line->original_unit_price;
                }
                $unit_price = 0;
                if ($line->unit_price) {
                    $unit_price = $line->unit_price;
                }
                $unit_cost = 0;
                if ($line->unit_cost) {
                    $unit_cost = $line->unit_cost;
                }

                // set data from springboard to the magento object
                $magentoInvoiceLine = $this->invoiceLineFactory->create()
                    ->setLineId($line->id)
                    ->setInvoiceId($line->sales_transaction_id)
                    ->setItemId($line->item_id)
                    ->setType($line->type)
                    ->setDescription($line->description)
                    ->setTaxRuleId($line->tax_rule_id)
                    ->setValue(number_format($value, 4, ".", ""))
                    ->setQty(number_format($qty, 4, ".", ""))
                    ->setOriginalUnitPrice(number_format($original_unit_price, 4, ".", ""))
                    ->setUnitPrice(number_format($unit_price, 4, ".", ""))
                    ->setUnitCost(number_format($unit_cost, 4, ".", ""));

                // get simple and configurable product fot this item
                if ($line->item_id) {
                    if (!empty($allMagentoProducts[$line->item_id])) {
                        $magentoProduct = $allMagentoProducts[$line->item_id];
                        // set simple product id
                        $magentoInvoiceLine->setSimpleProductId($magentoProduct['simple_id']);
                        // set configurable product id
                        $magentoInvoiceLine->setConfigurableProductId($magentoProduct['configurable_id']);
                    }
                }

                // get springboard ticket line data from the magento database
                $magentoInvoiceLineData = [];
                if (!empty($allSpringboardInvoiceItems[$line->id])) {
                    $magentoInvoiceLineData = $this->springboardHelper->getData($allSpringboardInvoiceItems[$line->id]);
                }

                // compare magento and springboard ticket data
                $diff = array_diff_assoc(
                    $this->springboardHelper->getData($magentoInvoiceLine),
                    $magentoInvoiceLineData
                );
                // need to update ticket in the magento
                if (!empty($diff)) {
                    $magentoInvoiceLine->setSpringboardSyncDt(null);
                    if (empty($magentoInvoiceLineData) && empty($magentoInvoiceLineData['id'])) {
                        // create new ticket line in the table
                        $this->logs->info(
                            "Line with ID: {$line->id} for invoice with ID: {$line->sales_transaction_id} was created",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    } else {
                        // update ticket line
                        $magentoInvoiceLine->setId($magentoInvoiceLineData['id']);
                        $this->logs->info(
                            "Line with ID: {$line->id} for invoice with ID: {$line->sales_transaction_id} was updated",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    }
                    // create / update object
                    $this->invoiceLineRepository->save($magentoInvoiceLine);
                }
            } catch (Throwable $ex) {
                $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
            }
        }
    }

    /**
     * Load list of All Springboard invoices from the Magento Database
     *
     * @param string $date
     *
     * @return array
     * @throws LocalizedException
     */
    private function loadAllSpringboardInvoices(string $date): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder->setField('created_date')
                ->setConditionType('gt')->setValue($this->springboardHelper->dateCorrection($date))->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $invoices = [];
        foreach ($this->invoiceRepository->getList($searchCriteria)->getItems() as $item) {
            $invoices[$item['invoice_id']] = $item;
        }

        return $invoices;
    }

    /**
     * Load list of All Springboard invoices items from the Magento Database
     *
     * @param array $allSpringboardInvoices
     *
     * @return array
     * @throws LocalizedException
     */
    private function loadAllSpringboardInvoiceItems(array $allSpringboardInvoices): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder->setField('invoice_id')
                ->setConditionType('in')->setValue(array_keys($allSpringboardInvoices))->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $invoiceLines = [];
        foreach ($this->invoiceLineRepository->getList($searchCriteria)->getItems() as $item) {
            $invoiceLines[$item['line_id']] = $item;
        }

        return $invoiceLines;
    }
}
