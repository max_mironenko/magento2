<?php declare(strict_types=1);

namespace C38\Springboard\Cron;

use C38\Springboard\Api\CustomerRepositoryInterface as SpringboardCustomerRepositoryInterface;
use C38\Springboard\Api\Data\CustomerInterface as SpringboardCustomerInterface;
use C38\Springboard\Api\Data\CustomerInterfaceFactory as SpringboardCustomerInterfaceFactory;
use C38\Springboard\Api\Data\MagentoCustomerInterfaceFactory as MagentoCustomerInterfaceFactory;
use C38\Springboard\Api\MagentoCustomerRepositoryInterface as MagentoCustomerRepositoryInterface;
use C38\Springboard\Helper\Data as SpringboardHelper;
use C38\Springboard\Helper\Logs;
use C38\Springboard\Service\SpringboardAPIClient;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Framework\Serialize\Serializer\Json;
use Throwable;

/**
 * Class ImportCustomers
 * Import Customers from the Springboard to the local storage.
 * Export customers from Magento to the Springboard
 */
class ImportCustomers
{
    const SPRINGBOARD_CRON_NAME = "ImportCustomers";

    const CUSTOMERS_PER_PAGE = 500;

    /**
     * @var SpringboardCustomerInterfaceFactory
     */
    private $springboardCustomerFactory;

    /**
     * @var SpringboardCustomerRepositoryInterface
     */
    private $springboardCustomerRepository;

    /**
     * @var MagentoCustomerInterfaceFactory
     */
    private $magentoCustomerFactory;

    /**
     * @var MagentoCustomerRepositoryInterface
     */
    private $magentoCustomerRepository;

    /**
     * @var CustomerInterfaceFactory
     */
    private $customerFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var SpringboardHelper
     */
    private $springboardHelper;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var Logs
     */
    private $logs;

    /**
     * ImportCustomers constructor.
     *
     * @param SpringboardCustomerInterfaceFactory $springboardCustomerFactory
     * @param SpringboardCustomerRepositoryInterface $springboardCustomerRepository
     * @param MagentoCustomerInterfaceFactory $magentoCustomerFactory
     * @param MagentoCustomerRepositoryInterface $magentoCustomerRepository
     * @param CustomerInterfaceFactory $customerFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param SpringboardHelper $springboardHelper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param Json $json
     * @param Logs $logs
     */
    public function __construct(
        SpringboardCustomerInterfaceFactory $springboardCustomerFactory,
        SpringboardCustomerRepositoryInterface $springboardCustomerRepository,
        MagentoCustomerInterfaceFactory $magentoCustomerFactory,
        MagentoCustomerRepositoryInterface $magentoCustomerRepository,
        CustomerInterfaceFactory $customerFactory,
        CustomerRepositoryInterface $customerRepository,
        SpringboardHelper $springboardHelper,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        Json $json,
        Logs $logs
    ) {
        $this->springboardCustomerFactory = $springboardCustomerFactory;
        $this->springboardCustomerRepository = $springboardCustomerRepository;
        $this->magentoCustomerFactory = $magentoCustomerFactory;
        $this->magentoCustomerRepository = $magentoCustomerRepository;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->springboardHelper = $springboardHelper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->json = $json;
        $this->logs = $logs;
    }

    /**
     * Import Springboard Customers
     */
    public function execute()
    {
        // start
        $this->logs->info("Import/Export Customers started...", self::SPRINGBOARD_CRON_NAME);

        // get customer sync date
        $startDate = $this->springboardHelper->getCustomersSyncDate();

        // Import customers from Springboard to the Localstorage
        $this->importCustomers($startDate);

        // Export customers to Springboard from the Localstorage
        $this->exportCustomers($startDate);

        // finish
        $this->logs->info("Import Import/Export finished", self::SPRINGBOARD_CRON_NAME);
    }

    /**
     * Import customers from Springboard to the Localstorage
     *
     * @param string $startDate
     */
    private function importCustomers(string $startDate)
    {
        // start
        $this->logs->info(
            "Import Customers from Springboard to the Localstorage started...",
            self::SPRINGBOARD_CRON_NAME
        );

        try {
            // get springboard client
            $client = $this->springboardHelper->getClient();
            // load all customers from springboard
            $customers = $this->loadCustomersFromSpringboard($client, $startDate);
            $totalCustomers = count($customers);
            $this->logs->info(
                "From {$startDate} found {$totalCustomers} customers for update",
                self::SPRINGBOARD_CRON_NAME
            );
            if ($totalCustomers == 0) {
                $this->logs->info(
                    "Import Customers from Springboard to the Localstorage finished",
                    self::SPRINGBOARD_CRON_NAME
                );
                return;
            }

            // load all customers from local storage
            $localSpringboardCustomers = $this->loadLocalCustomers(false, $customers);
            // load all customer by email
            $localSpringboardCustomersByEmail =
                $this->loadLocalCustomersByEmail($customers, $localSpringboardCustomers);

            // update each customer in the local storage
            $success = 0;
            foreach ($customers as $springboardCustomer) {
                try {
                    // update customer
                    $this->updateLocalCustomer(
                        $springboardCustomer,
                        $localSpringboardCustomers,
                        $localSpringboardCustomersByEmail
                    );
                    $success++;
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                }
            }

            $this->logs->info(
                "Successfully processed {$success} of {$totalCustomers} customers",
                self::SPRINGBOARD_CRON_NAME
            );
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // finish
        $this->logs->info(
            "Import Customers from Springboard to the Localstorage finished",
            self::SPRINGBOARD_CRON_NAME
        );
    }

    /**
     * Update Customer in the local springboard storage
     *
     * @param $customer
     * @param array $customerStorage
     * @param array $customerStorageByEmail
     *
     * @throws Throwable
     */
    private function updateLocalCustomer($customer, array $customerStorage, array $customerStorageByEmail)
    {
        // set parameters in the Customer object
        $magentoCustomer = $this->springboardCustomerFactory->create()
            ->setCustomerId($customer->id)
            ->setEmail($customer->email)
            ->setFirstName($customer->first_name)
            ->setLastName($customer->last_name)
            ->setIsActive(($customer->{'active?'} == 1) ? 1 : 0)
            ->setType($customer->type)
            ->setAddressId(null)
            ->setLine1(null)
            ->setLine2(null)
            ->setCity(null)
            ->setState(null)
            ->setPhone(null)
            ->setCountry(null)
            ->setPostalCode(null)
            ->setFullAddress(null)
            ->setLifetimeSpend(null)
            ->setBirthday(null)
            ->setLocationsShopped(null)
            ->setBrandsPurchased(null)
            ->setCustomerSince(null)
            ->setStoreCredit(null)
            ->setVip(null)
            ->setAmbassador(null)
            ->setCardCheck(null);

        // add customer address
        if ($customer->address_id) {
            $magentoCustomer->setAddressId($customer->address_id);
            if (!empty($customer->address)) {
                $magentoCustomer
                    ->setLine1($customer->address->line_1)
                    ->setLine2($customer->address->line_2)
                    ->setCity($customer->address->city)
                    ->setState($customer->address->state)
                    ->setPhone($customer->address->phone)
                    ->setCountry($customer->address->country)
                    ->setPostalCode($customer->address->postal_code)
                    ->setFullAddress($customer->address->full_address);
            }
        }

        // add customer custom fields
        if (!empty($customer->custom)) {
            if (!empty($customer->custom->lifetime_spend)) {
                $magentoCustomer->setLifetimeSpend($customer->custom->lifetime_spend);
            }
            if (!empty($customer->custom->birthday)) {
                $magentoCustomer->setBirthday($customer->custom->birthday);
            }
            if (!empty($customer->custom->phone)) {
                $magentoCustomer->setPhone($customer->custom->phone);
            }
            if (!empty($customer->custom->locations_shopped)) {
                $magentoCustomer->setLocationsShopped($customer->custom->locations_shopped);
            }
            if (!empty($customer->custom->brands_purchased)) {
                $magentoCustomer->setBrandsPurchased($customer->custom->brands_purchased);
            }
            if (!empty($customer->custom->customer_since)) {
                $magentoCustomer->setCustomerSince($customer->custom->customer_since);
            }
            if (!empty($customer->custom->store_credit)) {
                $magentoCustomer->setStoreCredit($customer->custom->store_credit);
            }
            if (!empty($customer->custom->vip)) {
                $magentoCustomer->setVip($customer->custom->vip);
            }
            if (!empty($customer->custom->ambassador)) {
                $magentoCustomer->setAmbassador($customer->custom->ambassador);
            }
            if (!empty($customer->custom->card_check)) {
                $magentoCustomer->setCardCheck($customer->custom->card_check);
            }
        }

        // get springboard customer data from the magento database
        $magentoCustomerData = [];
        if (!empty($customerStorage[$customer->id])) {
            $magentoCustomerData = $this->springboardHelper->getData($customerStorage[$customer->id]);
        } else {
            if (!empty($customerStorageByEmail[$customer->email])) {
                $magentoCustomerData = $this->springboardHelper->getData($customerStorageByEmail[$customer->email]);
            }
        }

        // compare springboard order address with magento data
        $diff = array_diff_assoc($this->springboardHelper->getData($magentoCustomer), $magentoCustomerData);

        // save / update customer
        if (!empty($diff)) {
            if (empty($magentoCustomerData) && empty($magentoCustomerData['id'])) {
                // create new customer
                $this->logs->info(
                    "Customer with ID: {$customer->id} was created",
                    self::SPRINGBOARD_CRON_NAME
                );
            } else {
                // update customer
                $magentoCustomer->setId($magentoCustomerData['id']);
                $this->logs->info(
                    "Customer with ID: {$customer->id} was updated",
                    self::SPRINGBOARD_CRON_NAME
                );
            }

            // set synced parameters
            $magentoCustomer->setIsSynced(1);
            $magentoCustomer->setSpringboardSyncDt(date("Y-m-d H:i:s"));

            // save / update customer
            $this->springboardCustomerRepository->save($magentoCustomer);
        }
    }

    /**
     * Export customers to Springboard from the Localstorage
     *
     * @param string $startDate
     */
    private function exportCustomers(string $startDate)
    {
        // start
        $this->logs->info(
            "Export Customers to Springboard from the Localstorage started...",
            self::SPRINGBOARD_CRON_NAME
        );

        try {
            // update springboard customers with magento data
            $this->springboardCustomerRepository->updateCustomers();

            // load all customers from local storage
            $localSpringboardCustomers = $this->loadLocalCustomers(true);
            $totalCustomers = count($localSpringboardCustomers);
            $this->logs->info(
                "From {$startDate} found {$totalCustomers} customers for update",
                self::SPRINGBOARD_CRON_NAME
            );
            if ($totalCustomers == 0) {
                $this->logs->info(
                    "Export Customers to Springboard from the Localstorage finished",
                    self::SPRINGBOARD_CRON_NAME
                );
                return;
            }

            // get springboard client
            $client = $this->springboardHelper->getClient();
            // update each customer in the local storage
            $success = 0;
            foreach ($localSpringboardCustomers as $springboardCustomer) {
                /** @var SpringboardCustomerInterface $springboardCustomer */
                try {
                    if ((int)$springboardCustomer->getCustomerId() > 100000000) {
                        // need to create new customer
                        $this->createSpringboardCustomer($client, $springboardCustomer);
                    } else {
                        // update customer
                        $this->updateSpringboardCustomer($client, $springboardCustomer);
                    }
                    $success++;
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                }
            }

            // update magento customers. set springboard id
            $this->updateMagentoCustomers();

            $this->logs->info(
                "Successfully processed {$success} of {$totalCustomers} customers",
                self::SPRINGBOARD_CRON_NAME
            );
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // finish
        $this->logs->info(
            "Export Customers to Springboard from the Localstorage finished",
            self::SPRINGBOARD_CRON_NAME
        );
    }

    /**
     * Update customer in the Springboard
     *
     * @param SpringboardAPIClient $client
     * @param SpringboardCustomerInterface $springboardCustomer
     *
     * @throws Throwable
     */
    private function updateSpringboardCustomer(
        SpringboardAPIClient $client,
        SpringboardCustomerInterface $springboardCustomer
    ) {
        // set parameters to update
        $customer = [];
        // create custom block
        $custom = [];
        $custom['lifetime_spend'] = $springboardCustomer->getLifetimeSpend();
        $custom['store_credit'] = $springboardCustomer->getStoreCredit();
        $custom['vip'] = $springboardCustomer->getVip();
        $custom['ambassador'] = $springboardCustomer->getAmbassador();
        // set custom block
        $customer['custom'] = $custom;
        $customer['id'] = $springboardCustomer->getCustomerId();

        // save customer in the springboard
        $response = $client->updateCustomer($customer, $springboardCustomer->getCustomerId());

        // get springboard error
        if (!empty($response['response']) && !empty($response["response"]->error)) {
            throw new LocalizedException(new Phrase(
                "Customer {$springboardCustomer->getCustomerId()}: " . $this->json->serialize($response)
            ));
        } else {
            $this->logs->info(
                "Customer with ID {$springboardCustomer->getCustomerId()} was updated on the Springboard",
                self::SPRINGBOARD_CRON_NAME
            );
        }

        // set is synced attribute
        $springboardCustomer->setIsSynced(1);
        $springboardCustomer->setSpringboardSyncDt(date("Y-m-d H:i:s"));

        // save springboard customer
        $this->springboardCustomerRepository->save($springboardCustomer);
    }

    /**
     * Create new customer in the Springboard
     *
     * @param SpringboardAPIClient $client
     * @param SpringboardCustomerInterface $springboardCustomer
     *
     * @throws Throwable
     */
    private function createSpringboardCustomer(
        SpringboardAPIClient $client,
        SpringboardCustomerInterface $springboardCustomer
    ) {
        $customer = [];

        // get magento customer id
        $magentoCustomerId = (int)$springboardCustomer->getCustomerId() - 100000000;

        // create basic customer parameters
        $customer['email'] = $springboardCustomer->getEmail();
        $customer['created_at'] = date("Y-m-d H:i:s");
        $customer['first_name'] = $springboardCustomer->getFirstName();
        $customer['last_name'] = $springboardCustomer->getLastName();
        $customer['active'] = 1;
        $customer['type'] = 'full';
        $customer['address_id'] = null;

        // create address block
        $address['line_1'] = $springboardCustomer->getLine1();
        $address['line_2'] = $springboardCustomer->getLine2();
        $address['city'] = $springboardCustomer->getCity();
        $address['state'] = $springboardCustomer->getState();
        $address['phone'] = $springboardCustomer->getPhone();
        $address['country'] = $springboardCustomer->getCountry();
        $address['postal_code'] = $this->getPostalCode(
            $springboardCustomer->getCountry(),
            $springboardCustomer->getPostalCode()
        );

        $address['full_address'] = $springboardCustomer->getFullAddress();
        // set address block
        $customer['address'] = $address;

        // create custom block
        $custom = [];
        $custom['lifetime_spend'] = $springboardCustomer->getLifetimeSpend();
        $custom['birthday'] = $springboardCustomer->getBirthday();
        $custom['phone'] = $springboardCustomer->getPhone();
        $custom['locations_shopped'] = $springboardCustomer->getLocationsShopped();
        $custom['brands_purchased'] = $springboardCustomer->getBrandsPurchased();
        $custom['customer_since'] = $springboardCustomer->getCustomerSince();
        $custom['store_credit'] = $springboardCustomer->getStoreCredit();
        $custom['vip'] = $springboardCustomer->getVip();
        $custom['ambassador'] = $springboardCustomer->getAmbassador();
        $custom['card_check'] = $springboardCustomer->getCardCheck();
        // set custom block
        $customer['custom'] = $custom;

        // save customer in the springboard
        $response = $client->createCustomer($customer);

        // get springboard error
        if (empty($response['location'])) {
            throw new LocalizedException(new Phrase(
                "Customer {$springboardCustomer->getCustomerId()}: " . $this->json->serialize($response)
            ));
        } else {
            $customerId = filter_var($response['location'], FILTER_SANITIZE_NUMBER_INT);
            $this->logs->info(
                "Customer with ID {$customerId} was created on the Springboard",
                self::SPRINGBOARD_CRON_NAME
            );
        }

        // update customer in the springboard local storage
        $springboardCustomer->setCustomerId($customerId);
        // set is synced attribute
        $springboardCustomer->setIsSynced(1);
        $springboardCustomer->setSpringboardSyncDt(date("Y-m-d H:i:s"));

        // save springboard customer
        $this->springboardCustomerRepository->save($springboardCustomer);

        // save magento and springboard id to the local storage
        $localIDs = $this->magentoCustomerFactory->create();
        $localIDs->setEntityId($magentoCustomerId);
        $localIDs->setSpringboardId($customerId);
        $this->magentoCustomerRepository->save($localIDs);
    }

    /**
     * Update customers in the magento. Set springboard id
     *
     * @throws Throwable
     */
    private function updateMagentoCustomers()
    {
        $springboardCustomers = [];
        // get list of springboard customers need to update in the magento db
        $filters = [
            $this->filterBuilder->setField('is_synced')
                ->setConditionType('eq')->setValue("0")->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // get list of customers
        foreach ($this->magentoCustomerRepository->getList($searchCriteria)->getItems() as $customer) {
            $springboardCustomers[$customer->getEntityId()] = $customer;
        }
        // check if any customers for update found
        if (empty($springboardCustomers)) {
            return;
        }

        // get list of magento customers for update
        $filters = [
            $this->filterBuilder->setField('entity_id')
                ->setConditionType('in')->setValue(array_keys($springboardCustomers))->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // get list of customers
        foreach ($this->customerRepository->getList($searchCriteria)->getItems() as $customer) {
            try {
                // get springboard customer
                if (empty($springboardCustomers[$customer->getId()])) {
                    continue;
                }
                $springboardCustomer = $springboardCustomers[$customer->getId()];

                // set springboard id as magento customer attribute
                $customer->setCustomAttribute('springboard_id', $springboardCustomer->getSpringboardId());
                // save magento customer
                $this->customerRepository->save($customer);

                // update springboard customer
                $springboardCustomer->setIsSynced(1);
                $this->magentoCustomerRepository->save($springboardCustomer);
            } catch (Throwable $ex) {
                $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
            }
        }
    }

    /**
     * Load customers from Springboard
     *
     * @param SpringboardAPIClient $client
     * @param string $startDate
     *
     * @return array
     * @throws Throwable
     */
    private function loadCustomersFromSpringboard(SpringboardAPIClient $client, string $startDate): array
    {
        $customersList = [];

        // load customers from springboard
        $allPages = null;
        // get customers from all pages
        for ($page = 1; $allPages >= $page || $allPages === null; $page++) {
            // get customers from current page
            $customers = $client->getCustomers($page, self::CUSTOMERS_PER_PAGE, $startDate);
            if (!is_object($customers)) {
                continue;
            }
            if ($allPages === null) {
                $allPages = $customers->pages;
            }
            // prepare customer
            foreach ($customers->results as $customer) {
                if (!empty($customer->id)) {
                    $customersList[$customer->id] = $customer;
                }
            }
        }

        // return result
        return $customersList;
    }

    /**
     * Load customers from local Springboard storage
     *
     * @param bool $isSynced
     * @param array $springboardCustomers
     *
     * @return array
     * @throws Throwable
     */
    private function loadLocalCustomers(bool $isSynced, array $springboardCustomers = []): array
    {
        $customersList = [];

        if ($isSynced) { // get only not synced customers
            // prepare filters
            $filters = [
                $this->filterBuilder->setField('is_synced')
                    ->setConditionType('eq')->setValue('0')->create()
            ];
        } else { // get all customers by customer_id
            // check data
            if (empty($springboardCustomers)) {
                return $customersList;
            }
            // get list of customer ids
            $customerIDs = array_keys($springboardCustomers);
            if (empty($customerIDs)) {
                return $customersList;
            }
            // prepare filters
            $filters = [
                $this->filterBuilder->setField('customer_id')
                    ->setConditionType('in')->setValue($customerIDs)->create()
            ];
        }
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        foreach ($this->springboardCustomerRepository->getList($searchCriteria)->getItems() as $item) {
            $customersList[$item['customer_id']] = $item;
        }

        // return result
        return $customersList;
    }

    /**
     * Load customers from local Springboard using email field
     * Excluding already loaded customers
     *
     * @param array $springboardCustomers
     * @param array $loadedCustomers
     *
     * @return array
     * @throws Throwable
     */
    private function loadLocalCustomersByEmail(array $springboardCustomers, array $loadedCustomers): array
    {
        $customersList = [];
        // check data
        if (empty($springboardCustomers)) {
            return $customersList;
        }
        // get list of customer ids
        $customerIDs = [];
        foreach ($springboardCustomers as $springboardCustomer) {
            if (!empty($springboardCustomer->email)) {
                $customerIDs[] = $springboardCustomer->email;
            }
        }
        if (empty($customerIDs)) {
            return $customersList;
        }
        // prepare filters
        $filters = [
            $this->filterBuilder->setField('email')
                ->setConditionType('in')->setValue($customerIDs)->create()
        ];

        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        foreach ($this->springboardCustomerRepository->getList($searchCriteria)->getItems() as $item) {
            if (empty($loadedCustomers[$item['customer_id']])) {
                $customersList[$item['email']] = $item;
            }
        }

        // return result
        return $customersList;
    }

    /**
     * Check if postal code exist and correct.
     * Return in the right format (for US), any format for other counties or null
     *
     * @param string|null $country
     * @param string|null $postalCode
     *
     * @return string|null
     */
    private function getPostalCode(?string $country, ?string $postalCode): ?string
    {
        // check data
        if (empty($postalCode)) {
            return null;
        }
        if (empty($country)) {
            return $postalCode;
        }

        // if not US return postal code without check
        if ($country != "US") {
            return $postalCode;
        }
        // if postal code numeric return it
        if (is_numeric($postalCode)) {
            return $postalCode;
        }

        // if postal code in the USPS format
        $postalCodes = explode("-", $postalCode);
        if (empty($postalCodes[0])) {
            return null;
        }
        if (is_numeric($postalCodes[0])) {
            return $postalCodes[0];
        }

        // postal code can't be found
        return null;
    }
}
