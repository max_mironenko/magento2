<?php declare(strict_types=1);

namespace C38\Springboard\Cron;

use C38\Springboard\Api\AddressRepositoryInterface;
use C38\Springboard\Api\Data\AddressInterfaceFactory;
use C38\Springboard\Api\Data\OrderInterfaceFactory;
use C38\Springboard\Api\Data\OrderLineInterfaceFactory;
use C38\Springboard\Api\OrderLineRepositoryInterface;
use C38\Springboard\Api\OrderRepositoryInterface;
use C38\Springboard\Helper\Data as SpringboardHelper;
use C38\Springboard\Helper\Logs;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Throwable;

/**
 * Class ImportOrders
 * Import orders from the Springboard to the local storage
 */
class ImportOrders
{
    const SPRINGBOARD_CRON_NAME = "ImportOrders";

    /**
     * @var OrderInterfaceFactory
     */
    private $orderFactory;

    /**
     * @var OrderLineInterfaceFactory
     */
    private $orderLineFactory;

    /**
     * @var AddressInterfaceFactory
     */
    private $addressFactory;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var OrderLineRepositoryInterface
     */
    private $orderLineRepository;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var SpringboardHelper
     */
    private $springboardHelper;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var Logs
     */
    private $logs;

    /**
     * Class constructor
     *
     * @param OrderInterfaceFactory $orderFactory
     * @param OrderLineInterfaceFactory $orderLineFactory
     * @param AddressInterfaceFactory $addressFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderLineRepositoryInterface $orderLineRepository
     * @param AddressRepositoryInterface $addressRepository
     * @param SpringboardHelper $springboardHelper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param Logs $logs
     */
    public function __construct(
        OrderInterfaceFactory $orderFactory,
        OrderLineInterfaceFactory $orderLineFactory,
        AddressInterfaceFactory $addressFactory,
        OrderRepositoryInterface $orderRepository,
        OrderLineRepositoryInterface $orderLineRepository,
        AddressRepositoryInterface $addressRepository,
        SpringboardHelper $springboardHelper,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        Logs $logs
    ) {
        $this->orderFactory = $orderFactory;
        $this->orderLineFactory = $orderLineFactory;
        $this->addressFactory = $addressFactory;
        $this->orderRepository = $orderRepository;
        $this->orderLineRepository = $orderLineRepository;
        $this->addressRepository = $addressRepository;
        $this->springboardHelper = $springboardHelper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->logs = $logs;
    }

    /**
     * Import Springboard Orders
     */
    public function execute()
    {
        // start
        $totalOrders = 0;
        $successOrders = 0;
        $startDate = $this->springboardHelper->getOrdersSyncDate();
        $this->logs->info("Import Orders started...", self::SPRINGBOARD_CRON_NAME);

        // get first retail location id
        $startFromLocationId = $this->springboardHelper->getHamptonLocationId();
        if (!$startFromLocationId) {
            $this->logs->error("Springboard location id not found", '', self::SPRINGBOARD_CRON_NAME);
            return;
        }

        // load data before order update
        try {

            // load all orders from magento database
            $allSpringboardOrders = $this->loadAllSpringboardOrders($startDate);

            // load all order items from magento database
            $allSpringboardOrderItems = $this->loadAllSpringboardOrderItems($allSpringboardOrders);

            // load all magento products
            $allMagentoProducts = $this->springboardHelper->loadMagentoProducts($allSpringboardOrderItems);

            // load all addresses from magento database
            $allSpringboardAddresses = $this->loadAllSpringboardAddresses();
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
            $this->logs->info("Import Orders finished", self::SPRINGBOARD_CRON_NAME);
            return;
        }

        // get all orders from Springboard
        try {
            // get springboard client
            $client = $this->springboardHelper->getClient();

            // load all pages with orders from Springboard
            $allPages = null;
            for ($page = 1; $allPages >= $page || $allPages === null; $page++) {
                // load all orders for current page
                $orders = $client->getOrders($page, $startDate, $startFromLocationId);
                if (!is_object($orders)) {
                    continue;
                }
                if ($allPages === null) {
                    $allPages = $orders->pages;
                    $this->logs->info("From {$startDate} found {$orders->total} orders", self::SPRINGBOARD_CRON_NAME);
                }

                // update each order for current page
                foreach ($orders->results as $order) {
                    try {
                        $totalOrders++;

                        // update order in the springboard table
                        $this->updateOrder($order, $allSpringboardOrders);
                        // update order lines in the springboard table
                        $this->updateOrderLines($order, $allSpringboardOrderItems, $allMagentoProducts);
                        // update order address
                        $allSpringboardAddresses = $this->updateOrderAddresses($order, $allSpringboardAddresses);

                        $successOrders++;
                    } catch (Throwable $ex) {
                        $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                    }
                }
            }
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // finish
        $this->logs->info(
            "Successfully processed {$successOrders} of {$totalOrders} orders",
            self::SPRINGBOARD_CRON_NAME
        );
        $this->logs->info("Import Orders finished", self::SPRINGBOARD_CRON_NAME);
    }

    /**
     * Update Order in the Springboard table
     *
     * @param $order
     * @param array $allSpringboardOrders
     * @throws LocalizedException
     */
    private function updateOrder($order, array $allSpringboardOrders)
    {
        // check data
        $qty = 0;
        if ($order->total_qty) {
            $qty = $order->total_qty;
        }
        $total = 0;
        if ($order->total) {
            $total = $order->total;
        }
        $balance = 0;
        if ($order->balance) {
            $balance = $order->balance;
        }
        $total_paid = 0;
        if ($order->total_paid) {
            $total_paid = $order->total_paid;
        }
        $shipping_charge = 0;
        if ($order->shipping_charge) {
            $shipping_charge = $order->shipping_charge;
        }
        $subtotal = 0;
        if ($order->subtotal) {
            $subtotal = $order->subtotal;
        }

        // create springboard magento order and add data from springboard order
        $magentoOrder = $this->orderFactory->create()
            ->setOrderId($order->id)
            ->setStatus($order->status)
            ->setCreatedDate(date("Y-m-d H:i:s", strtotime($order->created_at)))
            ->setCustomerId($order->customer_id)
            ->setStationId($order->station_id)
            ->setShipFrom($order->source_location_id)
            ->setBillingAddressId($order->billing_address_id)
            ->setShippingAddressId($order->shipping_address_id)
            ->setSalesRep($order->sales_rep)
            ->setShippingMethodId($order->shipping_method_id)
            ->setTotalQty(number_format($qty, 4, ".", ""))
            ->setTotal(number_format($total, 4, ".", ""))
            ->setBalance(number_format($balance, 4, ".", ""))
            ->setTotalPaid(number_format($total_paid, 4, ".", ""))
            ->setShippingCharge(number_format($shipping_charge, 4, ".", ""))
            ->setSubtotal(number_format($subtotal, 4, ".", ""));

        // get springboard order data from the magento database
        $magentoOrderData = [];
        if (!empty($allSpringboardOrders[$order->id])) {
            $magentoOrderData = $this->springboardHelper->getData($allSpringboardOrders[$order->id]);
        }

        // compare magento and springboard order data
        $diff = array_diff_assoc($this->springboardHelper->getData($magentoOrder), $magentoOrderData);
        // need to update order in the magento
        if (!empty($diff)) {
            $magentoOrder->setSpringboardSyncDt(null);
            if (empty($magentoOrderData) && empty($magentoOrderData['id'])) {
                // create new order in the table
                $this->logs->info(
                    "Order with ID: {$order->order_id} was created",
                    self::SPRINGBOARD_CRON_NAME
                );
            } else {
                // update order
                $magentoOrder->setId($magentoOrderData['id']);
                $this->logs->info(
                    "Order with ID: {$order->order_id} was updated",
                    self::SPRINGBOARD_CRON_NAME
                );
            }
            // create / update object
            $this->orderRepository->save($magentoOrder);
        }
    }

    /**
     * Update Order Lines in the SpringBoard table
     *
     * @param $order
     * @param array $allSpringboardOrderItems
     * @param array $allMagentoProducts
     * @noinspection DuplicatedCode
     */
    private function updateOrderLines($order, array $allSpringboardOrderItems, array $allMagentoProducts)
    {
        // check each item in the order
        foreach ($order->{'lines'} as $line) {
            try {
                // check data
                $qty = 0;
                if ($line->qty) {
                    $qty = $line->qty;
                }
                $unit_price = 0;
                if ($line->unit_price) {
                    $unit_price = $line->unit_price;
                }
                $total_price = 0;
                if ($line->total_price) {
                    $total_price = $line->total_price;
                }
                $vendor = '';
                if (!empty($line->item_custom->vendor)) {
                    $vendor = $line->item_custom->vendor;
                }

                // create springboard magento order item and add data from springboard order item
                $magentoOrderLine = $this->orderLineFactory->create()
                    ->setLineId($line->id)
                    ->setOrderId($line->order_id)
                    ->setItemId($line->item_id)
                    ->setDescription($line->description)
                    ->setLocationId($line->ship_from_location_id)
                    ->setQty(number_format($qty, 4, ".", ""))
                    ->setUnitPrice(number_format($unit_price, 4, ".", ""))
                    ->setTotalPrice(number_format($total_price, 4, ".", ""))
                    ->setVendor($vendor);

                // get simple and configurable product fot this item
                if ($line->item_id) {
                    if (!empty($allMagentoProducts[$line->item_id])) {
                        $magentoProduct = $allMagentoProducts[$line->item_id];
                        // set simple product id
                        $magentoOrderLine->setSimpleProductId($magentoProduct['simple_id']);
                        // set configurable product id
                        $magentoOrderLine->setConfigurableProductId($magentoProduct['configurable_id']);
                    }
                }

                // get springboard order item data from the magento database
                $magentoOrderLineData = [];
                if (!empty($allSpringboardOrderItems[$line->id])) {
                    $magentoOrderLineData = $this->springboardHelper->getData($allSpringboardOrderItems[$line->id]);
                }

                // compare magento and springboard order line data
                $diff = array_diff_assoc($this->springboardHelper->getData($magentoOrderLine), $magentoOrderLineData);
                // need update order line in the magento db
                if (!empty($diff)) {
                    $magentoOrderLine->setSpringboardSyncDt(null);
                    if (empty($magentoOrderLineData) && empty($magentoOrderLineData['id'])) {
                        // create new item line
                        $this->logs->info(
                            "Line with ID: {$line->id} for orders with ID: {$line->order_id} was created",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    } else {
                        // update item line
                        $magentoOrderLine->setId($magentoOrderLineData['id']);
                        $this->logs->info(
                            "Line with ID: {$line->id} for orders with ID: {$line->order_id} was updated",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    }
                    // create / update line
                    $this->orderLineRepository->save($magentoOrderLine);
                }
            } catch (Throwable $ex) {
                $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
            }
        }
    }

    /**
     * Update Billing and Shipping Address for the Order
     *
     * @param $order
     * @param array $allSpringboardAddresses
     *
     * @return array
     */
    private function updateOrderAddresses($order, array $allSpringboardAddresses): array
    {
        // billing address
        try {
            // if order has billing address
            if ($order->billing_address && !empty($order->billing_address->id)) {

                // create springboard magento address and add data from springboard
                $address = $this->addressFactory->create()
                    ->setAddressId($order->billing_address->address_id)
                    ->setFirstName($order->billing_address->first_name)
                    ->setLastName($order->billing_address->last_name)
                    ->setLine1($order->billing_address->line_1)
                    ->setLine2($order->billing_address->line_2)
                    ->setCity($order->billing_address->city)
                    ->setState($order->billing_address->state)
                    ->setCountry($order->billing_address->country)
                    ->setPostalCode($order->billing_address->postal_code)
                    ->setPhone($order->billing_address->phone);

                // get springboard address data from the magento database
                $magentoAddressData = [];
                if (!empty($allSpringboardAddresses[$order->billing_address->address_id])) {
                    $magentoAddressData = $this->springboardHelper->getData(
                        $allSpringboardAddresses[$order->billing_address->address_id]
                    );
                }

                // compare springboard order address with magento data
                $diff = array_diff_assoc($this->springboardHelper->getData($address), $magentoAddressData);
                // update address
                if (!empty($diff)) {
                    $address->setSpringboardSyncDt(null);
                    if (empty($magentoAddressData) && empty($magentoAddressData['id'])) {
                        // create new address
                        $this->logs->info(
                            "Address with ID: {$order->billing_address->address_id} was created",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    } else {
                        // update address
                        $address->setId($magentoAddressData['id']);
                        $this->logs->info(
                            "Address with ID: {$order->billing_address->address_id} was updated",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    }
                    // save / update address
                    $this->addressRepository->save($address);
                    // add address to address storage
                    $allSpringboardAddresses[$order->billing_address->address_id] = $address;
                }
            }
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // shipping address
        try {
            // if order has shipping address
            if ($order->shipping_address && !empty($order->shipping_address->id)) {
                // check if billing and shipping address the same
                if ($order->billing_address->id == $order->shipping_address->id) {
                    return $allSpringboardAddresses;
                }

                // create springboard magento address and add data from springboard
                $address = $this->addressFactory->create()
                    ->setAddressId($order->shipping_address->address_id)
                    ->setFirstName($order->shipping_address->first_name)
                    ->setLastName($order->shipping_address->last_name)
                    ->setLine1($order->shipping_address->line_1)
                    ->setLine2($order->shipping_address->line_2)
                    ->setCity($order->shipping_address->city)
                    ->setState($order->shipping_address->state)
                    ->setCountry($order->shipping_address->country)
                    ->setPostalCode($order->shipping_address->postal_code)
                    ->setPhone($order->shipping_address->phone);

                // get springboard address data from the magento database
                $magentoAddressData = [];
                if (!empty($allSpringboardAddresses[$order->shipping_address->address_id])) {
                    $magentoAddressData = $this->springboardHelper->getData(
                        $allSpringboardAddresses[$order->shipping_address->address_id]
                    );
                }

                // compare springboard order address with magento data
                $diff = array_diff_assoc($this->springboardHelper->getData($address), $magentoAddressData);
                // update address
                if (!empty($diff)) {
                    $address->setSpringboardSyncDt(null);
                    if (empty($magentoAddressData) && empty($magentoAddressData['id'])) {
                        // create new address
                        $this->logs->info(
                            "Address with ID: {$order->shipping_address->address_id} was created",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    } else {
                        // update address
                        $address->setId($magentoAddressData['id']);
                        $this->logs->info(
                            "Address with ID: {$order->shipping_address->address_id} was updated",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    }
                    // save / update address
                    $this->addressRepository->save($address);
                    // add address to address storage
                    $allSpringboardAddresses[$order->shipping_address->address_id] = $address;
                }
            }
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // return storage with new addresses
        return $allSpringboardAddresses;
    }

    /**
     * Load list of All Springboard orders from the Magento Database
     *
     * @param string $date
     *
     * @return array
     * @throws LocalizedException
     */
    private function loadAllSpringboardOrders(string $date): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder->setField('created_date')
                ->setConditionType('gt')->setValue($this->springboardHelper->dateCorrection($date))->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $orders = [];
        foreach ($this->orderRepository->getList($searchCriteria)->getItems() as $item) {
            $orders[$item['order_id']] = $item;
        }

        return $orders;
    }

    /**
     * Load list of All Springboard order items from the Magento Database
     *
     * @param array $allSpringboardOrders
     *
     * @return array
     * @throws LocalizedException
     */
    private function loadAllSpringboardOrderItems(array $allSpringboardOrders): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder->setField('order_id')
                ->setConditionType('in')->setValue(array_keys($allSpringboardOrders))->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $orderLines = [];
        foreach ($this->orderLineRepository->getList($searchCriteria)->getItems() as $item) {
            $orderLines[$item['line_id']] = $item;
        }

        return $orderLines;
    }

    /**
     * Load list of All Springboard addresses for orders from the Magento Database
     *
     *
     * @return array
     * @throws LocalizedException
     */
    private function loadAllSpringboardAddresses(): array
    {
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters([])->create();
        // prepare list of items
        $orderLines = [];
        foreach ($this->addressRepository->getList($searchCriteria)->getItems() as $item) {
            $orderLines[$item['address_id']] = $item;
        }

        return $orderLines;
    }
}
