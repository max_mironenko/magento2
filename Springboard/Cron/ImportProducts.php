<?php declare(strict_types=1);

namespace C38\Springboard\Cron;

use C38\Springboard\Api\Data\ProductInterfaceFactory;
use C38\Springboard\Api\ProductRepositoryInterface;
use C38\Springboard\Helper\Data as SpringboardHelper;
use C38\Springboard\Helper\Logs;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Serialize\Serializer\Json;
use Throwable;

/**
 * Class ImportProducts
 * Import products from the Springboard to the localstorage
 */
class ImportProducts
{
    const SPRINGBOARD_CRON_NAME = "ImportProducts";

    const PRODUCT_PER_PAGE = 1000;

    /**
     * @var ProductInterfaceFactory
     */
    private $productFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var SpringboardHelper
     */
    private $springboardHelper;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var Logs
     */
    private $logs;

    /**
     * ImportProducts constructor.
     *
     * @param ProductInterfaceFactory $productFactory
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param SpringboardHelper $springboardHelper
     * @param Json $json
     * @param Logs $logs
     */
    public function __construct(
        ProductInterfaceFactory $productFactory,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        SpringboardHelper $springboardHelper,
        Json $json,
        Logs $logs
    ) {
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->springboardHelper = $springboardHelper;
        $this->json = $json;
        $this->logs = $logs;
    }

    /**
     * Import Springboard products
     */
    public function execute()
    {
        // start
        $this->logs->info("Import Products started...", self::SPRINGBOARD_CRON_NAME);

        try {
            // get updated start from date
            $startDate = $this->springboardHelper->getProductsSyncDate();

            // load products from springboard
            $springboardProducts = $this->loadProductsFromSpringboard($startDate);

            // check products
            $total = count($springboardProducts);
            $this->logs->info(
                "From {$startDate} found {$total} products for update from Springboard",
                self::SPRINGBOARD_CRON_NAME
            );
            if ($total == 0) {
                $this->logs->info("Import Products finished", self::SPRINGBOARD_CRON_NAME);
                return;
            }

            // load local products
            $localSpringboardProducts = $this->loadProductsFromLocalStorage($springboardProducts);

            // update products
            $success = 0;
            foreach ($springboardProducts as $product) {
                try {
                    $this->updateProduct($product, $localSpringboardProducts);
                    $success++;
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                }
            }

            $this->logs->info(
                "Successfully processed {$success} of {$total} products",
                self::SPRINGBOARD_CRON_NAME
            );
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // finish
        $this->logs->info("Import Products finished", self::SPRINGBOARD_CRON_NAME);
    }

    /**
     * Update Springboard product in the Magento Local Storage
     *
     * @param $product
     * @param array $springboardLocalProducts
     * @throws Throwable
     */
    private function updateProduct($product, array $springboardLocalProducts)
    {
        // convert Springboard Product Object to the array
        $product = $this->json->unserialize(
            $this->json->serialize($product)
        );

        // create magento product object
        $magentoProduct = $this->productFactory->create();
        // set data from springboard to the magento object
        $magentoProduct
            ->setProductId($this->getParameter($product, 'id'))
            ->setSku($this->getCustomParameter($product, 'sku'))
            ->setConfigurableSku($this->getCustomParameter($product, 'configurable_sku'))
            ->setStyleName($this->getCustomParameter($product, 'style_name'))
            ->setUpc($this->getCustomParameter($product, 'upc'))
            ->setDescription($this->getParameter($product, 'description'))
            ->setLongDescription($this->getParameter($product, 'long_description'))
            ->setActive($this->isProductActive($product))
            ->setCost($this->formatFloat($product, 'cost'))
            ->setPrice($this->formatFloat($product, 'price'))
            ->setOriginalPrice($this->formatFloat($product, 'original_price'));
        // set custom parameters
        $magentoProduct
            ->setPrimaryVendorId(
                empty($this->getParameter($product, 'primary_vendor_id')) ? 0 :
                    $this->getParameter($product, 'primary_vendor_id')
            )
            ->setPrimaryBarcode($this->getParameter($product, 'primary_barcode'))
            ->setPrimaryImageId(
                empty($this->getParameter($product, 'primary_image_id')) ? 0 :
                $this->getParameter($product, 'primary_image_id')
            )
            ->setWeight($this->formatFloat($product, 'weight'))
            ->setWidth($this->formatFloat($product, 'width'))
            ->setHeight($this->formatFloat($product, 'height'))
            ->setDepth($this->formatFloat($product, 'depth'))
            ->setSize($this->getCustomParameter($product, 'size'))
            ->setBrand($this->getCustomParameter($product, 'brand'))
            ->setColor($this->getCustomParameter($product, 'color'))
            ->setVendor($this->getCustomParameter($product, 'vendor'))
            ->setCategory($this->getCustomParameter($product, 'category'))
            ->setDepartment($this->getCustomParameter($product, 'department'))
            ->setSubcategory($this->getCustomParameter($product, 'subcategory'));

        // get springboard product from the local storage
        $localProductData = [];
        if (!empty($springboardLocalProducts[$product['id']])) {
            $localProductData = $this->springboardHelper->getData($springboardLocalProducts[$product['id']]);
        }

        // compare magento and springboard product data
        $diff = array_diff_assoc($this->springboardHelper->getData($magentoProduct), $localProductData);
        // need to update product in the magento
        if (!empty($diff)) {
            // set sync date
            $magentoProduct->setSpringboardSyncDt(date("Y-m-d H:i:s"));

            // update / create product
            if (empty($localProductData) && empty($localProductData['id'])) {
                // set dates
                $magentoProduct
                    ->setCreatedAt($this->getParameter($product, 'created_at'))
                    ->setUpdatedAt($this->getParameter($product, 'updated_at'));
                // create new transfer
                $this->logs->info(
                    "Product with ID: {$product['id']} was created",
                    self::SPRINGBOARD_CRON_NAME
                );
            } else {
                // update transfer
                $magentoProduct->setId($localProductData['id']);
                $this->logs->info(
                    "Product with ID: {$product['id']} was updated",
                    self::SPRINGBOARD_CRON_NAME
                );
            }
            // create / update object
            $this->productRepository->save($magentoProduct);
        }
    }

    /**
     * Is Product Active
     *
     * @param array $product
     * @return int
     */
    private function isProductActive(array $product): int
    {
        if (empty($product['active?']) || empty($product['active?']) == 'false') {
            return 0;
        }
        return 1;
    }

    /**
     * Get parameter from the product data array
     *
     * @param array $product
     * @param string $parameter
     *
     * @return string
     */
    private function getParameter(array $product, string $parameter): string
    {
        return empty($product[$parameter]) ? "" : (string)$product[$parameter];
    }

    /**
     * Get parameter from the product custom data array
     *
     * @param array $product
     * @param string $parameter
     *
     * @return string
     */
    private function getCustomParameter(array $product, string $parameter): string
    {
        if (empty($product['custom'])) {
            return "";
        }
        return empty($product['custom'][$parameter]) ? "" : $product['custom'][$parameter];
    }

    /**
     * Format float value
     *
     * @param array $product
     * @param string $parameter
     *
     * @return string
     */
    private function formatFloat(array $product, string $parameter): string
    {
        $value = 0;

        // check if property exist
        if (!empty($product[$parameter])) {
            $value = $product[$parameter];
        }

        // format value
        return number_format($value, 4, ".", "");
    }

    /**
     * Load products from Springboard
     *
     * @param string $startDate
     *
     * @return array
     * @throws Throwable
     */
    private function loadProductsFromSpringboard(string $startDate): array
    {
        $springboardProducts = [];

        // get springboard client
        $client = $this->springboardHelper->getClient();

        // load products from springboard
        $allPages = null;
        for ($page = 1; $allPages >= $page || $allPages === null; $page++) {
            // get products from current page
            $products = $client->getItems($page, self::PRODUCT_PER_PAGE, $startDate);
            if (!is_object($products)) {
                continue;
            }
            if ($allPages === null) {
                $allPages = $products->pages;
            }
            // update each product
            foreach ($products->results as $product) {
                try {
                    // add product to the list
                    $springboardProducts[$product->id] = $product;
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                }
            }
        }

        return $springboardProducts;
    }

    /**
     * Load product from the Local Springboard Storage
     *
     * @param array $productIDs
     *
     * @return array
     * @throws Throwable
     */
    private function loadProductsFromLocalStorage(array $productIDs): array
    {
        if (empty($productIDs)) {
            return [];
        }

        // prepare filters
        $filters = [
            $this->filterBuilder->setField('product_id')
                ->setConditionType('in')->setValue(array_keys($productIDs))->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $localProducts = [];
        foreach ($this->productRepository->getList($searchCriteria)->getItems() as $item) {
            $localProducts[$item->getProductId()] = $item;
        }

        return $localProducts;
    }
}
