<?php declare(strict_types=1);

namespace C38\Springboard\Cron;

use C38\Springboard\Api\Data\TransferInterface;
use C38\Springboard\Api\Data\TransferLineInterface;
use C38\Springboard\Api\TransferLineRepositoryInterface;
use C38\Springboard\Api\TransferRepositoryInterface;
use C38\Springboard\Helper\ASNHelper;
use C38\Springboard\Helper\Data as SpringboardHelper;
use C38\Springboard\Helper\Logs;
use C38\Springboard\Service\SpringboardAPIClient;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Throwable;

/**
 * Class CreateASNsFromTransfers
 * Create ASNs on the Masonhub from imported Springboard's transfers
 */
class CreateASNsFromTransfers
{
    const SPRINGBOARD_CRON_NAME = "CreateASNsFromTransfers";

    /**
     * @var TransferRepositoryInterface
     */
    private $transferRepository;

    /**
     * @var TransferLineRepositoryInterface
     */
    private $transferLineRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var SpringboardHelper
     */
    private $springboardHelper;

    /**
     * @var ASNHelper
     */
    private $asnHelper;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var Logs
     */
    private $logs;

    /**
     * CreateASNsFromTransfers constructor.
     *
     * @param TransferRepositoryInterface $transferRepository
     * @param TransferLineRepositoryInterface $transferLineRepository
     * @param ProductRepositoryInterface $productRepository
     * @param SpringboardHelper $springboardHelper
     * @param ASNHelper $asnHelper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param Logs $logs
     */
    public function __construct(
        TransferRepositoryInterface $transferRepository,
        TransferLineRepositoryInterface $transferLineRepository,
        ProductRepositoryInterface $productRepository,
        SpringboardHelper $springboardHelper,
        ASNHelper $asnHelper,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        Logs $logs
    ) {
        $this->transferRepository = $transferRepository;
        $this->transferLineRepository = $transferLineRepository;
        $this->productRepository = $productRepository;
        $this->springboardHelper = $springboardHelper;
        $this->asnHelper = $asnHelper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->logs = $logs;
    }

    /**
     * CreateASNsFromTransfers
     */
    public function execute()
    {
        // start
        $this->logs->info("Create ASNs from springboard's transfers started...", self::SPRINGBOARD_CRON_NAME);

        try {
            // load list of transfer need to be converted to ASNs
            $transfers = $this->loadTransfers();
            $total = count($transfers);
            $success = 0;
            if ($total == 0) {
                $this->logs->info("Unconverted transfers weren't found.", self::SPRINGBOARD_CRON_NAME);
                $this->logs->info("Create ASNs from springboard's transfers finished", self::SPRINGBOARD_CRON_NAME);
                return;
            }
            $this->logs->info("Found {$total} transfers need to be converted to ASNs", self::SPRINGBOARD_CRON_NAME);
            // load list of transfers lines
            $transfersLines = $this->loadTransferLines($transfers);

            // load list of magento products
            $products = $this->loadProducts($transfersLines);

            // get springboard client
            $client = $this->springboardHelper->getClient();

            // create ASNs for each transfer
            foreach ($transfers as $transfer) {
                /** @var TransferInterface $transfer */
                try {
                    // get transfer lines for the transfer
                    if (empty($transfersLines[$transfer->getTransferId()])) {
                        throw new LocalizedException(
                            __("Can't find lines for transfer with id {$transfer->getTransferId()}.")
                        );
                    }
                    // create ASN
                    $this->createASN($transfer, $transfersLines[$transfer->getTransferId()], $products, $client);
                    $success++;
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                    // save error in the transfer line
                    $transfer->setMHConvertError($ex->getMessage());
                    $transfer->setMHConvertDate(date("Y-m-d H:i:s"));
                    $this->transferRepository->save($transfer);

                }
            }
            $this->logs->info("{$success} of {$total} transfers were converted to ASNs", self::SPRINGBOARD_CRON_NAME);

        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // finish
        $this->logs->info("Create ASNs from springboard's transfers finished", self::SPRINGBOARD_CRON_NAME);
    }

    /**
     * Create ASN from the springboard transfer
     *
     * @param TransferInterface $transfer
     * @param TransferLineInterface[] $transferLines
     * @param ProductInterface[] $products
     * @param SpringboardAPIClient $client
     *
     * @throws Throwable
     */
    private function createASN(
        TransferInterface $transfer,
        array $transferLines,
        array $products,
        SpringboardAPIClient $client
    ) {
        // create items data
        $itemsData = [];
        $itemId = 0;
        foreach ($transferLines as $transferLine) {
            // get product
            if (empty($products[$transferLine->getItemId()])) {
                throw new LocalizedException(
                    __("Product with id {$transferLine->getItemId()} not found " .
                        "for transfer line {$transferLine->getLineId()}")
                );
            }
            $product = $products[$transferLine->getItemId()];
            // prepare item data
            $itemsData["item_id_$itemId"] = [
                'sku' => $product->getSku(),
                'original_qty' => $transferLine->getQtyShipped(),
                'description' => $product->getName()
            ];
            $itemId++;
        }

        // create ASN data
        $asnData = [
            'asn_po' => "SB-{$transfer->getTransferId()}",
            'asn_name' => "Springboard Transfer {$transfer->getTransferId()}",
            'warehouse_id' => 11,
            'status' => 'CREATED',
            'ship_date' => $transfer->getCreatedDate(),
            'from_location_id' => $transfer->getFromLocationId(),
            'shipped_via' => '',
            'tracking_no' => $transfer->getTransferNumber(),
            'estimated_arrival_date' => strtotime($transfer->getCreatedDate() . ' + 30 days'),
            'pallet_count' => '',
            'carton_count' => '',
            'weight' => '',
            'notes' => ''
        ];

        // send data to the Washington
        $response = $this->asnHelper->createAndSendAsn($asnData, $itemsData, $this->springboardHelper);

        // prepare answer
        if (empty($response['manifest_id'])) {
            // error
            $transfer->setMHConvertError($response['message']);
            $this->logs->error(
                "Can't convert transfer {$transfer->getTransferId()} to ASN",
                $response['message'],
                self::SPRINGBOARD_CRON_NAME
            );
        } else {
            // success
            $transfer->setManifestId($response['manifest_id']);
            // update transfer in the springboard
            $client->updateTransfer(
                [
                    'custom' => [
                        'transfer' => $transfer->getTransferNumber(),
                        'manifest_id' => $transfer->getManifestId()
                    ]
                ],
                $transfer->getTransferId()
            );
            $this->logs->info(
                "Transfer {$transfer->getTransferId()} was successfully converted to ASN with #" .
                    $transfer->getManifestId(),
                self::SPRINGBOARD_CRON_NAME
            );
        }
        // save transfer
        $transfer->setMHConvertDate(date("Y-m-d H:i:s"));
        $this->transferRepository->save($transfer);
    }

    /**
     * Load list of transfer need to be converted to ASN
     *
     * @return TransferInterface[]
     * @throws Throwable
     */
    private function loadTransfers(): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder
                ->setField(TransferInterface::MANIFEST_ID)
                ->setConditionType('null')
                ->setValue(true)
                ->create(),
            $this->filterBuilder->setField(TransferInterface::TO_LOCATION_ID)
                ->setConditionType('eq')
                ->setValue($this->springboardHelper->getOnlineLocationId())
                ->create(),
            $this->filterBuilder
                ->setField(TransferInterface::MH_CONVERT_ERROR)
                ->setConditionType('null')
                ->setValue(true)
                ->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $transfers = [];
        foreach ($this->transferRepository->getList($searchCriteria)->getItems() as $item) {
            $transfers[$item->getTransferId()] = $item;
        }

        return $transfers;
    }

    /**
     * Load list of transfers lines
     *
     * @param TransferInterface[] $transfers
     *
     * @return TransferLineInterface[][]
     * @throws Throwable
     */
    private function loadTransferLines(array $transfers): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder
                ->setField(TransferLineInterface::TRANSFER_ID)
                ->setConditionType('in')
                ->setValue(array_keys($transfers))
                ->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $transferLines = [];
        foreach ($this->transferLineRepository->getList($searchCriteria)->getItems() as $item) {
            $transferLines[$item->getTransferId()][$item->getLineId()] = $item;
        }

        return $transferLines;
    }

    /**
     * Get List of products from the transfer lines
     *
     * @param TransferLineInterface[][] $transfers
     *
     * @return ProductInterface[]
     * @throws Throwable
     * @noinspection PhpUndefinedMethodInspection
     */
    private function loadProducts(array $transfers): array
    {
        // prepare list of products
        $magentoProducts = [];
        foreach ($transfers as $transferLines) {
            foreach ($transferLines as $transferLine) {
                $magentoProducts[] = $transferLine->getItemId();
            }
        }

        // prepare filters
        $filters = [
            $this->filterBuilder
                ->setField('springboard_id')
                ->setConditionType('in')
                ->setValue($magentoProducts)
                ->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();

        // prepare list of simple products
        $products = [];
        foreach ($this->productRepository->getList($searchCriteria)->getItems() as $item) {
            $products[$item->getSpringboardId()] = $item;
        }

        return $products;
    }
}
