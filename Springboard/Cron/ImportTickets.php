<?php declare(strict_types=1);

namespace C38\Springboard\Cron;

use C38\Springboard\Api\Data\TicketInterfaceFactory;
use C38\Springboard\Api\Data\TicketLineAdjustmentsInterfaceFactory;
use C38\Springboard\Api\Data\TicketLineInterfaceFactory;
use C38\Springboard\Api\TicketLineAdjustmentsRepositoryInterface;
use C38\Springboard\Api\TicketLineRepositoryInterface;
use C38\Springboard\Api\TicketRepositoryInterface;
use C38\Springboard\Helper\Data as SpringboardHelper;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Throwable;
use C38\Springboard\Helper\Logs;

/**
 * Class ImportTickets
 * Import tickets from the Springboard to the local storage
 */
class ImportTickets
{
    const SPRINGBOARD_CRON_NAME = "ImportTickets";

    /**
     * @var TicketInterfaceFactory
     */
    private $ticketFactory;

    /**
     * @var TicketLineInterfaceFactory
     */
    private $ticketLineFactory;

    /**
     * @var TicketLineAdjustmentsInterfaceFactory
     */
    private $ticketLineAdjustmentsFactory;

    /**
     * @var TicketRepositoryInterface
     */
    private $ticketRepository;

    /**
     * @var TicketLineRepositoryInterface
     */
    private $ticketLineRepository;

    /**
     * @var TicketLineAdjustmentsRepositoryInterface
     */
    private $ticketLineAdjustmentsRepository;

    /**
     * @var SpringboardHelper
     */
    private $springboardHelper;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var Logs
     */
    private $logs;

    /**
     * Class Constructor
     *
     * @param TicketInterfaceFactory $ticketFactory
     * @param TicketLineInterfaceFactory $ticketLineFactory
     * @param TicketLineAdjustmentsInterfaceFactory $ticketLineAdjustmentsFactory
     * @param TicketRepositoryInterface $ticketRepository
     * @param TicketLineRepositoryInterface $ticketLineRepository
     * @param TicketLineAdjustmentsRepositoryInterface $ticketLineAdjustmentsRepository
     * @param SpringboardHelper $springboardHelper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param Logs $logs
     */
    public function __construct(
        TicketInterfaceFactory $ticketFactory,
        TicketLineInterfaceFactory $ticketLineFactory,
        TicketLineAdjustmentsInterfaceFactory $ticketLineAdjustmentsFactory,
        TicketRepositoryInterface $ticketRepository,
        TicketLineRepositoryInterface $ticketLineRepository,
        TicketLineAdjustmentsRepositoryInterface $ticketLineAdjustmentsRepository,
        SpringboardHelper $springboardHelper,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        Logs $logs
    ) {
        $this->ticketFactory = $ticketFactory;
        $this->ticketLineFactory = $ticketLineFactory;
        $this->ticketLineAdjustmentsFactory = $ticketLineAdjustmentsFactory;
        $this->ticketRepository = $ticketRepository;
        $this->ticketLineRepository = $ticketLineRepository;
        $this->ticketLineAdjustmentsRepository = $ticketLineAdjustmentsRepository;
        $this->springboardHelper = $springboardHelper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->logs = $logs;
    }

    /**
     * Import Springboard Tickets
     */
    public function execute(): void
    {
        // start
        $totalTickets = 0;
        $successTickets = 0;
        $startDate = $this->springboardHelper->getTicketsSyncDate();
        $this->logs->info("Import Tickets started...", self::SPRINGBOARD_CRON_NAME);

        // get first retail location
        $startFromLocationId = $this->springboardHelper->getHamptonLocationId();
        if ($startFromLocationId === null) {
            $this->logs->error("Springboard location id not found", '', self::SPRINGBOARD_CRON_NAME);
            return;
        }

        // load data before order update
        try {

            // load all tickets from magento database
            $allSpringboardTickets = $this->loadAllSpringboardTickets($startDate);

            // load all ticket items from magento database
            $allSpringboardTicketItems = $this->loadAllSpringboardOrderItems($allSpringboardTickets);

            // load all magento products
            $allMagentoProducts = $this->springboardHelper->loadMagentoProducts($allSpringboardTicketItems);

            // load all ticket items adjustments from magento database
            $allSpringboardTicketItemsAdjustments =
                $this->loadAllSpringboardOrderItemsAdjustments($allSpringboardTicketItems);

        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
            $this->logs->info("Import tickets finished", self::SPRINGBOARD_CRON_NAME);
            return;
        }

        // load all tickets from Springboard
        try {
            // get springboard client
            $client = $this->springboardHelper->getClient();

            // load all pages with tickets from SpringBoards
            $allPages = null;
            for ($page = 1; $allPages >= $page || $allPages === null; $page++) {
                // load all tickets for current page
                $tickets = $client->getTickets($page, $startFromLocationId, $startDate);
                if (!is_object($tickets)) {
                    continue;
                }
                if ($allPages === null) {
                    $allPages = $tickets->pages;
                    $this->logs->info("From {$startDate} found {$tickets->total} tickets", self::SPRINGBOARD_CRON_NAME);
                }

                // update each ticket for current page
                foreach ($tickets->results as $ticket) {
                    try {
                        $totalTickets++;

                        // update ticket in the springboard table
                        $this->updateTicket($ticket, $allSpringboardTickets);
                        // update ticket lines in the springboard table
                        $ticketLinesAdjustments = $this->updateTicketLines(
                            $ticket,
                            $allSpringboardTicketItems,
                            $allMagentoProducts
                        );
                        // update ticket lines adjustments in the springboard table
                        $this->updateTicketLinesAdjustments(
                            $ticketLinesAdjustments,
                            $allSpringboardTicketItemsAdjustments
                        );

                        $successTickets++;
                    } catch (Throwable $ex) {
                        $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                    }
                }
            }
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // finish
        $this->logs->info(
            "Successfully processed {$successTickets} of {$totalTickets} tickets",
            self::SPRINGBOARD_CRON_NAME
        );
        $this->logs->info("Import Tickets finished", self::SPRINGBOARD_CRON_NAME);
    }

    /**
     * Update Ticket in the Springboard table
     *
     * @param $ticket
     * @param array $allSpringboardTickets
     * @throws LocalizedException
     * @noinspection DuplicatedCode
     */
    private function updateTicket($ticket, array $allSpringboardTickets)
    {
        // check data
        $total_item_qty = 0;
        if ($ticket->total_item_qty) {
            $total_item_qty = $ticket->total_item_qty;
        }
        $total = 0;
        if ($ticket->total) {
            $total = $ticket->total;
        }
        $total_paid = 0;
        if ($ticket->total_paid) {
            $total_paid = $ticket->total_paid;
        }
        $original_subtotal = 0;
        if ($ticket->original_subtotal) {
            $original_subtotal = $ticket->original_subtotal;
        }
        $total_discounts = 0;
        if ($ticket->total_discounts) {
            $total_discounts = $ticket->total_discounts;
        }

        // create springboard magento ticket and add data from springboard ticket
        $magentoTicket = $this->ticketFactory->create()
            ->setTicketId($ticket->id)
            ->setType($ticket->type)
            ->setStatus($ticket->status)
            ->setCreatedDate(date('Y-m-d H:i:s', strtotime($ticket->created_at)))
            ->setCustomerId($ticket->customer_id)
            ->setSalesRep($ticket->sales_rep)
            ->setStationId($ticket->station_id)
            ->setSourceLocationId($ticket->source_location_id)
            ->setTotalItemQty(number_format($total_item_qty, 4, '.', ''))
            ->setTotal(number_format($total, 4, '.', ''))
            ->setTotalPaid(number_format($total_paid, 4, '.', ''))
            ->setOriginalSubtotal(number_format($original_subtotal, 4, '.', ''))
            ->setTotalDiscounts(number_format($total_discounts, 4, '.', ''))
            ->setUpdatedAt(date('Y-m-d H:i:s', strtotime($ticket->updated_at)));

        // get springboard ticket data from the magento database
        $magentoTicketData = [];
        if (!empty($allSpringboardTickets[$ticket->id])) {
            $magentoTicketData = $this->springboardHelper->getData($allSpringboardTickets[$ticket->id]);
        }

        // compare magento and springboard ticket data
        $diff = array_diff_assoc($this->springboardHelper->getData($magentoTicket), $magentoTicketData);
        // need to update ticket in the magento
        if (!empty($diff)) {
            $magentoTicket->setSpringboardSyncDt(null);
            if (empty($magentoTicketData) && empty($magentoTicketData['id'])) {
                // create new ticket in the table
                $this->logs->info(
                    "Ticket with ID: {$ticket->id} was created",
                    self::SPRINGBOARD_CRON_NAME
                );
            } else {
                // update ticket
                $magentoTicket->setId($magentoTicketData['id']);
                $this->logs->info(
                    "Ticket with ID: {$ticket->id} was updated",
                    self::SPRINGBOARD_CRON_NAME
                );
            }
            // create / update object
            $this->ticketRepository->save($magentoTicket);
        }
    }

    /**
     * Update Ticket Lines in the SpringBoard table
     *
     * @param $ticket
     * @param array $allSpringboardTicketItems
     * @param array $allMagentoProducts
     *
     * @return array - list of tickets adjustments
     * @noinspection DuplicatedCode
     */
    private function updateTicketLines($ticket, array $allSpringboardTicketItems, array $allMagentoProducts): array
    {
        // list of ticket adjustments
        $ticketLinesAdjustments = [];

        // check each item in the ticket
        foreach ($ticket->{'lines'} as $line) {
            try {
                // check data
                $vendor = null;
                if (!empty($line->item_custom) && !empty($line->item_custom->vendor)) {
                    $vendor = $line->item_custom->vendor;
                }
                $lineValue = 0;
                if ($line->value) {
                    $lineValue = $line->value;
                }
                $lineQty = 0;
                if ($line->qty) {
                    $lineQty = $line->qty;
                }
                $lineOriginalUnitPrice = 0;
                if ($line->original_unit_price) {
                    $lineOriginalUnitPrice = $line->original_unit_price;
                }
                $lineUnitPrice = 0;
                if ($line->unit_price) {
                    $lineUnitPrice = $line->unit_price;
                }
                $lineUnitCost = 0;
                if ($line->unit_cost) {
                    $lineUnitCost = $line->unit_cost;
                }

                // create springboard magento ticket and add data from springboard ticket
                $magentoTicketLine = $this->ticketLineFactory->create()
                    ->setLineId($line->id)
                    ->setTicketId($line->sales_transaction_id)
                    ->setItemId($line->item_id)
                    ->setType($line->type)
                    ->setDescription($line->description)
                    ->setTaxRuleId($line->tax_rule_id)
                    ->setValue(number_format($lineValue, 4, '.', ''))
                    ->setQty(number_format($lineQty, 4, '.', ''))
                    ->setOriginalUnitPrice(number_format($lineOriginalUnitPrice, 4, '.', ''))
                    ->setUnitPrice(number_format($lineUnitPrice, 4, '.', ''))
                    ->setUnitCost(number_format($lineUnitCost, 4, '.', ''))
                    ->setVendor($vendor)
                    ->setItemLineId($line->item_line_id);

                // get simple and configurable product fot this item
                if ($line->item_id) {
                    if (!empty($allMagentoProducts[$line->item_id])) {
                        $magentoProduct = $allMagentoProducts[$line->item_id];
                        // set simple product id
                        $magentoTicketLine->setSimpleProductId($magentoProduct['simple_id']);
                        // set configurable product id
                        $magentoTicketLine->setConfigurableProductId($magentoProduct['configurable_id']);
                    }
                }

                // get springboard ticket line data from the magento database
                $magentoTicketLineData = [];
                if (!empty($allSpringboardTicketItems[$line->id])) {
                    $magentoTicketLineData = $this->springboardHelper->getData($allSpringboardTicketItems[$line->id]);
                }

                // price adjustments for each product
                foreach ($line->{'price_adjustments'} as $adjustments) {
                    $ticketLinesAdjustments[] = [
                        'line_id' => $line->id,
                        'adjustment_id' => $adjustments->id,
                        'delta_price' => $adjustments->delta_price,
                        'name' => $adjustments->name,
                        'description' => $adjustments->description
                    ];
                }

                // compare magento and springboard ticket data
                $diff = array_diff_assoc($this->springboardHelper->getData($magentoTicketLine), $magentoTicketLineData);
                // need to update ticket in the magento
                if (!empty($diff)) {
                    $magentoTicketLine->setSpringboardSyncDt(null);
                    if (empty($magentoTicketLineData) && empty($magentoTicketLineData['id'])) {
                        // create new ticket line in the table
                        $this->logs->info(
                            "Line with ID: {$line->id} for ticket with ID: {$line->sales_transaction_id} was created",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    } else {
                        // update ticket line
                        $magentoTicketLine->setId($magentoTicketLineData['id']);
                        $this->logs->info(
                            "Line with ID: {$line->id} for ticket with ID: {$line->sales_transaction_id} was updated",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    }
                    // create / update object
                    $this->ticketLineRepository->save($magentoTicketLine);
                }
            } catch (Throwable $ex) {
                $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
            }
        }

        // return list of ticket adjustments
        return $ticketLinesAdjustments;
    }

    /**
     * Update Ticket Lines Adjustments in the SpringBoard table
     *
     * @param array $ticketLinesAdjustments
     * @param array $allSpringboardOrderItemsAdjustments
     */
    private function updateTicketLinesAdjustments(
        array $ticketLinesAdjustments,
        array $allSpringboardOrderItemsAdjustments
    ) {
        // check if found any adjustments for this ticket
        if (empty($ticketLinesAdjustments)) {
            return;
        }

        // update adjustments
        // check each item in the ticket
        foreach ($ticketLinesAdjustments as $ticketLinesAdjustment) {
            try {
                // check data
                $deltaPrice = 0;
                if (!empty($ticketLinesAdjustment['delta_price'])) {
                    $deltaPrice = (float)$ticketLinesAdjustment['delta_price'];
                }

                // create springboard magento ticket adjustments and add data from springboard ticket
                $magentoTicketLineAdjustments = $this->ticketLineAdjustmentsFactory->create()
                    ->setLineId($ticketLinesAdjustment['line_id'])
                    ->setAdjustmentId($ticketLinesAdjustment['adjustment_id'])
                    ->setDeltaPrice(number_format($deltaPrice, 4, '.', ''))
                    ->setName($ticketLinesAdjustment['name'])
                    ->setDescription($ticketLinesAdjustment['description']);

                // get springboard ticket line adjustments data from the magento database
                $magentoTicketLineAdjustmentsData = [];
                if (!empty($allSpringboardOrderItemsAdjustments[$ticketLinesAdjustment['adjustment_id']])) {
                    $magentoTicketLineAdjustmentsData =
                        $this->springboardHelper->getData(
                            $allSpringboardOrderItemsAdjustments[$ticketLinesAdjustment['adjustment_id']]
                        );
                }

                // compare magento and springboard ticket data
                $diff = array_diff_assoc(
                    $this->springboardHelper->getData($magentoTicketLineAdjustments),
                    $magentoTicketLineAdjustmentsData
                );
                // need to update ticket in the magento
                if (!empty($diff)) {
                    $magentoTicketLineAdjustments->setSpringboardSyncDt(null);
                    if (empty($magentoTicketLineAdjustmentsData) && empty($magentoTicketLineAdjustmentsData['id'])) {
                        // create new ticket line in the table
                        $this->logs->info(
                            "Adjustments with ID: {$ticketLinesAdjustment['adjustment_id']} for " .
                            "ticket line with ID: {$ticketLinesAdjustment['line_id']} was created",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    } else {
                        // update ticket line
                        $magentoTicketLineAdjustments->setId($magentoTicketLineAdjustmentsData['id']);
                        $this->logs->info(
                            "Adjustments with ID: {$ticketLinesAdjustment['adjustment_id']} for " .
                            "ticket line with ID: {$ticketLinesAdjustment['line_id']} was updated",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    }
                    // create / update object
                    $this->ticketLineAdjustmentsRepository->save($magentoTicketLineAdjustments);
                }

            } catch (Throwable $ex) {
                $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
            }
        }
    }

    /**
     * Load list of All Springboard tickets from the Magento Database
     *
     * @param string $date
     *
     * @return array
     * @throws LocalizedException
     */
    private function loadAllSpringboardTickets(string $date): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder->setField('created_date')
                ->setConditionType('gt')->setValue($this->springboardHelper->dateCorrection($date))->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $tickets = [];
        foreach ($this->ticketRepository->getList($searchCriteria)->getItems() as $item) {
            $tickets[$item['ticket_id']] = $item;
        }

        return $tickets;
    }

    /**
     * Load list of All Springboard ticket items from the Magento Database
     *
     * @param array $allSpringboardTickets
     *
     * @return array
     * @throws LocalizedException
     */
    private function loadAllSpringboardOrderItems(array $allSpringboardTickets): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder->setField('ticket_id')
                ->setConditionType('in')->setValue(array_keys($allSpringboardTickets))->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $ticketLines = [];
        foreach ($this->ticketLineRepository->getList($searchCriteria)->getItems() as $item) {
            $ticketLines[$item['line_id']] = $item;
        }

        return $ticketLines;
    }

    /**
     * Load list of All Springboard Items Adjustments from the Magento Database
     *
     * @param array $allSpringboardTicketItems
     *
     * @return array
     * @throws LocalizedException
     */
    private function loadAllSpringboardOrderItemsAdjustments(array $allSpringboardTicketItems): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder->setField('line_id')
                ->setConditionType('in')->setValue(array_keys($allSpringboardTicketItems))->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $ticketLinesAdjustments = [];
        foreach ($this->ticketLineAdjustmentsRepository->getList($searchCriteria)->getItems() as $item) {
            $ticketLinesAdjustments[$item['adjustment_id']] = $item;
        }

        return $ticketLinesAdjustments;
    }
}
