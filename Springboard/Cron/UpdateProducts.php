<?php declare(strict_types=1);

namespace C38\Springboard\Cron;
//@codingStandardsIgnoreFile

use C38\Catalog\Model\Product\AttributeValueResolver;
use C38\CatalogInventory\Api\ConfigurableQtyCollectionProcessorInterface as ConfigurableQtyCollectionProcessor;
use C38\Core\Model\MysqlQueryExecutor;
use C38\Core\Model\TableNameResolver;
use C38\Springboard\Api\Data\VendorInterfaceFactory;
use C38\Springboard\Api\LocationRepositoryInterface;
use C38\Springboard\Api\VendorRepositoryInterface;
use C38\Springboard\Helper\Data as SpringboardHelper;
use C38\Springboard\Helper\Logs;
use C38\Springboard\Model\ResourceModel\Location\CollectionFactory as LocationCollectionFactory;
use C38\Springboard\Model\ResourceModel\Vendor\CollectionFactory as VendorCollectionFactory;
use C38\Springboard\Service\SpringboardAPIClient;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type\Price as PriceCalculator;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Eav\Model\Entity\AttributeFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface as StoreManager;
use Throwable;

/**
 * Class UpdateProducts
 * Import products from the Springboard to the local storage.
 * Export products from Magento to the springboard and local storage
 */
class UpdateProducts
{
    const SPRINGBOARD_CRON_NAME = "UpdateProducts";

    const DEFAULT_STORE_ID = 0;

    /**
     * @var MysqlQueryExecutor
     */
    private $mysqlQueryExecutor;

    /**
     * @var TableNameResolver
     */
    private $tableNameResolver;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var AttributeValueResolver
     */
    private $attributeValueResolver;

    /**
     * @var LocationRepositoryInterface
     */
    private $locationRepository;

    /**
     * @var VendorRepositoryInterface
     */
    private $vendorRepository;

    /**
     * @var VendorInterfaceFactory
     */
    private $vendorFactory;

    /**
     * @var SpringboardHelper
     */
    private $springboardHelper;

    /**
     * @var LocationCollectionFactory
     */
    private $locationCollectionFactory;

    /**
     * @var VendorCollectionFactory
     */
    private $vendorCollectionFactory;

    /**
     * @var AttributeFactory
     */
    private $attributeFactory;

    /**
     * @var PriceCalculator
     */
    private $priceCalculator;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var Status
     */
    private $productStatus;

    /**
     * @var ConfigurableQtyCollectionProcessor
     */
    private $collectionProcessor;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var StoreManager
     */
    private $storeManager;

    /**
     * @var Logs
     */
    private $logs;

    /**
     * Class constructor
     *
     * @param MysqlQueryExecutor $mysqlQueryExecutor
     * @param TableNameResolver $tableNameResolver
     * @param ResourceConnection $resourceConnection
     * @param AttributeValueResolver $attributeValueResolver
     * @param LocationRepositoryInterface $locationRepository
     * @param VendorRepositoryInterface $vendorRepository
     * @param SpringboardHelper $springboardHelper
     * @param LocationCollectionFactory $locationCollectionFactory
     * @param VendorCollectionFactory $vendorCollectionFactory
     * @param VendorInterfaceFactory $vendorFactory
     * @param AttributeFactory $attributeFactory
     * @param PriceCalculator $priceCalculator
     * @param ProductCollectionFactory $productCollectionFactory
     * @param Status $productStatus
     * @param ConfigurableQtyCollectionProcessor $collectionProcessor
     * @param DateTime $date
     * @param StoreManager $storeManager
     * @param Logs $logs
     */
    public function __construct(
        MysqlQueryExecutor $mysqlQueryExecutor,
        TableNameResolver $tableNameResolver,
        ResourceConnection $resourceConnection,
        AttributeValueResolver $attributeValueResolver,
        LocationRepositoryInterface $locationRepository,
        VendorRepositoryInterface $vendorRepository,
        SpringboardHelper $springboardHelper,
        LocationCollectionFactory $locationCollectionFactory,
        VendorCollectionFactory $vendorCollectionFactory,
        VendorInterfaceFactory $vendorFactory,
        AttributeFactory $attributeFactory,
        PriceCalculator $priceCalculator,
        ProductCollectionFactory $productCollectionFactory,
        Status $productStatus,
        ConfigurableQtyCollectionProcessor $collectionProcessor,
        DateTime $date,
        StoreManager $storeManager,
        Logs $logs
    ) {
        $this->mysqlQueryExecutor = $mysqlQueryExecutor;
        $this->tableNameResolver = $tableNameResolver;
        $this->resourceConnection = $resourceConnection;
        $this->attributeValueResolver = $attributeValueResolver;
        $this->locationRepository = $locationRepository;
        $this->vendorRepository = $vendorRepository;
        $this->springboardHelper = $springboardHelper;
        $this->locationCollectionFactory = $locationCollectionFactory;
        $this->vendorCollectionFactory = $vendorCollectionFactory;
        $this->vendorFactory = $vendorFactory;
        $this->attributeFactory = $attributeFactory;
        $this->priceCalculator = $priceCalculator;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productStatus = $productStatus;
        $this->collectionProcessor = $collectionProcessor;
        $this->date = $date;
        $this->storeManager = $storeManager;
        $this->logs = $logs;
    }

    /**
     * Update Springboard Products
     */
    public function execute()
    {
        // update location information
        $this->updateLocations();
        // update products
        $this->exportProducts();
    }

    /**
     * Sync Inventory
     */
    public function syncInventory()
    {
        // update location information
        $this->updateLocations();
    }

    /**
     * Update SpringBoard Locations
     */
    private function updateLocations()
    {
        try {
            // get springboard client
            $client = $this->springboardHelper->getClient();
            // get location
            $locations = $client->getLocations();

            // update all locations data
            $springboardIds = [];
            foreach ($locations->results as $location) {
                try {
                    // get data from springboard
                    $data = [
                        'springboard_id' => $location->{'id'},
                        'location_number' => $location->{'public_id'},
                        'name' => $location->{'name'},
                        'timezone_identifier_id' => $location->{'timezone_identifier_id'},
                        'is_active' => ($location->{'active?'} == 1) ? 1 : 0,
                        'is_deleted' => 0,
                    ];

                    $springboardIds[] = $data['springboard_id'];
                    // get location from database
                    $location = $this->locationRepository->loadBySpringboardId("" . $data['springboard_id']);
                    if (!$location->getId()) {
                        // create new one
                        $location->setSpringboardId($data['springboard_id']);
                    }
                    // update parameters
                    $location->setLocationNumber($data['location_number']);
                    $location->setName($data['name']);
                    $location->setTimezoneIdentifierId($data['timezone_identifier_id']);
                    $location->setIsActive($data['is_active']);
                    $location->setIsDeleted($data['is_deleted']);
                    $location->setUpdatedAt(date("Y-m-d H:i:s"));

                    // save location
                    $this->locationRepository->save($location);
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                }
            }

            // delete location if need to
            if (!empty($springboardIds)) {
                $outdatedLocations = $this->locationCollectionFactory->create();
                $outdatedLocations->addFieldToFilter('springboard_id', ['nin' => $springboardIds]);

                foreach ($outdatedLocations as $outdatedLocation) {
                    $outdatedLocation->setIsDeleted(1)->setUpdatedAt(null);
                    $this->locationRepository->save($outdatedLocation);
                }
            }

            // update inventory for retail locations only
            $retailLocations = $this->springboardHelper->getRetailLocations();
            foreach ($retailLocations as $locationSpringboardId) {
                try {
                    // import inventory to the database
                    $this->importInventory($client, $locationSpringboardId);

                    // get spring board location code
                    $locationSpringboardName = '';
                    if ($locationSpringboardId == '100042') {
                        $locationSpringboardName = 'ham';
                    }
                    if ($locationSpringboardId == '100043') {
                        $locationSpringboardName = 'pal';
                    }

                    // update magento inventory table
                    $sql = "INSERT " . "INTO inventory_source_item (source_code, sku, quantity, status)
                        SELECT '$locationSpringboardName', e.sku, spb.qty, '1' FROM c38_springboard_inventory AS spb
                        INNER JOIN catalog_product_entity AS e ON e.springboard_id = spb.product_id
                        WHERE spb.location_id = $locationSpringboardId
                        ON DUPLICATE KEY UPDATE quantity = spb.qty;";
                    $this->mysqlQueryExecutor->execute($sql);

                    // update magento report inventory table
                    $sql = "INSERT " . "INTO dc_catalog_inventory (dc_code, product_id, sku, is_in_stock, qty_available)
                        SELECT '$locationSpringboardName',
                        e.entity_id, e.sku, '1', spb.qty FROM c38_springboard_inventory AS spb
                        INNER JOIN catalog_product_entity AS e ON e.springboard_id = spb.product_id
                        WHERE spb.location_id = $locationSpringboardId
                        ON DUPLICATE KEY UPDATE qty_available = spb.qty;";
                    $this->mysqlQueryExecutor->execute($sql);
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                }
            }
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }
    }

    /**
     * Update inventory for SpringBoard Retail Location
     *
     * @param SpringboardAPIClient $client
     * @param string $locationId
     */
    private function importInventory(SpringboardAPIClient $client, string $locationId): void
    {
        // get SpringBoard Inventory Table
        $springboardInventoryTable = $this->tableNameResolver->resolve('c38_springboard_inventory');

        // get data from SpringBoard
        $allPages = null;
        for ($page = 1; $allPages >= $page || $allPages === null; $page++) {
            try {
                // get inventory for springboard
                $inventoryValues = $client->getInventoryValues($locationId, $page);
                if (!is_object($inventoryValues)) {
                    continue;
                }
                if ($allPages === null) {
                    $allPages = $inventoryValues->pages;
                }

                $values = [];
                // create list of items to update
                foreach ($inventoryValues->results as $inventoryValue) {
                    $values[] = '(' . $inventoryValue->item_id . ','
                        . $inventoryValue->location_id . ",'"
                        . $inventoryValue->qty . "')";
                }

                // update database
                $valueString = implode(',', $values);
                $sql = "INSERT " . "INTO {$springboardInventoryTable} (product_id, location_id, qty)
                            VALUES {$valueString} ON DUPLICATE KEY UPDATE qty = VALUES(qty)";
                $this->mysqlQueryExecutor->execute($sql);
            } catch (Throwable $ex) {
                $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
            }
        }
    }

    /**
     * Export Products to SpringBoard
     * @noinspection PhpComposerExtensionStubsInspection
     */
    private function exportProducts()
    {
        try {
            // create springboard connection
            $client = $this->springboardHelper->getClient();

            // get base media url
            $baseMediaUrl =
                $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product';

            // get all colors
            $colorOptions = [];
            $allOptions = $this->attributeValueResolver->getAllOptions('color');
            foreach ($allOptions as $option) {
                $colorOptions[$option['value']] = $option['label'];
            }

            // get all product sizes
            $sizeOptions = [];
            $allOptions = $this->attributeValueResolver->getAllOptions('size');
            foreach ($allOptions as $option) {
                $sizeOptions[$option['value']] = $option['label'];
            }

            // get all collections
            $collectionNameOptions = [];
            $allOptions = $this->attributeValueResolver->getAllOptions('collection_name');
            foreach ($allOptions as $option) {
                $collectionNameOptions[$option['value']] = $option['label'];
            }

            // get all vendors
            $vendorSpringboardIds = $this->getSpringboardVendors();

            // get all products for synchronization
            $productCollection = $this->getProductSyncCollection();

            // statistics
            $totalProducts = count($productCollection);
            $created = 0;
            $updated = 0;
            $this->logs->info($totalProducts . " products ready prepared for update.", self::SPRINGBOARD_CRON_NAME);

            // update all products in the Springboard
            foreach ($productCollection as $product) {
                try {
                    // prepare product
                    $productId = $product['id'];
                    $productParentImage = "";
                    if (!empty($product['parent_image'])) {
                        $productParentImage = $product['parent_image'];
                    }
                    $productSpringboardImage = "";
                    if (!empty($product['springboard_image'])) {
                        $productSpringboardImage = $product['springboard_image'];
                    }

                    if (empty($product['color']) || empty($colorOptions[$product['color']])) {
                        $product['custom']['color'] = 'N/A';
                    } else {
                        $product['custom']['color'] = $colorOptions[$product['color']];
                        $product['long_description'] = $product['name'] . ' ' . $colorOptions[$product['color']];
                    }
                    if (!empty($product['size']) && !empty($sizeOptions[$product['size']])) {
                        $product['custom']['size'] = $sizeOptions[$product['size']];
                    }
                    if (!empty($product['manufacturer']) && !empty($vendorSpringboardIds[$product['manufacturer']])) {
                        $vendor = $vendorSpringboardIds[$product['manufacturer']];
                        $product['custom']['vendor'] = $vendor['name'];
                        $product['primary_vendor_id'] = $vendor['vendor_id'];
                    }
                    if (!empty($product['collection']) && !empty($collectionNameOptions[$product['collection']])) {
                        $product['custom']['collection'] = $collectionNameOptions[$product['collection']];
                    }
                    $product['custom']['style_name'] = $product['parent_name'];
                    $product['custom']['configurable_sku'] = $product['configurable_sku'];

                    if (!empty($product['cost_after_discount'])) {
                        $product['cost'] = $product['cost_after_discount'];
                    }

                    // get spring board id from product
                    if (!empty($product['springboard_id'])) {
                        $springboardId = $product['springboard_id'];
                    } else {
                        $springboardId = -1;
                    }

                    // unset unused parameters
                    $unUsedParameters = [
                        'color', 'size', 'vendor', 'collection', 'manufacturer', 'id', 'springboard_id',
                        'springboard_image', 'name', 'parent_name', 'parent_image', 'configurable_sku',
                        'cost_after_discount'
                    ];
                    // remove unused parameters
                    foreach ($unUsedParameters as $unUsedParameter) {
                        unset($product[$unUsedParameter]);
                    }

                    // save save product at SpringBoard and Update Magento Information
                    if ($springboardId > 0) {
                        // product exist, need to update
                        $result = $client->updateItem($product, $springboardId);
                        if (empty($result->id)) {
                            $this->logs->warning(
                                "Can't update product: " . $productId,
                                json_encode($result),
                                self::SPRINGBOARD_CRON_NAME
                            );
                            continue;
                        }

                        // add image to springboard
                        if (!empty($productParentImage)) {
                            if ($productParentImage != $productSpringboardImage &&
                                $productParentImage != 'no_selection') {
                                $springboardImageId =
                                    $client->uploadImage("" . $springboardId, $baseMediaUrl . $productParentImage);
                                if ($springboardImageId > 0) {
                                    $client->setPrimaryImage("" . $springboardId, "" . $springboardImageId);
                                    $productSpringboardImage = $productParentImage;
                                } else {
                                    $this->logs->warning(
                                        "Can't update image for product: " . $productId,
                                        '',
                                        self::SPRINGBOARD_CRON_NAME
                                    );
                                }
                            }
                        }

                        // update product updated at springboard date in the magento db
                        $sql = "UPDATE " . "catalog_product_entity SET springboard_image='{$productSpringboardImage}',
                        springboard_sync_dt=now() WHERE entity_id=" . $productId;
                        $this->mysqlQueryExecutor->execute($sql);
                        $updated++;
                    } else {
                        // create new product at spring board
                        $result = $client->createRealItem($product);

                        if (!empty($result->message)) {
                            $this->logs->warning(
                                "Can't create product: " . $productId,
                                json_encode($result),
                                self::SPRINGBOARD_CRON_NAME
                            );
                            continue;
                        } else {
                            $springboardId = filter_var($result, FILTER_SANITIZE_NUMBER_INT);
                        }

                        // add image to springboard
                        if (!empty($productParentImage) && $productParentImage != 'no_selection') {
                            $springboardImageId = $client->uploadImage(
                                "" . $springboardId,
                                $baseMediaUrl . $productParentImage
                            );
                            if ($springboardImageId > 0) {
                                $client->setPrimaryImage("" . $springboardId, "" . $springboardImageId);
                                $productSpringboardImage = $productParentImage;
                            } else {
                                $this->logs->warning(
                                    "Can't update image for product: " . $productId,
                                    '',
                                    self::SPRINGBOARD_CRON_NAME
                                );
                                $productSpringboardImage = "";
                            }
                        }

                        // update product in the magento db
                        $sql = "UPDATE " . "catalog_product_entity SET springboard_id={$springboardId},
                            springboard_image='{$productSpringboardImage}',
                            springboard_sync_dt=now() WHERE entity_id=" . $productId;
                        $this->mysqlQueryExecutor->execute($sql);
                        $created++;
                    }
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                }
            }

            $processed = $created + $updated;
            $this->logs->info(
                "{$created} products were created. {$updated} were updated. " .
                "Successfully processed {$processed} of {$totalProducts}",
                self::SPRINGBOARD_CRON_NAME
            );
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }
    }

    /**
     * Get Product Collection for Synchronization
     *
     * @return array
     */
    public function getProductSyncCollection(): array
    {
        // product collection
        $productCollection = [];

        try {
            // create magento product collection
            $collection = $this->productCollectionFactory->create();

            $attributes = [
                'sku', 'name', 'attribute_set_id', 'tx_category_id', 'tx_sub_category_id', 'upc', 'cost',
                'cost_after_discount', 'weight', 'status', 'color', 'size', 'manufacturer', 'visibility', 'price',
                'special_price', 'special_from_date', 'special_to_date'
            ];
            $collection->addAttributeToSelect($attributes)
                //->addAttributeToFilter('status', ['in' => $this->productStatus->getVisibleStatusIds()])
                ->addAttributeToFilter('type_id', ['eq' => 'simple']);

            $collection->joinField(
                'parent_id',
                'catalog_product_super_link',
                'parent_id',
                'product_id=entity_id',
                null,
                'left'
            );

            $collection->joinField(
                'category_name',
                'c38_taxonomy_categories',
                'tx_category_name',
                'tx_category_id=tx_category_id',
                null,
                'left'
            );

            $collection->joinField(
                'sub_category_name',
                'c38_taxonomy_sub_categories',
                'tx_sub_category_name',
                'tx_sub_category_id=tx_sub_category_id',
                null,
                'left'
            );

            $collection->getSelect()->joinLeft(
                ['parent' => 'catalog_product_entity'],
                'at_parent_id.parent_id = parent.entity_id',
                ['parent_updated_at' => 'parent.updated_at', 'configurable_sku' => 'parent.sku']
            );

            // prepare parents attributes
            $parentAttributes = [
                'name',
                'image',
                'cost',
                'cost_after_discount',
                'price',
                'special_price',
                'special_from_date',
                'special_to_date',
                'collection_name'
            ];
            foreach ($parentAttributes as $attributeCode) {
                $alias = $attributeCode . '_parent_table';

                $attribute = $this->attributeFactory->create();
                $attribute->loadByCode(Product::ENTITY, $attributeCode);

                $collection->getSelect()->joinLeft(
                    [$alias => $attribute->getBackendTable()],
                    "parent_id=$alias.entity_id AND $alias.attribute_id={$attribute->getId()}
                    AND $alias.store_id=" . self::DEFAULT_STORE_ID,
                    ['parent_' . $attributeCode => 'value']
                );
            }

            // add where condition
            $collection->getSelect()->where('
                (`e`.`updated_at` > `e`.`springboard_sync_dt`) OR (`parent`.`updated_at` > `e`.`springboard_sync_dt`)
                OR (
                    special_from_date_parent_table.value IS NOT NULL
                    AND special_to_date_parent_table.value IS NOT NULL
                    AND special_from_date_parent_table.value < special_to_date_parent_table.value
                    AND
                    (
                        (
                            e.springboard_sync_dt < special_from_date_parent_table.value
                            AND now() > special_from_date_parent_table.value
                        )
                        OR (
                            e.springboard_sync_dt < special_to_date_parent_table.value
                            AND now() > special_to_date_parent_table.value
                        )
                    )
                )
            ');

            // add data to product collection
            foreach ($collection as $product) {
                try {
                    // basic attributes
                    $productData = [
                        'description' => $product->getName(),
                        'weight' => $product->getWeight(),
                        'custom' => [
                            'sku' => $product->getSku(),
                            'upc' => $product->getUpc(),
                            'category' => $product->getCategoryName(),
                            'subcategory' => $product->getSubCategoryName(),
                        ],
                        'active' => ($product->getStatus() == 1) ? 1 : 0,
                        'color' => $product->getColor(),
                        'size' => $product->getSize(),
                        'vendor' => $product->getManufacturer(),
                        'manufacturer' => $product->getManufacturer(),
                        'id' => $product->getId(),
                        'springboard_id' => $product->getSpringboardId(),
                        'springboard_image' => $product->getSpringboardImage(),
                        'name' => $product->getName(),
                        'parent_name' => $product->getParentName(),
                        'parent_image' => $product->getParentImage(),
                        'configurable_sku' => $product->getConfigurableSku()
                    ];

                    // add parent attributes
                    if (!empty($product->getParentCost())) {
                        $productData['cost'] = $product->getParentCost();
                    } else {
                        $productData['cost'] = empty($product->getCost()) ? 0.0 : $product->getCost();
                    }
                    if (!empty($product->getParentCostAfterDiscount())) {
                        $productData['cost_after_discount'] = $product->getParentCostAfterDiscount();
                    } else {
                        $productData['cost_after_discount'] =
                            empty($product->getCostAfterDiscount()) ? 0.0 : $product->getCostAfterDiscount();
                    }
                    if (!empty($product->getParentPrice())) {
                        $productData['original_price'] = $product->getParentPrice();
                        $productData['price'] = $this->priceCalculator->calculateSpecialPrice(
                            $product->getParentPrice(),
                            $product->getParentSpecialPrice(),
                            $product->getParentSpecialFromDate(),
                            $product->getParentSpecialToDate(),
                            self::DEFAULT_STORE_ID
                        );
                    } else {
                        $productData['original_price'] = $product->getPrice();
                        $productData['price'] = $this->priceCalculator->calculateSpecialPrice(
                            $product->getPrice(),
                            $product->getSpecialPrice(),
                            $product->getSpecialFromDate(),
                            $product->getSpecialToDate(),
                            self::DEFAULT_STORE_ID
                        );
                    }
                    if (!empty($product->getParentCollectionName())) {
                        $productData['collection'] = $product->getParentCollectionName();
                    } else {
                        $productData['collection'] = $product->getCollectionName();
                    }

                    // add information about product
                    $productCollection[] = $productData;
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                }
            }
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // return result
        return $productCollection;
    }

    /**
     * Get List of Vendors
     *
     * @return array
     * @throws LocalizedException
     */
    private function getSpringboardVendors(): array
    {
        // synchronize Vendors between Magento, Local Springboard and Springboard API
        $this->synchronizeVendors();

        // create vendor by id collection
        $vendorById = [];
        // load vendors
        $vendors = $this->getVendorsFromLocalSpringboard();
        foreach ($vendors as $vendor) {
            $vendorById[$vendor['designer_id']] = ['vendor_id' => $vendor['vendor_id'], 'name' => $vendor['name']];
        }

        // return result
        return $vendorById;
    }

    /**
     * Synchronize All Vendors
     */
    private function synchronizeVendors()
    {
        try {
            // get vendors from Magento
            $magentoVendors = $this->getVendorsFromMagento();
            // get vendors from Local Springboard DB
            $springboardLocalVendors = $this->getVendorsFromLocalSpringboard();
            // get vendors from Springboard API
            $springboardVendors = $this->getVendorsFromSpringboard();

            // update Local Springboard DB using Springboard API data
            foreach ($springboardVendors as $springboardVendorName => $springboardVendor) {
                try {
                    if (empty($springboardVendorName)) {
                        continue;
                    }
                    // get local vendor information
                    if (empty($springboardLocalVendors[$springboardVendorName])) {
                        // create new vendor
                        $this->createLocalVendor($springboardVendor);
                    }
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                }
            }

            // get vendors from Local Springboard DB
            $springboardLocalVendors = $this->getVendorsFromLocalSpringboard();
            // update Local Springboard DB using Magento data
            foreach ($magentoVendors as $magentoVendorName => $magentoVendorId) {
                try {
                    if (empty($magentoVendorName)) {
                        continue;
                    }

                    // get local vendor information
                    if (empty($springboardLocalVendors[$magentoVendorName])) {
                        // create new vendor
                        $this->createVendor($magentoVendorId, $magentoVendorName);
                    } else {
                        // update vendor
                        $this->updateVendor($springboardLocalVendors[$magentoVendorName], $magentoVendorId);
                    }
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                }
            }
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }
    }

    /**
     * Get List of all Vendors from Magento DB
     *
     * @return array
     * @throws LocalizedException
     */
    private function getVendorsFromMagento(): array
    {
        $vendorList = [];
        try {
            // get list of all vendors
            $allOptions = $this->attributeValueResolver->getAllOptions('manufacturer');
            // prepare vendors collection
            foreach ($allOptions as $option) {
                $vendorList[strtoupper(trim($option['label']))] = $option['value'];
            }
        } catch (Throwable $ex) {
            throw new LocalizedException(new Phrase($ex->getMessage()), $ex);
        }

        // return result
        return $vendorList;
    }

    /**
     * Get List of all Vendors from Local Springboard DB
     *
     * @return array
     * @throws LocalizedException
     */
    private function getVendorsFromLocalSpringboard(): array
    {
        $vendorList = [];
        try {
            // load list of vendors from Local Springboard DB
            $collection = $this->vendorCollectionFactory->create();
            $collection->addFieldToFilter('id', ['gt' => 0]);
            // create collection
            foreach ($collection as $vendor) {
                $name = strtoupper(trim($vendor->getName()));
                $vendorList[$name] = [
                    "id" => $vendor->getId(),
                    "vendor_id" => $vendor->getVendorId(),
                    "designer_id" => $vendor->getDesignerId(),
                    "name" => $name
                ];
            }
        } catch (Throwable $ex) {
            throw new LocalizedException(new Phrase($ex->getMessage()), $ex);
        }

        // return result
        return $vendorList;
    }

    /**
     * Get List of all Vendors from Springboard API
     *
     * @return array
     * @throws LocalizedException
     */
    private function getVendorsFromSpringboard(): array
    {
        $vendorList = [];
        try {
            // get Springboard connection
            $client = $this->springboardHelper->getClient();

            // get all pages with data from Springboard
            $allPages = null;
            for ($page = 1; $allPages >= $page || $allPages === null; $page++) {
                // get vendors
                $vendors = $client->getVendors($page);
                if (!is_object($vendors)) {
                    continue;
                }
                // get page count
                if ($allPages === null) {
                    $allPages = $vendors->pages;
                }
                // prepare list of vendors
                foreach ($vendors->results as $vendor) {
                    $name = strtoupper(trim($vendor->name));
                    $vendorList[$name] = [
                        "vendor_id" => $vendor->id,
                        "name" => $name
                    ];
                }
            }
        } catch (Throwable $ex) {
            throw new LocalizedException(new Phrase($ex->getMessage()), $ex);
        }
        // return result
        return $vendorList;
    }

    /**
     * Create Vendor in the Local Springboard Database
     *
     * @param array $springboardVendor
     * @param string $designerId
     * @throws LocalizedException
     */
    private function createLocalVendor(array $springboardVendor, string $designerId = '')
    {
        $this->logs->info(
            "Creating vendor {$springboardVendor['name']} in the local Springboard Database",
            self::SPRINGBOARD_CRON_NAME
        );
        try {
            $vendor = $this->vendorFactory->create();
            $vendor = $vendor->setVendorId($springboardVendor['vendor_id'])->setName($springboardVendor['name']);
            if (!empty($designerId)) {
                $vendor->setDesignerId($designerId);
            }
            $this->vendorRepository->save($vendor);
        } catch (Throwable $ex) {
            throw new LocalizedException(new Phrase($ex->getMessage()), $ex);
        }
    }

    /**
     * Create Vendor in the SpringboardApi
     *
     * @param string $designerId
     * @param string $vendorName
     * @throws LocalizedException
     */
    private function createVendor(string $designerId, string $vendorName)
    {
        $this->logs->info(
            "Creating vendor {$vendorName} in the Springboard API",
            self::SPRINGBOARD_CRON_NAME
        );
        try {
            // create vendor on springboard
            $client = $this->springboardHelper->getClient();
            $vendorId = $client->createVendor(['name' => $vendorName, 'custom' => ['ws_pl' => "WHOLESALE"]]);
            if ($vendorId) {
                // create local vendor
                $this->createLocalVendor(['vendor_id' => $vendorId, 'name' => $vendorName], $designerId);
            }
        } catch (Throwable $ex) {
            throw new LocalizedException(new Phrase($ex->getMessage()), $ex);
        }
    }

    /**
     * Update in the Local Springboard Database
     *
     * @param array $vendor
     * @param string $designerId
     * @throws LocalizedException
     */
    private function updateVendor(array $vendor, string $designerId)
    {
        try {
            if (empty($vendor['designer_id']) || $vendor['designer_id'] == 0) {
                $this->logs->info(
                    "Update Designer ID for Vendor {$vendor['name']} in the local Springboard Database",
                    self::SPRINGBOARD_CRON_NAME
                );
                $vendor = $this->vendorRepository->loadByVendorId($vendor['vendor_id']);
                $vendor->setDesignerId($designerId);
                $this->vendorRepository->save($vendor);
            }
        } catch (Throwable $ex) {
            throw new LocalizedException(new Phrase($ex->getMessage()), $ex);
        }
    }
}
