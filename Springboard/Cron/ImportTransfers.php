<?php declare(strict_types=1);

namespace C38\Springboard\Cron;

use C38\Springboard\Api\Data\TransferInterfaceFactory;
use C38\Springboard\Api\Data\TransferLineInterfaceFactory;
use C38\Springboard\Api\TransferLineRepositoryInterface;
use C38\Springboard\Api\TransferRepositoryInterface;
use C38\Springboard\Helper\Data as SpringboardHelper;
use C38\Springboard\Helper\Logs;
use C38\Springboard\Service\SpringboardAPIClient;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Throwable;

/**
 * Class ImportTransfers
 * Import Transfers from Springboard to the Local Storage
 */
class ImportTransfers
{
    const SPRINGBOARD_CRON_NAME = "ImportTransfers";

    /**
     * @var TransferInterfaceFactory
     */
    private $transferFactory;

    /**
     * @var TransferRepositoryInterface
     */
    private $transferRepository;

    /**
     * @var TransferLineInterfaceFactory
     */
    private $transferLineFactory;

    /**
     * @var TransferLineRepositoryInterface
     */
    private $transferLineRepository;

    /**
     * @var SpringboardHelper
     */
    private $springboardHelper;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var Logs
     */
    private $logs;

    /**
     * ImportTransfers constructor.
     *
     * @param TransferInterfaceFactory $transferFactory
     * @param TransferRepositoryInterface $transferRepository
     * @param TransferLineInterfaceFactory $transferLineFactory
     * @param TransferLineRepositoryInterface $transferLineRepository
     * @param SpringboardHelper $springboardHelper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param Logs $logs
     */
    public function __construct(
        TransferInterfaceFactory $transferFactory,
        TransferRepositoryInterface $transferRepository,
        TransferLineInterfaceFactory $transferLineFactory,
        TransferLineRepositoryInterface $transferLineRepository,
        SpringboardHelper $springboardHelper,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        Logs $logs
    ) {
        $this->transferFactory = $transferFactory;
        $this->transferRepository = $transferRepository;
        $this->transferLineFactory = $transferLineFactory;
        $this->transferLineRepository = $transferLineRepository;
        $this->springboardHelper = $springboardHelper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->logs = $logs;
    }

    /**
     * Import Springboard Transfers
     */
    public function execute()
    {
        // start
        $this->logs->info("Import Springboard Transfers started...", self::SPRINGBOARD_CRON_NAME);

        try {
            // load list of all transfers from local storage
            $localTransfers = $this->loadLocalTransfers();

            // get springboard client
            $client = $this->springboardHelper->getClient();

            // update local transfers from springboard
            $updatedTransfers = $this->updateLocalTransfers($client, $localTransfers);
            if (empty($updatedTransfers)) {
                $this->logs->info("Import Springboard Transfers finished", self::SPRINGBOARD_CRON_NAME);
                return;
            }

            // load list of local transfer lines for updated transfers
            $localTransferLines = $this->loadLocalTransferLines($updatedTransfers);

            // update lines for each updated transfer
            foreach ($updatedTransfers as $updatedTransfer) {
                $this->updateLocalTransferLines($client, $updatedTransfer, $localTransferLines);
            }
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // finish
        $this->logs->info("Import Springboard Transfers finished", self::SPRINGBOARD_CRON_NAME);
    }

    /**
     * Update Local Transfers from Springboard
     *
     * @param SpringboardAPIClient $client
     * @param array $localTransfers
     *
     * @return array
     */
    private function updateLocalTransfers(SpringboardAPIClient $client, array $localTransfers): array
    {
        // list of updated transfers
        $updatedTransfers = [];

        // load all pages with transfers
        $allPages = null;
        $processed = 0;
        for ($page = 1; $allPages >= $page || $allPages === null; $page++) {
            try {
                // load transfers for current page
                $transfers = $client->getTransfers($page);
                if (!is_object($transfers)) {
                    continue;
                }
                if ($allPages === null) {
                    $allPages = $transfers->pages;
                    $this->logs->info(
                        "Found {$transfers->total} transfers in the Springboard",
                        self::SPRINGBOARD_CRON_NAME
                    );
                }

                // update transfers for current page
                foreach ($transfers->results as $transfer) {
                    try {
                        // update transfer
                        if ($this->updateTransfer($transfer, $localTransfers)) {
                            // add transfer id to the transfer updated list
                            $updatedTransfers[] = "" . $transfer->id;
                        }
                        $processed++;
                    } catch (Throwable $ex) {
                        $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                    }
                }
            } catch (Throwable $ex) {
                $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
            }
        }

        $this->logs->info(
            "Successfully processed {$processed} transfers from the Springboard",
            self::SPRINGBOARD_CRON_NAME
        );

        // return list of updated transfers
        return $updatedTransfers;
    }

    /**
     * Update Springboard transfer in the local storage.
     * Return true if transfer was updated.
     *
     * @param $transfer
     * @param array $localTransfers
     *
     * @return bool
     * @throws Throwable
     */
    private function updateTransfer($transfer, array $localTransfers): bool
    {
        // create magento transfer object
        $magentoTransfer = $this->transferFactory->create();
        // set data from springboard to the magento object
        $magentoTransfer
            ->setTransferId($transfer->id)
            ->setStatus($transfer->status)
            ->setFromLocationId($transfer->from_location_id)
            ->setToLocationId($transfer->to_location_id)
            ->setTotalQtyShipped($this->springboardHelper->formatFloat($transfer, 'total_qty_shipped'))
            ->setTotalQtyReceived($this->springboardHelper->formatFloat($transfer, 'total_qty_received'))
            ->setTotalQtyLost($this->springboardHelper->formatFloat($transfer, 'total_qty_lost'))
            ->setTotalQtyRequested($this->springboardHelper->formatFloat($transfer, 'total_qty_requested'))
            ->setTotalQtyDiscrepant($this->springboardHelper->formatFloat($transfer, 'total_qty_discrepant'));

        // get transfer number
        if (!empty($transfer->custom) && !empty($transfer->custom->transfer)) {
            $magentoTransfer->setTransferNumber($transfer->custom->transfer);
        }

        // get springboard transfer data from the magento database
        $magentoTransferData = [];
        if (!empty($localTransfers[$transfer->id])) {
            $magentoTransferData = $this->springboardHelper->getData($localTransfers[$transfer->id]);
        }

        // compare magento and springboard transfer data
        $diff = array_diff_assoc($this->springboardHelper->getData($magentoTransfer), $magentoTransferData);
        // need to update invoice in the magento
        if (!empty($diff)) {
            // add parameters
            $magentoTransfer->setSpringboardSyncDt(null);
            $magentoTransfer->setCreatedDate($transfer->created_at);

            if (empty($magentoTransferData) && empty($magentoTransferData['id'])) {
                // create new transfer
                $this->logs->info(
                    "Transfer with ID: {$transfer->id} was created",
                    self::SPRINGBOARD_CRON_NAME
                );
            } else {
                // update transfer
                $magentoTransfer->setId($magentoTransferData['id']);
                $this->logs->info(
                    "Transfer with ID: {$transfer->id} was updated",
                    self::SPRINGBOARD_CRON_NAME
                );
            }
            // create / update object
            $this->transferRepository->save($magentoTransfer);

            // was updated or created
            return true;
        }

        // no update needed
        return false;
    }

    /**
     * Update Local Transfers Lines from Springboard
     *
     * @param SpringboardAPIClient $client
     * @param string $transferId
     * @param array $localTransferLines
     */
    private function updateLocalTransferLines(
        SpringboardAPIClient $client,
        string $transferId,
        array $localTransferLines
    ) {
        // load all pages with transfer lines
        try {
            $allPages = null;
            $processed = 0;
            for ($page = 1; $allPages >= $page || $allPages === null; $page++) {
                try {
                    // load transfers for current page
                    $transferLines = $client->getTransferLines($transferId, $page);
                    if (!is_object($transferLines)) {
                        continue;
                    }
                    if ($allPages === null) {
                        $allPages = $transferLines->pages;
                        $this->logs->info(
                            "Found {$transferLines->total} transfers lines for transfer " .
                            "with id {$transferId} in the Springboard",
                            self::SPRINGBOARD_CRON_NAME
                        );
                    }

                    // update transfers for current page
                    foreach ($transferLines->results as $transfer) {
                        try {
                            // update transfer line
                            $this->updateTransferLine($transfer, $transferId, $localTransferLines);
                            $processed++;
                        } catch (Throwable $ex) {
                            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                        }
                    }
                } catch (Throwable $ex) {
                    $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                }
            }
            $this->logs->info(
                "Successfully processed {$processed} transfers lines for transfer " .
                "with id {$transferId} from the Springboard",
                self::SPRINGBOARD_CRON_NAME
            );
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }
    }

    /**
     * Update Springboard transfer line in the local storage.
     *
     * @param $transferLine
     * @param string $transferId
     * @param array $localTransferLines
     *
     * @throws Throwable
     */
    private function updateTransferLine($transferLine, string $transferId, array $localTransferLines)
    {
        // create magento transfer line object
        $magentoTransferLine = $this->transferLineFactory->create();
        // set data from springboard to the magento object
        $magentoTransferLine
            ->setLineId($transferLine->id)
            ->setTransferId($transferLine->transfer_id)
            ->setItemId($transferLine->item_id)
            ->setUnitCost($this->springboardHelper->formatFloat($transferLine, 'unit_cost'))
            ->setQtyShipped($this->springboardHelper->formatFloat($transferLine, 'qty_shipped'))
            ->setQtyReceived($this->springboardHelper->formatFloat($transferLine, 'qty_received'))
            ->setQtyLost($this->springboardHelper->formatFloat($transferLine, 'qty_lost'))
            ->setQtyDiscrepant($this->springboardHelper->formatFloat($transferLine, 'qty_discrepant'))
            ->setQtyOver($this->springboardHelper->formatFloat($transferLine, 'qty_over'))
            ->setQtyShort($this->springboardHelper->formatFloat($transferLine, 'qty_short'))
            ->setQtyRequested($this->springboardHelper->formatFloat($transferLine, 'qty_requested'));

        // get springboard transfer line data from the magento database
        $magentoTransferLineData = [];
        if (!empty($localTransferLines[$transferLine->id])) {
            $magentoTransferLineData = $this->springboardHelper->getData($localTransferLines[$transferLine->id]);
        }

        // compare magento and springboard invoice data
        $diff = array_diff_assoc($this->springboardHelper->getData($magentoTransferLine), $magentoTransferLineData);
        // need to update transfer line in the magento
        if (!empty($diff)) {
            // add parameters
            $magentoTransferLine->setSpringboardSyncDt(null);

            if (empty($magentoTransferLineData) && empty($magentoTransferLineData['id'])) {
                // create new transfer line
                $this->logs->info(
                    "Transfer Line with ID: {$transferLine->id} for transfer " .
                    "with id {$transferId} was created",
                    self::SPRINGBOARD_CRON_NAME
                );
            } else {
                // update transfer line
                $magentoTransferLine->setId($magentoTransferLineData['id']);
                $this->logs->info(
                    "Transfer Line with ID: {$transferLine->id} for transfer " .
                    "with id {$transferId} was updated",
                    self::SPRINGBOARD_CRON_NAME
                );
            }
            // create / update object
            $this->transferLineRepository->save($magentoTransferLine);
        }
    }

    /**
     * Get local transfers
     *
     * @return array
     * @throws Throwable
     */
    private function loadLocalTransfers(): array
    {
        // prepare filters
        $filters = [];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $transfers = [];
        foreach ($this->transferRepository->getList($searchCriteria)->getItems() as $item) {
            $transfers[$item->getTransferId()] = $item;
        }

        return $transfers;
    }

    /**
     * Get local transfer lines for specified list of transfers
     *
     * @param array $transfers
     *
     * @return array
     * @throws Throwable
     */
    private function loadLocalTransferLines(array $transfers): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder->setField('transfer_id')
                ->setConditionType('in')->setValue($transfers)->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $transferLines = [];
        foreach ($this->transferLineRepository->getList($searchCriteria)->getItems() as $item) {
            $transferLines[$item->getLineId()] = $item;
        }

        return $transferLines;
    }
}
