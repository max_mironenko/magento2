<?php declare(strict_types=1);

namespace C38\Springboard\Cron;

use C38\Springboard\Api\Data\InventoryInterfaceFactory;
use C38\Springboard\Api\InventoryRepositoryInterface;
use C38\Springboard\Helper\Data as SpringboardHelper;
use C38\Springboard\Helper\Logs;
use C38\Springboard\Service\SpringboardAPIClient;
use Magento\Catalog\Model\Product\Type as Simple;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use C38\CatalogInventory\Api\StockTableNameResolverInterface as StockTableNameResolver;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\DB\Select;
use Exception;
use Throwable;

/**
 * Class UpdateInventory
 * Update Inventory from the Springboard to the local storage
 */
class UpdateInventory
{
    const SPRINGBOARD_CRON_NAME = "UpdateInventory";

    /**
     * @var InventoryInterfaceFactory
     */
    private $inventoryFactory;

    /**
     * @var InventoryRepositoryInterface
     */
    private $inventoryRepository;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var SpringboardHelper
     */
    private $springboardHelper;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var StockTableNameResolver
     */
    private $stockTableNameResolver;

    /**
     * @var Logs
     */
    private $logs;

    /**
     * UpdateInventory constructor.
     *
     * @param InventoryInterfaceFactory $inventoryFactory
     * @param InventoryRepositoryInterface $inventoryRepository
     * @param ProductCollectionFactory $productCollectionFactory
     * @param SpringboardHelper $springboardHelper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param StockTableNameResolver $stockTableNameResolver
     * @param Logs $logs
     */
    public function __construct(
        InventoryInterfaceFactory $inventoryFactory,
        InventoryRepositoryInterface $inventoryRepository,
        ProductCollectionFactory $productCollectionFactory,
        SpringboardHelper $springboardHelper,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        StockTableNameResolver $stockTableNameResolver,
        Logs $logs
    ) {
        $this->inventoryFactory = $inventoryFactory;
        $this->inventoryRepository = $inventoryRepository;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->springboardHelper = $springboardHelper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->stockTableNameResolver = $stockTableNameResolver;
        $this->logs = $logs;
    }

    /**
     * Update Springboard Inventory
     */
    public function execute()
    {
        $this->updateInventory($this->springboardHelper->getInventorySyncDate());
    }

    /**
     * Full Update all inventory
     */
    public function fullUpdate()
    {
        $this->updateInventory('');
    }

    /**
     * Daily Springboard Inventory
     *
     * @param string $startDate
     */
    private function updateInventory(string $startDate)
    {
        // start
        $totalLines = 0;
        $successLines = 0;
        $this->logs->info("Update Inventory started...", self::SPRINGBOARD_CRON_NAME);

        // get first retail location
        $locationId = $this->springboardHelper->getOnlineLocationId();
        if ($locationId === null) {
            $this->logs->error("Springboard location id not found", '', self::SPRINGBOARD_CRON_NAME);
            return;
        }

        // load data before order update
        try {

            // get magento inventory ready for Springboard
            $magentoInventory = $this->getMagentoInventory();

            // get local springboard inventory
            $localSpringboardInventory = $this->getLocalSpringboardInventory();
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
            $this->logs->info("Update Inventory finished", self::SPRINGBOARD_CRON_NAME);
            return;
        }

        // load all inventory from Springboard
        try {
            // get springboard client
            $client = $this->springboardHelper->getClient();

            // load all pages with inventory from Springboard
            $allPages = null;
            for ($page = 1; $allPages >= $page || $allPages === null; $page++) {
                // load all inventory for current page
                $inventory = $client->getInventoryValues($locationId, $page, 500, $startDate);

                if (!is_object($inventory)) {
                    continue;
                }
                if ($allPages === null) {
                    $allPages = $inventory->pages;
                    $this->logs->info(
                        "From {$startDate} found {$inventory->total} inventory lines",
                        self::SPRINGBOARD_CRON_NAME
                    );
                }

                // update each ticket for current page
                foreach ($inventory->results as $inventoryLine) {
                    try {
                        $totalLines++;

                        // update inventory
                        $this->updateInventoryLine(
                            $client,
                            $inventoryLine,
                            $magentoInventory,
                            $localSpringboardInventory
                        );

                        $successLines++;
                    } catch (Throwable $ex) {
                        $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                    }
                }
            }
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        // finish
        $this->logs->info(
            "Successfully processed {$successLines} of {$totalLines} inventory lines",
            self::SPRINGBOARD_CRON_NAME
        );
        $this->logs->info("Update Inventory finished", self::SPRINGBOARD_CRON_NAME);
    }

    /**
     * Update Inventory Line
     *
     * @param SpringboardAPIClient $client
     * @param $invoiceLine
     * @param array $magentoInventory
     * @param array $localSpringboardInventory
     * @throws Exception
     */
    private function updateInventoryLine(
        SpringboardAPIClient $client,
        $invoiceLine,
        array $magentoInventory,
        array $localSpringboardInventory
    ) {
        $adjustmentQty = 0;

        // check magento item inventory
        if (!empty($magentoInventory[$invoiceLine->item_id])) {
            // get magento item inventory
            $magentoItemInventory = $magentoInventory[$invoiceLine->item_id];
            // get inventory adjustments (between magento and springboard)
            $adjustmentQty = (float)$magentoItemInventory['qty'] - (float)$invoiceLine->qty;

            // inventory need to be updated
            if ($adjustmentQty != 0) {
                // get product cost
                $productCost = $magentoItemInventory['cost'];
                if (!empty($magentoItemInventory['cost_after_discount'])) {
                    $productCost = $magentoItemInventory['cost_after_discount'];
                }
                // update data on the springboard
                $client->setOnlineStoreQty(
                    (string)$invoiceLine->item_id,
                    (string)$adjustmentQty,
                    (string)$productCost,
                    (string)$invoiceLine->location_id
                );
                $this->logs->info(
                    "Adjustment for product : {$invoiceLine->item_id} was sent to Springboard",
                    self::SPRINGBOARD_CRON_NAME
                );
            }
        }

        // set data from springboard to the magento object
        $springboardInventory = $this->inventoryFactory->create()
            ->setProductId($invoiceLine->item_id)
            ->setLocationId($invoiceLine->location_id)
            ->setQty(number_format($adjustmentQty, 4, '.', ''));

        // get springboard inventory data from the local springboard storage
        $localItemInventory = [];
        $index = $invoiceLine->item_id . ":" . $invoiceLine->location_id;
        if (!empty($localSpringboardInventory[$index])) {
            $localItemInventory = $this->springboardHelper->getData($localSpringboardInventory[$index]);
        }

        // compare magento and springboard inventory data
        $diff = array_diff_assoc($this->springboardHelper->getData($springboardInventory), $localItemInventory);
        // need to update inventory in the magento
        if (!empty($diff)) {
            $springboardInventory->setSpringboardSyncDt(null);
            if (empty($localItemInventory) && empty($localItemInventory['id'])) {
                // create new inventory line in the table
                $this->logs->info(
                    "Inventory line for product: {$invoiceLine->item_id} was created",
                    self::SPRINGBOARD_CRON_NAME
                );
            } else {
                // update inventory line
                $springboardInventory->setId($localItemInventory['id']);
                $this->logs->info(
                    "Inventory line for product: {$invoiceLine->item_id} was updated",
                    self::SPRINGBOARD_CRON_NAME
                );
            }
            // create / update object
            $this->inventoryRepository->save($springboardInventory);
        }
    }

    /**
     * Get Local Springboard inventory
     *
     * @return array
     * @throws Throwable
     */
    private function getLocalSpringboardInventory(): array
    {
        // prepare filters
        $filters = [];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // prepare list of items
        $inventory = [];
        foreach ($this->inventoryRepository->getList($searchCriteria)->getItems() as $item) {
            $inventory[$item['product_id'] . ":" . $item['location_id']] = $item;
        }

        return $inventory;
    }

    /**
     * Get Magento inventory
     *
     * @return array
     * @throws Throwable
     */
    private function getMagentoInventory(): array
    {
        // get product collection from magento ready for springboard integration
        $productCollection = $this->productCollectionFactory->create()
            ->addAttributeToFilter('cost', ['gt' => -1])
            ->addAttributeToFilter(
                'cost_after_discount',
                [['null' => true], ['gt' => -1]],
                'left'
            )
            ->addAttributeToFilter('status', ['in' => 1])
            ->addAttributeToFilter('type_id', ['eq' => Simple::TYPE_SIMPLE])
            ->addFieldToFilter('springboard_id', ['gt' => 0]);

        // add stock information
        $productCollection
            ->getSelect()
            ->joinLeft(
                ['csi' => $this->stockTableNameResolver->resolve()],
                'e.sku = csi.sku',
                ['csi.quantity']
            );

        // prepare output data
        $select = $productCollection->getSelect();
        $select->reset(Select::COLUMNS);
        $select->columns([
            'entity_id' => 'e.entity_id',
            'springboard_id' => 'e.springboard_id',
            'qty' => 'csi.quantity',
            'cost' => 'at_cost.value',
            'cost_after_discount' => 'at_cost_after_discount.value'
        ]);

        $magentoInventory = [];
        // prepare product collection
        foreach ($productCollection->getData() as $item) {
            $magentoInventory[$item['springboard_id']] = $item;
        }

        // return result
        return $magentoInventory;
    }
}
