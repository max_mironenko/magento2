<?php declare(strict_types=1);

namespace C38\Springboard\Cron;

use C38\Springboard\Api\Data\SalesPlanInterfaceFactory;
use C38\Springboard\Api\SalesPlanRepositoryInterface;
use C38\Springboard\Helper\Data as SpringboardHelper;
use C38\Springboard\Helper\Logs;
use C38\Springboard\Model\ResourceModel\SalesPlan\CollectionFactory as SalesPlanCollectionFactory;
use Throwable;

/**
 * Class ImportSalesPlans
 * Import SalesPlans from the Springboard to the local storage
 */
class ImportSalesPlans
{
    const SPRINGBOARD_CRON_NAME = "ImportSalesPlans";

    /**
     * @var SalesPlanInterfaceFactory
     */
    private $salesPlanFactory;

    /**
     * @var SalesPlanRepositoryInterface
     */
    private $salesPlanRepository;

    /**
     * @var SalesPlanCollectionFactory
     */
    private $salesPlanCollectionFactory;

    /**
     * @var SpringboardHelper
     */
    private $springboardHelper;

    /**
     * @var Logs
     */
    private $logs;

    /**
     * Class Constructor
     *
     * @param SalesPlanInterfaceFactory $salesPlanFactory
     * @param SalesPlanRepositoryInterface $salesPlanRepository
     * @param SalesPlanCollectionFactory $salesPlanCollectionFactory
     * @param SpringboardHelper $springboardHelper
     * @param Logs $logs
     */
    public function __construct(
        SalesPlanInterfaceFactory $salesPlanFactory,
        SalesPlanRepositoryInterface $salesPlanRepository,
        SalesPlanCollectionFactory $salesPlanCollectionFactory,
        SpringboardHelper $springboardHelper,
        Logs $logs
    ) {
        $this->salesPlanFactory = $salesPlanFactory;
        $this->salesPlanRepository = $salesPlanRepository;
        $this->salesPlanCollectionFactory = $salesPlanCollectionFactory;
        $this->springboardHelper = $springboardHelper;
        $this->logs = $logs;
    }

    /**
     * Import Springboard Sales Plan
     */
    public function execute()
    {
        $total = 0;
        $created = 0;
        $deleted = 0;
        $this->logs->info("Import Sales Plans started...", self::SPRINGBOARD_CRON_NAME);

        try {
            // get collection of magento sales plans id
            $magentoSalesPlans = $this->salesPlanCollectionFactory->create()->getColumnValues('public_id');

            // list of all Springboard Sales Plans
            $springboardSalesPlans = [];

            // get SpringBoard Client
            $client = $this->springboardHelper->getClient();
            // load all pages with Sales Plans from Springboard
            $allPages = null;
            for ($page = 1; $allPages >= $page || $allPages === null; $page++) {
                // load page with sales plans
                $salesPlans = $client->getSalesPlans($page);
                if (!is_object($salesPlans)) {
                    continue;
                }
                if ($allPages === null) {
                    $allPages = $salesPlans->pages;
                    $this->logs->info("Found {$salesPlans->total} sales plans", self::SPRINGBOARD_CRON_NAME);
                }

                // load Sales Plans for current page
                foreach ($salesPlans->results as $salesPlan) {
                    try {
                        $total++;
                        // get sales plan ID
                        $springboardSalesPlans[] = $salesPlan->public_id;
                        // if sales plan already in the database
                        if (in_array($salesPlan->public_id, $magentoSalesPlans)) {
                            continue;
                        }

                        // create new sales plan
                        $salesPlanNew = $this->salesPlanFactory->create();
                        $salesPlanNew
                            ->setPublicId($salesPlan->public_id)
                            ->setDate(strtotime($salesPlan->date))
                            ->setLocationId($salesPlan->location_id)
                            ->setAmount($salesPlan->amount);
                        $this->salesPlanRepository->save($salesPlanNew);
                        $this->logs->info(
                            "Sales Plan with ID: {$salesPlan->public_id} was created",
                            self::SPRINGBOARD_CRON_NAME
                        );
                        $created++;
                    } catch (Throwable $ex) {
                        $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
                    }
                }
            }

            // remove unused sales plans from database
            $removedSalesPlans = array_diff($magentoSalesPlans, $springboardSalesPlans);
            foreach ($removedSalesPlans as $salesPlanPublicId) {
                $salesPlanForDelete = $this->salesPlanRepository->loadByPublicId($salesPlanPublicId);
                $this->salesPlanRepository->delete($salesPlanForDelete);
                $this->logs->info(
                    "Sales Plan with ID: {$salesPlanForDelete['public_id']} was deleted",
                    self::SPRINGBOARD_CRON_NAME
                );
                $deleted++;
            }
        } catch (Throwable $ex) {
            $this->logs->error($ex->getMessage(), $ex->getTraceAsString(), self::SPRINGBOARD_CRON_NAME);
        }

        $this->logs->info("Successfully processed {$total} sales plans. Created {$created} / " .
            "Deleted {$deleted}", self::SPRINGBOARD_CRON_NAME);
        $this->logs->info("Import Sales Plans finished", self::SPRINGBOARD_CRON_NAME);
    }
}
