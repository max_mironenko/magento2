<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\AddressInterface;
use C38\Springboard\Api\Data\AddressSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface AddressRepositoryInterface
 */
interface AddressRepositoryInterface
{
    /**
     * Save Address
     *
     * @param AddressInterface $address
     *
     * @return AddressInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(AddressInterface $address): AddressInterface;

    /**
     * Get Address by id.
     *
     * @param $addressId
     *
     * @return AddressInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($addressId): AddressInterface;

    /**
     * Find Address by id.
     *
     * @param $addressId
     * @return AddressInterface|null
     */
    public function findById($addressId): ?AddressInterface;

    /**
     * Find Address by Springboard Id.
     *
     * @param $addressId
     * @return AddressInterface
     */
    public function loadByAddressId($addressId): AddressInterface;

    /**
     * Retrieve Address matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|AddressSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Address
     *
     * @param AddressInterface $address
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(AddressInterface $address): bool;

    /**
     * Delete Address by ID.
     *
     * @param $addressId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($addressId): bool;
}
