<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\SalesPlanInterface;
use C38\Springboard\Api\Data\SalesPlanSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface SalesPlanRepositoryInterface
 */
interface SalesPlanRepositoryInterface
{
    /**
     * Save SalesPlan
     *
     * @param SalesPlanInterface $salesPlan
     *
     * @return SalesPlanInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(SalesPlanInterface $salesPlan): SalesPlanInterface;

    /**
     * Get SalesPlan by id.
     *
     * @param int $salesPlanId
     *
     * @return SalesPlanInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById(int $salesPlanId): SalesPlanInterface;

    /**
     * Find SalesPlan by id.
     *
     * @param int $salesPlanId
     * @return SalesPlanInterface|null
     */
    public function findById(int $salesPlanId): ?SalesPlanInterface;

    /**
     * Find SalesPlan by public_id.
     *
     * @param $publicId
     * @return SalesPlanInterface
     */
    public function loadByPublicId($publicId): SalesPlanInterface;

    /**
     * Retrieve SalesPlan matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|SalesPlanSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete SalesPlan
     *
     * @param SalesPlanInterface $salesPlan
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(SalesPlanInterface $salesPlan): bool;

    /**
     * Delete SalesPlan by ID.
     *
     * @param int $salesPlanId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById(int $salesPlanId): bool;
}
