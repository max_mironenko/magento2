<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\InventoryInterface;
use C38\Springboard\Api\Data\InventorySearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface InventoryRepositoryInterface
 */
interface InventoryRepositoryInterface
{
    /**
     * Save Inventory
     *
     * @param InventoryInterface $inventory
     *
     * @return InventoryInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(InventoryInterface $inventory): InventoryInterface;

    /**
     * Get Inventory by id.
     *
     * @param $inventoryId
     *
     * @return InventoryInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($inventoryId): InventoryInterface;

    /**
     * Retrieve Inventory matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|InventorySearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;
}
