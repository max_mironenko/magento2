<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\TransferLineSearchResultsInterface;
use C38\Springboard\Api\Data\TransferLineInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface TransferLineRepositoryInterface
 */
interface TransferLineRepositoryInterface
{
    /**
     * Save Transfer Line
     *
     * @param TransferLineInterface $transferLine
     *
     * @return TransferLineInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(TransferLineInterface $transferLine): TransferLineInterface;

    /**
     * Get Transfer line by id.
     *
     * @param $transferLineId
     *
     * @return TransferLineInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($transferLineId): TransferLineInterface;

    /**
     * Find Transfer Line by id.
     *
     * @param $transferLineId
     *
     * @return TransferLineInterface|null
     */
    public function findById($transferLineId): ?TransferLineInterface;

    /**
     * Find Transfer by Springboard Transfer line Id.
     *
     * @param $lineId
     *
     * @return TransferLineInterface
     */
    public function loadByLineId($lineId): TransferLineInterface;

    /**
     * Retrieve Transfer lines matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|TransferLineSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Transfer
     *
     * @param TransferLineInterface $transferLine
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(TransferLineInterface $transferLine): bool;

    /**
     * Delete Transfer line by ID.
     *
     * @param $transferLineId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($transferLineId): bool;
}
