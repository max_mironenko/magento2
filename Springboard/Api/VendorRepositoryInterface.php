<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\VendorInterface;
use C38\Springboard\Api\Data\VendorSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface VendorRepositoryInterface
 */
interface VendorRepositoryInterface
{
    /**
     * Save Vendor
     *
     * @param VendorInterface $vendor
     *
     * @return VendorInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(VendorInterface $vendor): VendorInterface;

    /**
     * Get Vendor by id.
     *
     * @param int $vendorId
     *
     * @return VendorInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById(int $vendorId): VendorInterface;

    /**
     * Find Vendor by id.
     *
     * @param int $vendorId
     * @return VendorInterface|null
     */
    public function findById(int $vendorId): ?VendorInterface;

    /**
     * Find Vendor by Vendor Id.
     *
     * @param string $vendorId
     * @return VendorInterface
     */
    public function loadByVendorId(string $vendorId): VendorInterface;

    /**
     * Retrieve Vendor matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|VendorSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Vendor
     *
     * @param VendorInterface $vendor
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(VendorInterface $vendor): bool;

    /**
     * Delete Vendor by ID.
     *
     * @param int $vendorId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById(int $vendorId): bool;
}
