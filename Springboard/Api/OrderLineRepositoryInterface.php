<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\OrderLineInterface;
use C38\Springboard\Api\Data\OrderLineSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface OrderLineRepositoryInterface
 */
interface OrderLineRepositoryInterface
{
    /**
     * Save OrderLine
     *
     * @param OrderLineInterface $orderLine
     *
     * @return OrderLineInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(OrderLineInterface $orderLine): OrderLineInterface;

    /**
     * Get OrderLine by id.
     *
     * @param $orderLineId
     *
     * @return OrderLineInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($orderLineId): OrderLineInterface;

    /**
     * Find OrderLine by id.
     *
     * @param $orderLineId
     * @return OrderLineInterface|null
     */
    public function findById($orderLineId): ?OrderLineInterface;

    /**
     * Find Order by Springboard Order Id.
     *
     * @param $lineId
     * @return OrderLineInterface
     */
    public function loadByLineId($lineId): OrderLineInterface;

    /**
     * Retrieve OrderLine matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|OrderLineSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete OrderLine
     *
     * @param OrderLineInterface $orderLine
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(OrderLineInterface $orderLine): bool;

    /**
     * Delete OrderLine by ID.
     *
     * @param $orderLineId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($orderLineId): bool;
}
