<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\CustomerInterface;
use C38\Springboard\Api\Data\CustomerSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface CustomerRepositoryInterface
 */
interface CustomerRepositoryInterface
{
    /**
     * Save Customer
     *
     * @param CustomerInterface $customer
     *
     * @return CustomerInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(CustomerInterface $customer): CustomerInterface;

    /**
     * Get Customer by id.
     *
     * @param int $customerId
     *
     * @return CustomerInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById(int $customerId): CustomerInterface;

    /**
     * Find Customer by id.
     *
     * @param int $customerId
     * @return CustomerInterface|null
     */
    public function findById(int $customerId): ?CustomerInterface;

    /**
     * Find Springboard Customer by customer_id.
     *
     * @param int $customerId
     * @return CustomerInterface
     */
    public function loadByCustomerId(int $customerId): CustomerInterface;

    /**
     * Retrieve Customer matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|CustomerSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Customer
     *
     * @param CustomerInterface $customer
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(CustomerInterface $customer): bool;

    /**
     * Delete Customer by ID.
     *
     * @param int $customerId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById(int $customerId): bool;

    /**
     * Update Springboard customers with Magento data
     */
    public function updateCustomers();
}
