<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\TransferSearchResultsInterface;
use C38\Springboard\Api\Data\TransferInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface TransferRepositoryInterface
 */
interface TransferRepositoryInterface
{
    /**
     * Save Transfer
     *
     * @param TransferInterface $transfer
     *
     * @return TransferInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(TransferInterface $transfer): TransferInterface;

    /**
     * Get Transfer by id.
     *
     * @param $transferId
     *
     * @return TransferInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($transferId): TransferInterface;

    /**
     * Find Transfer Line by id.
     *
     * @param $transferId
     * @return TransferInterface|null
     */
    public function findById($transferId): ?TransferInterface;

    /**
     * Find Transfer by Springboard Transfer Id.
     *
     * @param $transferId
     * @return TransferInterface
     */
    public function loadByTransferId($transferId): TransferInterface;

    /**
     * Retrieve Transfer matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|TransferSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Transfer
     *
     * @param TransferInterface $transfer
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(TransferInterface $transfer): bool;

    /**
     * Delete Transfer by ID.
     *
     * @param $transferId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($transferId): bool;
}
