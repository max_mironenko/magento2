<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\TicketLineInterface;
use C38\Springboard\Api\Data\TicketLineSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface TicketLineRepositoryInterface
 */
interface TicketLineRepositoryInterface
{
    /**
     * Save Ticket Line
     *
     * @param TicketLineInterface $ticketLine
     *
     * @return TicketLineInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(TicketLineInterface $ticketLine): TicketLineInterface;

    /**
     * Get Ticket Line by id.
     *
     * @param $ticketLineId
     *
     * @return TicketLineInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($ticketLineId): TicketLineInterface;

    /**
     * Find Ticket Line by id.
     *
     * @param $ticketLineId
     *
     * @return TicketLineInterface|null
     */
    public function findById($ticketLineId): ?TicketLineInterface;

    /**
     * Retrieve Ticket Line matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|TicketLineSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Ticket Line
     *
     * @param TicketLineInterface $ticketLine
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(TicketLineInterface $ticketLine): bool;

    /**
     * Delete Ticket Line by ID.
     *
     * @param $ticketLineId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($ticketLineId): bool;
}
