<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\LoggerInterface;

/**
 * Interface LoggerRepositoryInterface
 */
interface LoggerRepositoryInterface
{
    /**
     * Save Log Message
     *
     * @param LoggerInterface $logger
     * @return LoggerInterface
     */
    public function save(LoggerInterface $logger): LoggerInterface;
}
