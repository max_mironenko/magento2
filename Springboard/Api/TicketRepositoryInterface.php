<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\TicketSearchResultsInterface;
use C38\Springboard\Api\Data\TicketInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface TicketRepositoryInterface
 */
interface TicketRepositoryInterface
{
    /**
     * Save Ticket
     *
     * @param TicketInterface $ticket
     *
     * @return TicketInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(TicketInterface $ticket): TicketInterface;

    /**
     * Get Ticket by id.
     *
     * @param $ticketId
     *
     * @return TicketInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($ticketId): TicketInterface;

    /**
     * Find Ticket Line by id.
     *
     * @param $ticketId
     *
     * @return TicketInterface|null
     */
    public function findById($ticketId): ?TicketInterface;

    /**
     * Find Ticket by Springboard Ticket Id.
     *
     * @param $ticketId
     *
     * @return TicketInterface
     */
    public function loadByTicketId($ticketId): TicketInterface;

    /**
     * Retrieve Ticket matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|TicketSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Ticket
     *
     * @param TicketInterface $ticket
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(TicketInterface $ticket): bool;

    /**
     * Delete Ticket by ID.
     *
     * @param $ticketId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($ticketId): bool;
}
