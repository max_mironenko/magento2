<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\MagentoCustomerInterface;
use C38\Springboard\Api\Data\MagentoCustomerSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface MagentoCustomerRepositoryInterface
 */
interface MagentoCustomerRepositoryInterface
{
    /**
     * Save Customer
     *
     * @param MagentoCustomerInterface $customer
     *
     * @return MagentoCustomerInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(MagentoCustomerInterface $customer): MagentoCustomerInterface;

    /**
     * Retrieve Customer matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|MagentoCustomerSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;
}
