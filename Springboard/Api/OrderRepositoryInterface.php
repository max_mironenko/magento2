<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\OrderInterface;
use C38\Springboard\Api\Data\OrderSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface OrderRepositoryInterface
 */
interface OrderRepositoryInterface
{
    /**
     * Save Order
     *
     * @param OrderInterface $order
     *
     * @return OrderInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(OrderInterface $order): OrderInterface;

    /**
     * Get Order by id.
     *
     * @param $orderId
     *
     * @return OrderInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($orderId): OrderInterface;

    /**
     * Find Order by id.
     *
     * @param $orderId
     * @return OrderInterface|null
     */
    public function findById($orderId): ?OrderInterface;

    /**
     * Find Order by Springboard Order Id.
     *
     * @param $orderId
     * @return OrderInterface
     */
    public function loadByOrderId($orderId): OrderInterface;

    /**
     * Retrieve Order matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|OrderSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Order
     *
     * @param OrderInterface $order
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(OrderInterface $order): bool;

    /**
     * Delete Order by ID.
     *
     * @param $orderId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($orderId): bool;
}
