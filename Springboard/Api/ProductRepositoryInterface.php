<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\ProductInterface;
use C38\Springboard\Api\Data\ProductSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface ProductRepositoryInterface
 */
interface ProductRepositoryInterface
{
    /**
     * Save Product
     *
     * @param ProductInterface $product
     *
     * @return ProductInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(ProductInterface $product): ProductInterface;

    /**
     * Get Product by id.
     *
     * @param $productId
     *
     * @return ProductInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($productId): ProductInterface;

    /**
     * Find Product by id.
     *
     * @param $productId
     * @return ProductInterface|null
     */
    public function findById($productId): ?ProductInterface;

    /**
     * Find Product by Springboard Product Id.
     *
     * @param $productId
     * @return ProductInterface
     */
    public function loadByProductId($productId): ProductInterface;

    /**
     * Retrieve Products matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|ProductSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Product
     *
     * @param ProductInterface $product
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(ProductInterface $product): bool;

    /**
     * Delete Product by ID.
     *
     * @param $productId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($productId): bool;
}
