<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\InvoiceInterface;
use C38\Springboard\Api\Data\InvoiceSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface InvoiceRepositoryInterface
 */
interface InvoiceRepositoryInterface
{
    /**
     * Save Invoice
     *
     * @param InvoiceInterface $invoice
     *
     * @return InvoiceInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(InvoiceInterface $invoice): InvoiceInterface;

    /**
     * Get Invoice by id.
     *
     * @param $invoiceId
     *
     * @return InvoiceInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($invoiceId): InvoiceInterface;

    /**
     * Find Invoice by id.
     *
     * @param $invoiceId
     * @return InvoiceInterface|null
     */
    public function findById($invoiceId): ?InvoiceInterface;

    /**
     * Find Invoice by Invoice Id.
     *
     * @param $invoiceId
     * @return InvoiceInterface
     */
    public function loadByInvoiceId($invoiceId): InvoiceInterface;

    /**
     * Retrieve Invoice matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|InvoiceSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Invoice
     *
     * @param InvoiceInterface $invoice
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(InvoiceInterface $invoice): bool;

    /**
     * Delete Invoice by ID.
     *
     * @param $invoiceId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($invoiceId): bool;
}
