<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\TicketLineAdjustmentsInterface;
use C38\Springboard\Api\Data\TicketLineAdjustmentsSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface TicketLineAdjustmentsRepositoryInterface
 */
interface TicketLineAdjustmentsRepositoryInterface
{
    /**
     * Save Ticket Line Adjustment
     *
     * @param TicketLineAdjustmentsInterface $ticketLineAdjustments
     *
     * @return TicketLineAdjustmentsInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(TicketLineAdjustmentsInterface $ticketLineAdjustments): TicketLineAdjustmentsInterface;

    /**
     * Get Ticket Line Adjustment by id.
     *
     * @param $ticketLineAdjustmentsId
     *
     * @return TicketLineAdjustmentsInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($ticketLineAdjustmentsId): TicketLineAdjustmentsInterface;

    /**
     * Retrieve Ticket Line Adjustment matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|TicketLineAdjustmentsSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Ticket Line Adjustment
     *
     * @param TicketLineAdjustmentsInterface $ticketLineAdjustments
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(TicketLineAdjustmentsInterface $ticketLineAdjustments): bool;

    /**
     * Delete Ticket Line Adjustment by ID.
     *
     * @param $ticketLineAdjustmentsId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($ticketLineAdjustmentsId): bool;
}
