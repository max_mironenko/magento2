<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\InvoiceLineInterface;
use C38\Springboard\Api\Data\InvoiceLineSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface InvoiceLineRepositoryInterface
 */
interface InvoiceLineRepositoryInterface
{
    /**
     * Save InvoiceLine
     *
     * @param InvoiceLineInterface $invoiceLine
     *
     * @return InvoiceLineInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(InvoiceLineInterface $invoiceLine): InvoiceLineInterface;

    /**
     * Get InvoiceLine by id.
     *
     * @param $invoiceLineId
     *
     * @return InvoiceLineInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($invoiceLineId): InvoiceLineInterface;

    /**
     * Find InvoiceLine by id.
     *
     * @param $invoiceLineId
     * @return InvoiceLineInterface|null
     */
    public function findById($invoiceLineId): ?InvoiceLineInterface;

    /**
     * Find InvoiceLine by Line Id.
     *
     * @param $lineId
     * @return InvoiceLineInterface
     */
    public function loadByLineId($lineId): InvoiceLineInterface;

    /**
     * Retrieve InvoiceLine matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|InvoiceLineSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete InvoiceLine
     *
     * @param InvoiceLineInterface $invoiceLine
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(InvoiceLineInterface $invoiceLine): bool;

    /**
     * Delete InvoiceLine by ID.
     *
     * @param $invoiceLineId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($invoiceLineId): bool;
}
