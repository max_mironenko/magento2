<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface VendorInterface
 */
interface VendorInterface
{

    // table name
    const TABLE_NAME = 'c38_springboard_vendors';

    // table columns
    const ID = 'id';
    const VENDOR_ID = 'vendor_id';
    const DESIGNER_ID = 'designer_id';
    const NAME = 'name';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return VendorInterface
     */
    public function setId($value): VendorInterface;

    /**
     * Get VendorId value
     *
     * @return string
     */
    public function getVendorId(): string;

    /**
     * Set VendorId value
     *
     * @param $value
     * @return VendorInterface
     */
    public function setVendorId($value): VendorInterface;

    /**
     * Get DesignerId value
     *
     * @return string|null
     */
    public function getDesignerId(): ?string;

    /**
     * Set DesignerId value
     *
     * @param $value
     * @return VendorInterface
     */
    public function setDesignerId($value): VendorInterface;

    /**
     * Get Name value
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Set Name value
     *
     * @param $value
     * @return VendorInterface
     */
    public function setName($value): VendorInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return VendorInterface
     */
    public function setSpringboardSyncDt($value): VendorInterface;
}
