<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface MagentoCustomerSearchResultsInterface
 */
interface MagentoCustomerSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Customer list.
     *
     * @return MagentoCustomerInterface[]
     */
    public function getItems(): array;

    /**
     * Set Customer list.
     *
     * @param MagentoCustomerInterface[] $items
     * @return MagentoCustomerSearchResultsInterface
     */
    public function setItems(array $items): MagentoCustomerSearchResultsInterface;
}
