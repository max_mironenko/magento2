<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface ProductSearchResultsInterface
 */
interface ProductSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Products list.
     *
     * @return ProductInterface[]
     */
    public function getItems(): array;

    /**
     * Set Products list.
     *
     * @param ProductInterface[] $items
     * @return ProductSearchResultsInterface
     */
    public function setItems(array $items): ProductSearchResultsInterface;
}
