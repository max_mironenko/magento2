<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface InventoryInterface
 */
interface InventoryInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_inventory';

    // table columns
    const ID = 'id';
    const PRODUCT_ID = 'product_id';
    const LOCATION_ID = 'location_id';
    const QTY = 'qty';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return InventoryInterface
     */
    public function setId($value): InventoryInterface;

    /**
     * Get ProductId value
     *
     * @return string
     */
    public function getProductId(): string;

    /**
     * Set ProductId value
     *
     * @param $value
     * @return InventoryInterface
     */
    public function setProductId($value): InventoryInterface;

    /**
     * Get LocationId value
     *
     * @return string
     */
    public function getLocationId(): string;

    /**
     * Set LocationId value
     *
     * @param $value
     * @return InventoryInterface
     */
    public function setLocationId($value): InventoryInterface;

    /**
     * Get QTY value
     *
     * @return string
     */
    public function getQty(): string;

    /**
     * Set QTY value
     *
     * @param $value
     * @return InventoryInterface
     */
    public function setQty($value): InventoryInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return InventoryInterface
     */
    public function setSpringboardSyncDt($value): InventoryInterface;
}
