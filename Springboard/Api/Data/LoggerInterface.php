<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface LoggerInterface
 */
interface LoggerInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_logs';

    // table columns
    const ID = 'id';
    const TYPE = 'type';
    const MESSAGE = 'message';
    const STACK = 'stack';
    const CRON_NAME = 'cron_name';
    const CREATED_AT = 'created_at';

    /**
     * Get ID
     *
     * @return string | null
     */
    public function getId(): ?string;

    /**
     * Set ID
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setId($value): LoggerInterface;

    /**
     * Get Message Type
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Set Message Type
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setType($value): LoggerInterface;

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage(): string;

    /**
     * Set Message
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setMessage($value): LoggerInterface;

    /**
     * Get Error stack trace
     *
     * @return string
     */
    public function getStack(): string;

    /**
     * Set Error stack trace
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setStack($value): LoggerInterface;

    /**
     * Get Cron Name
     *
     * @return string
     */
    public function getCronName(): string;

    /**
     * Set Cron Name
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setCronName($value): LoggerInterface;

    /**
     * Get Created At
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Set Created At
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setCreatedAt($value): LoggerInterface;
}
