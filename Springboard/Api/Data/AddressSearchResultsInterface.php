<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface AddressSearchResultsInterface
 */
interface AddressSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Address list.
     *
     * @return AddressInterface[]
     */
    public function getItems(): array;

    /**
     * Set Address list.
     *
     * @param AddressInterface[] $items
     * @return AddressSearchResultsInterface
     */
    public function setItems(array $items): AddressSearchResultsInterface;
}
