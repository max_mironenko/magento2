<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface TicketInterface
 */
interface TicketInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_tickets';

    // table columns
    const ID = 'id';
    const TICKET_ID = 'ticket_id';
    const TYPE = 'type';
    const STATUS = 'status';
    const CREATED_DATE = 'created_date';
    const CUSTOMER_ID = 'customer_id';
    const SALES_REP = 'sales_rep';
    const STATION_ID = 'station_id';
    const SOURCE_LOCATION_ID = 'source_location_id';
    const TOTAL_ITEM_QTY = 'total_item_qty';
    const TOTAL = 'total';
    const TOTAL_PAID = 'total_paid';
    const ORIGINAL_SUBTOTAL = 'original_subtotal';
    const TOTAL_DISCOUNTS = 'total_discounts';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';
    const TJ_RETRIES = 'tj_retries';
    const UPDATED_AT = 'updated_at';
    const TJ_SALESTAX_SYNC_DATE = 'tj_salestax_sync_date';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setId($value): TicketInterface;

    /**
     * Get TicketId value
     *
     * @return string
     */
    public function getTicketId(): string;

    /**
     * Set TicketId value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTicketId($value): TicketInterface;

    /**
     * Get Type value
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Set Type value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setType($value): TicketInterface;

    /**
     * Get Status value
     *
     * @return string
     */
    public function getStatus(): string;

    /**
     * Set Status value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setStatus($value): TicketInterface;

    /**
     * Get CreatedDate value
     *
     * @return string
     */
    public function getCreatedDate(): string;

    /**
     * Set CreatedDate value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setCreatedDate($value): TicketInterface;

    /**
     * Get CustomerId value
     *
     * @return string
     */
    public function getCustomerId(): string;

    /**
     * Set CustomerId value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setCustomerId($value): TicketInterface;

    /**
     * Get SalesRep value
     *
     * @return string
     */
    public function getSalesRep(): string;

    /**
     * Set SalesRep value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setSalesRep($value): TicketInterface;

    /**
     * Get StationId value
     *
     * @return string
     */
    public function getStationId(): string;

    /**
     * Set StationId value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setStationId($value): TicketInterface;

    /**
     * Get SourceLocationId value
     *
     * @return string
     */
    public function getSourceLocationId(): string;

    /**
     * Set SourceLocationId value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setSourceLocationId($value): TicketInterface;

    /**
     * Get TotalItemQty value
     *
     * @return float
     */
    public function getTotalItemQty(): float;

    /**
     * Set TotalItemQty value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTotalItemQty($value): TicketInterface;

    /**
     * Get Total value
     *
     * @return float
     */
    public function getTotal(): float;

    /**
     * Set Total value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTotal($value): TicketInterface;

    /**
     * Get TotalPaid value
     *
     * @return float
     */
    public function getTotalPaid(): float;

    /**
     * Set TotalPaid value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTotalPaid($value): TicketInterface;

    /**
     * Get OriginalSubtotal value
     *
     * @return float
     */
    public function getOriginalSubtotal(): float;

    /**
     * Set OriginalSubtotal value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setOriginalSubtotal($value): TicketInterface;

    /**
     * Get TotalDiscounts value
     *
     * @return float
     */
    public function getTotalDiscounts(): float;

    /**
     * Set TotalDiscounts value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTotalDiscounts($value): TicketInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setSpringboardSyncDt($value): TicketInterface;

    /**
     * Get TjRetries value
     *
     * @return string
     */
    public function getTjRetries(): string;

    /**
     * Set TjRetries value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTjRetries($value): TicketInterface;

    /**
     * Get UpdatedAt value
     *
     * @return string
     */
    public function getUpdatedAt(): string;

    /**
     * Set UpdatedAt value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setUpdatedAt($value): TicketInterface;

    /**
     * Get TjSalestaxSyncDate value
     *
     * @return string
     */
    public function getTjSalestaxSyncDate(): string;

    /**
     * Set TjSalestaxSyncDate value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTjSalestaxSyncDate($value): TicketInterface;
}
