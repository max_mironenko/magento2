<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface VendorSearchResultsInterface
 */
interface VendorSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Vendor list.
     *
     * @return VendorInterface[]
     */
    public function getItems(): array;

    /**
     * Set Vendor list.
     *
     * @param VendorInterface[] $items
     * @return VendorSearchResultsInterface
     */
    public function setItems(array $items): VendorSearchResultsInterface;
}
