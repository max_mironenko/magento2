<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface InvoiceLineSearchResultsInterface
 */
interface InvoiceLineSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get InvoiceLine list.
     *
     * @return InvoiceLineInterface[]
     */
    public function getItems(): array;

    /**
     * Set InvoiceLine list.
     *
     * @param InvoiceLineInterface[] $items
     * @return InvoiceLineSearchResultsInterface
     */
    public function setItems(array $items): InvoiceLineSearchResultsInterface;
}
