<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface TransferLineSearchResultsInterface
 */
interface TransferLineSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Transfer line list.
     *
     * @return TransferLineInterface[]
     */
    public function getItems(): array;

    /**
     * Set Transfer line list.
     *
     * @param TransferLineInterface[] $items
     * @return TransferLineSearchResultsInterface
     */
    public function setItems(array $items): TransferLineSearchResultsInterface;
}
