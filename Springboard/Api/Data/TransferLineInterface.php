<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface TransferLineInterface
 */
interface TransferLineInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_transfer_lines';

    // table columns
    const ID = 'id';
    const LINE_ID = 'line_id';
    const TRANSFER_ID = 'transfer_id';
    const ITEM_ID = 'item_id';
    const UNIT_COST = 'unit_cost';
    const QTY_SHIPPED = 'qty_shipped';
    const QTY_RECEIVED = 'qty_received';
    const QTY_LOST = 'qty_lost';
    const QTY_DISCREPANT = 'qty_discrepant';
    const QTY_OVER = 'qty_over';
    const QTY_SHORT = 'qty_short';
    const QTY_REQUESTED = 'qty_requested';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';
    const MANIFEST_ID = 'manifest_id';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setId($value): TransferLineInterface;

    /**
     * Get LineId value
     *
     * @return string
     */
    public function getLineId(): string;

    /**
     * Set LineId value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setLineId($value): TransferLineInterface;

    /**
     * Get TransferId value
     *
     * @return string
     */
    public function getTransferId(): string;

    /**
     * Set TransferId value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setTransferId($value): TransferLineInterface;

    /**
     * Get ItemId value
     *
     * @return string
     */
    public function getItemId(): string;

    /**
     * Set ItemId value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setItemId($value): TransferLineInterface;

    /**
     * Get UnitCost value
     *
     * @return float
     */
    public function getUnitCost(): float;

    /**
     * Set UnitCost value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setUnitCost($value): TransferLineInterface;

    /**
     * Get QtyShipped value
     *
     * @return string
     */
    public function getQtyShipped(): string;

    /**
     * Set QtyShipped value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyShipped($value): TransferLineInterface;

    /**
     * Get QtyReceived value
     *
     * @return float
     */
    public function getQtyReceived(): float;

    /**
     * Set QtyReceived value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyReceived($value): TransferLineInterface;

    /**
     * Get QtyLost value
     *
     * @return float
     */
    public function getQtyLost(): float;

    /**
     * Set QtyLost value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyLost($value): TransferLineInterface;

    /**
     * Get QtyDiscrepant value
     *
     * @return float
     */
    public function getQtyDiscrepant(): float;

    /**
     * Set QtyDiscrepant value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyDiscrepant($value): TransferLineInterface;

    /**
     * Get QtyOver value
     *
     * @return float
     */
    public function getQtyOver(): float;

    /**
     * Set QtyOver value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyOver($value): TransferLineInterface;

    /**
     * Get QtyShort value
     *
     * @return float
     */
    public function getQtyShort(): float;

    /**
     * Set QtyShort value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyShort($value): TransferLineInterface;

    /**
     * Get QtyRequested value
     *
     * @return float
     */
    public function getQtyRequested(): float;

    /**
     * Set QtyRequested value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyRequested($value): TransferLineInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setSpringboardSyncDt($value): TransferLineInterface;

    /**
     * Get ManifestId value
     *
     * @return string
     */
    public function getManifestId(): string;

    /**
     * Set ManifestId value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setManifestId($value): TransferLineInterface;
}
