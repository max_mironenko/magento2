<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface ProductInterface
 */
interface ProductInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_products';

    // table columns
    const ID = 'id';
    const PRODUCT_ID = 'product_id';
    const SKU = 'sku';
    const CONFIGURABLE_SKU = 'configurable_sku';
    const STYLE_NAME = 'style_name';
    const UPC = 'upc';
    const DESCRIPTION = 'description';
    const LONG_DESCRIPTION = 'long_description';
    const ACTIVE = 'active';
    const COST = 'cost';
    const PRICE = 'price';
    const ORIGINAL_PRICE = 'original_price';
    const PRIMARY_VENDOR_ID = 'primary_vendor_id';
    const PRIMARY_BARCODE = 'primary_barcode';
    const PRIMARY_IMAGE_ID = 'primary_image_id';
    const WEIGHT = 'weight';
    const WIDTH = 'width';
    const HEIGHT = 'height';
    const DEPTH = 'depth';
    const SIZE = 'size';
    const BRAND = 'brand';
    const COLOR = 'color';
    const VENDOR = 'vendor';
    const CATEGORY = 'category';
    const DEPARTMENT = 'department';
    const SUBCATEGORY = 'subcategory';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setId($value): ProductInterface;

    /**
     * Get ProductId value
     *
     * @return string
     */
    public function getProductId(): string;

    /**
     * Set ProductId value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setProductId($value): ProductInterface;

    /**
     * Get Sku value
     *
     * @return string
     */
    public function getSku(): string;

    /**
     * Set Sku value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setSku($value): ProductInterface;

    /**
     * Get ConfigurableSku value
     *
     * @return string
     */
    public function getConfigurableSku(): string;

    /**
     * Set ConfigurableSku value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setConfigurableSku($value): ProductInterface;

    /**
     * Get StyleName value
     *
     * @return string
     */
    public function getStyleName(): string;

    /**
     * Set StyleName value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setStyleName($value): ProductInterface;

    /**
     * Get Upc value
     *
     * @return string
     */
    public function getUpc(): string;

    /**
     * Set Upc value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setUpc($value): ProductInterface;

    /**
     * Get Description value
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Set Description value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setDescription($value): ProductInterface;

    /**
     * Get LongDescription value
     *
     * @return string
     */
    public function getLongDescription(): string;

    /**
     * Set LongDescription value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setLongDescription($value): ProductInterface;

    /**
     * Get Active value
     *
     * @return int
     */
    public function getActive(): int;

    /**
     * Set Active value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setActive($value): ProductInterface;

    /**
     * Get Cost value
     *
     * @return float
     */
    public function getCost(): float;

    /**
     * Set Cost value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setCost($value): ProductInterface;

    /**
     * Get Price value
     *
     * @return float
     */
    public function getPrice(): float;

    /**
     * Set Price value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setPrice($value): ProductInterface;

    /**
     * Get OriginalPrice value
     *
     * @return float
     */
    public function getOriginalPrice(): float;

    /**
     * Set OriginalPrice value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setOriginalPrice($value): ProductInterface;

    /**
     * Get PrimaryVendorId value
     *
     * @return int
     */
    public function getPrimaryVendorId(): int;

    /**
     * Set PrimaryVendorId value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setPrimaryVendorId($value): ProductInterface;

    /**
     * Get PrimaryBarcode value
     *
     * @return string
     */
    public function getPrimaryBarcode(): string;

    /**
     * Set PrimaryBarcode value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setPrimaryBarcode($value): ProductInterface;

    /**
     * Get PrimaryImageId value
     *
     * @return int
     */
    public function getPrimaryImageId(): int;

    /**
     * Set PrimaryImageId value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setPrimaryImageId($value): ProductInterface;

    /**
     * Get Weight value
     *
     * @return float
     */
    public function getWeight(): float;

    /**
     * Set Weight value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setWeight($value): ProductInterface;

    /**
     * Get Width value
     *
     * @return float
     */
    public function getWidth(): float;

    /**
     * Set Width value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setWidth($value): ProductInterface;

    /**
     * Get Height value
     *
     * @return float
     */
    public function getHeight(): float;

    /**
     * Set Height value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setHeight($value): ProductInterface;

    /**
     * Get Depth value
     *
     * @return float
     */
    public function getDepth(): float;

    /**
     * Set Depth value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setDepth($value): ProductInterface;

    /**
     * Get Size value
     *
     * @return string
     */
    public function getSize(): string;

    /**
     * Set Size value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setSize($value): ProductInterface;

    /**
     * Get Brand value
     *
     * @return string
     */
    public function getBrand(): string;

    /**
     * Set Brand value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setBrand($value): ProductInterface;

    /**
     * Get Color value
     *
     * @return string
     */
    public function getColor(): string;

    /**
     * Set Color value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setColor($value): ProductInterface;

    /**
     * Get Vendor value
     *
     * @return string
     */
    public function getVendor(): string;

    /**
     * Set Vendor value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setVendor($value): ProductInterface;

    /**
     * Get Category value
     *
     * @return string
     */
    public function getCategory(): string;

    /**
     * Set Category value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setCategory($value): ProductInterface;

    /**
     * Get Department value
     *
     * @return string
     */
    public function getDepartment(): string;

    /**
     * Set Department value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setDepartment($value): ProductInterface;

    /**
     * Get Subcategory value
     *
     * @return string
     */
    public function getSubcategory(): string;

    /**
     * Set Subcategory value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setSubcategory($value): ProductInterface;

    /**
     * Get CreatedAt value
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Set CreatedAt value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setCreatedAt($value): ProductInterface;

    /**
     * Get UpdatedAt value
     *
     * @return string
     */
    public function getUpdatedAt(): string;

    /**
     * Set UpdatedAt value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setUpdatedAt($value): ProductInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setSpringboardSyncDt($value): ProductInterface;
}
