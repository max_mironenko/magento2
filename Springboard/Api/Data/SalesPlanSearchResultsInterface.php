<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface SalesPlanSearchResultsInterface
 */
interface SalesPlanSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get SalesPlan list.
     *
     * @return SalesPlanInterface[]
     */
    public function getItems(): array;

    /**
     * Set SalesPlan list.
     *
     * @param SalesPlanInterface[] $items
     * @return SalesPlanSearchResultsInterface
     */
    public function setItems(array $items): SalesPlanSearchResultsInterface;
}
