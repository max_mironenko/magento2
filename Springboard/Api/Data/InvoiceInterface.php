<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface InvoiceInterface
 */
interface InvoiceInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_invoices';

    // table columns
    const ID = 'id';
    const INVOICE_ID = 'invoice_id';
    const ORDER_ID = 'order_id';
    const TYPE = 'type';
    const STATUS = 'status';
    const CREATED_DATE = 'created_date';
    const UPDATED_AT = 'updated_at';
    const CUSTOMER_ID = 'customer_id';
    const SALES_REP = 'sales_rep';
    const STATION_ID = 'station_id';
    const SOURCE_LOCATION_ID = 'source_location_id';
    const TOTAL_ITEM_QTY = 'total_item_qty';
    const TOTAL = 'total';
    const TOTAL_PAID = 'total_paid';
    const ORIGINAL_SUBTOTAL = 'original_subtotal';
    const TOTAL_DISCOUNTS = 'total_discounts';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setId($value): InvoiceInterface;

    /**
     * Get InvoiceId value
     *
     * @return string
     */
    public function getInvoiceId(): string;

    /**
     * Set InvoiceId value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setInvoiceId($value): InvoiceInterface;

    /**
     * Get OrderId value
     *
     * @return string
     */
    public function getOrderId(): string;

    /**
     * Set OrderId value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setOrderId($value): InvoiceInterface;

    /**
     * Get Type value
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Set Type value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setType($value): InvoiceInterface;

    /**
     * Get Status value
     *
     * @return string
     */
    public function getStatus(): string;

    /**
     * Set Status value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setStatus($value): InvoiceInterface;

    /**
     * Get CreatedDate value
     *
     * @return string
     */
    public function getCreatedDate(): string;

    /**
     * Set CreatedDate value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setCreatedDate($value): InvoiceInterface;

    /**
     * Get UpdatedAt value
     *
     * @return string
     */
    public function getUpdatedAt(): string;

    /**
     * Set UpdatedAt value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setUpdatedAt($value): InvoiceInterface;

    /**
     * Get CustomerId value
     *
     * @return string
     */
    public function getCustomerId(): string;

    /**
     * Set CustomerId value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setCustomerId($value): InvoiceInterface;

    /**
     * Get SalesRep value
     *
     * @return string
     */
    public function getSalesRep(): string;

    /**
     * Set SalesRep value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setSalesRep($value): InvoiceInterface;

    /**
     * Get StationId value
     *
     * @return string
     */
    public function getStationId(): string;

    /**
     * Set StationId value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setStationId($value): InvoiceInterface;

    /**
     * Get SourceLocationId value
     *
     * @return string
     */
    public function getSourceLocationId(): string;

    /**
     * Set SourceLocationId value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setSourceLocationId($value): InvoiceInterface;

    /**
     * Get TotalItemQty value
     *
     * @return float
     */
    public function getTotalItemQty(): float;

    /**
     * Set TotalItemQty value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setTotalItemQty($value): InvoiceInterface;

    /**
     * Get Total value
     *
     * @return float
     */
    public function getTotal(): float;

    /**
     * Set Total value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setTotal($value): InvoiceInterface;

    /**
     * Get TotalPaid value
     *
     * @return float
     */
    public function getTotalPaid(): float;

    /**
     * Set TotalPaid value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setTotalPaid($value): InvoiceInterface;

    /**
     * Get OriginalSubtotal value
     *
     * @return float
     */
    public function getOriginalSubtotal(): float;

    /**
     * Set OriginalSubtotal value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setOriginalSubtotal($value): InvoiceInterface;

    /**
     * Get TotalDiscounts value
     *
     * @return float
     */
    public function getTotalDiscounts(): float;

    /**
     * Set TotalDiscounts value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setTotalDiscounts($value): InvoiceInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setSpringboardSyncDt($value): InvoiceInterface;
}
