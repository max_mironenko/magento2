<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface OrderLineInterface
 */
interface OrderLineInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_order_lines';

    // table columns
    const ID = 'id';
    const LINE_ID = 'line_id';
    const ORDER_ID = 'order_id';
    const ITEM_ID = 'item_id';
    const DESCRIPTION = 'description';
    const LOCATION_ID = 'location_id';
    const QTY = 'qty';
    const UNIT_PRICE = 'unit_price';
    const TOTAL_PRICE = 'total_price';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';
    const VENDOR = 'vendor';
    const CONFIGURABLE_PRODUCT_ID = 'configurable_product_id';
    const SIMPLE_PRODUCT_ID = 'simple_product_id';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setId($value): OrderLineInterface;

    /**
     * Get LineId value
     *
     * @return string
     */
    public function getLineId(): string;

    /**
     * Set LineId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setLineId($value): OrderLineInterface;

    /**
     * Get OrderId value
     *
     * @return string
     */
    public function getOrderId(): string;

    /**
     * Set OrderId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setOrderId($value): OrderLineInterface;

    /**
     * Get ItemId value
     *
     * @return string
     */
    public function getItemId(): string;

    /**
     * Set ItemId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setItemId($value): OrderLineInterface;

    /**
     * Get Description value
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Set Description value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setDescription($value): OrderLineInterface;

    /**
     * Get LocationId value
     *
     * @return string
     */
    public function getLocationId(): string;

    /**
     * Set LocationId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setLocationId($value): OrderLineInterface;

    /**
     * Get Qty value
     *
     * @return float
     */
    public function getQty(): float;

    /**
     * Set Qty value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setQty($value): OrderLineInterface;

    /**
     * Get UnitPrice value
     *
     * @return float
     */
    public function getUnitPrice(): float;

    /**
     * Set UnitPrice value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setUnitPrice($value): OrderLineInterface;

    /**
     * Get TotalPrice value
     *
     * @return float
     */
    public function getTotalPrice(): float;

    /**
     * Set TotalPrice value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setTotalPrice($value): OrderLineInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setSpringboardSyncDt($value): OrderLineInterface;

    /**
     * Get Vendor value
     *
     * @return string
     */
    public function getVendor(): string;

    /**
     * Set Vendor value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setVendor($value): OrderLineInterface;

    /**
     * Get ConfigurableProductId value
     *
     * @return string
     */
    public function getConfigurableProductId(): string;

    /**
     * Set ConfigurableProductId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setConfigurableProductId($value): OrderLineInterface;

    /**
     * Get SimpleProductId value
     *
     * @return string
     */
    public function getSimpleProductId(): string;

    /**
     * Set SimpleProductId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setSimpleProductId($value): OrderLineInterface;
}
