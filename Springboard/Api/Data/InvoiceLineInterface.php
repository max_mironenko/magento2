<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface InvoiceLineInterface
 */
interface InvoiceLineInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_invoice_lines';

    // table columns
    const ID = 'id';
    const LINE_ID = 'line_id';
    const INVOICE_ID = 'invoice_id';
    const ITEM_ID = 'item_id';
    const TYPE = 'type';
    const DESCRIPTION = 'description';
    const TAX_RULE_ID = 'tax_rule_id';
    const VALUE = 'value';
    const QTY = 'qty';
    const ORIGINAL_UNIT_PRICE = 'original_unit_price';
    const UNIT_PRICE = 'unit_price';
    const UNIT_COST = 'unit_cost';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';
    const CONFIGURABLE_PRODUCT_ID = 'configurable_product_id';
    const SIMPLE_PRODUCT_ID = 'simple_product_id';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setId($value): InvoiceLineInterface;

    /**
     * Get LineId value
     *
     * @return string
     */
    public function getLineId(): string;

    /**
     * Set LineId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setLineId($value): InvoiceLineInterface;

    /**
     * Get InvoiceId value
     *
     * @return string
     */
    public function getInvoiceId(): string;

    /**
     * Set InvoiceId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setInvoiceId($value): InvoiceLineInterface;

    /**
     * Get ItemId value
     *
     * @return string
     */
    public function getItemId(): string;

    /**
     * Set ItemId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setItemId($value): InvoiceLineInterface;

    /**
     * Get Type value
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Set Type value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setType($value): InvoiceLineInterface;

    /**
     * Get Description value
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Set Description value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setDescription($value): InvoiceLineInterface;

    /**
     * Get TaxRuleId value
     *
     * @return string
     */
    public function getTaxRuleId(): string;

    /**
     * Set TaxRuleId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setTaxRuleId($value): InvoiceLineInterface;

    /**
     * Get Value value
     *
     * @return float
     */
    public function getValue(): float;

    /**
     * Set Value value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setValue($value): InvoiceLineInterface;

    /**
     * Get Qty value
     *
     * @return float
     */
    public function getQty(): float;

    /**
     * Set Qty value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setQty($value): InvoiceLineInterface;

    /**
     * Get OriginalUnitPrice value
     *
     * @return float
     */
    public function getOriginalUnitPrice(): float;

    /**
     * Set OriginalUnitPrice value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setOriginalUnitPrice($value): InvoiceLineInterface;

    /**
     * Get UnitPrice value
     *
     * @return float
     */
    public function getUnitPrice(): float;

    /**
     * Set UnitPrice value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setUnitPrice($value): InvoiceLineInterface;

    /**
     * Get UnitCost value
     *
     * @return float
     */
    public function getUnitCost(): float;

    /**
     * Set UnitCost value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setUnitCost($value): InvoiceLineInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setSpringboardSyncDt($value): InvoiceLineInterface;

    /**
     * Get ConfigurableProductId value
     *
     * @return string
     */
    public function getConfigurableProductId(): string;

    /**
     * Set ConfigurableProductId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setConfigurableProductId($value): InvoiceLineInterface;

    /**
     * Get SimpleProductId value
     *
     * @return string
     */
    public function getSimpleProductId(): string;

    /**
     * Set SimpleProductId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setSimpleProductId($value): InvoiceLineInterface;
}
