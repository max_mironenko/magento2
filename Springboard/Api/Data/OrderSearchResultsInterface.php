<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface OrderSearchResultsInterface
 */
interface OrderSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Order list.
     *
     * @return OrderInterface[]
     */
    public function getItems(): array;

    /**
     * Set Order list.
     *
     * @param OrderInterface[] $items
     * @return OrderSearchResultsInterface
     */
    public function setItems(array $items): OrderSearchResultsInterface;
}
