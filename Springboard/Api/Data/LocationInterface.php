<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface LocationInterface
 */
interface LocationInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_locations';

    // table columns
    const ID = 'id';
    const SPRINGBOARD_ID = 'springboard_id';
    const LOCATION_NUMBER = 'location_number';
    const NAME = 'name';
    const TIMEZONE_IDENTIFIER_ID = 'timezone_identifier_id';
    const IS_ACTIVE = 'is_active';
    const IS_DELETED = 'is_deleted';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setId($value): LocationInterface;

    /**
     * Get SpringboardId value
     *
     * @return string|null
     */
    public function getSpringboardId(): ?string;

    /**
     * Set SpringboardId value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setSpringboardId($value): LocationInterface;

    /**
     * Get LocationNumber value
     *
     * @return string
     */
    public function getLocationNumber(): string;

    /**
     * Set LocationNumber value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setLocationNumber($value): LocationInterface;

    /**
     * Get Name value
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Set Name value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setName($value): LocationInterface;

    /**
     * Get TimezoneIdentifierId value
     *
     * @return int
     */
    public function getTimezoneIdentifierId(): int;

    /**
     * Set TimezoneIdentifierId value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setTimezoneIdentifierId($value): LocationInterface;

    /**
     * Get IsActive value
     *
     * @return int
     */
    public function getIsActive(): int;

    /**
     * Set IsActive value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setIsActive($value): LocationInterface;

    /**
     * Get IsDeleted value
     *
     * @return int
     */
    public function getIsDeleted(): int;

    /**
     * Set IsDeleted value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setIsDeleted($value): LocationInterface;

    /**
     * Get CreatedAt value
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Set CreatedAt value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setCreatedAt($value): LocationInterface;

    /**
     * Get UpdatedAt value
     *
     * @return string
     */
    public function getUpdatedAt(): string;

    /**
     * Set UpdatedAt value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setUpdatedAt($value): LocationInterface;
}
