<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface TicketLineInterface
 */
interface TicketLineInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_ticket_lines';

    // table columns
    const ID = 'id';
    const LINE_ID = 'line_id';
    const TICKET_ID = 'ticket_id';
    const ITEM_ID = 'item_id';
    const TYPE = 'type';
    const DESCRIPTION = 'description';
    const TAX_RULE_ID = 'tax_rule_id';
    const VALUE = 'value';
    const QTY = 'qty';
    const ORIGINAL_UNIT_PRICE = 'original_unit_price';
    const UNIT_PRICE = 'unit_price';
    const UNIT_COST = 'unit_cost';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';
    const VENDOR = 'vendor';
    const ITEM_LINE_ID = 'item_line_id';
    const CONFIGURABLE_PRODUCT_ID = 'configurable_product_id';
    const SIMPLE_PRODUCT_ID = 'simple_product_id';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setId($value): TicketLineInterface;

    /**
     * Get LineId value
     *
     * @return string
     */
    public function getLineId(): string;

    /**
     * Set LineId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setLineId($value): TicketLineInterface;

    /**
     * Get TicketId value
     *
     * @return string
     */
    public function getTicketId(): string;

    /**
     * Set TicketId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setTicketId($value): TicketLineInterface;

    /**
     * Get ItemId value
     *
     * @return string
     */
    public function getItemId(): string;

    /**
     * Set ItemId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setItemId($value): TicketLineInterface;

    /**
     * Get Type value
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Set Type value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setType($value): TicketLineInterface;

    /**
     * Get Description value
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Set Description value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setDescription($value): TicketLineInterface;

    /**
     * Get TaxRuleId value
     *
     * @return string
     */
    public function getTaxRuleId(): string;

    /**
     * Set TaxRuleId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setTaxRuleId($value): TicketLineInterface;

    /**
     * Get Value value
     *
     * @return string
     */
    public function getValue(): string;

    /**
     * Set Value value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setValue($value): TicketLineInterface;

    /**
     * Get Qty value
     *
     * @return float
     */
    public function getQty(): float;

    /**
     * Set Qty value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setQty($value): TicketLineInterface;

    /**
     * Get OriginalUnitPrice value
     *
     * @return float
     */
    public function getOriginalUnitPrice(): float;

    /**
     * Set OriginalUnitPrice value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setOriginalUnitPrice($value): TicketLineInterface;

    /**
     * Get UnitPrice value
     *
     * @return float
     */
    public function getUnitPrice(): float;

    /**
     * Set UnitPrice value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setUnitPrice($value): TicketLineInterface;

    /**
     * Get UnitCost value
     *
     * @return float
     */
    public function getUnitCost(): float;

    /**
     * Set UnitCost value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setUnitCost($value): TicketLineInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setSpringboardSyncDt($value): TicketLineInterface;

    /**
     * Get Vendor value
     *
     * @return string
     */
    public function getVendor(): string;

    /**
     * Set Vendor value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setVendor($value): TicketLineInterface;

    /**
     * Get ItemLineId value
     *
     * @return string
     */
    public function getItemLineId(): string;

    /**
     * Set ItemLineId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setItemLineId($value): TicketLineInterface;

    /**
     * Get ConfigurableProductId value
     *
     * @return string
     */
    public function getConfigurableProductId(): string;

    /**
     * Set ConfigurableProductId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setConfigurableProductId($value): TicketLineInterface;

    /**
     * Get SimpleProductId value
     *
     * @return string
     */
    public function getSimpleProductId(): string;

    /**
     * Set SimpleProductId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setSimpleProductId($value): TicketLineInterface;
}
