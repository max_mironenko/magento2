<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface InventorySearchResultsInterface
 */
interface InventorySearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Inventory list.
     *
     * @return InventoryInterface[]
     */
    public function getItems(): array;

    /**
     * Set Inventory list.
     *
     * @param InventoryInterface[] $items
     * @return InventorySearchResultsInterface
     */
    public function setItems(array $items): InventorySearchResultsInterface;
}
