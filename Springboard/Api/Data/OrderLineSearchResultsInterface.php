<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface OrderLineSearchResultsInterface
 */
interface OrderLineSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get OrderLine list.
     *
     * @return OrderLineInterface[]
     */
    public function getItems(): array;

    /**
     * Set OrderLine list.
     *
     * @param OrderLineInterface[] $items
     * @return OrderLineSearchResultsInterface
     */
    public function setItems(array $items): OrderLineSearchResultsInterface;
}
