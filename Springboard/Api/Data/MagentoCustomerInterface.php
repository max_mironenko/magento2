<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface MagentoCustomerInterface
 */
interface MagentoCustomerInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_customer_entity';

    // table columns
    const ID = 'id';
    const ENTITY_ID = 'entity_id';
    const SPRINGBOARD_ID = 'springboard_id';
    const CREATED_AT = 'created_at';
    const IS_SYNCED = 'is_synced';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return MagentoCustomerInterface
     */
    public function setId($value): MagentoCustomerInterface;

    /**
     * Get EntityId value
     *
     * @return string|null
     */
    public function getEntityId(): ?string;

    /**
     * Set EntityId value
     *
     * @param $entityId
     * @return MagentoCustomerInterface
     */
    public function setEntityId($entityId): MagentoCustomerInterface;

    /**
     * Get SpringboardId value
     *
     * @return string|null
     */
    public function getSpringboardId(): ?string;

    /**
     * Set SpringboardId value
     *
     * @param $value
     * @return MagentoCustomerInterface
     */
    public function setSpringboardId($value): MagentoCustomerInterface;

    /**
     * Get CreatedAt value
     *
     * @return string|null
     */
    public function getCreatedAt(): ?string;

    /**
     * Set CreatedAt value
     *
     * @param $value
     * @return MagentoCustomerInterface
     */
    public function setCreatedAt($value): MagentoCustomerInterface;

    /**
     * Get IsSynced value
     *
     * @return string|null
     */
    public function getIsSynced(): ?string;

    /**
     * Set IsSynced value
     *
     * @param $value
     * @return MagentoCustomerInterface
     */
    public function setIsSynced($value): MagentoCustomerInterface;
}
