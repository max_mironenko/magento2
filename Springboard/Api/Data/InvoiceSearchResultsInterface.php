<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface InvoiceSearchResultsInterface
 */
interface InvoiceSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Invoice list.
     *
     * @return InvoiceInterface[]
     */
    public function getItems(): array;

    /**
     * Set Invoice list.
     *
     * @param InvoiceInterface[] $items
     * @return InvoiceSearchResultsInterface
     */
    public function setItems(array $items): InvoiceSearchResultsInterface;
}
