<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface CustomerSearchResultsInterface
 */
interface CustomerSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Customer list.
     *
     * @return CustomerInterface[]
     */
    public function getItems(): array;

    /**
     * Set Customer list.
     *
     * @param CustomerInterface[] $items
     * @return CustomerSearchResultsInterface
     */
    public function setItems(array $items): CustomerSearchResultsInterface;
}
