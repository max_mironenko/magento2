<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface OrderInterface
 */
interface OrderInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_orders';

    // table columns
    const ID = 'id';
    const ORDER_ID = 'order_id';
    const MAGENTO_ORDER_ID = 'magento_order_id';
    const STATUS = 'status';
    const CREATED_DATE = 'created_date';
    const CUSTOMER_ID = 'customer_id';
    const STATION_ID = 'station_id';
    const SHIP_FROM = 'ship_from';
    const BILLING_ADDRESS_ID = 'billing_address_id';
    const SHIPPING_ADDRESS_ID = 'shipping_address_id';
    const SALES_REP = 'sales_rep';
    const SHIPPING_METHOD_ID = 'shipping_method_id';
    const TOTAL_QTY = 'total_qty';
    const TOTAL = 'total';
    const BALANCE = 'balance';
    const TOTAL_PAID = 'total_paid';
    const SHIPPING_CHARGE = 'shipping_charge';
    const SUBTOTAL = 'subtotal';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';
    const MAGENTO_CUSTOMER_ID = 'magento_customer_id';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setId($value): OrderInterface;

    /**
     * Get OrderId value
     *
     * @return string
     */
    public function getOrderId(): string;

    /**
     * Set OrderId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setOrderId($value): OrderInterface;

    /**
     * Get MagentoOrderId value
     *
     * @return string
     */
    public function getMagentoOrderId(): string;

    /**
     * Set MagentoOrderId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setMagentoOrderId($value): OrderInterface;

    /**
     * Get Status value
     *
     * @return string
     */
    public function getStatus(): string;

    /**
     * Set Status value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setStatus($value): OrderInterface;

    /**
     * Get CreatedDate value
     *
     * @return string
     */
    public function getCreatedDate(): string;

    /**
     * Set CreatedDate value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setCreatedDate($value): OrderInterface;

    /**
     * Get CustomerId value
     *
     * @return string
     */
    public function getCustomerId(): string;

    /**
     * Set CustomerId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setCustomerId($value): OrderInterface;

    /**
     * Get StationId value
     *
     * @return string
     */
    public function getStationId(): string;

    /**
     * Set StationId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setStationId($value): OrderInterface;

    /**
     * Get ShipFrom value
     *
     * @return string
     */
    public function getShipFrom(): string;

    /**
     * Set ShipFrom value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setShipFrom($value): OrderInterface;

    /**
     * Get BillingAddressId value
     *
     * @return string
     */
    public function getBillingAddressId(): string;

    /**
     * Set BillingAddressId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setBillingAddressId($value): OrderInterface;

    /**
     * Get ShippingAddressId value
     *
     * @return string
     */
    public function getShippingAddressId(): string;

    /**
     * Set ShippingAddressId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setShippingAddressId($value): OrderInterface;

    /**
     * Get SalesRep value
     *
     * @return string
     */
    public function getSalesRep(): string;

    /**
     * Set SalesRep value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setSalesRep($value): OrderInterface;

    /**
     * Get ShippingMethodId value
     *
     * @return string
     */
    public function getShippingMethodId(): string;

    /**
     * Set ShippingMethodId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setShippingMethodId($value): OrderInterface;

    /**
     * Get TotalQty value
     *
     * @return float
     */
    public function getTotalQty(): float;

    /**
     * Set TotalQty value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setTotalQty($value): OrderInterface;

    /**
     * Get Total value
     *
     * @return float
     */
    public function getTotal(): float;

    /**
     * Set Total value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setTotal($value): OrderInterface;

    /**
     * Get Balance value
     *
     * @return float
     */
    public function getBalance(): float;

    /**
     * Set Balance value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setBalance($value): OrderInterface;

    /**
     * Get TotalPaid value
     *
     * @return float
     */
    public function getTotalPaid(): float;

    /**
     * Set TotalPaid value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setTotalPaid($value): OrderInterface;

    /**
     * Get ShippingCharge value
     *
     * @return float
     */
    public function getShippingCharge(): float;

    /**
     * Set ShippingCharge value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setShippingCharge($value): OrderInterface;

    /**
     * Get Subtotal value
     *
     * @return float
     */
    public function getSubtotal(): float;

    /**
     * Set Subtotal value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setSubtotal($value): OrderInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setSpringboardSyncDt($value): OrderInterface;

    /**
     * Get MagentoCustomerId value
     *
     * @return string
     */
    public function getMagentoCustomerId(): string;

    /**
     * Set MagentoCustomerId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setMagentoCustomerId($value): OrderInterface;
}
