<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface TicketLineAdjustmentsSearchResultsInterface
 */
interface TicketLineAdjustmentsSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Ticket Lines Adjustments list.
     *
     * @return TicketLineAdjustmentsInterface[]
     */
    public function getItems(): array;

    /**
     * Set Ticket Lines Adjustments list.
     *
     * @param TicketLineAdjustmentsInterface[] $items
     * @return TicketLineAdjustmentsSearchResultsInterface
     */
    public function setItems(array $items): TicketLineAdjustmentsSearchResultsInterface;
}
