<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface TicketLineAdjustmentsInterface
 */
interface TicketLineAdjustmentsInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_ticket_line_adjustments';

    // table columns
    const ID = 'id';
    const LINE_ID = 'line_id';
    const ADJUSTMENT_ID = 'adjustment_id';
    const DELTA_PRICE = 'delta_price';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setId($value): TicketLineAdjustmentsInterface;

    /**
     * Get LineId value
     *
     * @return string
     */
    public function getLineId(): string;

    /**
     * Set LineId value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setLineId($value): TicketLineAdjustmentsInterface;

    /**
     * Get AdjustmentId value
     *
     * @return string
     */
    public function getAdjustmentId(): string;

    /**
     * Set AdjustmentId value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setAdjustmentId($value): TicketLineAdjustmentsInterface;

    /**
     * Get DeltaPrice value
     *
     * @return float
     */
    public function getDeltaPrice(): float;

    /**
     * Set DeltaPrice value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setDeltaPrice($value): TicketLineAdjustmentsInterface;

    /**
     * Get Name value
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Set Name value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setName($value): TicketLineAdjustmentsInterface;

    /**
     * Get Description value
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Set Description value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setDescription($value): TicketLineAdjustmentsInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setSpringboardSyncDt($value): TicketLineAdjustmentsInterface;
}
