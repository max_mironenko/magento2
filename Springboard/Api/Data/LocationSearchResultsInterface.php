<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface LocationSearchResultsInterface
 */
interface LocationSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Location list.
     *
     * @return LocationInterface[]
     */
    public function getItems(): array;

    /**
     * Set Location list.
     *
     * @param LocationInterface[] $items
     * @return LocationSearchResultsInterface
     */
    public function setItems(array $items): LocationSearchResultsInterface;
}
