<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface AddressInterface
 */
interface AddressInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_address';

    // table columns
    const ID = 'id';
    const ADDRESS_ID = 'address_id';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const LINE1 = 'line1';
    const LINE2 = 'line2';
    const CITY = 'city';
    const STATE = 'state';
    const COUNTRY = 'country';
    const POSTAL_CODE = 'postal_code';
    const PHONE = 'phone';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setId($value): AddressInterface;

    /**
     * Get AddressId value
     *
     * @return string
     */
    public function getAddressId(): string;

    /**
     * Set AddressId value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setAddressId($value): AddressInterface;

    /**
     * Get FirstName value
     *
     * @return string
     */
    public function getFirstName(): string;

    /**
     * Set FirstName value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setFirstName($value): AddressInterface;

    /**
     * Get LastName value
     *
     * @return string
     */
    public function getLastName(): string;

    /**
     * Set LastName value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setLastName($value): AddressInterface;

    /**
     * Get Line1 value
     *
     * @return string
     */
    public function getLine1(): string;

    /**
     * Set Line1 value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setLine1($value): AddressInterface;

    /**
     * Get Line2 value
     *
     * @return string
     */
    public function getLine2(): string;

    /**
     * Set Line2 value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setLine2($value): AddressInterface;

    /**
     * Get City value
     *
     * @return string
     */
    public function getCity(): string;

    /**
     * Set City value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setCity($value): AddressInterface;

    /**
     * Get State value
     *
     * @return string
     */
    public function getState(): string;

    /**
     * Set State value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setState($value): AddressInterface;

    /**
     * Get Country value
     *
     * @return string
     */
    public function getCountry(): string;

    /**
     * Set Country value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setCountry($value): AddressInterface;

    /**
     * Get PostalCode value
     *
     * @return string
     */
    public function getPostalCode(): string;

    /**
     * Set PostalCode value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setPostalCode($value): AddressInterface;

    /**
     * Get Phone value
     *
     * @return string
     */
    public function getPhone(): string;

    /**
     * Set Phone value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setPhone($value): AddressInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setSpringboardSyncDt($value): AddressInterface;
}
