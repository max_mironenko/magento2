<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface CustomerInterface
 */
interface CustomerInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_customers';

    // table columns
    const ID = 'id';
    const CUSTOMER_ID = 'customer_id';
    const EMAIL = 'email';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const IS_ACTIVE = 'is_active';
    const BIRTHDAY = 'birthday';
    const TYPE = 'type';
    const ADDRESS_ID = 'address_id';
    const LINE1 = 'line1';
    const LINE2 = 'line2';
    const CITY = 'city';
    const STATE = 'state';
    const PHONE = 'phone';
    const COUNTRY = 'country';
    const POSTAL_CODE = 'postal_code';
    const FULL_ADDRESS = 'full_address';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';
    const LIFETIME_SPEND = 'lifetime_spend';
    const LOCATIONS_SHOPPED = 'locations_shopped';
    const BRANDS_PURCHASED = 'brands_purchased';
    const CUSTOMER_SINCE = 'customer_since';
    const VIP = 'vip';
    const AMBASSADOR = 'ambassador';
    const CARD_CHECK = 'card_check';
    const CREATED_AT = 'created_at';
    const STORE_CREDIT = 'store_credit';
    const UPDATED_AT = 'updated_at';
    const IS_SYNCED = 'is_synced';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setId($value): CustomerInterface;

    /**
     * Get CustomerId value
     *
     * @return string|null
     */
    public function getCustomerId(): ?string;

    /**
     * Set CustomerId value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCustomerId($value): CustomerInterface;

    /**
     * Get Email value
     *
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * Set Email value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setEmail($value): CustomerInterface;

    /**
     * Get FirstName value
     *
     * @return string|null
     */
    public function getFirstName(): ?string;

    /**
     * Set FirstName value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setFirstName($value): CustomerInterface;

    /**
     * Get LastName value
     *
     * @return string|null
     */
    public function getLastName(): ?string;

    /**
     * Set LastName value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setLastName($value): CustomerInterface;

    /**
     * Get IsActive value
     *
     * @return int
     */
    public function getIsActive(): int;

    /**
     * Set IsActive value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setIsActive($value): CustomerInterface;

    /**
     * Get Birthday value
     *
     * @return string|null
     */
    public function getBirthday(): ?string;

    /**
     * Set Birthday value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setBirthday($value): CustomerInterface;

    /**
     * Get Type value
     *
     * @return string|null
     */
    public function getType(): ?string;

    /**
     * Set Type value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setType($value): CustomerInterface;

    /**
     * Get AddressId value
     *
     * @return string|null
     */
    public function getAddressId(): ?string;

    /**
     * Set AddressId value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setAddressId($value): CustomerInterface;

    /**
     * Get Line1 value
     *
     * @return string|null
     */
    public function getLine1(): ?string;

    /**
     * Set Line1 value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setLine1($value): CustomerInterface;

    /**
     * Get Line2 value
     *
     * @return string|null
     */
    public function getLine2(): ?string;

    /**
     * Set Line2 value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setLine2($value): CustomerInterface;

    /**
     * Get City value
     *
     * @return string|null
     */
    public function getCity(): ?string;

    /**
     * Set City value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCity($value): CustomerInterface;

    /**
     * Get State value
     *
     * @return string|null
     */
    public function getState(): ?string;

    /**
     * Set State value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setState($value): CustomerInterface;

    /**
     * Get Phone value
     *
     * @return string|null
     */
    public function getPhone(): ?string;

    /**
     * Set Phone value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setPhone($value): CustomerInterface;

    /**
     * Get Country value
     *
     * @return string|null
     */
    public function getCountry(): ?string;

    /**
     * Set Country value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCountry($value): CustomerInterface;

    /**
     * Get PostalCode value
     *
     * @return string|null
     */
    public function getPostalCode(): ?string;

    /**
     * Set PostalCode value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setPostalCode($value): CustomerInterface;

    /**
     * Get FullAddress value
     *
     * @return string|null
     */
    public function getFullAddress(): ?string;

    /**
     * Set FullAddress value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setFullAddress($value): CustomerInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string|null
     */
    public function getSpringboardSyncDt(): ?string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setSpringboardSyncDt($value): CustomerInterface;

    /**
     * Get LifetimeSpend value
     *
     * @return string|null
     */
    public function getLifetimeSpend(): ?string;

    /**
     * Set LifetimeSpend value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setLifetimeSpend($value): CustomerInterface;

    /**
     * Get LocationsShopped value
     *
     * @return string|null
     */
    public function getLocationsShopped(): ?string;

    /**
     * Set LocationsShopped value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setLocationsShopped($value): CustomerInterface;

    /**
     * Get BrandsPurchased value
     *
     * @return string|null
     */
    public function getBrandsPurchased(): ?string;

    /**
     * Set BrandsPurchased value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setBrandsPurchased($value): CustomerInterface;

    /**
     * Get CustomerSince value
     *
     * @return string|null
     */
    public function getCustomerSince(): ?string;

    /**
     * Set CustomerSince value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCustomerSince($value): CustomerInterface;

    /**
     * Get Vip value
     *
     * @return string|null
     */
    public function getVip(): ?string;

    /**
     * Set Vip value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setVip($value): CustomerInterface;

    /**
     * Get Ambassador value
     *
     * @return string|null
     */
    public function getAmbassador(): ?string;

    /**
     * Set Ambassador value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setAmbassador($value): CustomerInterface;

    /**
     * Get CardCheck value
     *
     * @return string|null
     */
    public function getCardCheck(): ?string;

    /**
     * Set CardCheck value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCardCheck($value): CustomerInterface;

    /**
     * Get CreatedAt value
     *
     * @return string|null
     */
    public function getCreatedAt(): ?string;

    /**
     * Set CreatedAt value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCreatedAt($value): CustomerInterface;

    /**
     * Get StoreCredit value
     *
     * @return string|null
     */
    public function getStoreCredit(): ?string;

    /**
     * Set StoreCredit value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setStoreCredit($value): CustomerInterface;

    /**
     * Get UpdatedAt value
     *
     * @return string|null
     */
    public function getUpdatedAt(): ?string;

    /**
     * Set UpdatedAt value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setUpdatedAt($value): CustomerInterface;

    /**
     * Get IsSynced value
     *
     * @return int|null
     */
    public function getIsSynced(): ?int;

    /**
     * Set IsSynced value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setIsSynced($value): CustomerInterface;
}
