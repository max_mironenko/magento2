<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface TicketLineSearchResultsInterface
 */
interface TicketLineSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Ticket Lines list.
     *
     * @return TicketLineInterface[]
     */
    public function getItems(): array;

    /**
     * Set Ticket Lines list.
     *
     * @param TicketLineInterface[] $items
     * @return TicketLineSearchResultsInterface
     */
    public function setItems(array $items): TicketLineSearchResultsInterface;
}
