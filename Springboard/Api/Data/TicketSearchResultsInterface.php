<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface TicketSearchResultsInterface
 */
interface TicketSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Ticket list.
     *
     * @return TicketInterface[]
     */
    public function getItems(): array;

    /**
     * Set Ticket list.
     *
     * @param TicketInterface[] $items
     * @return TicketSearchResultsInterface
     */
    public function setItems(array $items): TicketSearchResultsInterface;
}
