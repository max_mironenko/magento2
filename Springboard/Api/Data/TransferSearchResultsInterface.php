<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface TransferSearchResultsInterface
 */
interface TransferSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Transfer list.
     *
     * @return TransferInterface[]
     */
    public function getItems(): array;

    /**
     * Set Transfer list.
     *
     * @param TransferInterface[] $items
     * @return TransferSearchResultsInterface
     */
    public function setItems(array $items): TransferSearchResultsInterface;
}
