<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface TransferInterface
 */
interface TransferInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_transfers';

    // table columns
    const ID = 'id';
    const TRANSFER_ID = 'transfer_id';
    const STATUS = 'status';
    const TRANSFER_NUMBER = 'transfer_number';
    const FROM_LOCATION_ID = 'from_location_id';
    const TO_LOCATION_ID = 'to_location_id';
    const CREATED_DATE = 'created_date';
    const TOTAL_QTY_SHIPPED = 'total_qty_shipped';
    const TOTAL_QTY_RECEIVED = 'total_qty_received';
    const TOTAL_QTY_LOST = 'total_qty_lost';
    const TOTAL_QTY_REQUESTED = 'total_qty_requested';
    const TOTAL_QTY_DISCREPANT = 'total_qty_discrepant';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';
    const MANIFEST_ID = 'manifest_id';
    const MH_CONVERT_DATE = 'mh_convert_date';
    const MH_CONVERT_ERROR = 'mh_convert_error';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setId($value): TransferInterface;

    /**
     * Get TransferId value
     *
     * @return string
     */
    public function getTransferId(): string;

    /**
     * Set TransferId value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTransferId($value): TransferInterface;

    /**
     * Get Status value
     *
     * @return string
     */
    public function getStatus(): string;

    /**
     * Set Status value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setStatus($value): TransferInterface;

    /**
     * Get TransferNumber value
     *
     * @return string
     */
    public function getTransferNumber(): string;

    /**
     * Set TransferNumber value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTransferNumber($value): TransferInterface;

    /**
     * Get FromLocationId value
     *
     * @return string
     */
    public function getFromLocationId(): string;

    /**
     * Set FromLocationId value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setFromLocationId($value): TransferInterface;

    /**
     * Get ToLocationId value
     *
     * @return int
     */
    public function getToLocationId(): int;

    /**
     * Set ToLocationId value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setToLocationId($value): TransferInterface;

    /**
     * Get CreatedDate value
     *
     * @return string
     */
    public function getCreatedDate(): string;

    /**
     * Set CreatedDate value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setCreatedDate($value): TransferInterface;

    /**
     * Get TotalQtyShipped value
     *
     * @return float
     */
    public function getTotalQtyShipped(): float;

    /**
     * Set TotalQtyShipped value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTotalQtyShipped($value): TransferInterface;

    /**
     * Get TotalQtyReceived value
     *
     * @return float
     */
    public function getTotalQtyReceived(): float;

    /**
     * Set TotalQtyReceived value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTotalQtyReceived($value): TransferInterface;

    /**
     * Get TotalQtyLost value
     *
     * @return float
     */
    public function getTotalQtyLost(): float;

    /**
     * Set TotalQtyLost value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTotalQtyLost($value): TransferInterface;

    /**
     * Get TotalQtyRequested value
     *
     * @return float
     */
    public function getTotalQtyRequested(): float;

    /**
     * Set TotalQtyRequested value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTotalQtyRequested($value): TransferInterface;

    /**
     * Get TotalQtyDiscrepant value
     *
     * @return float
     */
    public function getTotalQtyDiscrepant(): float;

    /**
     * Set TotalQtyDiscrepant value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTotalQtyDiscrepant($value): TransferInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setSpringboardSyncDt($value): TransferInterface;

    /**
     * Get ManifestId value
     *
     * @return string
     */
    public function getManifestId(): string;

    /**
     * Set ManifestId value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setManifestId($value): TransferInterface;

    /**
     * Get MHConvertDate value
     *
     * @return string
     */
    public function getMHConvertDate(): string;

    /**
     * Set MHConvertDate value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setMHConvertDate($value): TransferInterface;

    /**
     * Get MHConvertError value
     *
     * @return string
     */
    public function getMHConvertError(): string;

    /**
     * Set MHConvertError value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setMHConvertError($value): TransferInterface;
}
