<?php declare(strict_types=1);

namespace C38\Springboard\Api\Data;

/**
 * Interface SalesPlanInterface
 */
interface SalesPlanInterface
{
    // table name
    const TABLE_NAME = 'c38_springboard_sales_plan';

    // table columns
    const ID = 'id';
    const PUBLIC_ID = 'public_id';
    const LOCATION_ID = 'location_id';
    const AMOUNT = 'amount';
    const DATE = 'date';
    const SPRINGBOARD_SYNC_DT = 'springboard_sync_dt';

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setId($value): SalesPlanInterface;

    /**
     * Get PublicId value
     *
     * @return string
     */
    public function getPublicId(): string;

    /**
     * Set PublicId value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setPublicId($value): SalesPlanInterface;

    /**
     * Get LocationId value
     *
     * @return string
     */
    public function getLocationId(): string;

    /**
     * Set LocationId value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setLocationId($value): SalesPlanInterface;

    /**
     * Get Amount value
     *
     * @return float
     */
    public function getAmount(): float;

    /**
     * Set Amount value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setAmount($value): SalesPlanInterface;

    /**
     * Get Date value
     *
     * @return string
     */
    public function getDate(): string;

    /**
     * Set Date value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setDate($value): SalesPlanInterface;

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string;

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setSpringboardSyncDt($value): SalesPlanInterface;
}
