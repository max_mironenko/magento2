<?php declare(strict_types=1);

namespace C38\Springboard\Api;

use C38\Springboard\Api\Data\LocationInterface;
use C38\Springboard\Api\Data\LocationSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface LocationRepositoryInterface
 */
interface LocationRepositoryInterface
{
    /**
     * Save Location
     *
     * @param LocationInterface $location
     *
     * @return LocationInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(LocationInterface $location): LocationInterface;

    /**
     * Get Location by id.
     *
     * @param int $locationId
     *
     * @return LocationInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById(int $locationId): LocationInterface;

    /**
     * Find Location by id.
     *
     * @param int $locationId
     * @return LocationInterface|null
     */
    public function findById(int $locationId): ?LocationInterface;

    /**
     * Find Location by location number.
     *
     * @param int $number
     * @return LocationInterface
     */
    public function loadByLocationNumber(int $number): LocationInterface;

    /**
     * Find Location by Springboard Id.
     *
     * @param string $springboardId
     * @return LocationInterface
     */
    public function loadBySpringboardId(string $springboardId): LocationInterface;

    /**
     * Retrieve Location matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|LocationSearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Location
     *
     * @param LocationInterface $location
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(LocationInterface $location): bool;

    /**
     * Delete Location by ID.
     *
     * @param int $locationId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById(int $locationId): bool;
}
