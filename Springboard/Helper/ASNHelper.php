<?php declare(strict_types=1);

namespace C38\Springboard\Helper;

use C38\MultiDC\Helper\MasonHub as MasonHubHelper;
use \C38\Springboard\Helper\Data as SpringboardHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Throwable;

/**
 * Class ASNHelper
 * Helpers create and send ASN to the Washington
 */
class ASNHelper
{
    const INVENTORY_LOCATION_ID = 'b6db6051-d444-cc37-c6b7-1d33c234d106';

    const WASHINGTON_API_CREATE_ASN_URL = 'https://als.carbon38.com/api/newAsn';

    /**
     * @var MasonHubHelper
     */
    private $masonhubHelper;

    /**
     * @var Json
     */
    private $json;

    /**
     * ASNHelper constructor.
     *
     * @param MasonHubHelper $masonhubHelper
     * @param Json $json
     */
    public function __construct(
        MasonHubHelper $masonhubHelper,
        Json $json
    ) {
        $this->masonhubHelper = $masonhubHelper;
        $this->json = $json;
    }

    /**
     * Create and Send ASN to the Washington
     *
     * @param array $asnData
     * @param array $itemsData
     * @param SpringboardHelper $springboardHelper
     *
     * @return array
     */
    public function createAndSendAsn(array $asnData, array $itemsData, SpringboardHelper $springboardHelper): array
    {
        // prepare list of items
        $mhItems = [];
        foreach ($itemsData as $item) {
            $qty = 0;
            if (!empty($item['original_qty'])) {
                $qty = (int)$item['original_qty'];
            }
            if ($qty > 0) {
                $mhItems[] = [
                    "customer_sku_id" => $item['sku'],
                    "quantity" => $qty
                ];
            }
        }

        // Get the name of the location where transfer from
        switch ($asnData["from_location_id"]) {
            case $springboardHelper->getHamptonLocationId():
                $shipmentCategory = 'Hampton Transfer';
                $shipperName = "Hampton";
                break;
            case $springboardHelper->getPalisadesLocationId():
                $shipmentCategory = 'Palisades Transfer';
                $shipperName = "Palisades";
                break;
            default:
                $shipmentCategory = "Happy Returns";
                $shipperName = "Unknown";
                break;
        }

        $asnNumber = "{$asnData['asn_po']}";
        // create asn object for the Washington
        $mhAsnData = [
            "customer_identifier" => $asnNumber,
            "customer_purchase_order_id" => "{$asnData['asn_po']}",
            "inventory_location_id" => self::INVENTORY_LOCATION_ID,
            "carrier_information" => [
                "name" => "Unknown",
                "type" => "TL",
                "contact_name" => "Unknown",
                "driver_name" => "Unknown",
                "driver_phone" => "1234567890"
            ],
            "tracking_number" => "{$asnData['tracking_no']}",
            "shipper_name" => "{$shipperName}",
            "shipper_city" => "Unknown",
            "shipper_locale" => "CA",
            "shipper_country" => "US",
            "expected_arrival_date" => date('Y-m-d\TH:i:s\Z', $asnData['estimated_arrival_date']),
            "special_instructions" => "",
            "comments" => "",
            "inbound_shipment_category" => $shipmentCategory,
            "line_items" => $mhItems
        ];

        // send data to the Washington
        try {
            return $this->getAnswer(
                $this->masonhubHelper->sendRequestToWashington(
                    self::WASHINGTON_API_CREATE_ASN_URL,
                    $mhAsnData
                ),
                $asnNumber
            );
        } catch (Throwable $ex) {
            return $this->prepareAnswer(null, $ex->getMessage());
        }
    }

    /**
     * Get List of ManifestIDs (ASN Numbers) from the Masonhub response
     *
     * @param string $response
     * @param string $asnNumber
     * @return array
     *
     * @throws Throwable
     */
    private function getAnswer(string $response, string $asnNumber): array
    {
        // get MH answer
        $answer = $this->json->unserialize($response);
        if (empty($answer['results']) || empty($answer['results'][0])) {
            throw new LocalizedException(
                __("Masonhub didn't respond.")
            );
        }
        // get the first answer from Masonhub (was sent only one ASN)
        $result = $answer['results'][0];

        // error
        if (empty($result['status']) || $result['status'] != 'success') {
            // get error message
            if (empty($result['messages'])) {
                $message = 'Unknown Masonhub error';
            } else {
                $message = $this->json->serialize($result['messages']);
            }
            return $this->prepareAnswer(null, $message);
        }

        // ANS created successfully
        if (empty($result['customer_identifier'])) {
            $customerIdentifier = $asnNumber;
        } else {
            $customerIdentifier = $result['customer_identifier'];
        }
        return $this->prepareAnswer($customerIdentifier, null);
    }

    /**
     * Prepare answer line
     *
     * @param string|null $manifestId
     * @param string|null $message
     *
     * @return array
     */
    private function prepareAnswer(?string $manifestId, ?string $message): array
    {
        return [
            'manifest_id' => $manifestId,
            'message' => $message
        ];
    }
}
