<?php declare(strict_types=1);

namespace C38\Springboard\Helper;

use C38\Springboard\Api\LocationRepositoryInterface;
use C38\Springboard\Service\SpringboardAPIClient;
use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection as ResourceConnection;

/**
 * Class Data
 * Common springboard methods
 * @extends AbstractHelper
 */
class Data extends AbstractHelper
{
    private const HAMPTON_LOCATION_ID = 100042;
    private const PALISADES_LOCATION_ID = 100043;
    private const ONLINE_LOCATION_ID = 100044;

    private const CONFIG_PATH_API_HOST = 'c38_springboard/configuration/api_host';
    private const CONFIG_PATH_API_TOKEN = 'c38_springboard/configuration/api_token';

    /**
     * @var SpringboardAPIClient
     */
    private $springboardAPIClient;

    /**
     * @var LocationRepositoryInterface
     */
    private $locationRepository;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var array
     */
    private $locationStorage;

    /**
     * Class constructor.
     *
     * @param SpringboardAPIClient $springboardAPIClient
     * @param LocationRepositoryInterface $locationRepository
     * @param ResourceConnection $resourceConnection
     * @param Context $context
     */
    public function __construct(
        SpringboardAPIClient $springboardAPIClient,
        LocationRepositoryInterface $locationRepository,
        ResourceConnection $resourceConnection,
        Context $context
    ) {
        $this->springboardAPIClient = $springboardAPIClient;
        $this->locationRepository = $locationRepository;
        $this->resourceConnection = $resourceConnection;
        $this->locationStorage = [];
        parent::__construct($context);

        // setup springboard API client parameters
        $this->springboardAPIClient->setupAPI(
            "https://" . $this->scopeConfig->getValue(self::CONFIG_PATH_API_HOST),
            $this->scopeConfig->getValue(self::CONFIG_PATH_API_TOKEN),
            false
        );
    }

    /**
     * Get Springboard Client
     *
     * @return SpringboardAPIClient
     * @throws Exception
     */
    public function getClient(): SpringboardAPIClient
    {
        return $this->springboardAPIClient;
    }

    /**
     * Sync Springboard orders from this date
     *
     * @return string
     */
    public function getOrdersSyncDate(): string
    {
        return $this->getOffsetDate("-30 days");
    }

    /**
     * Sync Springboard tickets from this date
     *
     * @return string
     */
    public function getTicketsSyncDate(): string
    {
        return $this->getOffsetDate("-30 days");
    }

    /**
     * Sync Springboard products from this date
     *
     * @return string
     */
    public function getProductsSyncDate(): string
    {
        return $this->getOffsetDate("-3 days");
    }

    /**
     * Sync Springboard invoices from this date
     *
     * @return string
     */
    public function getInvoicesSyncDate(): string
    {
        return $this->getOffsetDate("-30 days");
    }

    /**
     * Sync Springboard customers from this date
     *
     * @return string
     */
    public function getCustomersSyncDate(): string
    {
        return $this->getOffsetDate("-1 days");
    }

    /**
     * Sync Springboard inventory from this date
     *
     * @return string
     */
    public function getInventorySyncDate(): string
    {
        return $this->getOffsetDate("-3 days");
    }

    /**
     * Date correction for select data from local storage
     *
     * @param string $date
     * @return string
     */
    public function dateCorrection(string $date): string
    {
        return $this->getOffsetDate($date . " -3 days");
    }

    /**
     * Get Location ID for Hampton store
     *
     * @return string|null
     */
    public function getHamptonLocationId(): ?string
    {
        return $this->getLocationRealIdByPublicId(self::HAMPTON_LOCATION_ID);
    }

    /**
     * Get Location ID for Palisades store
     *
     * @return string|null
     */
    public function getPalisadesLocationId(): ?string
    {
        return $this->getLocationRealIdByPublicId(self::PALISADES_LOCATION_ID);
    }

    /**
     * Get Location ID for Online store
     *
     * @return string|null
     */
    public function getOnlineLocationId(): ?string
    {
        return $this->getLocationRealIdByPublicId(self::ONLINE_LOCATION_ID);
    }

    /**
     * Get List of all Offline (retail) stores
     *
     * @return array
     */
    public function getRetailLocations(): array
    {
        return [
            $this->getHamptonLocationId(),
            $this->getPalisadesLocationId()
        ];
    }

    /**
     * Load list of magento products with selected entity_id ready for Springboard integration
     * with Simple <-> configurable relation
     *
     * @param array $allSpringboardItems
     * @return array
     */
    public function loadMagentoProducts(array $allSpringboardItems): array
    {
        $products = [];
        // get list of all simple products
        $simpleProductsId = [];
        foreach ($allSpringboardItems as $invoiceItem) {
            if (!empty($invoiceItem['item_id'])) {
                $simpleProductsId[] = $invoiceItem['item_id'];
            }
        }

        // get connection to magento database
        $connection = $this->resourceConnection->getConnection(ResourceConnection::DEFAULT_CONNECTION);

        // get simple <-> configurable products relations
        $statement = $connection
            ->select()
            ->from(
                'catalog_product_super_link',
                ['product_id', 'parent_id']
            );
        $relationData = [];
        foreach ($connection->fetchAll($statement) as $relation) {
            $relationData[$relation['product_id']] = $relation['parent_id'];
        }

        // get list products from Magento
        $statement = $connection
            ->select()
            ->from(
                'catalog_product_entity',
                ['entity_id', 'springboard_id']
            )
            ->where('springboard_id IN (?)', array_values($simpleProductsId));

        // prepare product list
        foreach ($connection->fetchAll($statement) as $item) {
            // get configurable product
            $configurableId = '0';
            if (!empty($relationData[$item['entity_id']])) {
                $configurableId = $relationData[$item['entity_id']];
            }

            //
            $line = [
                'springboard_id' => $item['springboard_id'],
                'simple_id' => $item['entity_id'],
                'configurable_id' => $configurableId
            ];
            // add product data to the list
            $products[$item['springboard_id']] = $line;
        }

        // return products list
        return $products;
    }

    /**
     * Get assoc array with data from object
     *
     * @param $object
     * @return array
     */
    public function getData($object): array
    {
        // check data
        if (!$object) {
            return [];
        }
        if (empty($object)) {
            return [];
        }

        // result
        $result = [];

        // add all elements from object
        foreach ($object->getData() as $key => $value) {
            $result[$key] = $value;
        }

        // return result
        return $result;
    }

    /**
     * Check and format float value
     *
     * @param $object
     * @param string $property
     *
     * @return string
     */
    public function formatFloat($object, string $property): string
    {
        $value = 0;

        // check if property exist
        if (property_exists($object, $property) && !empty($object->$property)) {
            $value = $object->$property;
        }

        // format value
        return number_format($value, 4, ".", "");
    }

    /**
     * Get current date with selected offset
     *
     * @param string $offset
     * @return string
     */
    private function getOffsetDate(string $offset): string
    {
        return date("Y-m-d", strtotime($offset));
    }

    /**
     * Get Location ID from Repository
     *
     * @param int $publicId
     * @return string|null
     */
    private function getLocationRealIdByPublicId(int $publicId): ?string
    {
        if (!empty($this->locationStorage[$publicId])) {
            return $this->locationStorage[$publicId];
        }
        $location = $this->locationRepository->loadByLocationNumber($publicId);
        $this->locationStorage[$publicId] = $location->getSpringboardId();
        return $this->locationStorage[$publicId];
    }
}
