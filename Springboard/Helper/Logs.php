<?php declare(strict_types=1);

namespace C38\Springboard\Helper;

use C38\Springboard\Api\Data\LoggerInterfaceFactory;
use C38\Springboard\Api\LoggerRepositoryInterface;

/**
 * Class Logs
 * Springboard Logger Helper class
 */
class Logs
{
    const SPRINGBOARD_LOG_ERROR = 'ERROR';

    const SPRINGBOARD_LOG_INFO = 'INFO';

    const SPRINGBOARD_LOG_WARNING = 'WARNING';

    /**
     * @var LoggerInterfaceFactory
     */
    private $loggerFactory;

    /**
     * @var LoggerRepositoryInterface
     */
    private $loggerRepository;

    /**
     * Logger constructor.
     *
     * @param LoggerInterfaceFactory $loggerFactory
     * @param LoggerRepositoryInterface $loggerRepository
     */
    public function __construct(
        LoggerInterfaceFactory $loggerFactory,
        LoggerRepositoryInterface $loggerRepository
    ) {
        $this->loggerFactory = $loggerFactory;
        $this->loggerRepository = $loggerRepository;
    }

    /**
     * Log Message in the Database
     *
     * @param string $type
     * @param string $message
     * @param string $stackTrace
     * @param string $cronName
     */
    public function log(string $type, string $message, string $stackTrace, string $cronName)
    {
        // create new log record
        $log = $this->loggerFactory->create();

        // add data to the record
        $log->setType($type);
        $log->setMessage($message);
        $log->setStack($stackTrace);
        $log->setCronName($cronName);

        // save record
        $this->loggerRepository->save($log);
    }

    /**
     * Log Error
     *
     * @param string $message
     * @param string $stackTrace
     * @param string $cronName
     */
    public function error(string $message, string $stackTrace, string $cronName)
    {
        $this->log(self::SPRINGBOARD_LOG_ERROR, $message, $stackTrace, $cronName);
    }

    /**
     * Log Warning
     *
     * @param string $message
     * @param string $stackTrace
     * @param string $cronName
     */
    public function warning(string $message, string $stackTrace, string $cronName)
    {
        $this->log(self::SPRINGBOARD_LOG_WARNING, $message, $stackTrace, $cronName);
    }

    /**
     * Log Info
     *
     * @param string $message
     * @param string $cronName
     */
    public function info(string $message, string $cronName)
    {
        $this->log(self::SPRINGBOARD_LOG_INFO, $message, '', $cronName);
    }
}
