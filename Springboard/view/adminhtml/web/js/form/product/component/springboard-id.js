define([
    'Magento_Ui/js/form/element/abstract'
], function (Element) {
    'use strict';

    return Element.extend({
        defaults: {
            imports: {
                productId: '${ $.provider }:data.product.current_product_id'
            },
            listens: {
                productId: 'handleChanges'
            }
        },

        handleChanges: function (value) {
            if (value) {
                this.disable();
            } else {
                this.enable();
            }
        }
    });
});
