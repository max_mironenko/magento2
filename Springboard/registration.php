<?php declare(strict_types=1);

/**
 * @migration-new
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'C38_Springboard',
    __DIR__
);
