<?php declare(strict_types=1);

namespace C38\Springboard\Ui\Product\DataProvider\Form\Modifier;

use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magento\Framework\Stdlib\ArrayManager;
use C38\Springboard\Model\Eav\Product\SpringboardId as SpringboardIdAttribute;

/**
 * Class SpringboardId
 * Add SpringboardId to the Product Form
 */
class SpringboardId implements ModifierInterface
{
    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * SpringboardId constructor.
     * @param ArrayManager $arrayManager
     */
    public function __construct(ArrayManager $arrayManager)
    {
        $this->arrayManager = $arrayManager;
    }

    /**
     * Modify Form Data
     * @param array $data
     *
     * @return array
     */
    public function modifyData(array $data): array
    {
        return $data;
    }

    /**
     * Modify Meta Data
     * @param array $meta
     *
     * @return array
     */
    public function modifyMeta(array $meta): array
    {
        $meta = $this->arrayManager->merge(
            $this->getPath($meta),
            $meta,
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'component' => 'C38_Springboard/js/form/product/component/springboard-id',
                        ],
                    ],
                ],
            ]
        );

        return $meta;
    }

    /**
     * Get Path
     * @param array $meta
     *
     * @return string
     */
    private function getPath(array $meta): string
    {
        return $this->arrayManager->findPath(
            SpringboardIdAttribute::ATTRIBUTE_CODE,
            $meta,
            null,
            'children'
        );
    }
}
