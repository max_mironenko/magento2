<?php declare(strict_types=1);

namespace C38\Springboard\Ui\Product\DataProvider\Form\Modifier;

use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magento\Framework\Stdlib\ArrayManager;
use C38\Springboard\Model\Eav\Product\SpringboardSyncDt as SpringboardSyncDtAttribute;

/**
 * Class SpringboardSyncDt
 * Add Springboard DataSyncDate parameter to the Form
 */
class SpringboardSyncDt implements ModifierInterface
{
    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * SpringboardSyncDt constructor.
     * @param ArrayManager $arrayManager
     */
    public function __construct(ArrayManager $arrayManager)
    {
        $this->arrayManager = $arrayManager;
    }

    /**
     * Modify Data
     * @param array $data
     *
     * @return array
     */
    public function modifyData(array $data): array
    {
        return $data;
    }

    /**
     * Modify Meta
     * @param array $meta
     *
     * @return array
     */
    public function modifyMeta(array $meta): array
    {
        $meta = $this->arrayManager->merge(
            $this->getPath($meta),
            $meta,
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'component' => 'C38_Springboard/js/form/product/component/springboard-sync-dt',
                        ],
                    ],
                ],
            ]
        );

        return $meta;
    }

    /**
     * Get Path
     * @param array $meta
     *
     * @return string
     */
    private function getPath(array $meta): string
    {
        return $this->arrayManager->findPath(
            SpringboardSyncDtAttribute::ATTRIBUTE_CODE,
            $meta,
            null,
            'children'
        );
    }
}
