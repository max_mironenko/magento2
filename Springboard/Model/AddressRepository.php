<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\AddressRepositoryInterface;
use C38\Springboard\Api\Data\AddressInterface;
use C38\Springboard\Api\Data\AddressInterfaceFactory;
use C38\Springboard\Api\Data\AddressSearchResultsInterface;
use C38\Springboard\Api\Data\AddressSearchResultsInterfaceFactory;
use C38\Springboard\Model\ResourceModel\Address;
use C38\Springboard\Model\ResourceModel\Address\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Throwable;

/**
 * Class AddressRepository
 * Springboard address repository
 */
class AddressRepository implements AddressRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var AddressInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var Address
     */
    private $resource;

    /**
     * @var AddressSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * AddressRepository constructor.
     *
     * @param Address $resource
     * @param AddressInterfaceFactory $addressFactory
     * @param CollectionFactory $collectionFactory
     * @param AddressSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        Address $resource,
        AddressInterfaceFactory $addressFactory,
        CollectionFactory $collectionFactory,
        AddressSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $addressFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Address
     *
     * @param AddressInterface $address
     *
     * @return AddressInterface
     * @throws Exception
     */
    public function save(AddressInterface $address): AddressInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($address);
        } catch (Throwable $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $address;
    }

    /**
     * Get Address by id.
     *
     * @param $addressId
     *
     * @return AddressInterface
     * @throws Exception
     */
    public function getById($addressId): AddressInterface
    {
        $address = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($address, $addressId);
        if (!$address->getId()) {
            throw new NoSuchEntityException(__('Address with id "%1" does not exist.', $addressId));
        }

        return $address;
    }

    /**
     * Find Address by id.
     *
     * @param $addressId
     * @return AddressInterface|null
     */
    public function findById($addressId): ?AddressInterface
    {
        $address = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($address, $addressId);
        if (!$address->getId()) {
            return null;
        }
        return $address;
    }

    /**
     * Find Address by Springboard Id.
     *
     * @param $addressId
     * @return AddressInterface
     */
    public function loadByAddressId($addressId): AddressInterface
    {
        $address = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($address, $addressId, AddressInterface::ADDRESS_ID);

        return $address;
    }

    /**
     * Retrieve Address matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|AddressSearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Address
     *
     * @param AddressInterface $address
     *
     * @return bool
     * @throws Exception
     */
    public function delete(AddressInterface $address): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($address);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Address by ID.
     *
     * @param $addressId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById($addressId): bool
    {
        return $this->delete($this->getById($addressId));
    }
}
