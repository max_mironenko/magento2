<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\TicketLineAdjustmentsInterface;
use C38\Springboard\Model\ResourceModel\TicketLineAdjustments as ResourceTicketLineAdjustments;
use C38\Springboard\Model\ResourceModel\TicketLineAdjustments\Collection as ResourceTicketLineAdjustmentsCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class TicketLineAdjustments
 * Springboard ticket line adjustments model
 *
 * @method ResourceTicketLineAdjustments getResource()
 * @method ResourceTicketLineAdjustmentsCollection getCollection()
 * @method ResourceTicketLineAdjustmentsCollection getResourceCollection()
 */
class TicketLineAdjustments extends AbstractModel implements TicketLineAdjustmentsInterface
{
    protected $_eventPrefix = 'c38_springboard_model_ticket_line_adjustments';

    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init(ResourceTicketLineAdjustments::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setId($value): TicketLineAdjustmentsInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get LineId value
     *
     * @return string
     */
    public function getLineId(): string
    {
        return $this->_getData(self::LINE_ID);
    }

    /**
     * Set LineId value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setLineId($value): TicketLineAdjustmentsInterface
    {
        $this->setData(self::LINE_ID, $value);
        return $this;
    }

    /**
     * Get AdjustmentId value
     *
     * @return string
     */
    public function getAdjustmentId(): string
    {
        return $this->_getData(self::ADJUSTMENT_ID);
    }

    /**
     * Set AdjustmentId value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setAdjustmentId($value): TicketLineAdjustmentsInterface
    {
        $this->setData(self::ADJUSTMENT_ID, $value);
        return $this;
    }

    /**
     * Get DeltaPrice value
     *
     * @return float
     */
    public function getDeltaPrice(): float
    {
        return $this->_getData(self::DELTA_PRICE);
    }

    /**
     * Set DeltaPrice value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setDeltaPrice($value): TicketLineAdjustmentsInterface
    {
        $this->setData(self::DELTA_PRICE, $value);
        return $this;
    }

    /**
     * Get Name value
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->_getData(self::NAME);
    }

    /**
     * Set Name value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setName($value): TicketLineAdjustmentsInterface
    {
        $this->setData(self::NAME, $value);
        return $this;
    }

    /**
     * Get Description value
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->_getData(self::DESCRIPTION);
    }

    /**
     * Set Description value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setDescription($value): TicketLineAdjustmentsInterface
    {
        $this->setData(self::DESCRIPTION, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return TicketLineAdjustmentsInterface
     */
    public function setSpringboardSyncDt($value): TicketLineAdjustmentsInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }
}
