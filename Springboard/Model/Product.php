<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\ProductInterface;
use C38\Springboard\Model\ResourceModel\Order as ResourceOrder;
use C38\Springboard\Model\ResourceModel\Product as ResourceProduct;
use C38\Springboard\Model\ResourceModel\Product\Collection as ResourceProductCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Product
 * Springboard product model
 *
 * @method ResourceProduct getResource()
 * @method ResourceProductCollection getCollection()
 * @method ResourceProductCollection getResourceCollection()
 */
class Product extends AbstractModel implements ProductInterface
{
    protected $_eventPrefix = 'c38_springboard_model_product';

    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init(ResourceOrder::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setId($value): ProductInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get ProductId value
     *
     * @return string
     */
    public function getProductId(): string
    {
        return $this->_getData(self::PRODUCT_ID);
    }

    /**
     * Set ProductId value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setProductId($value): ProductInterface
    {
        $this->setData(self::PRODUCT_ID, $value);
        return $this;
    }

    /**
     * Get Sku value
     *
     * @return string
     */
    public function getSku(): string
    {
        return $this->_getData(self::SKU);
    }

    /**
     * Set Sku value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setSku($value): ProductInterface
    {
        $this->setData(self::SKU, $value);
        return $this;
    }

    /**
     * Get ConfigurableSku value
     *
     * @return string
     */
    public function getConfigurableSku(): string
    {
        return $this->_getData(self::CONFIGURABLE_SKU);
    }

    /**
     * Set ConfigurableSku value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setConfigurableSku($value): ProductInterface
    {
        $this->setData(self::CONFIGURABLE_SKU, $value);
        return $this;
    }

    /**
     * Get StyleName value
     *
     * @return string
     */
    public function getStyleName(): string
    {
        return $this->_getData(self::STYLE_NAME);
    }

    /**
     * Set StyleName value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setStyleName($value): ProductInterface
    {
        $this->setData(self::STYLE_NAME, $value);
        return $this;
    }

    /**
     * Get Upc value
     *
     * @return string
     */
    public function getUpc(): string
    {
        return $this->_getData(self::UPC);
    }

    /**
     * Set Upc value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setUpc($value): ProductInterface
    {
        $this->setData(self::UPC, $value);
        return $this;
    }

    /**
     * Get Description value
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->_getData(self::DESCRIPTION);
    }

    /**
     * Set Description value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setDescription($value): ProductInterface
    {
        $this->setData(self::DESCRIPTION, $value);
        return $this;
    }

    /**
     * Get LongDescription value
     *
     * @return string
     */
    public function getLongDescription(): string
    {
        return $this->_getData(self::LONG_DESCRIPTION);
    }

    /**
     * Set LongDescription value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setLongDescription($value): ProductInterface
    {
        $this->setData(self::LONG_DESCRIPTION, $value);
        return $this;
    }

    /**
     * Get Active value
     *
     * @return int
     */
    public function getActive(): int
    {
        return $this->_getData(self::ACTIVE);
    }

    /**
     * Set Active value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setActive($value): ProductInterface
    {
        $this->setData(self::ACTIVE, $value);
        return $this;
    }

    /**
     * Get Cost value
     *
     * @return float
     */
    public function getCost(): float
    {
        return $this->_getData(self::COST);
    }

    /**
     * Set Cost value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setCost($value): ProductInterface
    {
        $this->setData(self::COST, $value);
        return $this;
    }

    /**
     * Get Price value
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->_getData(self::PRICE);
    }

    /**
     * Set Price value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setPrice($value): ProductInterface
    {
        $this->setData(self::PRICE, $value);
        return $this;
    }

    /**
     * Get OriginalPrice value
     *
     * @return float
     */
    public function getOriginalPrice(): float
    {
        return $this->_getData(self::ORIGINAL_PRICE);
    }

    /**
     * Set OriginalPrice value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setOriginalPrice($value): ProductInterface
    {
        $this->setData(self::ORIGINAL_PRICE, $value);
        return $this;
    }

    /**
     * Get PrimaryVendorId value
     *
     * @return int
     */
    public function getPrimaryVendorId(): int
    {
        return $this->_getData(self::PRIMARY_VENDOR_ID);
    }

    /**
     * Set PrimaryVendorId value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setPrimaryVendorId($value): ProductInterface
    {
        $this->setData(self::PRIMARY_VENDOR_ID, $value);
        return $this;
    }

    /**
     * Get PrimaryBarcode value
     *
     * @return string
     */
    public function getPrimaryBarcode(): string
    {
        return $this->_getData(self::PRIMARY_BARCODE);
    }

    /**
     * Set PrimaryBarcode value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setPrimaryBarcode($value): ProductInterface
    {
        $this->setData(self::PRIMARY_BARCODE, $value);
        return $this;
    }

    /**
     * Get PrimaryImageId value
     *
     * @return int
     */
    public function getPrimaryImageId(): int
    {
        return $this->_getData(self::PRIMARY_IMAGE_ID);
    }

    /**
     * Set PrimaryImageId value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setPrimaryImageId($value): ProductInterface
    {
        $this->setData(self::PRIMARY_IMAGE_ID, $value);
        return $this;
    }

    /**
     * Get Weight value
     *
     * @return float
     */
    public function getWeight(): float
    {
        return $this->_getData(self::WEIGHT);
    }

    /**
     * Set Weight value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setWeight($value): ProductInterface
    {
        $this->setData(self::WEIGHT, $value);
        return $this;
    }

    /**
     * Get Width value
     *
     * @return float
     */
    public function getWidth(): float
    {
        return $this->_getData(self::WIDTH);
    }

    /**
     * Set Width value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setWidth($value): ProductInterface
    {
        $this->setData(self::WIDTH, $value);
        return $this;
    }

    /**
     * Get Height value
     *
     * @return float
     */
    public function getHeight(): float
    {
        return $this->_getData(self::HEIGHT);
    }

    /**
     * Set Height value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setHeight($value): ProductInterface
    {
        $this->setData(self::HEIGHT, $value);
        return $this;
    }

    /**
     * Get Depth value
     *
     * @return float
     */
    public function getDepth(): float
    {
        return $this->_getData(self::DEPTH);
    }

    /**
     * Set Depth value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setDepth($value): ProductInterface
    {
        $this->setData(self::DEPTH, $value);
        return $this;
    }

    /**
     * Get Size value
     *
     * @return string
     */
    public function getSize(): string
    {
        return $this->_getData(self::SIZE);
    }

    /**
     * Set Size value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setSize($value): ProductInterface
    {
        $this->setData(self::SIZE, $value);
        return $this;
    }

    /**
     * Get Brand value
     *
     * @return string
     */
    public function getBrand(): string
    {
        return $this->_getData(self::BRAND);
    }

    /**
     * Set Brand value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setBrand($value): ProductInterface
    {
        $this->setData(self::BRAND, $value);
        return $this;
    }

    /**
     * Get Color value
     *
     * @return string
     */
    public function getColor(): string
    {
        return $this->_getData(self::COLOR);
    }

    /**
     * Set Color value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setColor($value): ProductInterface
    {
        $this->setData(self::COLOR, $value);
        return $this;
    }

    /**
     * Get Vendor value
     *
     * @return string
     */
    public function getVendor(): string
    {
        return $this->_getData(self::VENDOR);
    }

    /**
     * Set Vendor value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setVendor($value): ProductInterface
    {
        $this->setData(self::VENDOR, $value);
        return $this;
    }

    /**
     * Get Category value
     *
     * @return string
     */
    public function getCategory(): string
    {
        return $this->_getData(self::CATEGORY);
    }

    /**
     * Set Category value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setCategory($value): ProductInterface
    {
        $this->setData(self::CATEGORY, $value);
        return $this;
    }

    /**
     * Get Department value
     *
     * @return string
     */
    public function getDepartment(): string
    {
        return $this->_getData(self::DEPARTMENT);
    }

    /**
     * Set Department value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setDepartment($value): ProductInterface
    {
        $this->setData(self::DEPARTMENT, $value);
        return $this;
    }

    /**
     * Get Subcategory value
     *
     * @return string
     */
    public function getSubcategory(): string
    {
        return $this->_getData(self::SUBCATEGORY);
    }

    /**
     * Set Subcategory value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setSubcategory($value): ProductInterface
    {
        $this->setData(self::SUBCATEGORY, $value);
        return $this;
    }

    /**
     * Get CreatedAt value
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setCreatedAt($value): ProductInterface
    {
        $this->setData(self::CREATED_AT, $value);
        return $this;
    }

    /**
     * Get UpdatedAt value
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->_getData(self::UPDATED_AT);
    }

    /**
     * Set UpdatedAt value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setUpdatedAt($value): ProductInterface
    {
        $this->setData(self::UPDATED_AT, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return ProductInterface
     */
    public function setSpringboardSyncDt($value): ProductInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }
}
