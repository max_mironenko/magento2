<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\OrderLineInterface;
use C38\Springboard\Api\Data\OrderLineInterfaceFactory;
use C38\Springboard\Api\Data\OrderLineSearchResultsInterfaceFactory;
use C38\Springboard\Api\OrderLineRepositoryInterface;
use C38\Springboard\Model\ResourceModel\OrderLine;
use C38\Springboard\Model\ResourceModel\OrderLine\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class OrderLineRepository
 * Springboard order line repository
 */
class OrderLineRepository implements OrderLineRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var OrderLineInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var OrderLine
     */
    private $resource;

    /**
     * @var OrderLineSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * OrderLineRepository constructor.
     *
     * @param OrderLine $resource
     * @param OrderLineInterfaceFactory $orderLineFactory
     * @param CollectionFactory $collectionFactory
     * @param OrderLineSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        OrderLine $resource,
        OrderLineInterfaceFactory $orderLineFactory,
        CollectionFactory $collectionFactory,
        OrderLineSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $orderLineFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save OrderLine
     *
     * @param OrderLineInterface $orderLine
     *
     * @return OrderLineInterface
     * @throws Exception
     */
    public function save(OrderLineInterface $orderLine): OrderLineInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($orderLine);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $orderLine;
    }

    /**
     * Get OrderLine by id.
     *
     * @param $orderLineId
     *
     * @return OrderLineInterface
     * @throws Exception
     */
    public function getById($orderLineId): OrderLineInterface
    {
        $orderLine = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($orderLine, $orderLineId);
        if (!$orderLine->getId()) {
            throw new NoSuchEntityException(__('OrderLine with id "%1" does not exist.', $orderLineId));
        }

        return $orderLine;
    }

    /**
     * Find OrderLine by id.
     *
     * @param $orderLineId
     * @return OrderLineInterface|null
     */
    public function findById($orderLineId): ?OrderLineInterface
    {
        $orderLine = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($orderLine, $orderLineId);

        if (!$orderLine->getId()) {
            return null;
        }

        return $orderLine;
    }

    /**
     * Find Order by Springboard Order Id.
     *
     * @param $lineId
     * @return OrderLineInterface
     */
    public function loadByLineId($lineId): OrderLineInterface
    {
        $orderLine = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($orderLine, $lineId, OrderLineInterface::LINE_ID);

        return $orderLine;
    }

    /**
     * Retrieve OrderLine matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete OrderLine
     *
     * @param OrderLineInterface $orderLine
     *
     * @return bool
     * @throws Exception
     */
    public function delete(OrderLineInterface $orderLine): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($orderLine);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete OrderLine by ID.
     *
     * @param $orderLineId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById($orderLineId): bool
    {
        return $this->delete($this->getById($orderLineId));
    }
}
