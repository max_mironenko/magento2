<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\CustomerInterface;
use C38\Springboard\Model\ResourceModel\Customer as ResourceCustomer;
use C38\Springboard\Model\ResourceModel\Customer\Collection as ResourceCustomerCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Customer
 * Springboard customer model
 *
 * @method ResourceCustomer getResource()
 * @method ResourceCustomerCollection getCollection()
 * @method ResourceCustomerCollection getResourceCollection()
 */
class Customer extends AbstractModel implements CustomerInterface
{
    protected $_eventPrefix = 'c38_springboard_model_customer';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceCustomer::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setId($value): CustomerInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get CustomerId value
     *
     * @return string|null
     */
    public function getCustomerId(): ?string
    {
        return $this->_getData(self::CUSTOMER_ID);
    }

    /**
     * Set CustomerId value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCustomerId($value): CustomerInterface
    {
        $this->setData(self::CUSTOMER_ID, $value);
        return $this;
    }

    /**
     * Get Email value
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->_getData(self::EMAIL);
    }

    /**
     * Set Email value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setEmail($value): CustomerInterface
    {
        $this->setData(self::EMAIL, $value);
        return $this;
    }

    /**
     * Get FirstName value
     *
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->_getData(self::FIRST_NAME);
    }

    /**
     * Set FirstName value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setFirstName($value): CustomerInterface
    {
        $this->setData(self::FIRST_NAME, $value);
        return $this;
    }

    /**
     * Get LastName value
     *
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->_getData(self::LAST_NAME);
    }

    /**
     * Set LastName value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setLastName($value): CustomerInterface
    {
        $this->setData(self::LAST_NAME, $value);
        return $this;
    }

    /**
     * Get IsActive value
     *
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->_getData(self::IS_ACTIVE);
    }

    /**
     * Set IsActive value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setIsActive($value): CustomerInterface
    {
        $this->setData(self::IS_ACTIVE, $value);
        return $this;
    }

    /**
     * Get Birthday value
     *
     * @return string|null
     */
    public function getBirthday(): ?string
    {
        return $this->_getData(self::BIRTHDAY);
    }

    /**
     * Set Birthday value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setBirthday($value): CustomerInterface
    {
        $this->setData(self::BIRTHDAY, $value);
        return $this;
    }

    /**
     * Get Type value
     *
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->_getData(self::TYPE);
    }

    /**
     * Set Type value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setType($value): CustomerInterface
    {
        $this->setData(self::TYPE, $value);
        return $this;
    }

    /**
     * Get AddressId value
     *
     * @return string|null
     */
    public function getAddressId(): ?string
    {
        return $this->_getData(self::ADDRESS_ID);
    }

    /**
     * Set AddressId value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setAddressId($value): CustomerInterface
    {
        $this->setData(self::ADDRESS_ID, $value);
        return $this;
    }

    /**
     * Get Line1 value
     *
     * @return string|null
     */
    public function getLine1(): ?string
    {
        return $this->_getData(self::LINE1);
    }

    /**
     * Set Line1 value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setLine1($value): CustomerInterface
    {
        $this->setData(self::LINE1, $value);
        return $this;
    }

    /**
     * Get Line2 value
     *
     * @return string|null
     */
    public function getLine2(): ?string
    {
        return $this->_getData(self::LINE2);
    }

    /**
     * Set Line2 value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setLine2($value): CustomerInterface
    {
        $this->setData(self::LINE2, $value);
        return $this;
    }

    /**
     * Get City value
     *
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->_getData(self::CITY);
    }

    /**
     * Set City value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCity($value): CustomerInterface
    {
        $this->setData(self::CITY, $value);
        return $this;
    }

    /**
     * Get State value
     *
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->_getData(self::STATE);
    }

    /**
     * Set State value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setState($value): CustomerInterface
    {
        $this->setData(self::STATE, $value);
        return $this;
    }

    /**
     * Get Phone value
     *
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->_getData(self::PHONE);
    }

    /**
     * Set Phone value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setPhone($value): CustomerInterface
    {
        $this->setData(self::PHONE, $value);
        return $this;
    }

    /**
     * Get Country value
     *
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->_getData(self::COUNTRY);
    }

    /**
     * Set Country value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCountry($value): CustomerInterface
    {
        $this->setData(self::COUNTRY, $value);
        return $this;
    }

    /**
     * Get PostalCode value
     *
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->_getData(self::POSTAL_CODE);
    }

    /**
     * Set PostalCode value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setPostalCode($value): CustomerInterface
    {
        $this->setData(self::POSTAL_CODE, $value);
        return $this;
    }

    /**
     * Get FullAddress value
     *
     * @return string|null
     */
    public function getFullAddress(): ?string
    {
        return $this->_getData(self::FULL_ADDRESS);
    }

    /**
     * Set FullAddress value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setFullAddress($value): CustomerInterface
    {
        $this->setData(self::FULL_ADDRESS, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string|null
     */
    public function getSpringboardSyncDt(): ?string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setSpringboardSyncDt($value): CustomerInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }

    /**
     * Get LifetimeSpend value
     *
     * @return string|null
     */
    public function getLifetimeSpend(): ?string
    {
        return $this->_getData(self::LIFETIME_SPEND);
    }

    /**
     * Set LifetimeSpend value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setLifetimeSpend($value): CustomerInterface
    {
        $this->setData(self::LIFETIME_SPEND, $value);
        return $this;
    }

    /**
     * Get LocationsShopped value
     *
     * @return string|null
     */
    public function getLocationsShopped(): ?string
    {
        return $this->_getData(self::LOCATIONS_SHOPPED);
    }

    /**
     * Set LocationsShopped value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setLocationsShopped($value): CustomerInterface
    {
        $this->setData(self::LOCATIONS_SHOPPED, $value);
        return $this;
    }

    /**
     * Get BrandsPurchased value
     *
     * @return string|null
     */
    public function getBrandsPurchased(): ?string
    {
        return $this->_getData(self::BRANDS_PURCHASED);
    }

    /**
     * Set BrandsPurchased value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setBrandsPurchased($value): CustomerInterface
    {
        $this->setData(self::BRANDS_PURCHASED, $value);
        return $this;
    }

    /**
     * Get CustomerSince value
     *
     * @return string|null
     */
    public function getCustomerSince(): ?string
    {
        return $this->_getData(self::CUSTOMER_SINCE);
    }

    /**
     * Set CustomerSince value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCustomerSince($value): CustomerInterface
    {
        $this->setData(self::CUSTOMER_SINCE, $value);
        return $this;
    }

    /**
     * Get Vip value
     *
     * @return string|null
     */
    public function getVip(): ?string
    {
        return $this->_getData(self::VIP);
    }

    /**
     * Set Vip value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setVip($value): CustomerInterface
    {
        $this->setData(self::VIP, $value);
        return $this;
    }

    /**
     * Get Ambassador value
     *
     * @return string|null
     */
    public function getAmbassador(): ?string
    {
        return $this->_getData(self::AMBASSADOR);
    }

    /**
     * Set Ambassador value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setAmbassador($value): CustomerInterface
    {
        $this->setData(self::AMBASSADOR, $value);
        return $this;
    }

    /**
     * Get CardCheck value
     *
     * @return string|null
     */
    public function getCardCheck(): ?string
    {
        return $this->_getData(self::CARD_CHECK);
    }

    /**
     * Set CardCheck value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCardCheck($value): CustomerInterface
    {
        $this->setData(self::CARD_CHECK, $value);
        return $this;
    }

    /**
     * Get CreatedAt value
     *
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setCreatedAt($value): CustomerInterface
    {
        $this->setData(self::CREATED_AT, $value);
        return $this;
    }

    /**
     * Get StoreCredit value
     *
     * @return string|null
     */
    public function getStoreCredit(): ?string
    {
        return $this->_getData(self::STORE_CREDIT);
    }

    /**
     * Set StoreCredit value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setStoreCredit($value): CustomerInterface
    {
        $this->setData(self::STORE_CREDIT, $value);
        return $this;
    }

    /**
     * Get UpdatedAt value
     *
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->_getData(self::UPDATED_AT);
    }

    /**
     * Set UpdatedAt value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setUpdatedAt($value): CustomerInterface
    {
        $this->setData(self::UPDATED_AT, $value);
        return $this;
    }

    /**
     * Get IsSynced value
     *
     * @return int|null
     */
    public function getIsSynced(): ?int
    {
        return $this->_getData(self::IS_SYNCED);
    }

    /**
     * Set IsSynced value
     *
     * @param $value
     * @return CustomerInterface
     */
    public function setIsSynced($value): CustomerInterface
    {
        $this->setData(self::IS_SYNCED, $value);
        return $this;
    }
}
