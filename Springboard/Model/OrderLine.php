<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\OrderLineInterface;
use C38\Springboard\Model\ResourceModel\OrderLine as ResourceOrderLine;
use C38\Springboard\Model\ResourceModel\OrderLine\Collection as ResourceOrderLineCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class OrderLine
 * Springboard order line model
 *
 * @method ResourceOrderLine getResource()
 * @method ResourceOrderLineCollection getCollection()
 * @method ResourceOrderLineCollection getResourceCollection()
 */
class OrderLine extends AbstractModel implements OrderLineInterface
{
    protected $_eventPrefix = 'c38_springboard_model_order_line';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceOrderLine::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setId($value): OrderLineInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get LineId value
     *
     * @return string
     */
    public function getLineId(): string
    {
        return $this->_getData(self::LINE_ID);
    }

    /**
     * Set LineId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setLineId($value): OrderLineInterface
    {
        $this->setData(self::LINE_ID, $value);
        return $this;
    }

    /**
     * Get OrderId value
     *
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->_getData(self::ORDER_ID);
    }

    /**
     * Set OrderId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setOrderId($value): OrderLineInterface
    {
        $this->setData(self::ORDER_ID, $value);
        return $this;
    }

    /**
     * Get ItemId value
     *
     * @return string
     */
    public function getItemId(): string
    {
        return $this->_getData(self::ITEM_ID);
    }

    /**
     * Set ItemId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setItemId($value): OrderLineInterface
    {
        $this->setData(self::ITEM_ID, $value);
        return $this;
    }

    /**
     * Get Description value
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->_getData(self::DESCRIPTION);
    }

    /**
     * Set Description value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setDescription($value): OrderLineInterface
    {
        $this->setData(self::DESCRIPTION, $value);
        return $this;
    }

    /**
     * Get LocationId value
     *
     * @return string
     */
    public function getLocationId(): string
    {
        return $this->_getData(self::LOCATION_ID);
    }

    /**
     * Set LocationId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setLocationId($value): OrderLineInterface
    {
        $this->setData(self::LOCATION_ID, $value);
        return $this;
    }

    /**
     * Get Qty value
     *
     * @return float
     */
    public function getQty(): float
    {
        return $this->_getData(self::QTY);
    }

    /**
     * Set Qty value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setQty($value): OrderLineInterface
    {
        $this->setData(self::QTY, $value);
        return $this;
    }

    /**
     * Get UnitPrice value
     *
     * @return float
     */
    public function getUnitPrice(): float
    {
        return $this->_getData(self::UNIT_PRICE);
    }

    /**
     * Set UnitPrice value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setUnitPrice($value): OrderLineInterface
    {
        $this->setData(self::UNIT_PRICE, $value);
        return $this;
    }

    /**
     * Get TotalPrice value
     *
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->_getData(self::TOTAL_PRICE);
    }

    /**
     * Set TotalPrice value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setTotalPrice($value): OrderLineInterface
    {
        $this->setData(self::TOTAL_PRICE, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setSpringboardSyncDt($value): OrderLineInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }

    /**
     * Get Vendor value
     *
     * @return string
     */
    public function getVendor(): string
    {
        return $this->_getData(self::VENDOR);
    }

    /**
     * Set Vendor value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setVendor($value): OrderLineInterface
    {
        $this->setData(self::VENDOR, $value);
        return $this;
    }

    /**
     * Get ConfigurableProductId value
     *
     * @return string
     */
    public function getConfigurableProductId(): string
    {
        return $this->_getData(self::CONFIGURABLE_PRODUCT_ID);
    }

    /**
     * Set ConfigurableProductId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setConfigurableProductId($value): OrderLineInterface
    {
        $this->setData(self::CONFIGURABLE_PRODUCT_ID, $value);
        return $this;
    }

    /**
     * Get SimpleProductId value
     *
     * @return string
     */
    public function getSimpleProductId(): string
    {
        return $this->_getData(self::SIMPLE_PRODUCT_ID);
    }

    /**
     * Set SimpleProductId value
     *
     * @param $value
     * @return OrderLineInterface
     */
    public function setSimpleProductId($value): OrderLineInterface
    {
        $this->setData(self::SIMPLE_PRODUCT_ID, $value);
        return $this;
    }
}
