<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\SalesPlanInterface;
use C38\Springboard\Model\ResourceModel\SalesPlan as ResourceSalesPlan;
use C38\Springboard\Model\ResourceModel\SalesPlan\Collection as ResourceSalesPlanCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class SalesPlan
 * Springboard sales plan model
 *
 * @method ResourceSalesPlan getResource()
 * @method ResourceSalesPlanCollection getCollection()
 * @method ResourceSalesPlanCollection getResourceCollection()
 */
class SalesPlan extends AbstractModel implements SalesPlanInterface
{
    protected $_eventPrefix = 'c38_springboard_model_sales_plan';

    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init(ResourceSalesPlan::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setId($value): SalesPlanInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get PublicId value
     *
     * @return string
     */
    public function getPublicId(): string
    {
        return $this->_getData(self::PUBLIC_ID);
    }

    /**
     * Set PublicId value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setPublicId($value): SalesPlanInterface
    {
        $this->setData(self::PUBLIC_ID, $value);
        return $this;
    }

    /**
     * Get LocationId value
     *
     * @return string
     */
    public function getLocationId(): string
    {
        return $this->_getData(self::LOCATION_ID);
    }

    /**
     * Set LocationId value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setLocationId($value): SalesPlanInterface
    {
        $this->setData(self::LOCATION_ID, $value);
        return $this;
    }

    /**
     * Get Amount value
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->_getData(self::AMOUNT);
    }

    /**
     * Set Amount value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setAmount($value): SalesPlanInterface
    {
        $this->setData(self::AMOUNT, $value);
        return $this;
    }

    /**
     * Get Date value
     *
     * @return string
     */
    public function getDate(): string
    {
        return $this->_getData(self::DATE);
    }

    /**
     * Set Date value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setDate($value): SalesPlanInterface
    {
        $this->setData(self::DATE, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return SalesPlanInterface
     */
    public function setSpringboardSyncDt($value): SalesPlanInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }
}
