<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\InventoryInterface;
use C38\Springboard\Api\Data\InventoryInterfaceFactory;
use C38\Springboard\Api\Data\InventorySearchResultsInterface;
use C38\Springboard\Api\Data\InventorySearchResultsInterfaceFactory;
use C38\Springboard\Api\InventoryRepositoryInterface;
use C38\Springboard\Model\ResourceModel\Inventory;
use C38\Springboard\Model\ResourceModel\Inventory\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class InventoryRepository
 * Springboard inventory repository
 */
class InventoryRepository implements InventoryRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var InventoryInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var Inventory
     */
    private $resource;

    /**
     * @var InventorySearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * InventoryRepository constructor.
     *
     * @param Inventory $resource
     * @param InventoryInterfaceFactory $inventoryFactory
     * @param CollectionFactory $collectionFactory
     * @param InventorySearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        Inventory $resource,
        InventoryInterfaceFactory $inventoryFactory,
        CollectionFactory $collectionFactory,
        InventorySearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $inventoryFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Inventory
     *
     * @param InventoryInterface $inventory
     *
     * @return InventoryInterface
     * @throws Exception
     */
    public function save(InventoryInterface $inventory): InventoryInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($inventory);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $inventory;
    }

    /**
     * Get Inventory by id.
     *
     * @param $inventoryId
     *
     * @return InventoryInterface
     * @throws Exception
     */
    public function getById($inventoryId): InventoryInterface
    {
        $inventory = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($inventory, $inventoryId);
        if (!$inventory->getId()) {
            throw new NoSuchEntityException(__('Inventory with id "%1" does not exist.', $inventoryId));
        }

        return $inventory;
    }

    /**
     * Retrieve Inventory matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|InventorySearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }
}
