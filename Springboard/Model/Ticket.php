<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\TicketInterface;
use C38\Springboard\Model\ResourceModel\Ticket as ResourceTicket;
use C38\Springboard\Model\ResourceModel\Ticket\Collection as ResourceTicketCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Ticket
 * Springboard ticket model
 *
 * @method ResourceTicket getResource()
 * @method ResourceTicketCollection getCollection()
 * @method ResourceTicketCollection getResourceCollection()
 */
class Ticket extends AbstractModel implements TicketInterface
{
    protected $_eventPrefix = 'c38_springboard_model_ticket';

    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init(ResourceTicket::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setId($value): TicketInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get TicketId value
     *
     * @return string
     */
    public function getTicketId(): string
    {
        return $this->_getData(self::TICKET_ID);
    }

    /**
     * Set TicketId value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTicketId($value): TicketInterface
    {
        $this->setData(self::TICKET_ID, $value);
        return $this;
    }

    /**
     * Get Type value
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->_getData(self::TYPE);
    }

    /**
     * Set Type value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setType($value): TicketInterface
    {
        $this->setData(self::TYPE, $value);
        return $this;
    }

    /**
     * Get Status value
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->_getData(self::STATUS);
    }

    /**
     * Set Status value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setStatus($value): TicketInterface
    {
        $this->setData(self::STATUS, $value);
        return $this;
    }

    /**
     * Get CreatedDate value
     *
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->_getData(self::CREATED_DATE);
    }

    /**
     * Set CreatedDate value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setCreatedDate($value): TicketInterface
    {
        $this->setData(self::CREATED_DATE, $value);
        return $this;
    }

    /**
     * Get CustomerId value
     *
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->_getData(self::CUSTOMER_ID);
    }

    /**
     * Set CustomerId value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setCustomerId($value): TicketInterface
    {
        $this->setData(self::CUSTOMER_ID, $value);
        return $this;
    }

    /**
     * Get SalesRep value
     *
     * @return string
     */
    public function getSalesRep(): string
    {
        return $this->_getData(self::SALES_REP);
    }

    /**
     * Set SalesRep value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setSalesRep($value): TicketInterface
    {
        $this->setData(self::SALES_REP, $value);
        return $this;
    }

    /**
     * Get StationId value
     *
     * @return string
     */
    public function getStationId(): string
    {
        return $this->_getData(self::STATION_ID);
    }

    /**
     * Set StationId value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setStationId($value): TicketInterface
    {
        $this->setData(self::STATION_ID, $value);
        return $this;
    }

    /**
     * Get SourceLocationId value
     *
     * @return string
     */
    public function getSourceLocationId(): string
    {
        return $this->_getData(self::SOURCE_LOCATION_ID);
    }

    /**
     * Set SourceLocationId value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setSourceLocationId($value): TicketInterface
    {
        $this->setData(self::SOURCE_LOCATION_ID, $value);
        return $this;
    }

    /**
     * Get TotalItemQty value
     *
     * @return float
     */
    public function getTotalItemQty(): float
    {
        return $this->_getData(self::TOTAL_ITEM_QTY);
    }

    /**
     * Set TotalItemQty value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTotalItemQty($value): TicketInterface
    {
        $this->setData(self::TOTAL_ITEM_QTY, $value);
        return $this;
    }

    /**
     * Get Total value
     *
     * @return float
     */
    public function getTotal(): float
    {
        return $this->_getData(self::TOTAL);
    }

    /**
     * Set Total value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTotal($value): TicketInterface
    {
        $this->setData(self::TOTAL, $value);
        return $this;
    }

    /**
     * Get TotalPaid value
     *
     * @return float
     */
    public function getTotalPaid(): float
    {
        return $this->_getData(self::TOTAL_PAID);
    }

    /**
     * Set TotalPaid value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTotalPaid($value): TicketInterface
    {
        $this->setData(self::TOTAL_PAID, $value);
        return $this;
    }

    /**
     * Get OriginalSubtotal value
     *
     * @return float
     */
    public function getOriginalSubtotal(): float
    {
        return $this->_getData(self::ORIGINAL_SUBTOTAL);
    }

    /**
     * Set OriginalSubtotal value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setOriginalSubtotal($value): TicketInterface
    {
        $this->setData(self::ORIGINAL_SUBTOTAL, $value);
        return $this;
    }

    /**
     * Get TotalDiscounts value
     *
     * @return float
     */
    public function getTotalDiscounts(): float
    {
        return $this->_getData(self::TOTAL_DISCOUNTS);
    }

    /**
     * Set TotalDiscounts value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTotalDiscounts($value): TicketInterface
    {
        $this->setData(self::TOTAL_DISCOUNTS, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setSpringboardSyncDt($value): TicketInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }

    /**
     * Get TjRetries value
     *
     * @return string
     */
    public function getTjRetries(): string
    {
        return $this->_getData(self::TJ_RETRIES);
    }

    /**
     * Set TjRetries value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTjRetries($value): TicketInterface
    {
        $this->setData(self::TJ_RETRIES, $value);
        return $this;
    }

    /**
     * Get UpdatedAt value
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->_getData(self::UPDATED_AT);
    }

    /**
     * Set UpdatedAt value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setUpdatedAt($value): TicketInterface
    {
        $this->setData(self::UPDATED_AT, $value);
        return $this;
    }

    /**
     * Get TjSalestaxSyncDate value
     *
     * @return string
     */
    public function getTjSalestaxSyncDate(): string
    {
        return $this->_getData(self::TJ_SALESTAX_SYNC_DATE);
    }

    /**
     * Set TjSalestaxSyncDate value
     *
     * @param $value
     * @return TicketInterface
     */
    public function setTjSalestaxSyncDate($value): TicketInterface
    {
        $this->setData(self::TJ_SALESTAX_SYNC_DATE, $value);
        return $this;
    }
}
