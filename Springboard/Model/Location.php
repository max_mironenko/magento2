<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\LocationInterface;
use C38\Springboard\Model\ResourceModel\Location as ResourceLocation;
use C38\Springboard\Model\ResourceModel\Location\Collection as ResourceLocationCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Location
 * Springboard location model
 *
 * @method ResourceLocation getResource()
 * @method ResourceLocationCollection getCollection()
 * @method ResourceLocationCollection getResourceCollection()
 */
class Location extends AbstractModel implements LocationInterface
{
    protected $_eventPrefix = 'c38_springboard_model_location';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceLocation::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setId($value): LocationInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get SpringboardId value
     *
     * @return string|null
     */
    public function getSpringboardId(): ?string
    {
        return $this->_getData(self::SPRINGBOARD_ID);
    }

    /**
     * Set SpringboardId value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setSpringboardId($value): LocationInterface
    {
        $this->setData(self::SPRINGBOARD_ID, $value);
        return $this;
    }

    /**
     * Get LocationNumber value
     *
     * @return string
     */
    public function getLocationNumber(): string
    {
        return $this->_getData(self::LOCATION_NUMBER);
    }

    /**
     * Set LocationNumber value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setLocationNumber($value): LocationInterface
    {
        $this->setData(self::LOCATION_NUMBER, $value);
        return $this;
    }

    /**
     * Get Name value
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->_getData(self::NAME);
    }

    /**
     * Set Name value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setName($value): LocationInterface
    {
        $this->setData(self::NAME, $value);
        return $this;
    }

    /**
     * Get TimezoneIdentifierId value
     *
     * @return int
     */
    public function getTimezoneIdentifierId(): int
    {
        return $this->_getData(self::TIMEZONE_IDENTIFIER_ID);
    }

    /**
     * Set TimezoneIdentifierId value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setTimezoneIdentifierId($value): LocationInterface
    {
        $this->setData(self::TIMEZONE_IDENTIFIER_ID, $value);
        return $this;
    }

    /**
     * Get IsActive value
     *
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->_getData(self::IS_ACTIVE);
    }

    /**
     * Set IsActive value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setIsActive($value): LocationInterface
    {
        $this->setData(self::IS_ACTIVE, $value);
        return $this;
    }

    /**
     * Get IsDeleted value
     *
     * @return int
     */
    public function getIsDeleted(): int
    {
        return $this->_getData(self::IS_DELETED);
    }

    /**
     * Set IsDeleted value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setIsDeleted($value): LocationInterface
    {
        $this->setData(self::IS_DELETED, $value);
        return $this;
    }

    /**
     * Get CreatedAt value
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setCreatedAt($value): LocationInterface
    {
        $this->setData(self::CREATED_AT, $value);
        return $this;
    }

    /**
     * Get UpdatedAt value
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->_getData(self::UPDATED_AT);
    }

    /**
     * Set UpdatedAt value
     *
     * @param mixed $value
     * @return LocationInterface
     */
    public function setUpdatedAt($value): LocationInterface
    {
        $this->setData(self::UPDATED_AT, $value);
        return $this;
    }
}
