<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\VendorInterface;
use C38\Springboard\Api\Data\VendorInterfaceFactory;
use C38\Springboard\Api\Data\VendorSearchResultsInterface;
use C38\Springboard\Api\Data\VendorSearchResultsInterfaceFactory;
use C38\Springboard\Api\VendorRepositoryInterface;
use C38\Springboard\Model\ResourceModel\Vendor;
use C38\Springboard\Model\ResourceModel\Vendor\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class VendorRepository
 * Springboard vendor repository
 */
class VendorRepository implements VendorRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var VendorInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var Vendor
     */
    private $resource;

    /**
     * @var VendorSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * VendorRepository constructor.
     *
     * @param Vendor $resource
     * @param VendorInterfaceFactory $vendorFactory
     * @param CollectionFactory $collectionFactory
     * @param VendorSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        Vendor $resource,
        VendorInterfaceFactory $vendorFactory,
        CollectionFactory $collectionFactory,
        VendorSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $vendorFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Vendor
     *
     * @param VendorInterface $vendor
     *
     * @return VendorInterface
     * @throws Exception
     */
    public function save(VendorInterface $vendor): VendorInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($vendor);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $vendor;
    }

    /**
     * Get Vendor by id.
     *
     * @param int $vendorId
     *
     * @return VendorInterface
     * @throws Exception
     */
    public function getById(int $vendorId): VendorInterface
    {
        $vendor = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($vendor, $vendorId);
        if (!$vendor->getId()) {
            throw new NoSuchEntityException(__('Vendor with id "%1" does not exist.', $vendorId));
        }

        return $vendor;
    }

    /**
     * Find Vendor by id.
     *
     * @param int $vendorId
     * @return VendorInterface|null
     */
    public function findById(int $vendorId): ?VendorInterface
    {
        $vendor = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($vendor, $vendorId);

        if (!$vendor->getId()) {
            return null;
        }

        return $vendor;
    }

    /**
     * Find Vendor by Vendor Id.
     *
     * @param string $vendorId
     * @return VendorInterface
     */
    public function loadByVendorId(string $vendorId): VendorInterface
    {
        $vendor = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($vendor, $vendorId, VendorInterface::VENDOR_ID);

        return $vendor;
    }

    /**
     * Retrieve Vendor matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|VendorSearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Vendor
     *
     * @param VendorInterface $vendor
     *
     * @return bool
     * @throws Exception
     */
    public function delete(VendorInterface $vendor): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($vendor);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Vendor by ID.
     *
     * @param int $vendorId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById(int $vendorId): bool
    {
        return $this->delete($this->getById($vendorId));
    }
}
