<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\InvoiceLineInterface;
use C38\Springboard\Api\Data\InvoiceLineInterfaceFactory;
use C38\Springboard\Api\Data\InvoiceLineSearchResultsInterface;
use C38\Springboard\Api\Data\InvoiceLineSearchResultsInterfaceFactory;
use C38\Springboard\Api\InvoiceLineRepositoryInterface;
use C38\Springboard\Model\ResourceModel\InvoiceLine;
use C38\Springboard\Model\ResourceModel\InvoiceLine\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class InvoiceLineRepository
 * Springboard invoice line repository
 */
class InvoiceLineRepository implements InvoiceLineRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var InvoiceLineInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var InvoiceLine
     */
    private $resource;

    /**
     * @var InvoiceLineSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * InvoiceLineRepository constructor.
     *
     * @param InvoiceLine $resource
     * @param InvoiceLineInterfaceFactory $invoiceLineFactory
     * @param CollectionFactory $collectionFactory
     * @param InvoiceLineSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        InvoiceLine $resource,
        InvoiceLineInterfaceFactory $invoiceLineFactory,
        CollectionFactory $collectionFactory,
        InvoiceLineSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $invoiceLineFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save InvoiceLine
     *
     * @param InvoiceLineInterface $invoiceLine
     *
     * @return InvoiceLineInterface
     * @throws Exception
     */
    public function save(InvoiceLineInterface $invoiceLine): InvoiceLineInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($invoiceLine);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $invoiceLine;
    }

    /**
     * Get InvoiceLine by id.
     *
     * @param $invoiceLineId
     *
     * @return InvoiceLineInterface
     * @throws Exception
     */
    public function getById($invoiceLineId): InvoiceLineInterface
    {
        $invoiceLine = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($invoiceLine, $invoiceLineId);
        if (!$invoiceLine->getId()) {
            throw new NoSuchEntityException(__('InvoiceLine with id "%1" does not exist.', $invoiceLineId));
        }

        return $invoiceLine;
    }

    /**
     * Find InvoiceLine by id.
     *
     * @param $invoiceLineId
     * @return InvoiceLineInterface|null
     */
    public function findById($invoiceLineId): ?InvoiceLineInterface
    {
        $invoiceLine = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($invoiceLine, $invoiceLineId);

        if (!$invoiceLine->getId()) {
            return null;
        }

        return $invoiceLine;
    }

    /**
     * Find InvoiceLine by Line Id.
     *
     * @param $lineId
     * @return InvoiceLineInterface
     */
    public function loadByLineId($lineId): InvoiceLineInterface
    {
        $invoiceLine = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($invoiceLine, $lineId, InvoiceLineInterface::LINE_ID);

        return $invoiceLine;
    }

    /**
     * Retrieve InvoiceLine matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|InvoiceLineSearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete InvoiceLine
     *
     * @param InvoiceLineInterface $invoiceLine
     *
     * @return bool
     * @throws Exception
     */
    public function delete(InvoiceLineInterface $invoiceLine): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($invoiceLine);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete InvoiceLine by ID.
     *
     * @param $invoiceLineId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById($invoiceLineId): bool
    {
        return $this->delete($this->getById($invoiceLineId));
    }
}
