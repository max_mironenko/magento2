<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\VendorInterface;
use C38\Springboard\Model\ResourceModel\Vendor as ResourceVendor;
use C38\Springboard\Model\ResourceModel\Vendor\Collection as ResourceVendorCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Vendor
 * Springboard vendor model
 *
 * @method ResourceVendor getResource()
 * @method ResourceVendorCollection getCollection()
 * @method ResourceVendorCollection getResourceCollection()
 */
class Vendor extends AbstractModel implements VendorInterface
{
    protected $_eventPrefix = 'c38_springboard_model_vendor';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceVendor::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return VendorInterface
     */
    public function setId($value): VendorInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get VendorId value
     *
     * @return string
     */
    public function getVendorId(): string
    {
        return $this->_getData(self::VENDOR_ID);
    }

    /**
     * Set VendorId value
     *
     * @param $value
     * @return VendorInterface
     */
    public function setVendorId($value): VendorInterface
    {
        $this->setData(self::VENDOR_ID, $value);
        return $this;
    }

    /**
     * Get DesignerId value
     *
     * @return string|null
     */
    public function getDesignerId(): ?string
    {
        return $this->_getData(self::DESIGNER_ID);
    }

    /**
     * Set DesignerId value
     *
     * @param $value
     * @return VendorInterface
     */
    public function setDesignerId($value): VendorInterface
    {
        $this->setData(self::DESIGNER_ID, $value);
        return $this;
    }

    /**
     * Get Name value
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->_getData(self::NAME);
    }

    /**
     * Set Name value
     *
     * @param $value
     * @return VendorInterface
     */
    public function setName($value): VendorInterface
    {
        $this->setData(self::NAME, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return VendorInterface
     */
    public function setSpringboardSyncDt($value): VendorInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }
}
