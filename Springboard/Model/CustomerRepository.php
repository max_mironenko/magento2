<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Core\Model\MysqlQueryExecutor;
use C38\Springboard\Api\CustomerRepositoryInterface;
use C38\Springboard\Api\Data\CustomerInterface;
use C38\Springboard\Api\Data\CustomerInterfaceFactory;
use C38\Springboard\Api\Data\CustomerSearchResultsInterface;
use C38\Springboard\Api\Data\CustomerSearchResultsInterfaceFactory;
use C38\Springboard\Model\ResourceModel\Customer;
use C38\Springboard\Model\ResourceModel\Customer\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class CustomerRepository
 * Springboard customer repository
 */
class CustomerRepository implements CustomerRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var CustomerInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var Customer
     */
    private $resource;

    /**
     * @var CustomerSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var MysqlQueryExecutor
     */
    private $mysqlQueryExecutor;

    /**
     * CustomerRepository constructor.
     *
     * @param Customer $resource
     * @param CustomerInterfaceFactory $customerFactory
     * @param CollectionFactory $collectionFactory
     * @param CustomerSearchResultsInterfaceFactory $searchResultFactory
     * @param MysqlQueryExecutor $mysqlQueryExecutor
     */
    public function __construct(
        Customer $resource,
        CustomerInterfaceFactory $customerFactory,
        CollectionFactory $collectionFactory,
        CustomerSearchResultsInterfaceFactory $searchResultFactory,
        MysqlQueryExecutor $mysqlQueryExecutor
    ) {
        $this->resource = $resource;
        $this->entityFactory = $customerFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->mysqlQueryExecutor = $mysqlQueryExecutor;
    }

    /**
     * Save Customer
     *
     * @param CustomerInterface $customer
     *
     * @return CustomerInterface
     * @throws Exception
     */
    public function save(CustomerInterface $customer): CustomerInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($customer);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $customer;
    }

    /**
     * Get Customer by id.
     *
     * @param int $customerId
     *
     * @return CustomerInterface
     * @throws Exception
     */
    public function getById(int $customerId): CustomerInterface
    {
        $customer = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($customer, $customerId);
        if (!$customer->getId()) {
            throw new NoSuchEntityException(__('Customer with id "%1" does not exist.', $customerId));
        }

        return $customer;
    }

    /**
     * Find Customer by id.
     *
     * @param int $customerId
     * @return CustomerInterface|null
     */
    public function findById(int $customerId): ?CustomerInterface
    {
        $customer = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($customer, $customerId);

        if (!$customer->getId()) {
            return null;
        }

        return $customer;
    }

    /**
     * Find Springboard Customer by customer_id.
     *
     * @param int $customerId
     * @return CustomerInterface
     */
    public function loadByCustomerId(int $customerId): CustomerInterface
    {
        $customer = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($customer, $customerId, CustomerInterface::CUSTOMER_ID);

        return $customer;
    }

    /**
     * Retrieve Customer matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|CustomerSearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Customer
     *
     * @param CustomerInterface $customer
     *
     * @return bool
     * @throws Exception
     */
    public function delete(CustomerInterface $customer): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($customer);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Customer by ID.
     *
     * @param int $customerId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById(int $customerId): bool
    {
        return $this->delete($this->getById($customerId));
    }

    /**
     * Update Springboard customers with Magento data
     */
    public function updateCustomers()
    {
        // update springboard customers with magento data using DB procedure
        $this->mysqlQueryExecutor->execute("CALL update_springboard_customers();");
    }
}
