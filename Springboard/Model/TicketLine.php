<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\TicketLineInterface;
use C38\Springboard\Model\ResourceModel\TicketLine as ResourceTicketLine;
use C38\Springboard\Model\ResourceModel\TicketLine\Collection as ResourceTicketLineCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class TicketLine
 * Springboard ticket line model
 *
 * @method ResourceTicketLine getResource()
 * @method ResourceTicketLineCollection getCollection()
 * @method ResourceTicketLineCollection getResourceCollection()
 */
class TicketLine extends AbstractModel implements TicketLineInterface
{
    protected $_eventPrefix = 'c38_springboard_model_ticket_line';

    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init(ResourceTicketLine::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setId($value): TicketLineInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get LineId value
     *
     * @return string
     */
    public function getLineId(): string
    {
        return $this->_getData(self::LINE_ID);
    }

    /**
     * Set LineId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setLineId($value): TicketLineInterface
    {
        $this->setData(self::LINE_ID, $value);
        return $this;
    }

    /**
     * Get TicketId value
     *
     * @return string
     */
    public function getTicketId(): string
    {
        return $this->_getData(self::TICKET_ID);
    }

    /**
     * Set TicketId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setTicketId($value): TicketLineInterface
    {
        $this->setData(self::TICKET_ID, $value);
        return $this;
    }

    /**
     * Get ItemId value
     *
     * @return string
     */
    public function getItemId(): string
    {
        return $this->_getData(self::ITEM_ID);
    }

    /**
     * Set ItemId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setItemId($value): TicketLineInterface
    {
        $this->setData(self::ITEM_ID, $value);
        return $this;
    }

    /**
     * Get Type value
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->_getData(self::TYPE);
    }

    /**
     * Set Type value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setType($value): TicketLineInterface
    {
        $this->setData(self::TYPE, $value);
        return $this;
    }

    /**
     * Get Description value
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->_getData(self::DESCRIPTION);
    }

    /**
     * Set Description value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setDescription($value): TicketLineInterface
    {
        $this->setData(self::DESCRIPTION, $value);
        return $this;
    }

    /**
     * Get TaxRuleId value
     *
     * @return string
     */
    public function getTaxRuleId(): string
    {
        return $this->_getData(self::TAX_RULE_ID);
    }

    /**
     * Set TaxRuleId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setTaxRuleId($value): TicketLineInterface
    {
        $this->setData(self::TAX_RULE_ID, $value);
        return $this;
    }

    /**
     * Get Value value
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->_getData(self::VALUE);
    }

    /**
     * Set Value value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setValue($value): TicketLineInterface
    {
        $this->setData(self::VALUE, $value);
        return $this;
    }

    /**
     * Get Qty value
     *
     * @return float
     */
    public function getQty(): float
    {
        return $this->_getData(self::QTY);
    }

    /**
     * Set Qty value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setQty($value): TicketLineInterface
    {
        $this->setData(self::QTY, $value);
        return $this;
    }

    /**
     * Get OriginalUnitPrice value
     *
     * @return float
     */
    public function getOriginalUnitPrice(): float
    {
        return $this->_getData(self::ORIGINAL_UNIT_PRICE);
    }

    /**
     * Set OriginalUnitPrice value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setOriginalUnitPrice($value): TicketLineInterface
    {
        $this->setData(self::ORIGINAL_UNIT_PRICE, $value);
        return $this;
    }

    /**
     * Get UnitPrice value
     *
     * @return float
     */
    public function getUnitPrice(): float
    {
        return $this->_getData(self::UNIT_PRICE);
    }

    /**
     * Set UnitPrice value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setUnitPrice($value): TicketLineInterface
    {
        $this->setData(self::UNIT_PRICE, $value);
        return $this;
    }

    /**
     * Get UnitCost value
     *
     * @return float
     */
    public function getUnitCost(): float
    {
        return $this->_getData(self::UNIT_COST);
    }

    /**
     * Set UnitCost value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setUnitCost($value): TicketLineInterface
    {
        $this->setData(self::UNIT_COST, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setSpringboardSyncDt($value): TicketLineInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }

    /**
     * Get Vendor value
     *
     * @return string
     */
    public function getVendor(): string
    {
        return $this->_getData(self::VENDOR);
    }

    /**
     * Set Vendor value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setVendor($value): TicketLineInterface
    {
        $this->setData(self::VENDOR, $value);
        return $this;
    }

    /**
     * Get ItemLineId value
     *
     * @return string
     */
    public function getItemLineId(): string
    {
        return $this->_getData(self::ITEM_LINE_ID);
    }

    /**
     * Set ItemLineId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setItemLineId($value): TicketLineInterface
    {
        $this->setData(self::ITEM_LINE_ID, $value);
        return $this;
    }

    /**
     * Get ConfigurableProductId value
     *
     * @return string
     */
    public function getConfigurableProductId(): string
    {
        return $this->_getData(self::CONFIGURABLE_PRODUCT_ID);
    }

    /**
     * Set ConfigurableProductId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setConfigurableProductId($value): TicketLineInterface
    {
        $this->setData(self::CONFIGURABLE_PRODUCT_ID, $value);
        return $this;
    }

    /**
     * Get SimpleProductId value
     *
     * @return string
     */
    public function getSimpleProductId(): string
    {
        return $this->_getData(self::SIMPLE_PRODUCT_ID);
    }

    /**
     * Set SimpleProductId value
     *
     * @param $value
     * @return TicketLineInterface
     */
    public function setSimpleProductId($value): TicketLineInterface
    {
        $this->setData(self::SIMPLE_PRODUCT_ID, $value);
        return $this;
    }
}
