<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\TicketLineInterface;
use C38\Springboard\Api\Data\TicketLineInterfaceFactory;
use C38\Springboard\Api\Data\TicketLineSearchResultsInterfaceFactory;
use C38\Springboard\Api\TicketLineRepositoryInterface;
use C38\Springboard\Model\ResourceModel\TicketLine;
use C38\Springboard\Model\ResourceModel\TicketLine\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class TicketLineRepository
 * Springboard ticket line repository
 */
class TicketLineRepository implements TicketLineRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var TicketLineInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var TicketLine
     */
    private $resource;

    /**
     * @var TicketLineSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * TicketLineRepository constructor.
     *
     * @param TicketLine $resource
     * @param TicketLineInterfaceFactory $ticketLineFactory
     * @param CollectionFactory $collectionFactory
     * @param TicketLineSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        TicketLine $resource,
        TicketLineInterfaceFactory $ticketLineFactory,
        CollectionFactory $collectionFactory,
        TicketLineSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $ticketLineFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Ticket Line
     *
     * @param TicketLineInterface $ticketLine
     *
     * @return TicketLineInterface
     * @throws Exception
     */
    public function save(TicketLineInterface $ticketLine): TicketLineInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($ticketLine);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $ticketLine;
    }

    /**
     * Get Ticket Line by id.
     *
     * @param $ticketLineId
     *
     * @return TicketLineInterface
     * @throws Exception
     */
    public function getById($ticketLineId): TicketLineInterface
    {
        $ticketLine = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($ticketLine, $ticketLineId);
        if (!$ticketLine->getId()) {
            throw new NoSuchEntityException(__('TicketLine with id "%1" does not exist.', $ticketLineId));
        }

        return $ticketLine;
    }

    /**
     * Find Ticket Line by id.
     *
     * @param $ticketLineId
     *
     * @return TicketLineInterface|null
     */
    public function findById($ticketLineId): ?TicketLineInterface
    {
        $ticketLine = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($ticketLine, $ticketLineId);

        if (!$ticketLine->getId()) {
            return null;
        }

        return $ticketLine;
    }

    /**
     * Retrieve Ticket Line matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Ticket Line
     *
     * @param TicketLineInterface $ticketLine
     *
     * @return bool
     * @throws Exception
     */
    public function delete(TicketLineInterface $ticketLine): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($ticketLine);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Ticket Line by ID.
     *
     * @param $ticketLineId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById($ticketLineId): bool
    {
        return $this->delete($this->getById($ticketLineId));
    }
}
