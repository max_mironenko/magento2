<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\TransferInterface;
use C38\Springboard\Model\ResourceModel\Transfer as ResourceTransfer;
use C38\Springboard\Model\ResourceModel\Transfer\Collection as ResourceTransferCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Transfer
 * Springboard transfer model
 *
 * @method ResourceTransfer getResource()
 * @method ResourceTransferCollection getCollection()
 * @method ResourceTransferCollection getResourceCollection()
 */
class Transfer extends AbstractModel implements TransferInterface
{
    protected $_eventPrefix = 'c38_springboard_model_transfer';

    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init(ResourceTransfer::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setId($value): TransferInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get TransferId value
     *
     * @return string
     */
    public function getTransferId(): string
    {
        return $this->_getData(self::TRANSFER_ID);
    }

    /**
     * Set TransferId value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTransferId($value): TransferInterface
    {
        $this->setData(self::TRANSFER_ID, $value);
        return $this;
    }

    /**
     * Get Status value
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->_getData(self::STATUS);
    }

    /**
     * Set Status value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setStatus($value): TransferInterface
    {
        $this->setData(self::STATUS, $value);
        return $this;
    }

    /**
     * Get TransferNumber value
     *
     * @return string
     */
    public function getTransferNumber(): string
    {
        return $this->_getData(self::TRANSFER_NUMBER);
    }

    /**
     * Set TransferNumber value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTransferNumber($value): TransferInterface
    {
        $this->setData(self::TRANSFER_NUMBER, $value);
        return $this;
    }

    /**
     * Get FromLocationId value
     *
     * @return string
     */
    public function getFromLocationId(): string
    {
        return $this->_getData(self::FROM_LOCATION_ID);
    }

    /**
     * Set FromLocationId value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setFromLocationId($value): TransferInterface
    {
        $this->setData(self::FROM_LOCATION_ID, $value);
        return $this;
    }

    /**
     * Get ToLocationId value
     *
     * @return int
     */
    public function getToLocationId(): int
    {
        return $this->_getData(self::TO_LOCATION_ID);
    }

    /**
     * Set ToLocationId value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setToLocationId($value): TransferInterface
    {
        $this->setData(self::TO_LOCATION_ID, $value);
        return $this;
    }

    /**
     * Get CreatedDate value
     *
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->_getData(self::CREATED_DATE);
    }

    /**
     * Set CreatedDate value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setCreatedDate($value): TransferInterface
    {
        $this->setData(self::CREATED_DATE, $value);
        return $this;
    }
    /**
     * Get TotalQtyShipped value
     *
     * @return float
     */
    public function getTotalQtyShipped(): float
    {
        return $this->_getData(self::TOTAL_QTY_SHIPPED);
    }

    /**
     * Set TotalQtyShipped value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTotalQtyShipped($value): TransferInterface
    {
        $this->setData(self::TOTAL_QTY_SHIPPED, $value);
        return $this;
    }

    /**
     * Get TotalQtyReceived value
     *
     * @return float
     */
    public function getTotalQtyReceived(): float
    {
        return $this->_getData(self::TOTAL_QTY_RECEIVED);
    }

    /**
     * Set TotalQtyReceived value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTotalQtyReceived($value): TransferInterface
    {
        $this->setData(self::TOTAL_QTY_RECEIVED, $value);
        return $this;
    }

    /**
     * Get TotalQtyLost value
     *
     * @return float
     */
    public function getTotalQtyLost(): float
    {
        return $this->_getData(self::TOTAL_QTY_LOST);
    }

    /**
     * Set TotalQtyLost value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTotalQtyLost($value): TransferInterface
    {
        $this->setData(self::TOTAL_QTY_LOST, $value);
        return $this;
    }

    /**
     * Get TotalQtyRequested value
     *
     * @return float
     */
    public function getTotalQtyRequested(): float
    {
        return $this->_getData(self::TOTAL_QTY_REQUESTED);
    }

    /**
     * Set TotalQtyRequested value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTotalQtyRequested($value): TransferInterface
    {
        $this->setData(self::TOTAL_QTY_REQUESTED, $value);
        return $this;
    }

    /**
     * Get TotalQtyDiscrepant value
     *
     * @return float
     */
    public function getTotalQtyDiscrepant(): float
    {
        return $this->_getData(self::TOTAL_QTY_DISCREPANT);
    }

    /**
     * Set TotalQtyDiscrepant value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setTotalQtyDiscrepant($value): TransferInterface
    {
        $this->setData(self::TOTAL_QTY_DISCREPANT, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setSpringboardSyncDt($value): TransferInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }

    /**
     * Get ManifestId value
     *
     * @return string
     */
    public function getManifestId(): string
    {
        return $this->_getData(self::MANIFEST_ID);
    }

    /**
     * Set ManifestId value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setManifestId($value): TransferInterface
    {
        $this->setData(self::MANIFEST_ID, $value);
        return $this;
    }

    /**
     * Get MHConvertDate value
     *
     * @return string
     */
    public function getMHConvertDate(): string
    {
        return $this->_getData(self::MH_CONVERT_DATE);
    }

    /**
     * Set MHConvertDate value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setMHConvertDate($value): TransferInterface
    {
        $this->setData(self::MH_CONVERT_DATE, $value);
        return $this;
    }

    /**
     * Get MHConvertError value
     *
     * @return string
     */
    public function getMHConvertError(): string
    {
        return $this->_getData(self::MH_CONVERT_ERROR);
    }

    /**
     * Set MHConvertError value
     *
     * @param $value
     * @return TransferInterface
     */
    public function setMHConvertError($value): TransferInterface
    {
        $this->setData(self::MH_CONVERT_ERROR, $value);
        return $this;
    }
}
