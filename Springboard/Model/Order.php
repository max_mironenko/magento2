<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\OrderInterface;
use C38\Springboard\Model\ResourceModel\Order as ResourceOrder;
use C38\Springboard\Model\ResourceModel\Order\Collection as ResourceOrderCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Order
 * Springboard order model
 *
 * @method ResourceOrder getResource()
 * @method ResourceOrderCollection getCollection()
 * @method ResourceOrderCollection getResourceCollection()
 */
class Order extends AbstractModel implements OrderInterface
{
    protected $_eventPrefix = 'c38_springboard_model_order';

    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init(ResourceOrder::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setId($value): OrderInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get OrderId value
     *
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->_getData(self::ORDER_ID);
    }

    /**
     * Set OrderId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setOrderId($value): OrderInterface
    {
        $this->setData(self::ORDER_ID, $value);
        return $this;
    }

    /**
     * Get MagentoOrderId value
     *
     * @return string
     */
    public function getMagentoOrderId(): string
    {
        return $this->_getData(self::MAGENTO_ORDER_ID);
    }

    /**
     * Set MagentoOrderId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setMagentoOrderId($value): OrderInterface
    {
        $this->setData(self::MAGENTO_ORDER_ID, $value);
        return $this;
    }

    /**
     * Get Status value
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->_getData(self::STATUS);
    }

    /**
     * Set Status value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setStatus($value): OrderInterface
    {
        $this->setData(self::STATUS, $value);
        return $this;
    }

    /**
     * Get CreatedDate value
     *
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->_getData(self::CREATED_DATE);
    }

    /**
     * Set CreatedDate value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setCreatedDate($value): OrderInterface
    {
        $this->setData(self::CREATED_DATE, $value);
        return $this;
    }

    /**
     * Get CustomerId value
     *
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->_getData(self::CUSTOMER_ID);
    }

    /**
     * Set CustomerId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setCustomerId($value): OrderInterface
    {
        $this->setData(self::CUSTOMER_ID, $value);
        return $this;
    }

    /**
     * Get StationId value
     *
     * @return string
     */
    public function getStationId(): string
    {
        return $this->_getData(self::STATION_ID);
    }

    /**
     * Set StationId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setStationId($value): OrderInterface
    {
        $this->setData(self::STATION_ID, $value);
        return $this;
    }

    /**
     * Get ShipFrom value
     *
     * @return string
     */
    public function getShipFrom(): string
    {
        return $this->_getData(self::SHIP_FROM);
    }

    /**
     * Set ShipFrom value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setShipFrom($value): OrderInterface
    {
        $this->setData(self::SHIP_FROM, $value);
        return $this;
    }

    /**
     * Get BillingAddressId value
     *
     * @return string
     */
    public function getBillingAddressId(): string
    {
        return $this->_getData(self::BILLING_ADDRESS_ID);
    }

    /**
     * Set BillingAddressId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setBillingAddressId($value): OrderInterface
    {
        $this->setData(self::BILLING_ADDRESS_ID, $value);
        return $this;
    }

    /**
     * Get ShippingAddressId value
     *
     * @return string
     */
    public function getShippingAddressId(): string
    {
        return $this->_getData(self::SHIPPING_ADDRESS_ID);
    }

    /**
     * Set ShippingAddressId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setShippingAddressId($value): OrderInterface
    {
        $this->setData(self::SHIPPING_ADDRESS_ID, $value);
        return $this;
    }

    /**
     * Get SalesRep value
     *
     * @return string
     */
    public function getSalesRep(): string
    {
        return $this->_getData(self::SALES_REP);
    }

    /**
     * Set SalesRep value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setSalesRep($value): OrderInterface
    {
        $this->setData(self::SALES_REP, $value);
        return $this;
    }

    /**
     * Get ShippingMethodId value
     *
     * @return string
     */
    public function getShippingMethodId(): string
    {
        return $this->_getData(self::SHIPPING_METHOD_ID);
    }

    /**
     * Set ShippingMethodId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setShippingMethodId($value): OrderInterface
    {
        $this->setData(self::SHIPPING_METHOD_ID, $value);
        return $this;
    }

    /**
     * Get TotalQty value
     *
     * @return float
     */
    public function getTotalQty(): float
    {
        return $this->_getData(self::TOTAL_QTY);
    }

    /**
     * Set TotalQty value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setTotalQty($value): OrderInterface
    {
        $this->setData(self::TOTAL_QTY, $value);
        return $this;
    }

    /**
     * Get Total value
     *
     * @return float
     */
    public function getTotal(): float
    {
        return $this->_getData(self::TOTAL);
    }

    /**
     * Set Total value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setTotal($value): OrderInterface
    {
        $this->setData(self::TOTAL, $value);
        return $this;
    }

    /**
     * Get Balance value
     *
     * @return float
     */
    public function getBalance(): float
    {
        return $this->_getData(self::BALANCE);
    }

    /**
     * Set Balance value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setBalance($value): OrderInterface
    {
        $this->setData(self::BALANCE, $value);
        return $this;
    }

    /**
     * Get TotalPaid value
     *
     * @return float
     */
    public function getTotalPaid(): float
    {
        return $this->_getData(self::TOTAL_PAID);
    }

    /**
     * Set TotalPaid value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setTotalPaid($value): OrderInterface
    {
        $this->setData(self::TOTAL_PAID, $value);
        return $this;
    }

    /**
     * Get ShippingCharge value
     *
     * @return float
     */
    public function getShippingCharge(): float
    {
        return $this->_getData(self::SHIPPING_CHARGE);
    }

    /**
     * Set ShippingCharge value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setShippingCharge($value): OrderInterface
    {
        $this->setData(self::SHIPPING_CHARGE, $value);
        return $this;
    }

    /**
     * Get Subtotal value
     *
     * @return float
     */
    public function getSubtotal(): float
    {
        return $this->_getData(self::SUBTOTAL);
    }

    /**
     * Set Subtotal value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setSubtotal($value): OrderInterface
    {
        $this->setData(self::SUBTOTAL, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setSpringboardSyncDt($value): OrderInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }

    /**
     * Get MagentoCustomerId value
     *
     * @return string
     */
    public function getMagentoCustomerId(): string
    {
        return $this->_getData(self::MAGENTO_CUSTOMER_ID);
    }

    /**
     * Set MagentoCustomerId value
     *
     * @param $value
     * @return OrderInterface
     */
    public function setMagentoCustomerId($value): OrderInterface
    {
        $this->setData(self::MAGENTO_CUSTOMER_ID, $value);
        return $this;
    }
}
