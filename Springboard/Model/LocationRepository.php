<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\LocationInterface;
use C38\Springboard\Api\Data\LocationInterfaceFactory;
use C38\Springboard\Api\Data\LocationSearchResultsInterface;
use C38\Springboard\Api\Data\LocationSearchResultsInterfaceFactory;
use C38\Springboard\Api\LocationRepositoryInterface;
use C38\Springboard\Model\ResourceModel\Location;
use C38\Springboard\Model\ResourceModel\Location\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class LocationRepository
 * Springboard location repository
 */
class LocationRepository implements LocationRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var LocationInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var Location
     */
    private $resource;

    /**
     * @var LocationSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * LocationRepository constructor.
     *
     * @param Location $resource
     * @param LocationInterfaceFactory $locationFactory
     * @param CollectionFactory $collectionFactory
     * @param LocationSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        Location $resource,
        LocationInterfaceFactory $locationFactory,
        CollectionFactory $collectionFactory,
        LocationSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $locationFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Location
     *
     * @param LocationInterface $location
     *
     * @return LocationInterface
     * @throws Exception
     */
    public function save(LocationInterface $location): LocationInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($location);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $location;
    }

    /**
     * Get Location by id.
     *
     * @param int $locationId
     *
     * @return LocationInterface
     * @throws Exception
     */
    public function getById(int $locationId): LocationInterface
    {
        $location = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($location, $locationId);
        if (!$location->getId()) {
            throw new NoSuchEntityException(__('Location with id "%1" does not exist.', $locationId));
        }

        return $location;
    }

    /**
     * Find Location by id.
     *
     * @param int $locationId
     * @return LocationInterface|null
     */
    public function findById(int $locationId): ?LocationInterface
    {
        $location = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($location, $locationId);

        if (!$location->getId()) {
            return null;
        }

        return $location;
    }

    /**
     * Find Location by location number.
     *
     * @param int $number
     * @return LocationInterface
     */
    public function loadByLocationNumber(int $number): LocationInterface
    {
        $location = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($location, $number, LocationInterface::LOCATION_NUMBER);

        return $location;
    }

    /**
     * Find Location by Springboard Id.
     *
     * @param string $springboardId
     * @return LocationInterface
     */
    public function loadBySpringboardId(string $springboardId): LocationInterface
    {
        $location = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($location, $springboardId, LocationInterface::SPRINGBOARD_ID);

        return $location;
    }

    /**
     * Retrieve Location matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|LocationSearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Location
     *
     * @param LocationInterface $location
     *
     * @return bool
     * @throws Exception
     */
    public function delete(LocationInterface $location): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($location);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Location by ID.
     *
     * @param int $locationId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById(int $locationId): bool
    {
        return $this->delete($this->getById($locationId));
    }
}
