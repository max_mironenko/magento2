<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\ProductInterface;
use C38\Springboard\Api\Data\ProductInterfaceFactory;
use C38\Springboard\Api\Data\ProductSearchResultsInterfaceFactory;
use C38\Springboard\Api\ProductRepositoryInterface;
use C38\Springboard\Model\ResourceModel\Product;
use C38\Springboard\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Throwable;

/**
 * Class ProductRepository
 * Springboard product repository
 */
class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var ProductInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var Product
     */
    private $resource;

    /**
     * @var ProductSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * Product Repository constructor.
     *
     * @param Product $resource
     * @param ProductInterfaceFactory $productFactory
     * @param CollectionFactory $collectionFactory
     * @param ProductSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        Product $resource,
        ProductInterfaceFactory $productFactory,
        CollectionFactory $collectionFactory,
        ProductSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $productFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Product
     *
     * @param ProductInterface $product
     *
     * @return ProductInterface
     * @throws Throwable
     */
    public function save(ProductInterface $product): ProductInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($product);
        } catch (Throwable $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $product;
    }

    /**
     * Get Product by id.
     *
     * @param $productId
     *
     * @return ProductInterface
     * @throws Throwable
     */
    public function getById($productId): ProductInterface
    {
        $product = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($product, $product);
        if (!$product->getId()) {
            throw new NoSuchEntityException(__('Product with id "%1" does not exist.', $product));
        }

        return $product;
    }

    /**
     * Find Product by id.
     *
     * @param $productId
     * @return ProductInterface|null
     */
    public function findById($productId): ?ProductInterface
    {
        $product = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($product, $productId);

        if (!$product->getId()) {
            return null;
        }

        return $product;
    }

    /**
     * Find Product by Springboard Product Id.
     *
     * @param $productId
     * @return ProductInterface
     */
    public function loadByProductId($productId): ProductInterface
    {
        $product = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($product, $productId, ProductInterface::PRODUCT_ID);

        return $product;
    }

    /**
     * Retrieve Products matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Throwable
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Product
     *
     * @param ProductInterface $product
     *
     * @return bool
     * @throws Throwable
     */
    public function delete(ProductInterface $product): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($product);
        } catch (Throwable $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Product by ID.
     *
     * @param $productId
     *
     * @return bool
     * @throws Throwable
     */
    public function deleteById($productId): bool
    {
        return $this->delete($this->getById($productId));
    }
}
