<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\LoggerInterface;
use C38\Springboard\Model\ResourceModel\Logger as ResourceModel;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Logger
 * Springboard logger model
 */
class Logger extends AbstractModel implements LoggerInterface
{
    protected $_eventPrefix = 'c38_springboard_model_logger';

    /**
     * Class construct.
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * Get ID
     *
     * @return string | null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set ID
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setId($value): LoggerInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get Message Type
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->_getData(self::TYPE);
    }

    /**
     * Set Message Type
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setType($value): LoggerInterface
    {
        $this->setData(self::TYPE, $value);
        return $this;
    }

    /**
     * Get Log Message
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->_getData(self::MESSAGE);
    }

    /**
     * Set Log Message
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setMessage($value): LoggerInterface
    {
        $this->setData(self::MESSAGE, $value);
        return $this;
    }

    /**
     * Get Error Stack Trace
     *
     * @return string
     */
    public function getStack(): string
    {
        return $this->_getData(self::STACK);
    }

    /**
     * Set Error Stack Trace
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setStack($value): LoggerInterface
    {
        $this->setData(self::STACK, $value);
        return $this;
    }

    /**
     * Get Cron Name
     *
     * @return string
     */
    public function getCronName(): string
    {
        return $this->_getData(self::CRON_NAME);
    }

    /**
     * Set Cron Name
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setCronName($value): LoggerInterface
    {
        $this->setData(self::CRON_NAME, $value);
        return $this;
    }

    /**
     * Get Created At
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set Created At
     *
     * @param $value
     * @return LoggerInterface
     */
    public function setCreatedAt($value): LoggerInterface
    {
        $this->setData(self::CREATED_AT, $value);
        return $this;
    }
}
