<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\AddressInterface;
use C38\Springboard\Model\ResourceModel\Address as ResourceAddress;
use C38\Springboard\Model\ResourceModel\Address\Collection as ResourceAddressCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Address
 * Springboard address model
 *
 * @method ResourceAddress getResource()
 * @method ResourceAddressCollection getCollection()
 * @method ResourceAddressCollection getResourceCollection()
 */
class Address extends AbstractModel implements AddressInterface
{
    protected $_eventPrefix = 'c38_springboard_model_address';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceAddress::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setId($value): AddressInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get AddressId value
     *
     * @return string
     */
    public function getAddressId(): string
    {
        return $this->_getData(self::ADDRESS_ID);
    }

    /**
     * Set AddressId value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setAddressId($value): AddressInterface
    {
        $this->setData(self::ADDRESS_ID, $value);
        return $this;
    }

    /**
     * Get FirstName value
     *
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->_getData(self::FIRST_NAME);
    }

    /**
     * Set FirstName value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setFirstName($value): AddressInterface
    {
        $this->setData(self::FIRST_NAME, $value);
        return $this;
    }

    /**
     * Get LastName value
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->_getData(self::LAST_NAME);
    }

    /**
     * Set LastName value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setLastName($value): AddressInterface
    {
        $this->setData(self::LAST_NAME, $value);
        return $this;
    }

    /**
     * Get Line1 value
     *
     * @return string
     */
    public function getLine1(): string
    {
        return $this->_getData(self::LINE1);
    }

    /**
     * Set Line1 value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setLine1($value): AddressInterface
    {
        $this->setData(self::LINE1, $value);
        return $this;
    }

    /**
     * Get Line2 value
     *
     * @return string
     */
    public function getLine2(): string
    {
        return $this->_getData(self::LINE2);
    }

    /**
     * Set Line2 value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setLine2($value): AddressInterface
    {
        $this->setData(self::LINE2, $value);
        return $this;
    }

    /**
     * Get City value
     *
     * @return string
     */
    public function getCity(): string
    {
        return $this->_getData(self::CITY);
    }

    /**
     * Set City value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setCity($value): AddressInterface
    {
        $this->setData(self::CITY, $value);
        return $this;
    }

    /**
     * Get State value
     *
     * @return string
     */
    public function getState(): string
    {
        return $this->_getData(self::STATE);
    }

    /**
     * Set State value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setState($value): AddressInterface
    {
        $this->setData(self::STATE, $value);
        return $this;
    }

    /**
     * Get Country value
     *
     * @return string
     */
    public function getCountry(): string
    {
        return $this->_getData(self::COUNTRY);
    }

    /**
     * Set Country value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setCountry($value): AddressInterface
    {
        $this->setData(self::COUNTRY, $value);
        return $this;
    }

    /**
     * Get PostalCode value
     *
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->_getData(self::POSTAL_CODE);
    }

    /**
     * Set PostalCode value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setPostalCode($value): AddressInterface
    {
        $this->setData(self::POSTAL_CODE, $value);
        return $this;
    }

    /**
     * Get Phone value
     *
     * @return string
     */
    public function getPhone(): string
    {
        return $this->_getData(self::PHONE);
    }

    /**
     * Set Phone value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setPhone($value): AddressInterface
    {
        $this->setData(self::PHONE, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return AddressInterface
     */
    public function setSpringboardSyncDt($value): AddressInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }
}
