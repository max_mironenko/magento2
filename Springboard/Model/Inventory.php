<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\InventoryInterface;
use C38\Springboard\Model\ResourceModel\Inventory as ResourceInventory;
use C38\Springboard\Model\ResourceModel\Inventory\Collection as ResourceInventoryCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Inventory
 * Springboard inventory model
 *
 * @method ResourceInventory getResource()
 * @method ResourceInventoryCollection getCollection()
 * @method ResourceInventoryCollection getResourceCollection()
 */
class Inventory extends AbstractModel implements InventoryInterface
{
    protected $_eventPrefix = 'c38_springboard_model_inventory';

    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init(ResourceInventory::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return InventoryInterface
     */
    public function setId($value): InventoryInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get ProductId value
     *
     * @return string
     */
    public function getProductId(): string
    {
        return $this->_getData(self::PRODUCT_ID);
    }

    /**
     * Set ProductId value
     *
     * @param $value
     * @return InventoryInterface
     */
    public function setProductId($value): InventoryInterface
    {
        $this->setData(self::PRODUCT_ID, $value);
        return $this;
    }

    /**
     * Get LocationId value
     *
     * @return string
     */
    public function getLocationId(): string
    {
        return $this->_getData(self::LOCATION_ID);
    }

    /**
     * Set LocationId value
     *
     * @param $value
     * @return InventoryInterface
     */
    public function setLocationId($value): InventoryInterface
    {
        $this->setData(self::LOCATION_ID, $value);
        return $this;
    }

    /**
     * Get QTY value
     *
     * @return string
     */
    public function getQty(): string
    {
        return $this->_getData(self::QTY);
    }

    /**
     * Set QTY value
     *
     * @param $value
     * @return InventoryInterface
     */
    public function setQty($value): InventoryInterface
    {
        $this->setData(self::QTY, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return InventoryInterface
     */
    public function setSpringboardSyncDt($value): InventoryInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }
}
