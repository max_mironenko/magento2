<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\TransferLineInterface;
use C38\Springboard\Api\Data\TransferLineInterfaceFactory;
use C38\Springboard\Api\Data\TransferLineSearchResultsInterfaceFactory;
use C38\Springboard\Api\TransferLineRepositoryInterface;
use C38\Springboard\Model\ResourceModel\TransferLine;
use C38\Springboard\Model\ResourceModel\TransferLine\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Transfer Line
 * Springboard transfer line objects repository
 */
class TransferLineRepository implements TransferLineRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var TransferLineInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var TransferLine
     */
    private $resource;

    /**
     * @var TransferLineSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * TicketLineRepository constructor.
     *
     * @param TransferLine $resource
     * @param TransferLineInterfaceFactory $transferLineFactory
     * @param CollectionFactory $collectionFactory
     * @param TransferLineSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        TransferLine $resource,
        TransferLineInterfaceFactory $transferLineFactory,
        CollectionFactory $collectionFactory,
        TransferLineSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $transferLineFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Transfer Line
     *
     * @param TransferLineInterface $transferLine
     *
     * @return TransferLineInterface
     * @throws CouldNotSaveException
     */
    public function save(TransferLineInterface $transferLine): TransferLineInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($transferLine);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $transferLine;
    }

    /**
     * Get Transfer line by id.
     *
     * @param $transferLineId
     *
     * @return TransferLineInterface
     * @throws NoSuchEntityException
     */
    public function getById($transferLineId): TransferLineInterface
    {
        $transferLine = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($transferLine, $transferLineId);
        if (!$transferLine->getId()) {
            throw new NoSuchEntityException(__('TicketLine with id "%1" does not exist.', $transferLineId));
        }

        return $transferLine;
    }

    /**
     * Find Transfer Line by id.
     *
     * @param $transferLineId
     *
     * @return TransferLineInterface|null
     */
    public function findById($transferLineId): ?TransferLineInterface
    {
        $transferLine = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($transferLine, $transferLineId);

        if (!$transferLine->getId()) {
            return null;
        }

        return $transferLine;
    }

    /**
     * Find Transfer by Springboard Transfer line Id.
     *
     * @param $lineId
     *
     * @return TransferLineInterface
     */
    public function loadByLineId($lineId): TransferLineInterface
    {
        $transferLine = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($transferLine, $lineId, TransferLineInterface::LINE_ID);

        return $transferLine;
    }

    /**
     * Retrieve Transfer lines matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Transfer
     *
     * @param TransferLineInterface $transferLine
     *
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(TransferLineInterface $transferLine): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($transferLine);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Transfer line by ID.
     *
     * @param $transferLineId
     *
     * @return bool
     * @throws NoSuchEntityException
     * @throws CouldNotDeleteException
     */
    public function deleteById($transferLineId): bool
    {
        return $this->delete($this->getById($transferLineId));
    }
}
