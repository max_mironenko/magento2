<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\LoggerRepositoryInterface;
use C38\Springboard\Api\Data\LoggerInterface;
use C38\Springboard\Model\ResourceModel\Logger;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Model\AbstractModel;
use Throwable;

/**
 * Class LoggerRepository
 * Springboard logger repository
 */
class LoggerRepository implements LoggerRepositoryInterface
{
    /**
     * @var Logger
     */
    private $resource;

    /**
     * CronLogRepository constructor.
     *
     * @param Logger $resource
     */
    public function __construct(Logger $resource)
    {
        $this->resource = $resource;
    }

    /**
     * Save Log to Database
     *
     * @param LoggerInterface $logger
     * @return LoggerInterface
     * @throws Throwable
     */
    public function save(LoggerInterface $logger): LoggerInterface
    {
        try {
            /** @var AbstractModel $logger */
            $this->resource->save($logger);
        } catch (Throwable $ex) {
            throw new CouldNotSaveException(__($ex->getMessage()));
        }

        return $logger;
    }
}
