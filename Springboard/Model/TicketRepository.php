<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\TicketInterface;
use C38\Springboard\Api\Data\TicketInterfaceFactory;
use C38\Springboard\Api\Data\TicketSearchResultsInterfaceFactory;
use C38\Springboard\Api\TicketRepositoryInterface;
use C38\Springboard\Model\ResourceModel\Ticket;
use C38\Springboard\Model\ResourceModel\Ticket\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class TicketRepository
 * Springboard ticket repository
 */
class TicketRepository implements TicketRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var TicketInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var Ticket
     */
    private $resource;

    /**
     * @var TicketSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * TicketRepository constructor.
     *
     * @param Ticket $resource
     * @param TicketInterfaceFactory $ticketFactory
     * @param CollectionFactory $collectionFactory
     * @param TicketSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        Ticket $resource,
        TicketInterfaceFactory $ticketFactory,
        CollectionFactory $collectionFactory,
        TicketSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $ticketFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Ticket
     *
     * @param TicketInterface $ticket
     *
     * @return TicketInterface
     * @throws Exception
     */
    public function save(TicketInterface $ticket): TicketInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($ticket);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $ticket;
    }

    /**
     * Get Ticket by id.
     *
     * @param $ticketId
     *
     * @return TicketInterface
     * @throws Exception
     */
    public function getById($ticketId): TicketInterface
    {
        $ticket = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($ticket, $ticketId);
        if (!$ticket->getId()) {
            throw new NoSuchEntityException(__('Ticket with id "%1" does not exist.', $ticketId));
        }

        return $ticket;
    }

    /**
     * Find Ticket by id.
     *
     * @param $ticketId
     * @return TicketInterface|null
     */
    public function findById($ticketId): ?TicketInterface
    {
        $ticket = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($ticket, $ticketId);

        if (!$ticket->getId()) {
            return null;
        }

        return $ticket;
    }

    /**
     * Find Ticket by Springboard Ticket Id.
     *
     * @param $ticketId
     * @return TicketInterface
     */
    public function loadByTicketId($ticketId): TicketInterface
    {
        $ticket = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($ticket, $ticketId, TicketInterface::TICKET_ID);

        return $ticket;
    }

    /**
     * Retrieve Ticket matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Ticket
     *
     * @param TicketInterface $ticket
     *
     * @return bool
     * @throws Exception
     */
    public function delete(TicketInterface $ticket): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($ticket);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Ticket by ID.
     *
     * @param $ticketId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById($ticketId): bool
    {
        return $this->delete($this->getById($ticketId));
    }
}
