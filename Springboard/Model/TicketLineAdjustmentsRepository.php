<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\TicketLineAdjustmentsInterface;
use C38\Springboard\Api\Data\TicketLineAdjustmentsInterfaceFactory;
use C38\Springboard\Api\Data\TicketLineAdjustmentsSearchResultsInterface;
use C38\Springboard\Api\Data\TicketLineAdjustmentsSearchResultsInterfaceFactory;
use C38\Springboard\Api\TicketLineAdjustmentsRepositoryInterface;
use C38\Springboard\Model\ResourceModel\TicketLineAdjustments;
use C38\Springboard\Model\ResourceModel\TicketLineAdjustments\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class TicketLineAdjustmentsRepository
 * Springboard ticket line adjustments repository
 */
class TicketLineAdjustmentsRepository implements TicketLineAdjustmentsRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var TicketLineAdjustmentsInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var TicketLineAdjustments
     */
    private $resource;

    /**
     * @var TicketLineAdjustmentsSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * TicketLineAdjustmentsRepository constructor.
     *
     * @param TicketLineAdjustments $resource
     * @param TicketLineAdjustmentsInterfaceFactory $ticketLineFactory
     * @param CollectionFactory $collectionFactory
     * @param TicketLineAdjustmentsSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        TicketLineAdjustments $resource,
        TicketLineAdjustmentsInterfaceFactory $ticketLineFactory,
        CollectionFactory $collectionFactory,
        TicketLineAdjustmentsSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $ticketLineFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Ticket Line Adjustment
     *
     * @param TicketLineAdjustmentsInterface $ticketLineAdjustments
     *
     * @return TicketLineAdjustmentsInterface
     * @throws Exception
     */
    public function save(TicketLineAdjustmentsInterface $ticketLineAdjustments): TicketLineAdjustmentsInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($ticketLineAdjustments);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $ticketLineAdjustments;
    }

    /**
     * Get Ticket Line Adjustment by id.
     *
     * @param $ticketLineAdjustmentsId
     *
     * @return TicketLineAdjustmentsInterface
     * @throws Exception
     */
    public function getById($ticketLineAdjustmentsId): TicketLineAdjustmentsInterface
    {
        $ticketLineAdjustments = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($ticketLineAdjustments, $ticketLineAdjustmentsId);
        if (!$ticketLineAdjustments->getId()) {
            throw new NoSuchEntityException(
                __(
                    'TicketLineAdjustments with id "%1" does not exist.',
                    $ticketLineAdjustmentsId
                )
            );
        }

        return $ticketLineAdjustments;
    }

    /**
     * Retrieve Ticket Line Adjustments matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|TicketLineAdjustmentsSearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Ticket Line Adjustment
     *
     * @param TicketLineAdjustmentsInterface $ticketLineAdjustments
     *
     * @return bool
     * @throws Exception
     */
    public function delete(TicketLineAdjustmentsInterface $ticketLineAdjustments): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($ticketLineAdjustments);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Ticket Line Adjustment by ID.
     *
     * @param $ticketLineAdjustmentsId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById($ticketLineAdjustmentsId): bool
    {
        return $this->delete($this->getById($ticketLineAdjustmentsId));
    }
}
