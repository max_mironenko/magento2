<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\InvoiceInterface;
use C38\Springboard\Api\Data\InvoiceInterfaceFactory;
use C38\Springboard\Api\Data\InvoiceSearchResultsInterface;
use C38\Springboard\Api\Data\InvoiceSearchResultsInterfaceFactory;
use C38\Springboard\Api\InvoiceRepositoryInterface;
use C38\Springboard\Model\ResourceModel\Invoice;
use C38\Springboard\Model\ResourceModel\Invoice\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class InvoiceRepository
 * Springboard invoice repository
 */
class InvoiceRepository implements InvoiceRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var InvoiceInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var Invoice
     */
    private $resource;

    /**
     * @var InvoiceSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * InvoiceRepository constructor.
     *
     * @param Invoice $resource
     * @param InvoiceInterfaceFactory $invoiceFactory
     * @param CollectionFactory $collectionFactory
     * @param InvoiceSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        Invoice $resource,
        InvoiceInterfaceFactory $invoiceFactory,
        CollectionFactory $collectionFactory,
        InvoiceSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $invoiceFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Invoice
     *
     * @param InvoiceInterface $invoice
     *
     * @return InvoiceInterface
     * @throws Exception
     */
    public function save(InvoiceInterface $invoice): InvoiceInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($invoice);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $invoice;
    }

    /**
     * Get Invoice by id.
     *
     * @param $invoiceId
     *
     * @return InvoiceInterface
     * @throws Exception
     */
    public function getById($invoiceId): InvoiceInterface
    {
        $invoice = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($invoice, $invoiceId);
        if (!$invoice->getId()) {
            throw new NoSuchEntityException(__('Invoice with id "%1" does not exist.', $invoiceId));
        }

        return $invoice;
    }

    /**
     * Find Invoice by id.
     *
     * @param $invoiceId
     * @return InvoiceInterface|null
     */
    public function findById($invoiceId): ?InvoiceInterface
    {
        $invoice = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($invoice, $invoiceId);

        if (!$invoice->getId()) {
            return null;
        }

        return $invoice;
    }

    /**
     * Find Invoice by Invoice Id.
     *
     * @param $invoiceId
     * @return InvoiceInterface
     */
    public function loadByInvoiceId($invoiceId): InvoiceInterface
    {
        $invoice = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($invoice, $invoiceId, InvoiceInterface::INVOICE_ID);

        return $invoice;
    }

    /**
     * Retrieve Invoice matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|InvoiceSearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Invoice
     *
     * @param InvoiceInterface $invoice
     *
     * @return bool
     * @throws Exception
     */
    public function delete(InvoiceInterface $invoice): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($invoice);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Invoice by ID.
     *
     * @param $invoiceId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById($invoiceId): bool
    {
        return $this->delete($this->getById($invoiceId));
    }
}
