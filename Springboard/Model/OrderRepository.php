<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\OrderInterface;
use C38\Springboard\Api\Data\OrderInterfaceFactory;
use C38\Springboard\Api\Data\OrderSearchResultsInterfaceFactory;
use C38\Springboard\Api\OrderRepositoryInterface;
use C38\Springboard\Model\ResourceModel\Order;
use C38\Springboard\Model\ResourceModel\Order\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class OrderRepository
 * Springboard order repository
 */
class OrderRepository implements OrderRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var OrderInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var Order
     */
    private $resource;

    /**
     * @var OrderSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * OrderRepository constructor.
     *
     * @param Order $resource
     * @param OrderInterfaceFactory $orderFactory
     * @param CollectionFactory $collectionFactory
     * @param OrderSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        Order $resource,
        OrderInterfaceFactory $orderFactory,
        CollectionFactory $collectionFactory,
        OrderSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $orderFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Order
     *
     * @param OrderInterface $order
     *
     * @return OrderInterface
     * @throws Exception
     */
    public function save(OrderInterface $order): OrderInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($order);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $order;
    }

    /**
     * Get Order by id.
     *
     * @param $orderId
     *
     * @return OrderInterface
     * @throws Exception
     */
    public function getById($orderId): OrderInterface
    {
        $order = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($order, $orderId);
        if (!$order->getId()) {
            throw new NoSuchEntityException(__('Order with id "%1" does not exist.', $orderId));
        }

        return $order;
    }

    /**
     * Find Order by id.
     *
     * @param $orderId
     * @return OrderInterface|null
     */
    public function findById($orderId): ?OrderInterface
    {
        $order = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($order, $orderId);

        if (!$order->getId()) {
            return null;
        }

        return $order;
    }

    /**
     * Find Order by Springboard Order Id.
     *
     * @param $orderId
     * @return OrderInterface
     */
    public function loadByOrderId($orderId): OrderInterface
    {
        $order = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($order, $orderId, OrderInterface::ORDER_ID);

        return $order;
    }

    /**
     * Retrieve Order matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Order
     *
     * @param OrderInterface $order
     *
     * @return bool
     * @throws Exception
     */
    public function delete(OrderInterface $order): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($order);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Order by ID.
     *
     * @param $orderId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById($orderId): bool
    {
        return $this->delete($this->getById($orderId));
    }
}
