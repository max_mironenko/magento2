<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\InvoiceInterface;
use C38\Springboard\Model\ResourceModel\Invoice as ResourceInvoice;
use C38\Springboard\Model\ResourceModel\Invoice\Collection as ResourceInvoiceCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Invoice
 * Springboard invoice model
 *
 * @method ResourceInvoice getResource()
 * @method ResourceInvoiceCollection getCollection()
 * @method ResourceInvoiceCollection getResourceCollection()
 */
class Invoice extends AbstractModel implements InvoiceInterface
{
    protected $_eventPrefix = 'c38_springboard_model_invoice';

    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init(ResourceInvoice::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setId($value): InvoiceInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get InvoiceId value
     *
     * @return string
     */
    public function getInvoiceId(): string
    {
        return $this->_getData(self::INVOICE_ID);
    }

    /**
     * Set InvoiceId value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setInvoiceId($value): InvoiceInterface
    {
        $this->setData(self::INVOICE_ID, $value);
        return $this;
    }

    /**
     * Get OrderId value
     *
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->_getData(self::ORDER_ID);
    }

    /**
     * Set OrderId value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setOrderId($value): InvoiceInterface
    {
        $this->setData(self::ORDER_ID, $value);
        return $this;
    }

    /**
     * Get Type value
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->_getData(self::TYPE);
    }

    /**
     * Set Type value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setType($value): InvoiceInterface
    {
        $this->setData(self::TYPE, $value);
        return $this;
    }

    /**
     * Get Status value
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->_getData(self::STATUS);
    }

    /**
     * Set Status value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setStatus($value): InvoiceInterface
    {
        $this->setData(self::STATUS, $value);
        return $this;
    }

    /**
     * Get CreatedDate value
     *
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->_getData(self::CREATED_DATE);
    }

    /**
     * Set CreatedDate value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setCreatedDate($value): InvoiceInterface
    {
        $this->setData(self::CREATED_DATE, $value);
        return $this;
    }

    /**
     * Get UpdatedAt value
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->_getData(self::UPDATED_AT);
    }

    /**
     * Set UpdatedAt value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setUpdatedAt($value): InvoiceInterface
    {
        $this->setData(self::UPDATED_AT, $value);
        return $this;
    }

    /**
     * Get CustomerId value
     *
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->_getData(self::CUSTOMER_ID);
    }

    /**
     * Set CustomerId value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setCustomerId($value): InvoiceInterface
    {
        $this->setData(self::CUSTOMER_ID, $value);
        return $this;
    }

    /**
     * Get SalesRep value
     *
     * @return string
     */
    public function getSalesRep(): string
    {
        return $this->_getData(self::SALES_REP);
    }

    /**
     * Set SalesRep value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setSalesRep($value): InvoiceInterface
    {
        $this->setData(self::SALES_REP, $value);
        return $this;
    }

    /**
     * Get StationId value
     *
     * @return string
     */
    public function getStationId(): string
    {
        return $this->_getData(self::STATION_ID);
    }

    /**
     * Set StationId value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setStationId($value): InvoiceInterface
    {
        $this->setData(self::STATION_ID, $value);
        return $this;
    }

    /**
     * Get SourceLocationId value
     *
     * @return string
     */
    public function getSourceLocationId(): string
    {
        return $this->_getData(self::SOURCE_LOCATION_ID);
    }

    /**
     * Set SourceLocationId value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setSourceLocationId($value): InvoiceInterface
    {
        $this->setData(self::SOURCE_LOCATION_ID, $value);
        return $this;
    }

    /**
     * Get TotalItemQty value
     *
     * @return float
     */
    public function getTotalItemQty(): float
    {
        return $this->_getData(self::TOTAL_ITEM_QTY);
    }

    /**
     * Set TotalItemQty value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setTotalItemQty($value): InvoiceInterface
    {
        $this->setData(self::TOTAL_ITEM_QTY, $value);
        return $this;
    }

    /**
     * Get Total value
     *
     * @return float
     */
    public function getTotal(): float
    {
        return $this->_getData(self::TOTAL);
    }

    /**
     * Set Total value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setTotal($value): InvoiceInterface
    {
        $this->setData(self::TOTAL, $value);
        return $this;
    }

    /**
     * Get TotalPaid value
     *
     * @return float
     */
    public function getTotalPaid(): float
    {
        return $this->_getData(self::TOTAL_PAID);
    }

    /**
     * Set TotalPaid value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setTotalPaid($value): InvoiceInterface
    {
        $this->setData(self::TOTAL_PAID, $value);
        return $this;
    }

    /**
     * Get OriginalSubtotal value
     *
     * @return float
     */
    public function getOriginalSubtotal(): float
    {
        return $this->_getData(self::ORIGINAL_SUBTOTAL);
    }

    /**
     * Set OriginalSubtotal value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setOriginalSubtotal($value): InvoiceInterface
    {
        $this->setData(self::ORIGINAL_SUBTOTAL, $value);
        return $this;
    }

    /**
     * Get TotalDiscounts value
     *
     * @return float
     */
    public function getTotalDiscounts(): float
    {
        return $this->_getData(self::TOTAL_DISCOUNTS);
    }

    /**
     * Set TotalDiscounts value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setTotalDiscounts($value): InvoiceInterface
    {
        $this->setData(self::TOTAL_DISCOUNTS, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return InvoiceInterface
     */
    public function setSpringboardSyncDt($value): InvoiceInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }
}
