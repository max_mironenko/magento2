<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\TransferLineInterface;
use C38\Springboard\Model\ResourceModel\TransferLine as ResourceTransferLine;
use C38\Springboard\Model\ResourceModel\TransferLine\Collection as ResourceTransferLineCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Ticket
 * Springboard ticket model
 *
 * @method ResourceTransferLine getResource()
 * @method ResourceTransferLineCollection getCollection()
 * @method ResourceTransferLineCollection getResourceCollection()
 */
class TransferLine extends AbstractModel implements TransferLineInterface
{
    protected $_eventPrefix = 'c38_springboard_model_transfer_line';

    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init(ResourceTransferLine::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setId($value): TransferLineInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get LineId value
     *
     * @return string
     */
    public function getLineId(): string
    {
        return $this->_getData(self::LINE_ID);
    }

    /**
     * Set LineId value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setLineId($value): TransferLineInterface
    {
        $this->setData(self::LINE_ID, $value);
        return $this;
    }

    /**
     * Get TransferId value
     *
     * @return string
     */
    public function getTransferId(): string
    {
        return $this->_getData(self::TRANSFER_ID);
    }

    /**
     * Set TransferId value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setTransferId($value): TransferLineInterface
    {
        $this->setData(self::TRANSFER_ID, $value);
        return $this;
    }

    /**
     * Get ItemId value
     *
     * @return string
     */
    public function getItemId(): string
    {
        return $this->_getData(self::ITEM_ID);
    }

    /**
     * Set ItemId value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setItemId($value): TransferLineInterface
    {
        $this->setData(self::ITEM_ID, $value);
        return $this;
    }

    /**
     * Get UnitCost value
     *
     * @return float
     */
    public function getUnitCost(): float
    {
        return $this->_getData(self::UNIT_COST);
    }

    /**
     * Set UnitCost value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setUnitCost($value): TransferLineInterface
    {
        $this->setData(self::UNIT_COST, $value);
        return $this;
    }

    /**
     * Get QtyShipped value
     *
     * @return string
     */
    public function getQtyShipped(): string
    {
        return $this->_getData(self::QTY_SHIPPED);
    }

    /**
     * Set QtyShipped value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyShipped($value): TransferLineInterface
    {
        $this->setData(self::QTY_SHIPPED, $value);
        return $this;
    }

    /**
     * Get QtyReceived value
     *
     * @return float
     */
    public function getQtyReceived(): float
    {
        return $this->_getData(self::QTY_RECEIVED);
    }

    /**
     * Set QtyReceived value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyReceived($value): TransferLineInterface
    {
        $this->setData(self::QTY_RECEIVED, $value);
        return $this;
    }

    /**
     * Get QtyLost value
     *
     * @return float
     */
    public function getQtyLost(): float
    {
        return $this->_getData(self::QTY_LOST);
    }

    /**
     * Set QtyLost value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyLost($value): TransferLineInterface
    {
        $this->setData(self::QTY_LOST, $value);
        return $this;
    }

    /**
     * Get QtyDiscrepant value
     *
     * @return float
     */
    public function getQtyDiscrepant(): float
    {
        return $this->_getData(self::QTY_DISCREPANT);
    }

    /**
     * Set QtyDiscrepant value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyDiscrepant($value): TransferLineInterface
    {
        $this->setData(self::QTY_DISCREPANT, $value);
        return $this;
    }

    /**
     * Get QtyOver value
     *
     * @return float
     */
    public function getQtyOver(): float
    {
        return $this->_getData(self::QTY_OVER);
    }

    /**
     * Set QtyOver value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyOver($value): TransferLineInterface
    {
        $this->setData(self::QTY_OVER, $value);
        return $this;
    }

    /**
     * Get QtyShort value
     *
     * @return float
     */
    public function getQtyShort(): float
    {
        return $this->_getData(self::QTY_SHORT);
    }

    /**
     * Set QtyShort value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyShort($value): TransferLineInterface
    {
        $this->setData(self::QTY_SHORT, $value);
        return $this;
    }

    /**
     * Get QtyRequested value
     *
     * @return float
     */
    public function getQtyRequested(): float
    {
        return $this->_getData(self::QTY_REQUESTED);
    }

    /**
     * Set QtyRequested value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setQtyRequested($value): TransferLineInterface
    {
        $this->setData(self::QTY_REQUESTED, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setSpringboardSyncDt($value): TransferLineInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }

    /**
     * Get ManifestId value
     *
     * @return string
     */
    public function getManifestId(): string
    {
        return $this->_getData(self::MANIFEST_ID);
    }

    /**
     * Set ManifestId value
     *
     * @param $value
     * @return TransferLineInterface
     */
    public function setManifestId($value): TransferLineInterface
    {
        $this->setData(self::MANIFEST_ID, $value);
        return $this;
    }
}
