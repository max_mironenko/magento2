<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\SalesPlanInterface;
use C38\Springboard\Api\Data\SalesPlanInterfaceFactory;
use C38\Springboard\Api\Data\SalesPlanSearchResultsInterfaceFactory;
use C38\Springboard\Api\SalesPlanRepositoryInterface;
use C38\Springboard\Model\ResourceModel\SalesPlan;
use C38\Springboard\Model\ResourceModel\SalesPlan\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class SalesPlanRepository
 * Springboard sales plan repository
 */
class SalesPlanRepository implements SalesPlanRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SalesPlanInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var SalesPlan
     */
    private $resource;

    /**
     * @var SalesPlanSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * SalesPlanRepository constructor.
     *
     * @param SalesPlan $resource
     * @param SalesPlanInterfaceFactory $salesPlanFactory
     * @param CollectionFactory $collectionFactory
     * @param SalesPlanSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        SalesPlan $resource,
        SalesPlanInterfaceFactory $salesPlanFactory,
        CollectionFactory $collectionFactory,
        SalesPlanSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $salesPlanFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save SalesPlan
     *
     * @param SalesPlanInterface $salesPlan
     *
     * @return SalesPlanInterface
     * @throws Exception
     */
    public function save(SalesPlanInterface $salesPlan): SalesPlanInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($salesPlan);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $salesPlan;
    }

    /**
     * Get SalesPlan by id.
     *
     * @param int $salesPlanId
     *
     * @return SalesPlanInterface
     * @throws Exception
     */
    public function getById(int $salesPlanId): SalesPlanInterface
    {
        $salesPlan = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($salesPlan, $salesPlanId);
        if (!$salesPlan->getId()) {
            throw new NoSuchEntityException(__('SalesPlan with id "%1" does not exist.', $salesPlanId));
        }

        return $salesPlan;
    }

    /**
     * Find SalesPlan by id.
     *
     * @param int $salesPlanId
     * @return SalesPlanInterface|null
     */
    public function findById(int $salesPlanId): ?SalesPlanInterface
    {
        $salesPlan = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($salesPlan, $salesPlanId);

        if (!$salesPlan->getId()) {
            return null;
        }

        return $salesPlan;
    }

    /**
     * Find SalesPlan by public_id.
     *
     * @param $publicId
     * @return SalesPlanInterface
     */
    public function loadByPublicId($publicId): SalesPlanInterface
    {
        $salesPlan = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($salesPlan, $publicId, SalesPlanInterface::PUBLIC_ID);

        return $salesPlan;
    }

    /**
     * Retrieve SalesPlan matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete SalesPlan
     *
     * @param SalesPlanInterface $salesPlan
     *
     * @return bool
     * @throws Exception
     */
    public function delete(SalesPlanInterface $salesPlan): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($salesPlan);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete SalesPlan by ID.
     *
     * @param int $salesPlanId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById(int $salesPlanId): bool
    {
        return $this->delete($this->getById($salesPlanId));
    }
}
