<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\MagentoCustomerInterface;
use C38\Springboard\Api\Data\MagentoCustomerSearchResultsInterface;
use C38\Springboard\Api\Data\MagentoCustomerSearchResultsInterfaceFactory;
use C38\Springboard\Api\MagentoCustomerRepositoryInterface;
use C38\Springboard\Model\ResourceModel\MagentoCustomer;
use C38\Springboard\Model\ResourceModel\MagentoCustomer\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Class CustomerRepository
 * Springboard magento customer repository
 */
class MagentoCustomerRepository implements MagentoCustomerRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var MagentoCustomer
     */
    private $resource;

    /**
     * @var MagentoCustomerSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * CustomerRepository constructor.
     *
     * @param MagentoCustomer $resource
     * @param CollectionFactory $collectionFactory
     * @param MagentoCustomerSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        MagentoCustomer $resource,
        CollectionFactory $collectionFactory,
        MagentoCustomerSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Customer
     *
     * @param MagentoCustomerInterface $customer
     *
     * @return MagentoCustomerInterface
     * @throws Exception
     */
    public function save(MagentoCustomerInterface $customer): MagentoCustomerInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($customer);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $customer;
    }

    /**
     * Retrieve Customer matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|MagentoCustomerSearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }
}
