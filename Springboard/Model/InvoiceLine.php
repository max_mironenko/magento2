<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\InvoiceLineInterface;
use C38\Springboard\Model\ResourceModel\InvoiceLine as ResourceInvoiceLine;
use C38\Springboard\Model\ResourceModel\InvoiceLine\Collection as ResourceInvoiceLineCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class InvoiceLine
 * Springboard invoice line model
 *
 * @method ResourceInvoiceLine getResource()
 * @method ResourceInvoiceLineCollection getCollection()
 * @method ResourceInvoiceLineCollection getResourceCollection()
 */
class InvoiceLine extends AbstractModel implements InvoiceLineInterface
{
    protected $_eventPrefix = 'c38_springboard_model_invoice_line';

    /**
     * Class Constructor
     */
    protected function _construct()
    {
        $this->_init(ResourceInvoiceLine::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setId($value): InvoiceLineInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get LineId value
     *
     * @return string
     */
    public function getLineId(): string
    {
        return $this->_getData(self::LINE_ID);
    }

    /**
     * Set LineId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setLineId($value): InvoiceLineInterface
    {
        $this->setData(self::LINE_ID, $value);
        return $this;
    }

    /**
     * Get InvoiceId value
     *
     * @return string
     */
    public function getInvoiceId(): string
    {
        return $this->_getData(self::INVOICE_ID);
    }

    /**
     * Set InvoiceId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setInvoiceId($value): InvoiceLineInterface
    {
        $this->setData(self::INVOICE_ID, $value);
        return $this;
    }

    /**
     * Get ItemId value
     *
     * @return string
     */
    public function getItemId(): string
    {
        return $this->_getData(self::ITEM_ID);
    }

    /**
     * Set ItemId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setItemId($value): InvoiceLineInterface
    {
        $this->setData(self::ITEM_ID, $value);
        return $this;
    }

    /**
     * Get Type value
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->_getData(self::TYPE);
    }

    /**
     * Set Type value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setType($value): InvoiceLineInterface
    {
        $this->setData(self::TYPE, $value);
        return $this;
    }

    /**
     * Get Description value
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->_getData(self::DESCRIPTION);
    }

    /**
     * Set Description value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setDescription($value): InvoiceLineInterface
    {
        $this->setData(self::DESCRIPTION, $value);
        return $this;
    }

    /**
     * Get TaxRuleId value
     *
     * @return string
     */
    public function getTaxRuleId(): string
    {
        return $this->_getData(self::TAX_RULE_ID);
    }

    /**
     * Set TaxRuleId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setTaxRuleId($value): InvoiceLineInterface
    {
        $this->setData(self::TAX_RULE_ID, $value);
        return $this;
    }

    /**
     * Get Value value
     *
     * @return float
     */
    public function getValue(): float
    {
        return $this->_getData(self::VALUE);
    }

    /**
     * Set Value value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setValue($value): InvoiceLineInterface
    {
        $this->setData(self::VALUE, $value);
        return $this;
    }

    /**
     * Get Qty value
     *
     * @return float
     */
    public function getQty(): float
    {
        return $this->_getData(self::QTY);
    }

    /**
     * Set Qty value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setQty($value): InvoiceLineInterface
    {
        $this->setData(self::QTY, $value);
        return $this;
    }

    /**
     * Get OriginalUnitPrice value
     *
     * @return float
     */
    public function getOriginalUnitPrice(): float
    {
        return $this->_getData(self::ORIGINAL_UNIT_PRICE);
    }

    /**
     * Set OriginalUnitPrice value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setOriginalUnitPrice($value): InvoiceLineInterface
    {
        $this->setData(self::ORIGINAL_UNIT_PRICE, $value);
        return $this;
    }

    /**
     * Get UnitPrice value
     *
     * @return float
     */
    public function getUnitPrice(): float
    {
        return $this->_getData(self::UNIT_PRICE);
    }

    /**
     * Set UnitPrice value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setUnitPrice($value): InvoiceLineInterface
    {
        $this->setData(self::UNIT_PRICE, $value);
        return $this;
    }

    /**
     * Get UnitCost value
     *
     * @return float
     */
    public function getUnitCost(): float
    {
        return $this->_getData(self::UNIT_COST);
    }

    /**
     * Set UnitCost value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setUnitCost($value): InvoiceLineInterface
    {
        $this->setData(self::UNIT_COST, $value);
        return $this;
    }

    /**
     * Get SpringboardSyncDt value
     *
     * @return string
     */
    public function getSpringboardSyncDt(): string
    {
        return $this->_getData(self::SPRINGBOARD_SYNC_DT);
    }

    /**
     * Set SpringboardSyncDt value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setSpringboardSyncDt($value): InvoiceLineInterface
    {
        $this->setData(self::SPRINGBOARD_SYNC_DT, $value);
        return $this;
    }

    /**
     * Get ConfigurableProductId value
     *
     * @return string
     */
    public function getConfigurableProductId(): string
    {
        return $this->_getData(self::CONFIGURABLE_PRODUCT_ID);
    }

    /**
     * Set ConfigurableProductId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setConfigurableProductId($value): InvoiceLineInterface
    {
        $this->setData(self::CONFIGURABLE_PRODUCT_ID, $value);
        return $this;
    }

    /**
     * Get SimpleProductId value
     *
     * @return string
     */
    public function getSimpleProductId(): string
    {
        return $this->_getData(self::SIMPLE_PRODUCT_ID);
    }

    /**
     * Set SimpleProductId value
     *
     * @param $value
     * @return InvoiceLineInterface
     */
    public function setSimpleProductId($value): InvoiceLineInterface
    {
        $this->setData(self::SIMPLE_PRODUCT_ID, $value);
        return $this;
    }
}
