<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\TransferInterface;
use C38\Springboard\Api\Data\TransferInterfaceFactory;
use C38\Springboard\Api\Data\TransferSearchResultsInterfaceFactory;
use C38\Springboard\Api\TransferRepositoryInterface;
use C38\Springboard\Model\ResourceModel\Transfer;
use C38\Springboard\Model\ResourceModel\Transfer\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class TransferRepository
 * Springboard transfer objects repository
 */
class TransferRepository implements TransferRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var TransferInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var Transfer
     */
    private $resource;

    /**
     * @var TransferSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * TransferRepository constructor.
     *
     * @param Transfer $resource
     * @param TransferInterfaceFactory $transferFactory
     * @param CollectionFactory $collectionFactory
     * @param TransferSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        Transfer $resource,
        TransferInterfaceFactory $transferFactory,
        CollectionFactory $collectionFactory,
        TransferSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->entityFactory = $transferFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Transfer
     *
     * @param TransferInterface $transfer
     *
     * @return TransferInterface
     * @throws CouldNotSaveException
     */
    public function save(TransferInterface $transfer): TransferInterface
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->save($transfer);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $transfer;
    }

    /**
     * Get Transfer by id.
     *
     * @param $transferId
     *
     * @return TransferInterface
     * @throws NoSuchEntityException
     */
    public function getById($transferId): TransferInterface
    {
        $transfer = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($transfer, $transferId);
        if (!$transfer->getId()) {
            throw new NoSuchEntityException(__('Ticket with id "%1" does not exist.', $transferId));
        }

        return $transfer;
    }

    /**
     * Find Transfer Line by id.
     *
     * @param $transferId
     *
     * @return TransferInterface|null
     */
    public function findById($transferId): ?TransferInterface
    {
        $transfer = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($transfer, $transferId);

        if (!$transfer->getId()) {
            return null;
        }

        return $transfer;
    }

    /**
     * Find Transfer by Springboard Transfer Id.
     *
     * @param $transferId
     * @return TransferInterface
     */
    public function loadByTransferId($transferId): TransferInterface
    {
        $transfer = $this->entityFactory->create();
        /** @noinspection PhpParamsInspection */
        $this->resource->load($transfer, $transferId, TransferInterface::TRANSFER_ID);

        return $transfer;
    }

    /**
     * Retrieve Transfer matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResultsInterface
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Transfer
     *
     * @param TransferInterface $transfer
     *
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(TransferInterface $transfer): bool
    {
        try {
            /** @noinspection PhpParamsInspection */
            $this->resource->delete($transfer);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Transfer by ID.
     *
     * @param $transferId
     *
     * @return bool
     * @throws NoSuchEntityException
     * @throws CouldNotDeleteException
     */
    public function deleteById($transferId): bool
    {
        return $this->delete($this->getById($transferId));
    }
}
