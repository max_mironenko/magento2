<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel;

use C38\Springboard\Api\Data\InventoryInterface as DataInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Inventory
 * Springboard inventory resource model
 */
class Inventory extends AbstractDb
{
    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(DataInterface::TABLE_NAME, DataInterface::ID);
    }
}
