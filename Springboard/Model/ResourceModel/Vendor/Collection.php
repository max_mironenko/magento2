<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\Vendor;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\Vendor;
use C38\Springboard\Model\ResourceModel\Vendor as ResourceVendor;

/**
 * Class Collection
 * Collection of Springboard vendors
 *
 * @method ResourceVendor getResource()
 * @method Vendor[] getItems()
 * @method Vendor[] getItemsByColumnValue($column, $value)
 * @method Vendor getFirstItem()
 * @method Vendor getLastItem()
 * @method Vendor getItemByColumnValue($column, $value)
 * @method Vendor getItemById($idValue)
 * @method Vendor getNewEmptyItem()
 * @method Vendor fetchItem()
 * @property Vendor[] _items
 * @property ResourceVendor _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_vendor_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Vendor::class, ResourceVendor::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
