<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use C38\Springboard\Api\Data\InvoiceLineInterface as DataInterface;

/**
 * Class InvoiceLine
 * Springboard invoice line resource model
 */
class InvoiceLine extends AbstractDb
{
    /**
     * Class constructor
     */
    protected function _construct()
    {
        $this->_init(DataInterface::TABLE_NAME, DataInterface::ID);
    }
}
