<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\MagentoCustomer;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\MagentoCustomer;
use C38\Springboard\Model\ResourceModel\MagentoCustomer as ResourceMagentoCustomer;

/**
 * Class Collection
 * Collection of Springboard magento customers
 *
 * @method ResourceMagentoCustomer getResource()
 * @method MagentoCustomer[] getItems()
 * @method MagentoCustomer[] getItemsByColumnValue($column, $value)
 * @method MagentoCustomer getFirstItem()
 * @method MagentoCustomer getLastItem()
 * @method MagentoCustomer getItemByColumnValue($column, $value)
 * @method MagentoCustomer getItemById($idValue)
 * @method MagentoCustomer getNewEmptyItem()
 * @method MagentoCustomer fetchItem()
 * @property MagentoCustomer[] _items
 * @property ResourceMagentoCustomer _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_magento_customer_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(MagentoCustomer::class, ResourceMagentoCustomer::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
