<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\InvoiceLine;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\InvoiceLine;
use C38\Springboard\Model\ResourceModel\InvoiceLine as ResourceInvoiceLine;

/**
 * Class Collection
 * Collection of Springboard invoice lines
 *
 * @method ResourceInvoiceLine getResource()
 * @method InvoiceLine[] getItems()
 * @method InvoiceLine[] getItemsByColumnValue($column, $value)
 * @method InvoiceLine getFirstItem()
 * @method InvoiceLine getLastItem()
 * @method InvoiceLine getItemByColumnValue($column, $value)
 * @method InvoiceLine getItemById($idValue)
 * @method InvoiceLine getNewEmptyItem()
 * @method InvoiceLine fetchItem()
 * @property InvoiceLine[] _items
 * @property ResourceInvoiceLine _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_invoice_line_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(InvoiceLine::class, ResourceInvoiceLine::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
