<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\Inventory;

use C38\Springboard\Model\Inventory;
use C38\Springboard\Model\ResourceModel\Inventory as ResourceInventory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * Collection of Springboard inventory
 *
 * @method ResourceInventory getResource()
 * @method Inventory[] getItems()
 * @method Inventory[] getItemsByColumnValue($column, $value)
 * @method Inventory getFirstItem()
 * @method Inventory getLastItem()
 * @method Inventory getItemByColumnValue($column, $value)
 * @method Inventory getItemById($idValue)
 * @method Inventory getNewEmptyItem()
 * @method Inventory fetchItem()
 * @property Inventory[] _items
 * @property ResourceInventory _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_inventory_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Inventory::class, ResourceInventory::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
