<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\OrderLine;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\OrderLine;
use C38\Springboard\Model\ResourceModel\OrderLine as ResourceOrderLine;

/**
 * Class Collection
 * Collection of Springboard order lines
 *
 * @method ResourceOrderLine getResource()
 * @method OrderLine[] getItems()
 * @method OrderLine[] getItemsByColumnValue($column, $value)
 * @method OrderLine getFirstItem()
 * @method OrderLine getLastItem()
 * @method OrderLine getItemByColumnValue($column, $value)
 * @method OrderLine getItemById($idValue)
 * @method OrderLine getNewEmptyItem()
 * @method OrderLine fetchItem()
 * @property OrderLine[] _items
 * @property ResourceOrderLine _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_order_line_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(OrderLine::class, ResourceOrderLine::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
