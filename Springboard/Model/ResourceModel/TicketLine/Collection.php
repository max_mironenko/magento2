<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\TicketLine;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\TicketLine;
use C38\Springboard\Model\ResourceModel\TicketLine as ResourceTicketLine;

/**
 * Class Collection
 * Collection of Springboard ticket lines
 *
 * @method ResourceTicketLine getResource()
 * @method TicketLine[] getItems()
 * @method TicketLine[] getItemsByColumnValue($column, $value)
 * @method TicketLine getFirstItem()
 * @method TicketLine getLastItem()
 * @method TicketLine getItemByColumnValue($column, $value)
 * @method TicketLine getItemById($idValue)
 * @method TicketLine getNewEmptyItem()
 * @method TicketLine fetchItem()
 * @property TicketLine[] _items
 * @property ResourceTicketLine _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_ticket_line_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(TicketLine::class, ResourceTicketLine::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
