<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\Product;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\Product;
use C38\Springboard\Model\ResourceModel\Product as ResourceProduct;

/**
 * Class Collection
 * Collection of Springboard products
 *
 * @method ResourceProduct getResource()
 * @method Product[] getItems()
 * @method Product[] getItemsByColumnValue($column, $value)
 * @method Product getFirstItem()
 * @method Product getLastItem()
 * @method Product getItemByColumnValue($column, $value)
 * @method Product getItemById($idValue)
 * @method Product getNewEmptyItem()
 * @method Product fetchItem()
 * @property Product[] _items
 * @property ResourceProduct _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_product_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Product::class, ResourceProduct::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
