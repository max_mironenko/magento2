<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\Customer;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\Customer;
use C38\Springboard\Model\ResourceModel\Customer as ResourceCustomer;

/**
 * Class Collection
 * Collection of Springboard customers
 *
 * @method ResourceCustomer getResource()
 * @method Customer[] getItems()
 * @method Customer[] getItemsByColumnValue($column, $value)
 * @method Customer getFirstItem()
 * @method Customer getLastItem()
 * @method Customer getItemByColumnValue($column, $value)
 * @method Customer getItemById($idValue)
 * @method Customer getNewEmptyItem()
 * @method Customer fetchItem()
 * @property Customer[] _items
 * @property ResourceCustomer _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_customer_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Customer::class, ResourceCustomer::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
