<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\Order;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\Order;
use C38\Springboard\Model\ResourceModel\Order as ResourceOrder;

/**
 * Class Collection
 * Collection of Springboard orders
 *
 * @method ResourceOrder getResource()
 * @method Order[] getItems()
 * @method Order[] getItemsByColumnValue($column, $value)
 * @method Order getFirstItem()
 * @method Order getLastItem()
 * @method Order getItemByColumnValue($column, $value)
 * @method Order getItemById($idValue)
 * @method Order getNewEmptyItem()
 * @method Order fetchItem()
 * @property Order[] _items
 * @property ResourceOrder _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_order_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Order::class, ResourceOrder::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
