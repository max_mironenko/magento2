<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\Ticket;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\Ticket;
use C38\Springboard\Model\ResourceModel\Ticket as ResourceTicket;

/**
 * Class Collection
 * Collection of Springboard tickets
 *
 * @method ResourceTicket getResource()
 * @method Ticket[] getItems()
 * @method Ticket[] getItemsByColumnValue($column, $value)
 * @method Ticket getFirstItem()
 * @method Ticket getLastItem()
 * @method Ticket getItemByColumnValue($column, $value)
 * @method Ticket getItemById($idValue)
 * @method Ticket getNewEmptyItem()
 * @method Ticket fetchItem()
 * @property Ticket[] _items
 * @property ResourceTicket _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_ticket_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Ticket::class, ResourceTicket::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
