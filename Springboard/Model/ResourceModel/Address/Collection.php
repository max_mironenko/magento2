<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\Address;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\Address;
use C38\Springboard\Model\ResourceModel\Address as ResourceAddress;

/**
 * Class Collection
 * Collection of Springboard addresses
 *
 * @method ResourceAddress getResource()
 * @method Address[] getItems()
 * @method Address[] getItemsByColumnValue($column, $value)
 * @method Address getFirstItem()
 * @method Address getLastItem()
 * @method Address getItemByColumnValue($column, $value)
 * @method Address getItemById($idValue)
 * @method Address getNewEmptyItem()
 * @method Address fetchItem()
 * @property Address[] _items
 * @property ResourceAddress _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_address_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Address::class, ResourceAddress::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
