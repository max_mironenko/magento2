<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\SalesPlan;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\SalesPlan;
use C38\Springboard\Model\ResourceModel\SalesPlan as ResourceSalesPlan;

/**
 * Class Collection
 * Collection of Springboard sale plans
 *
 * @method ResourceSalesPlan getResource()
 * @method SalesPlan[] getItems()
 * @method SalesPlan[] getItemsByColumnValue($column, $value)
 * @method SalesPlan getFirstItem()
 * @method SalesPlan getLastItem()
 * @method SalesPlan getItemByColumnValue($column, $value)
 * @method SalesPlan getItemById($idValue)
 * @method SalesPlan getNewEmptyItem()
 * @method SalesPlan fetchItem()
 * @property SalesPlan[] _items
 * @property ResourceSalesPlan _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_sales_plan_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(SalesPlan::class, ResourceSalesPlan::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
