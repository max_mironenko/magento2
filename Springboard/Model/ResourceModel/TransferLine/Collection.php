<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\TransferLine;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\TransferLine;
use C38\Springboard\Model\ResourceModel\TransferLine as ResourceTransferLine;

/**
 * Class Collection
 * Collection of Springboard transfer line objects
 *
 * @method ResourceTransferLine getResource()
 * @method TransferLine[] getItems()
 * @method TransferLine[] getItemsByColumnValue($column, $value)
 * @method TransferLine getFirstItem()
 * @method TransferLine getLastItem()
 * @method TransferLine getItemByColumnValue($column, $value)
 * @method TransferLine getItemById($idValue)
 * @method TransferLine getNewEmptyItem()
 * @method TransferLine fetchItem()
 * @property TransferLine[] _items
 * @property ResourceTransferLine _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_transfer_line_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(TransferLine::class, ResourceTransferLine::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
