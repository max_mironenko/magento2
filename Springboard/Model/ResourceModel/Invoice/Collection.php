<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\Invoice;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\Invoice;
use C38\Springboard\Model\ResourceModel\Invoice as ResourceInvoice;

/**
 * Class Collection
 * Collection of Springboard invoices
 *
 * @method ResourceInvoice getResource()
 * @method Invoice[] getItems()
 * @method Invoice[] getItemsByColumnValue($column, $value)
 * @method Invoice getFirstItem()
 * @method Invoice getLastItem()
 * @method Invoice getItemByColumnValue($column, $value)
 * @method Invoice getItemById($idValue)
 * @method Invoice getNewEmptyItem()
 * @method Invoice fetchItem()
 * @property Invoice[] _items
 * @property ResourceInvoice _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_invoice_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Invoice::class, ResourceInvoice::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
