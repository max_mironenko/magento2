<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\Location;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\Location;
use C38\Springboard\Model\ResourceModel\Location as ResourceLocation;

/**
 * Class Collection
 * Collection of Springboard locations
 *
 * @method ResourceLocation getResource()
 * @method Location[] getItems()
 * @method Location[] getItemsByColumnValue($column, $value)
 * @method Location getFirstItem()
 * @method Location getLastItem()
 * @method Location getItemByColumnValue($column, $value)
 * @method Location getItemById($idValue)
 * @method Location getNewEmptyItem()
 * @method Location fetchItem()
 * @property Location[] _items
 * @property ResourceLocation _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_location_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Location::class, ResourceLocation::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
