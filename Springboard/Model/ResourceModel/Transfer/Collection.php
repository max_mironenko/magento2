<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\Transfer;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\Transfer;
use C38\Springboard\Model\ResourceModel\Transfer as ResourceTransfer;

/**
 * Class Collection
 * Collection of Springboard transfer objects
 *
 * @method ResourceTransfer getResource()
 * @method Transfer[] getItems()
 * @method Transfer[] getItemsByColumnValue($column, $value)
 * @method Transfer getFirstItem()
 * @method Transfer getLastItem()
 * @method Transfer getItemByColumnValue($column, $value)
 * @method Transfer getItemById($idValue)
 * @method Transfer getNewEmptyItem()
 * @method Transfer fetchItem()
 * @property Transfer[] _items
 * @property ResourceTransfer _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_transfer_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Transfer::class, ResourceTransfer::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
