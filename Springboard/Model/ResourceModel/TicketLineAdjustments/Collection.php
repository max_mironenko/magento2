<?php declare(strict_types=1);

namespace C38\Springboard\Model\ResourceModel\TicketLineAdjustments;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\Springboard\Model\TicketLineAdjustments;
use C38\Springboard\Model\ResourceModel\TicketLineAdjustments as ResourceTicketLineAdjustments;

/**
 * Class Collection
 * Collection of Springboard ticket line adjustments
 *
 * @method ResourceTicketLineAdjustments getResource()
 * @method TicketLineAdjustments[] getItems()
 * @method TicketLineAdjustments[] getItemsByColumnValue($column, $value)
 * @method TicketLineAdjustments getFirstItem()
 * @method TicketLineAdjustments getLastItem()
 * @method TicketLineAdjustments getItemByColumnValue($column, $value)
 * @method TicketLineAdjustments getItemById($idValue)
 * @method TicketLineAdjustments getNewEmptyItem()
 * @method TicketLineAdjustments fetchItem()
 * @property TicketLineAdjustments[] _items
 * @property ResourceTicketLineAdjustments _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_springboard_ticket_line_adjustments_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(TicketLineAdjustments::class, ResourceTicketLineAdjustments::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
