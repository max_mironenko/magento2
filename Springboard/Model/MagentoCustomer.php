<?php declare(strict_types=1);

namespace C38\Springboard\Model;

use C38\Springboard\Api\Data\MagentoCustomerInterface;
use C38\Springboard\Model\ResourceModel\MagentoCustomer as ResourceMagentoCustomer;
use C38\Springboard\Model\ResourceModel\MagentoCustomer\Collection as ResourceMagentoCustomerCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class MagentoCustomer
 * Springboard magento customer model
 *
 * @method ResourceMagentoCustomer getResource()
 * @method ResourceMagentoCustomerCollection getCollection()
 * @method ResourceMagentoCustomerCollection getResourceCollection()
 */
class MagentoCustomer extends AbstractModel implements MagentoCustomerInterface
{
    protected $_eventPrefix = 'c38_springboard_model_magento_customer';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceMagentoCustomer::class);
    }

    /**
     * Get Id value
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id value
     *
     * @param $value
     * @return MagentoCustomerInterface
     */
    public function setId($value): MagentoCustomerInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get EntityId value
     *
     * @return string|null
     */
    public function getEntityId(): ?string
    {
        return $this->_getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId value
     *
     * @param $entityId
     * @return MagentoCustomerInterface
     */
    public function setEntityId($entityId): MagentoCustomerInterface
    {
        $this->setData(self::ENTITY_ID, $entityId);
        return $this;
    }

    /**
     * Get SpringboardId value
     *
     * @return string|null
     */
    public function getSpringboardId(): ?string
    {
        return $this->_getData(self::SPRINGBOARD_ID);
    }

    /**
     * Set SpringboardId value
     *
     * @param $value
     * @return MagentoCustomerInterface
     */
    public function setSpringboardId($value): MagentoCustomerInterface
    {
        $this->setData(self::SPRINGBOARD_ID, $value);
        return $this;
    }

    /**
     * Get CreatedAt value
     *
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt value
     *
     * @param $value
     * @return MagentoCustomerInterface
     */
    public function setCreatedAt($value): MagentoCustomerInterface
    {
        $this->setData(self::CREATED_AT, $value);
        return $this;
    }

    /**
     * Get IsSynced value
     *
     * @return string|null
     */
    public function getIsSynced(): ?string
    {
        return $this->_getData(self::IS_SYNCED);
    }

    /**
     * Set IsSynced value
     *
     * @param $value
     * @return MagentoCustomerInterface
     */
    public function setIsSynced($value): MagentoCustomerInterface
    {
        $this->setData(self::IS_SYNCED, $value);
        return $this;
    }
}
