<?php declare(strict_types=1);

namespace C38\Springboard\Model\Eav\Product;

/**
 * Class SpringboardId
 * Contain SpringboardId attribute for products
 */
class SpringboardId
{
    const ATTRIBUTE_CODE = 'springboard_id';
}
