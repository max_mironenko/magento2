<?php declare(strict_types=1);

namespace C38\Springboard\Model\Eav\Product;

/**
 * Class SpringboardSyncDt
 * Contain SpringboardSyncDt attribute for products
 */
class SpringboardSyncDt
{
    const ATTRIBUTE_CODE = 'springboard_sync_dt';
}
