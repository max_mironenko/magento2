<?php declare(strict_types=1);

namespace C38\ProductImport\Api;

use C38\ProductImport\Api\Data\SwatchAttributeInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface SwatchAttributeRepositoryInterface
 */
interface SwatchAttributeRepositoryInterface
{
    /**
     * Save Swatch Attribute
     *
     * @param SwatchAttributeInterface $swatchAttribute
     *
     * @return SwatchAttributeInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(SwatchAttributeInterface $swatchAttribute): SwatchAttributeInterface;

    /**
     * Retrieve Swatch Attributes matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;
}
