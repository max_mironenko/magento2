<?php declare(strict_types=1);

namespace C38\ProductImport\Api;

use C38\ProductImport\Api\Data\PurchaseOrderLoggerInterface;

/**
 * Interface PurchaseOrderLoggerRepositoryInterface
 */
interface PurchaseOrderLoggerRepositoryInterface
{
    /**
     * Save Log Message
     *
     * @param PurchaseOrderLoggerInterface $logger
     * @return PurchaseOrderLoggerInterface
     */
    public function save(PurchaseOrderLoggerInterface $logger): PurchaseOrderLoggerInterface;
}
