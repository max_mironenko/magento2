<?php declare(strict_types=1);

namespace C38\ProductImport\Api;

use C38\ProductImport\Api\Data\StyleHistoryInterface;
use Magento\Framework\Exception;

/**
 * Interface StyleHistoryRepositoryInterface
 */
interface StyleHistoryRepositoryInterface
{
    /**
     * Save StyleHistory
     *
     * @param StyleHistoryInterface $style
     *
     * @return StyleHistoryInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(StyleHistoryInterface $style): StyleHistoryInterface;
}
