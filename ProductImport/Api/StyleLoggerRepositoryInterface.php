<?php declare(strict_types=1);

namespace C38\ProductImport\Api;

use C38\ProductImport\Api\Data\StyleLoggerInterface;

/**
 * Interface StyleLoggerRepositoryInterface
 */
interface StyleLoggerRepositoryInterface
{
    /**
     * Save Log Message
     *
     * @param StyleLoggerInterface $logger
     * @return StyleLoggerInterface
     */
    public function save(StyleLoggerInterface $logger): StyleLoggerInterface;
}
