<?php declare(strict_types=1);

namespace C38\ProductImport\Api;

use C38\ProductImport\Api\Data\SuperAttributeInterface;
use Magento\Framework\Exception;

/**
 * Interface SuperAttributeRepositoryInterface
 */
interface SuperAttributeRepositoryInterface
{
    /**
     * Save Style
     *
     * @param SuperAttributeInterface $supperAttribute
     *
     * @return SuperAttributeInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(SuperAttributeInterface $supperAttribute): SuperAttributeInterface;
}
