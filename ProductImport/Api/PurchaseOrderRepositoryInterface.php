<?php declare(strict_types=1);

namespace C38\ProductImport\Api;

use C38\ProductImport\Api\Data\PurchaseOrderInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface PurchaseOrderRepositoryInterface
 */
interface PurchaseOrderRepositoryInterface
{
    /**
     * Save PurchaseOrder
     *
     * @param PurchaseOrderInterface $purchaseOrder
     *
     * @return PurchaseOrderInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(PurchaseOrderInterface $purchaseOrder): PurchaseOrderInterface;

    /**
     * Get PurchaseOrder by id.
     *
     * @param $purchaseOrderId
     *
     * @return PurchaseOrderInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($purchaseOrderId): PurchaseOrderInterface;

    /**
     * Find PurchaseOrder by id.
     *
     * @param $purchaseOrderId
     * @return PurchaseOrderInterface|null
     */
    public function findById($purchaseOrderId): ?PurchaseOrderInterface;

    /**
     * Get PurchaseOrder by PO number.
     *
     * @param $purchaseOrderNumber
     *
     * @return PurchaseOrderInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getByPurchaseOrderNumber($purchaseOrderNumber): PurchaseOrderInterface;

    /**
     * Retrieve PurchaseOrder matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete PurchaseOrder
     *
     * @param PurchaseOrderInterface $purchaseOrder
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(PurchaseOrderInterface $purchaseOrder): bool;

    /**
     * Delete PurchaseOrder by ID.
     *
     * @param $purchaseOrderId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($purchaseOrderId): bool;

    /**
     * Update Purchase Order
     *
     * @param mixed $purchaseOrderData
     * @return array
     * @throws Exception\CouldNotSaveException
     */
    public function updatePurchaseOrder($purchaseOrderData): array;
}
