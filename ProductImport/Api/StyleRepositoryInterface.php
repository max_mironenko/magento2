<?php declare(strict_types=1);

namespace C38\ProductImport\Api;

use C38\ProductImport\Api\Data\StyleInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface StyleRepositoryInterface
 */
interface StyleRepositoryInterface
{
    /**
     * Save Style
     *
     * @param StyleInterface $style
     *
     * @return StyleInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(StyleInterface $style): StyleInterface;

    /**
     * Get Style by id.
     *
     * @param $styleId
     *
     * @return StyleInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($styleId): StyleInterface;

    /**
     * Find Style by id.
     *
     * @param $styleId
     * @return StyleInterface|null
     */
    public function findById($styleId): ?StyleInterface;

    /**
     * Get Style by SKU.
     *
     * @param $styleSku
     *
     * @return StyleInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getBySku($styleSku): StyleInterface;

    /**
     * Retrieve Style matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete Style
     *
     * @param StyleInterface $style
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(StyleInterface $style): bool;

    /**
     * Delete Style by ID.
     *
     * @param $styleId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($styleId): bool;

    /**
     * Mark style record as processed and save to the History Repository
     *
     * @param $styleId
     * @param $productID
     *
     * @return bool
     * @throws Exception\CouldNotSaveException
     */
    public function markAsProcessed($styleId, $productID): bool;

    /**
     * Update style
     *
     * @param mixed $styleData
     * @return array
     * @throws Exception\CouldNotSaveException
     */
    public function updateStyle($styleData): array;
}
