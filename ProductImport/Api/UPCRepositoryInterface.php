<?php declare(strict_types=1);

namespace C38\ProductImport\Api;

use Throwable;

/**
 * Interface UPCRepositoryInterface
 */
interface UPCRepositoryInterface
{
    /**
     * Calculate UPC value for the product id
     *
     * @param string $productId
     *
     * @return string
     * @throws Throwable
     */
    public function calculateUPC(string $productId): string;
}
