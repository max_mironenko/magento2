<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

/**
 * Interface PurchaseOrderLoggerInterface
 */
interface PurchaseOrderLoggerInterface
{
    // table name
    const TABLE_NAME = 'c38_import_purchase_orders_logs';

    // table columns
    const ID = 'id';
    const TYPE = 'type';
    const MESSAGE = 'message';
    const STACK = 'stack';
    const CRON_NAME = 'cron_name';
    const CREATED_AT = 'created_at';

    /**
     * Get ID
     *
     * @return string | null
     */
    public function getId(): ?string;

    /**
     * Set ID
     *
     * @param $value
     * @return PurchaseOrderLoggerInterface
     */
    public function setId($value): PurchaseOrderLoggerInterface;

    /**
     * Get Message Type
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Set Message Type
     *
     * @param $value
     * @return PurchaseOrderLoggerInterface
     */
    public function setType($value): PurchaseOrderLoggerInterface;

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage(): string;

    /**
     * Set Message
     *
     * @param $value
     * @return PurchaseOrderLoggerInterface
     */
    public function setMessage($value): PurchaseOrderLoggerInterface;

    /**
     * Get Error stack trace
     *
     * @return string
     */
    public function getStack(): string;

    /**
     * Set Error stack trace
     *
     * @param $value
     * @return PurchaseOrderLoggerInterface
     */
    public function setStack($value): PurchaseOrderLoggerInterface;

    /**
     * Get Cron Name
     *
     * @return string
     */
    public function getCronName(): string;

    /**
     * Set Cron Name
     *
     * @param $value
     * @return PurchaseOrderLoggerInterface
     */
    public function setCronName($value): PurchaseOrderLoggerInterface;

    /**
     * Get Created At
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Set Created At
     *
     * @param $value
     * @return PurchaseOrderLoggerInterface
     */
    public function setCreatedAt($value): PurchaseOrderLoggerInterface;
}
