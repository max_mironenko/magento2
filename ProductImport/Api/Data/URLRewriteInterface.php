<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

/**
 * Interface URLRewriteInterface
 */
interface URLRewriteInterface
{
    // table name
    const TABLE_NAME = 'url_rewrite';

    // table columns
    const ID = 'url_rewrite_id';
    const ENTITY_TYPE = 'entity_type';
    const ENTITY_ID = 'entity_id';
    const REQUEST_PATH = 'request_path';
    const TARGET_PATH = 'target_path';
    const REDIRECT_TYPE = 'redirect_type';
    const STORE_ID = 'store_id';

    /**
     * Get Id value
     *
     * @return string | null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setId($value): URLRewriteInterface;

    /**
     * Get EntityType value
     *
     * @return string
     */
    public function getEntityType(): string;

    /**
     * Set EntityType value
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setEntityType($value): URLRewriteInterface;

    /**
     * Get EntityId value
     *
     * @return string
     */
    public function getEntityId(): string;

    /**
     * Set EntityId value
     *
     * @param $entityId
     * @return URLRewriteInterface
     */
    public function setEntityId($entityId): URLRewriteInterface;

    /**
     * Get RequestPath value
     *
     * @return string
     */
    public function getRequestPath(): string;

    /**
     * Set RequestPath value
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setRequestPath($value): URLRewriteInterface;

    /**
     * Get TargetPath value
     *
     * @return string
     */
    public function getTargetPath(): string;

    /**
     * Set TargetPath value
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setTargetPath($value): URLRewriteInterface;

    /**
     * Get RedirectType value
     *
     * @return string
     */
    public function getRedirectType(): string;

    /**
     * Set RedirectType value
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setRedirectType($value): URLRewriteInterface;

    /**
     * Get StoreId value
     *
     * @return string
     */
    public function getStoreId(): string;

    /**
     * Set StoreId value
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setStoreId($value): URLRewriteInterface;
}
