<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface StyleSearchResultsInterface
 */
interface StyleSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Style list.
     *
     * @return StyleInterface[]
     */
    public function getItems(): array;

    /**
     * Set Style list.
     *
     * @param StyleInterface[] $items
     * @return StyleSearchResultsInterface
     */
    public function setItems(array $items): StyleSearchResultsInterface;
}
