<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface URLRewriteSearchResultsInterface
 */
interface URLRewriteSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Style list.
     *
     * @return URLRewriteInterface[]
     */
    public function getItems(): array;

    /**
     * Set Style list.
     *
     * @param URLRewriteInterface[] $items
     * @return URLRewriteSearchResultsInterface
     */
    public function setItems(array $items): URLRewriteSearchResultsInterface;
}
