<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

/**
 * Interface StyleHistoryInterface
 */
interface StyleHistoryInterface
{
    // table name
    const TABLE_NAME = 'c38_import_styles_processing_history';

    // table columns
    const ID = 'id';
    const STYLE_CATEGORY_SUB_DESCRIPTION = 'StyleCategorySub_Description';
    const STYLE_SEASONS_LABEL = 'StyleSeasons_Label';
    const STYLE_COLOR_SECONDARY_NAME = 'StyleColorSecondary_Name';
    const STYLE_COLOR_SECONDARY_ABBREVIATION = 'StyleColorSecondary_Abbreviation';
    const STYLE_CATEGORY_DESCRIPTION = 'StyleCategory_Description';
    const STYLE_COLOR_VENDOR_SKU = 'StyleColor_VendorSku';
    const STYLE_COLOR_VENDOR_COLOR = 'StyleColor_VendorColor';
    const STYLE_COLOR_LAUNCH_DATE = 'StyleColor_LaunchDate';
    const STYLE_COLOR_STYLE_SKU_COLOR = 'StyleColor_StyleSkuColor';
    const STYLE_SKU_BASE = 'StyleSkuBase';
    const STYLE_BASE_NAME = 'StyleBase_Name';
    const STYLE_BASE_MSRP = 'StyleBase_MSRP';
    const STYLE_BASE_LAUNCH_DATE = 'StyleBase_LaunchDate';
    const STYLE_BASE_STYLE_SKU_CODE = 'StyleBase_StyleSkuCode';
    const STYLE_BASE_DEF_VENDOR_COST = 'StyleBase_DefVendorCost';
    const STYLE_BASE_STYLE_SKU_BASE = 'StyleBase_StyleSkuBase';
    const STYLE_BASE_STYLE_SKU_DESIGNER = 'StyleBase_StyleSkuDesigner';
    const STYLE_CATEGORY_PARENT_DESCRIPTION = 'StyleCategoryParent_Description';
    const INVENTORY_SKU = 'Inventory_SKU';
    const INVENTORY_SIZE = 'Inventory_Size';
    const STYLE_COLOR_PRIMARY_NAME = 'StyleColorPrimary_Name';
    const STYLE_COLOR_PRIMARY_ABBREVIATION = 'StyleColorPrimary_abbreviation';
    const STYLE_DESIGNER_DESCRIPTION = 'StyleDesigner_Description';
    const STYLE_COLOR_PATTERN_LABEL = 'StyleColorPattern_Label';
    const STYLE_BASE_VENDOR_DESCRIPTION = 'StyleBase_VendorDescription';
    const STYLE_COLOR_VENDOR_COST = 'StyleColor_VendorCost';
    const STYLE_CATEGORY_SUB_DEF_WEIGHT = 'StyleCategorySub_DefWeight';
    const STYLE_BASE_SKU_WITH_COLOR = 'StyleBaseSkuWithColor';
    const IS_PROCESSED = 'is_processed';
    const CREATED_AT = 'created_at';
    const IS_CONFIGURABLE = 'is_configurable';
    const IS_EXCLUSIVE = 'Is_Exclusive';
    const STYLE_COLOR_COST_AFTER_DISCOUNT = 'StyleColor_CostAfterDiscount';
    const STYLE_COLOR_STANDARD_COST = 'StyleColor_StandardCost';
    const COLLECTION = 'Collection';
    const PROCESSED_AT = 'processed_at';

    /**
     * Get Id value
     *
     * @return string | null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setId($value): StyleHistoryInterface;

    /**
     * Get StyleCategorySubDescription value
     *
     * @return string
     */
    public function getStyleCategorySubDescription(): string;

    /**
     * Set StyleCategorySubDescription value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleCategorySubDescription($value): StyleHistoryInterface;

    /**
     * Get StyleSeasonsLabel value
     *
     * @return string
     */
    public function getStyleSeasonsLabel(): string;

    /**
     * Set StyleSeasonsLabel value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleSeasonsLabel($value): StyleHistoryInterface;

    /**
     * Get StyleColorSecondaryName value
     *
     * @return string
     */
    public function getStyleColorSecondaryName(): string;

    /**
     * Set StyleColorSecondaryName value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorSecondaryName($value): StyleHistoryInterface;

    /**
     * Get StyleColorSecondaryAbbreviation value
     *
     * @return string
     */
    public function getStyleColorSecondaryAbbreviation(): string;

    /**
     * Set StyleColorSecondaryAbbreviation value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorSecondaryAbbreviation($value): StyleHistoryInterface;

    /**
     * Get StyleCategoryDescription value
     *
     * @return string
     */
    public function getStyleCategoryDescription(): string;

    /**
     * Set StyleCategoryDescription value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleCategoryDescription($value): StyleHistoryInterface;

    /**
     * Get StyleColorVendorSku value
     *
     * @return string
     */
    public function getStyleColorVendorSku(): string;

    /**
     * Set StyleColorVendorSku value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorVendorSku($value): StyleHistoryInterface;

    /**
     * Get StyleColorVendorColor value
     *
     * @return string
     */
    public function getStyleColorVendorColor(): string;

    /**
     * Set StyleColorVendorColor value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorVendorColor($value): StyleHistoryInterface;

    /**
     * Get StyleColorLaunchDate value
     *
     * @return string
     */
    public function getStyleColorLaunchDate(): string;

    /**
     * Set StyleColorLaunchDate value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorLaunchDate($value): StyleHistoryInterface;

    /**
     * Get StyleColorStyleSkuColor value
     *
     * @return string
     */
    public function getStyleColorStyleSkuColor(): string;

    /**
     * Set StyleColorStyleSkuColor value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorStyleSkuColor($value): StyleHistoryInterface;

    /**
     * Get StyleSkuBase value
     *
     * @return string
     */
    public function getStyleSkuBase(): string;

    /**
     * Set StyleSkuBase value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleSkuBase($value): StyleHistoryInterface;

    /**
     * Get StyleBaseName value
     *
     * @return string
     */
    public function getStyleBaseName(): string;

    /**
     * Set StyleBaseName value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleBaseName($value): StyleHistoryInterface;

    /**
     * Get StyleBaseMSRP value
     * @return string
     */
    public function getStyleBaseMSRP(): string;

    /**
     * Set StyleBaseMSRP value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleBaseMSRP($value): StyleHistoryInterface;

    /**
     * Get StyleBaseLaunchDate value
     *
     * @return string
     */
    public function getStyleBaseLaunchDate(): string;

    /**
     * Set StyleBaseLaunchDate value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleBaseLaunchDate($value): StyleHistoryInterface;

    /**
     * Get StyleBaseStyleSkuCode value
     *
     * @return string
     */
    public function getStyleBaseStyleSkuCode(): string;

    /**
     * Set StyleBaseStyleSkuCode value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleBaseStyleSkuCode($value): StyleHistoryInterface;

    /**
     * Get StyleBaseDefVendorCost value
     *
     * @return string
     */
    public function getStyleBaseDefVendorCost(): string;

    /**
     * Set StyleBaseDefVendorCost value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleBaseDefVendorCost($value): StyleHistoryInterface;

    /**
     * Get StyleBaseStyleSkuBase value
     *
     * @return string
     */
    public function getStyleBaseStyleSkuBase(): string;

    /**
     * Set StyleBaseStyleSkuBase value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleBaseStyleSkuBase($value): StyleHistoryInterface;

    /**
     * Get StyleBaseStyleSkuDesigner value
     *
     * @return string
     */
    public function getStyleBaseStyleSkuDesigner(): string;

    /**
     * Set StyleBaseStyleSkuDesigner value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleBaseStyleSkuDesigner($value): StyleHistoryInterface;

    /**
     * Get StyleCategoryParentDescription value
     *
     * @return string
     */
    public function getStyleCategoryParentDescription(): string;

    /**
     * Set StyleCategoryParentDescription value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleCategoryParentDescription($value): StyleHistoryInterface;

    /**
     * Get InventorySKU value
     *
     * @return string
     */
    public function getInventorySKU(): string;

    /**
     * Set InventorySKU value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setInventorySKU($value): StyleHistoryInterface;

    /**
     * Get InventorySize value
     *
     * @return string
     */
    public function getInventorySize(): string;

    /**
     * Set InventorySize value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setInventorySize($value): StyleHistoryInterface;

    /**
     * Get StyleColorPrimaryName value
     *
     * @return string
     */
    public function getStyleColorPrimaryName(): string;

    /**
     * Set StyleColorPrimaryName value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorPrimaryName($value): StyleHistoryInterface;

    /**
     * Get StyleColorPrimaryAbbreviation value
     *
     * @return string
     */
    public function getStyleColorPrimaryAbbreviation(): string;

    /**
     * Set StyleColorPrimaryAbbreviation value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorPrimaryAbbreviation($value): StyleHistoryInterface;

    /**
     * Get StyleDesignerDescription value
     *
     * @return string
     */
    public function getStyleDesignerDescription(): string;

    /**
     * Set StyleDesignerDescription value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleDesignerDescription($value): StyleHistoryInterface;

    /**
     * Get StyleColorPatternLabel value
     *
     * @return string
     */
    public function getStyleColorPatternLabel(): string;

    /**
     * Set StyleColorPatternLabel value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorPatternLabel($value): StyleHistoryInterface;

    /**
     * Get StyleBaseVendorDescription value
     *
     * @return string
     */
    public function getStyleBaseVendorDescription(): string;

    /**
     * Set StyleBaseVendorDescription value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleBaseVendorDescription($value): StyleHistoryInterface;

    /**
     * Get StyleColorVendorCost value
     *
     * @return string
     */
    public function getStyleColorVendorCost(): string;

    /**
     * Set StyleColorVendorCost value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorVendorCost($value): StyleHistoryInterface;

    /**
     * Get StyleCategorySubDefWeight value
     *
     * @return string
     */
    public function getStyleCategorySubDefWeight(): string;

    /**
     * Set StyleCategorySubDefWeight value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleCategorySubDefWeight($value): StyleHistoryInterface;

    /**
     * Get StyleBaseSkuWithColor value
     *
     * @return string
     */
    public function getStyleBaseSkuWithColor(): string;

    /**
     * Set StyleBaseSkuWithColor value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleBaseSkuWithColor($value): StyleHistoryInterface;

    /**
     * Get IsProcessed value
     *
     * @return string
     */
    public function getIsProcessed(): string;

    /**
     * Set IsProcessed value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setIsProcessed($value): StyleHistoryInterface;

    /**
     * Get CreatedAt value
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Set CreatedAt value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setCreatedAt($value): StyleHistoryInterface;

    /**
     * Get IsConfigurable value
     *
     * @return int
     */
    public function getIsConfigurable(): int;

    /**
     * Set IsConfigurable value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setIsConfigurable($value): StyleHistoryInterface;

    /**
     * Get IsExclusive value
     *
     * @return int
     */
    public function getIsExclusive(): int;

    /**
     * Set IsExclusive value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setIsExclusive($value): StyleHistoryInterface;

    /**
     * Get StyleColorCostAfterDiscount value
     *
     * @return string
     */
    public function getStyleColorCostAfterDiscount(): string;

    /**
     * Set StyleColorCostAfterDiscount value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorCostAfterDiscount($value): StyleHistoryInterface;

    /**
     * Get StyleColorStandardCost value
     *
     * @return string
     */
    public function getStyleColorStandardCost(): string;

    /**
     * Set StyleColorStandardCost value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setStyleColorStandardCost($value): StyleHistoryInterface;

    /**
     * Get Collection value
     *
     * @return string
     */
    public function getCollectionName(): string;

    /**
     * Set Collection value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setCollectionName($value): StyleHistoryInterface;

    /**
     * Get Processed At value
     *
     * @return string
     */
    public function getProcessedAt(): string;

    /**
     * Set Processed At value
     *
     * @param $value
     * @return StyleHistoryInterface
     */
    public function setProcessedAt($value): StyleHistoryInterface;
}
