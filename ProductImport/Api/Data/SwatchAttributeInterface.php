<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

/**
 * Interface SwatchAttributeInterface
 */
interface SwatchAttributeInterface
{
    // table name
    const TABLE_NAME = 'eav_attribute_option_swatch';

    // table columns
    const SWATCH_ID  = 'swatch_id';
    const OPTION_ID = 'option_id';
    const STORE_ID = 'store_id';
    const TYPE = 'type';
    const VALUE = 'value';

    /**
     * Get Swatch value
     *
     * @return string | null
     */
    public function getSwatchId(): ?string;

    /**
     * Set Swatch value
     *
     * @param $value
     * @return SwatchAttributeInterface
     */
    public function setSwatchId($value): SwatchAttributeInterface;

    /**
     * Get OptionId value
     *
     * @return string | null
     */
    public function getOptionId(): ?string;

    /**
     * Set OptionId value
     *
     * @param $value
     * @return SwatchAttributeInterface
     */
    public function setOptionId($value): SwatchAttributeInterface;

    /**
     * Get StoreId value
     *
     * @return string
     */
    public function getStoreId(): string;

    /**
     * Set StoreId value
     *
     * @param $value
     * @return SwatchAttributeInterface
     */
    public function setStoreId($value): SwatchAttributeInterface;

    /**
     * Get Type value
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Set Type value
     *
     * @param $value
     * @return SwatchAttributeInterface
     */
    public function setType($value): SwatchAttributeInterface;

    /**
     * Get Value value
     *
     * @return string | null
     */
    public function getValue(): ?string;

    /**
     * Set Value value
     *
     * @param $value
     * @return SwatchAttributeInterface
     */
    public function setValue($value): SwatchAttributeInterface;
}
