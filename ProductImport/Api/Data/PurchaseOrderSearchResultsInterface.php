<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface PurchaseOrderSearchResultsInterface
 */
interface PurchaseOrderSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Purchase Orders list.
     *
     * @return PurchaseOrderInterface[]
     */
    public function getItems(): array;

    /**
     * Set Purchase Orders list.
     *
     * @param PurchaseOrderInterface[] $items
     * @return PurchaseOrderSearchResultsInterface
     */
    public function setItems(array $items): PurchaseOrderSearchResultsInterface;
}
