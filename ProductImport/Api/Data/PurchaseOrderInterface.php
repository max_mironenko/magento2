<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

/**
 * Interface PurchaseOrderInterface
 */
interface PurchaseOrderInterface
{
    // table name
    const TABLE_NAME = 'c38_import_purchase_orders';

    // table columns
    const ID = 'id';
    const ORDER_ID = 'order_id';
    const PO_NUMBER = 'po_number';
    const PO_DATE = 'po_date';
    const VENDOR_NAME = 'vendor_name';
    const WAREHOUSE_NAME = 'warehouse_name';
    const USER_NAME = 'user_name';
    const USER_EMAIL = 'user_email';
    const MANIFEST_ID = 'manifest_id';
    const MANIFEST_DATE = 'manifest_date';
    const CREATED_AT = 'created_at';
    const PO_CANCEL_DATE = 'po_cancel_date';
    const UPDATED_AT = 'updated_at';
    const LINES_NUMBER = 'lines_number';

    /**
     * Get PO Id
     *
     * @return string | null
     */
    public function getId(): ?string;

    /**
     * Set PO Id
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setId($value): PurchaseOrderInterface;

    /**
     * Get Order Id
     *
     * @return string
     */
    public function getOrderId(): string;

    /**
     * Set Order Id
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setOrderId($value): PurchaseOrderInterface;

    /**
     * Get PO Number
     *
     * @return string
     */
    public function getPoNumber(): string;

    /**
     * Set PO Number
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setPoNumber($value): PurchaseOrderInterface;

    /**
     * Get PO Date
     *
     * @return string
     */
    public function getPoDate(): string;

    /**
     * Set PO Date
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setPoDate($value): PurchaseOrderInterface;

    /**
     * Get Vendor Name
     *
     * @return string
     */
    public function getVendorName(): string;

    /**
     * Set Vendor Name
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setVendorName($value): PurchaseOrderInterface;

    /**
     * Get Warehouse Name
     *
     * @return string
     */
    public function getWarehouseName(): string;

    /**
     * Set Warehouse Name
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setWarehouseName($value): PurchaseOrderInterface;

    /**
     * Get User Name
     *
     * @return string
     */
    public function getUserName(): string;

    /**
     * Set User Name
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setUserName($value): PurchaseOrderInterface;

    /**
     * Get User Email
     *
     * @return string
     */
    public function getUserEmail(): string;

    /**
     * Set User Email
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setUserEmail($value): PurchaseOrderInterface;

    /**
     * Get Manifest Id
     *
     * @return string
     */
    public function getManifestId(): string;

    /**
     * Set Manifest Id
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setManifestId($value): PurchaseOrderInterface;

    /**
     * Get Manifest Date
     *
     * @return string
     */
    public function getManifestDate(): string;

    /**
     * Set Manifest Date
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setManifestDate($value): PurchaseOrderInterface;

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Set Created Date
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setCreatedAt($value): PurchaseOrderInterface;

    /**
     * Get Cancel Date
     *
     * @return string
     */
    public function getPoCancelDate(): string;

    /**
     * Set Cancel Date
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setPoCancelDate($value): PurchaseOrderInterface;

    /**
     * Get Updated Date
     *
     * @return string
     */
    public function getUpdatedAt(): string;

    /**
     * Set Updated Date
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setUpdatedAt($value): PurchaseOrderInterface;

    /**
     * Get Lines Number
     *
     * @return string
     */
    public function getLinesNumber(): string;

    /**
     * Set Lines Number
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setLinesNumber($value): PurchaseOrderInterface;
}
