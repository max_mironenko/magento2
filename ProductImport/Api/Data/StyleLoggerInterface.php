<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

/**
 * Interface StyleLoggerInterface
 */
interface StyleLoggerInterface
{
    // table name
    const TABLE_NAME = 'c38_import_styles_log';

    // table columns
    const ID = 'id';
    const TYPE = 'type';
    const MESSAGE = 'message';
    const FULL_ERROR = 'full_error';
    const CRON_NAME = 'cron_name';
    const PRODUCT_ID = 'product_id';
    const CREATED_AT = 'created_at';

    /**
     * Get ID
     *
     * @return string | null
     */
    public function getId(): ?string;

    /**
     * Set ID
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setId($value): StyleLoggerInterface;

    /**
     * Get Message Type
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Set Message Type
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setType($value): StyleLoggerInterface;

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage(): string;

    /**
     * Set Message
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setMessage($value): StyleLoggerInterface;

    /**
     * Get Full Error (stack trace)
     *
     * @return string
     */
    public function getFullError(): string;

    /**
     * Set Full Error (stack trace)
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setFullError($value): StyleLoggerInterface;

    /**
     * Get Cron Name
     *
     * @return string
     */
    public function getCronName(): string;

    /**
     * Set Cron Name
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setCronName($value): StyleLoggerInterface;

    /**
     * Get ProductId
     *
     * @return string
     */
    public function getProductId(): string;

    /**
     * Set ProductId
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setProductId($value): StyleLoggerInterface;

    /**
     * Get Created At
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Set Created At
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setCreatedAt($value): StyleLoggerInterface;
}
