<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

/**
 * Interface PurchaseOrderLineInterface
 */
interface PurchaseOrderLineInterface
{
    // table name
    const TABLE_NAME = 'c38_import_purchase_orders_lines';

    // table columns
    const ID = 'id';
    const ORDER_ID = 'order_id';
    const SKU = 'sku';
    const SIZE = 'size';
    const VENDOR_COLOR = 'vendor_color';
    const QTY = 'qty';
    const COST = 'cost';
    const IDENTIFIER = 'identifier';
    const CREATED_AT = 'created_at';
    const MANIFEST_ID = 'manifest_id';
    const UPDATED_AT = 'updated_at';

    /**
     * Get Id
     *
     * @return string | null
     */
    public function getId(): ?string;

    /**
     * Set Id
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setId($value): PurchaseOrderLineInterface;

    /**
     * Get Order Id
     *
     * @return string
     */
    public function getOrderId(): string;

    /**
     * Set Order Id
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setOrderId($value): PurchaseOrderLineInterface;

    /**
     * Get Product Sku
     *
     * @return string
     */
    public function getSku(): string;

    /**
     * Set Product Sku
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setSku($value): PurchaseOrderLineInterface;

    /**
     * Get Product Size
     *
     * @return string
     */
    public function getSize(): string;

    /**
     * Set Product Size
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setSize($value): PurchaseOrderLineInterface;

    /**
     * Get Vendor Color
     *
     * @return string
     */
    public function getVendorColor(): string;

    /**
     * Set Vendor Color
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setVendorColor($value): PurchaseOrderLineInterface;

    /**
     * Get Qty
     *
     * @return float
     */
    public function getQty(): float;

    /**
     * Set Qty
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setQty($value): PurchaseOrderLineInterface;

    /**
     * Get Cost
     *
     * @return float
     */
    public function getCost(): float;

    /**
     * Set Cost
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setCost($value): PurchaseOrderLineInterface;

    /**
     * Get Identifier
     *
     * @return string
     */
    public function getIdentifier(): string;

    /**
     * Set Identifier
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setIdentifier($value): PurchaseOrderLineInterface;

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Set Created Date
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setCreatedAt($value): PurchaseOrderLineInterface;

    /**
     * Get Manifest Id
     *
     * @return string
     */
    public function getManifestId(): string;

    /**
     * Set Manifest Id
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setManifestId($value): PurchaseOrderLineInterface;

    /**
     * Get Updated Date
     *
     * @return string
     */
    public function getUpdatedAt(): string;

    /**
     * Set Updated Date
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setUpdatedAt($value): PurchaseOrderLineInterface;
}
