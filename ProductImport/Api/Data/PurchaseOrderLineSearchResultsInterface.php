<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface PurchaseOrderLineSearchResultsInterface
 */
interface PurchaseOrderLineSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Purchase Orders list.
     *
     * @return PurchaseOrderLineInterface[]
     */
    public function getItems(): array;

    /**
     * Set Purchase Orders list.
     *
     * @param PurchaseOrderLineInterface[] $items
     * @return PurchaseOrderLineSearchResultsInterface
     */
    public function setItems(array $items): PurchaseOrderLineSearchResultsInterface;
}
