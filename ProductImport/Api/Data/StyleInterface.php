<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

/**
 * Interface StyleInterface
 */
interface StyleInterface
{
    // table name
    const TABLE_NAME = 'c38_import_styles';

    // table columns
    const ID = 'id';
    const STYLE_CATEGORY_SUB_DESCRIPTION = 'StyleCategorySub_Description';
    const STYLE_SEASONS_LABEL = 'StyleSeasons_Label';
    const STYLE_COLOR_SECONDARY_NAME = 'StyleColorSecondary_Name';
    const STYLE_COLOR_SECONDARY_ABBREVIATION = 'StyleColorSecondary_Abbreviation';
    const STYLE_CATEGORY_DESCRIPTION = 'StyleCategory_Description';
    const STYLE_COLOR_VENDOR_SKU = 'StyleColor_VendorSku';
    const STYLE_COLOR_VENDOR_COLOR = 'StyleColor_VendorColor';
    const STYLE_COLOR_LAUNCH_DATE = 'StyleColor_LaunchDate';
    const STYLE_COLOR_STYLE_SKU_COLOR = 'StyleColor_StyleSkuColor';
    const STYLE_SKU_BASE = 'StyleSkuBase';
    const STYLE_BASE_NAME = 'StyleBase_Name';
    const STYLE_BASE_MSRP = 'StyleBase_MSRP';
    const STYLE_BASE_LAUNCH_DATE = 'StyleBase_LaunchDate';
    const STYLE_BASE_STYLE_SKU_CODE = 'StyleBase_StyleSkuCode';
    const STYLE_BASE_DEF_VENDOR_COST = 'StyleBase_DefVendorCost';
    const STYLE_BASE_STYLE_SKU_BASE = 'StyleBase_StyleSkuBase';
    const STYLE_BASE_STYLE_SKU_DESIGNER = 'StyleBase_StyleSkuDesigner';
    const STYLE_CATEGORY_PARENT_DESCRIPTION = 'StyleCategoryParent_Description';
    const INVENTORY_SKU = 'Inventory_SKU';
    const INVENTORY_SIZE = 'Inventory_Size';
    const STYLE_COLOR_PRIMARY_NAME = 'StyleColorPrimary_Name';
    const STYLE_COLOR_PRIMARY_ABBREVIATION = 'StyleColorPrimary_abbreviation';
    const STYLE_DESIGNER_DESCRIPTION = 'StyleDesigner_Description';
    const STYLE_COLOR_PATTERN_LABEL = 'StyleColorPattern_Label';
    const STYLE_BASE_VENDOR_DESCRIPTION = 'StyleBase_VendorDescription';
    const STYLE_COLOR_VENDOR_COST = 'StyleColor_VendorCost';
    const STYLE_CATEGORY_SUB_DEF_WEIGHT = 'StyleCategorySub_DefWeight';
    const STYLE_BASE_SKU_WITH_COLOR = 'StyleBaseSkuWithColor';
    const IS_PROCESSED = 'is_processed';
    const CREATED_AT = 'created_at';
    const IS_CONFIGURABLE = 'is_configurable';
    const IS_EXCLUSIVE = 'Is_Exclusive';
    const STYLE_COLOR_COST_AFTER_DISCOUNT = 'StyleColor_CostAfterDiscount';
    const STYLE_COLOR_STANDARD_COST = 'StyleColor_StandardCost';
    const COLLECTION = 'Collection';
    const ERROR = 'ERROR';

    /**
     * Get Id value
     *
     * @return string | null
     */
    public function getId(): ?string;

    /**
     * Set Id value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setId($value): StyleInterface;

    /**
     * Get StyleCategorySubDescription value
     *
     * @return string
     */
    public function getStyleCategorySubDescription(): string;

    /**
     * Set StyleCategorySubDescription value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleCategorySubDescription($value): StyleInterface;

    /**
     * Get StyleSeasonsLabel value
     *
     * @return string
     */
    public function getStyleSeasonsLabel(): string;

    /**
     * Set StyleSeasonsLabel value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleSeasonsLabel($value): StyleInterface;

    /**
     * Get StyleColorSecondaryName value
     *
     * @return string
     */
    public function getStyleColorSecondaryName(): string;

    /**
     * Set StyleColorSecondaryName value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorSecondaryName($value): StyleInterface;

    /**
     * Get StyleColorSecondaryAbbreviation value
     *
     * @return string
     */
    public function getStyleColorSecondaryAbbreviation(): string;

    /**
     * Set StyleColorSecondaryAbbreviation value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorSecondaryAbbreviation($value): StyleInterface;

    /**
     * Get StyleCategoryDescription value
     *
     * @return string
     */
    public function getStyleCategoryDescription(): string;

    /**
     * Set StyleCategoryDescription value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleCategoryDescription($value): StyleInterface;

    /**
     * Get StyleColorVendorSku value
     *
     * @return string
     */
    public function getStyleColorVendorSku(): string;

    /**
     * Set StyleColorVendorSku value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorVendorSku($value): StyleInterface;

    /**
     * Get StyleColorVendorColor value
     *
     * @return string
     */
    public function getStyleColorVendorColor(): string;

    /**
     * Set StyleColorVendorColor value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorVendorColor($value): StyleInterface;

    /**
     * Get StyleColorLaunchDate value
     *
     * @return string
     */
    public function getStyleColorLaunchDate(): string;

    /**
     * Set StyleColorLaunchDate value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorLaunchDate($value): StyleInterface;

    /**
     * Get StyleColorStyleSkuColor value
     *
     * @return string
     */
    public function getStyleColorStyleSkuColor(): string;

    /**
     * Set StyleColorStyleSkuColor value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorStyleSkuColor($value): StyleInterface;

    /**
     * Get StyleSkuBase value
     *
     * @return string
     */
    public function getStyleSkuBase(): string;

    /**
     * Set StyleSkuBase value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleSkuBase($value): StyleInterface;

    /**
     * Get StyleBaseName value
     *
     * @return string
     */
    public function getStyleBaseName(): string;

    /**
     * Set StyleBaseName value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseName($value): StyleInterface;

    /**
     * Get StyleBaseMSRP value
     * @return string
     */
    public function getStyleBaseMSRP(): string;

    /**
     * Set StyleBaseMSRP value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseMSRP($value): StyleInterface;

    /**
     * Get StyleBaseLaunchDate value
     *
     * @return string
     */
    public function getStyleBaseLaunchDate(): string;

    /**
     * Set StyleBaseLaunchDate value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseLaunchDate($value): StyleInterface;

    /**
     * Get StyleBaseStyleSkuCode value
     *
     * @return string
     */
    public function getStyleBaseStyleSkuCode(): string;

    /**
     * Set StyleBaseStyleSkuCode value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseStyleSkuCode($value): StyleInterface;

    /**
     * Get StyleBaseDefVendorCost value
     *
     * @return string
     */
    public function getStyleBaseDefVendorCost(): string;

    /**
     * Set StyleBaseDefVendorCost value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseDefVendorCost($value): StyleInterface;

    /**
     * Get StyleBaseStyleSkuBase value
     *
     * @return string
     */
    public function getStyleBaseStyleSkuBase(): string;

    /**
     * Set StyleBaseStyleSkuBase value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseStyleSkuBase($value): StyleInterface;

    /**
     * Get StyleBaseStyleSkuDesigner value
     *
     * @return string
     */
    public function getStyleBaseStyleSkuDesigner(): string;

    /**
     * Set StyleBaseStyleSkuDesigner value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseStyleSkuDesigner($value): StyleInterface;

    /**
     * Get StyleCategoryParentDescription value
     *
     * @return string
     */
    public function getStyleCategoryParentDescription(): string;

    /**
     * Set StyleCategoryParentDescription value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleCategoryParentDescription($value): StyleInterface;

    /**
     * Get InventorySKU value
     *
     * @return string
     */
    public function getInventorySKU(): string;

    /**
     * Set InventorySKU value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setInventorySKU($value): StyleInterface;

    /**
     * Get InventorySize value
     *
     * @return string
     */
    public function getInventorySize(): string;

    /**
     * Set InventorySize value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setInventorySize($value): StyleInterface;

    /**
     * Get StyleColorPrimaryName value
     *
     * @return string
     */
    public function getStyleColorPrimaryName(): string;

    /**
     * Set StyleColorPrimaryName value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorPrimaryName($value): StyleInterface;

    /**
     * Get StyleColorPrimaryAbbreviation value
     *
     * @return string
     */
    public function getStyleColorPrimaryAbbreviation(): string;

    /**
     * Set StyleColorPrimaryAbbreviation value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorPrimaryAbbreviation($value): StyleInterface;

    /**
     * Get StyleDesignerDescription value
     *
     * @return string
     */
    public function getStyleDesignerDescription(): string;

    /**
     * Set StyleDesignerDescription value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleDesignerDescription($value): StyleInterface;

    /**
     * Get StyleColorPatternLabel value
     *
     * @return string
     */
    public function getStyleColorPatternLabel(): string;

    /**
     * Set StyleColorPatternLabel value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorPatternLabel($value): StyleInterface;

    /**
     * Get StyleBaseVendorDescription value
     *
     * @return string
     */
    public function getStyleBaseVendorDescription(): string;

    /**
     * Set StyleBaseVendorDescription value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseVendorDescription($value): StyleInterface;

    /**
     * Get StyleColorVendorCost value
     *
     * @return string
     */
    public function getStyleColorVendorCost(): string;

    /**
     * Set StyleColorVendorCost value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorVendorCost($value): StyleInterface;

    /**
     * Get StyleCategorySubDefWeight value
     *
     * @return string
     */
    public function getStyleCategorySubDefWeight(): string;

    /**
     * Set StyleCategorySubDefWeight value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleCategorySubDefWeight($value): StyleInterface;

    /**
     * Get StyleBaseSkuWithColor value
     *
     * @return string
     */
    public function getStyleBaseSkuWithColor(): string;

    /**
     * Set StyleBaseSkuWithColor value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseSkuWithColor($value): StyleInterface;

    /**
     * Get IsProcessed value
     *
     * @return string|null
     */
    public function getIsProcessed(): ?string;

    /**
     * Set IsProcessed value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setIsProcessed($value): StyleInterface;

    /**
     * Get CreatedAt value
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Set CreatedAt value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setCreatedAt($value): StyleInterface;

    /**
     * Get IsConfigurable value
     *
     * @return string
     */
    public function getIsConfigurable(): string;

    /**
     * Set IsConfigurable value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setIsConfigurable($value): StyleInterface;

    /**
     * Get IsExclusive value
     *
     * @return string
     */
    public function getIsExclusive(): string;

    /**
     * Set IsExclusive value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setIsExclusive($value): StyleInterface;

    /**
     * Get StyleColorCostAfterDiscount value
     *
     * @return string
     */
    public function getStyleColorCostAfterDiscount(): string;

    /**
     * Set StyleColorCostAfterDiscount value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorCostAfterDiscount($value): StyleInterface;

    /**
     * Get StyleColorStandardCost value
     *
     * @return string
     */
    public function getStyleColorStandardCost(): string;

    /**
     * Set StyleColorStandardCost value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorStandardCost($value): StyleInterface;

    /**
     * Get Collection value
     *
     * @return string
     */
    public function getCollectionName(): string;

    /**
     * Set Collection value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setCollectionName($value): StyleInterface;

    /**
     * Get Error value
     *
     * @return string
     */
    public function getError(): string;

    /**
     * Set Error value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setError($value): StyleInterface;
}
