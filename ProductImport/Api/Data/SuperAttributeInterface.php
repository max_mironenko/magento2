<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

/**
 * Interface SuperAttributeInterface
 */
interface SuperAttributeInterface
{
    // table name
    const TABLE_NAME = 'catalog_product_super_attribute';

    // table columns
    const PRODUCT_SUPER_ATTRIBUTE_ID  = 'product_super_attribute_id';
    const PRODUCT_ID = 'product_id';
    const ATTRIBUTE_ID = 'attribute_id';
    const POSITION = 'position';

    /**
     * Get ProductSuperAttributeId value
     *
     * @return string | null
     */
    public function getProductSuperAttributeId(): ?string;

    /**
     * Set ProductSuperAttributeId value
     *
     * @param $value
     * @return SuperAttributeInterface
     */
    public function setProductSuperAttributeId($value): SuperAttributeInterface;

    /**
     * Get ProductId value
     *
     * @return string
     */
    public function getProductId(): string;

    /**
     * Set ProductId value
     *
     * @param $value
     * @return SuperAttributeInterface
     */
    public function setProductId($value): SuperAttributeInterface;

    /**
     * Get AttributeId value
     *
     * @return string
     */
    public function getAttributeId(): string;

    /**
     * Set AttributeId value
     *
     * @param $value
     * @return SuperAttributeInterface
     */
    public function setAttributeId($value): SuperAttributeInterface;

    /**
     * Get Position value
     *
     * @return string
     */
    public function getPosition(): string;

    /**
     * Set Position value
     *
     * @param $value
     * @return SuperAttributeInterface
     */
    public function setPosition($value): SuperAttributeInterface;
}
