<?php declare(strict_types=1);

namespace C38\ProductImport\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface SwatchAttributeSearchResultsInterface
 */
interface SwatchAttributeSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Swatch Attributes list.
     *
     * @return SwatchAttributeInterface[]
     */
    public function getItems(): array;

    /**
     * Set Swatch Attributes list.
     *
     * @param SwatchAttributeInterface[] $items
     * @return SwatchAttributeSearchResultsInterface
     */
    public function setItems(array $items): SwatchAttributeSearchResultsInterface;
}
