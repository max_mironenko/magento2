<?php declare(strict_types=1);

namespace C38\ProductImport\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface URLRewriteRepositoryInterface
 */
interface URLRewriteRepositoryInterface
{
    /**
     * Retrieve URLRewrites matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;
}
