<?php declare(strict_types=1);

namespace C38\ProductImport\Api;

use C38\ProductImport\Api\Data\PurchaseOrderLineInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception;

/**
 * Interface PurchaseOrderLineRepositoryInterface
 */
interface PurchaseOrderLineRepositoryInterface
{
    /**
     * Save PurchaseOrder Line
     *
     * @param PurchaseOrderLineInterface $purchaseOrderLine
     *
     * @return PurchaseOrderLineInterface
     * @throws Exception\CouldNotSaveException
     */
    public function save(PurchaseOrderLineInterface $purchaseOrderLine): PurchaseOrderLineInterface;

    /**
     * Get PurchaseOrder Line by id.
     *
     * @param $purchaseOrderLineId
     *
     * @return PurchaseOrderLineInterface
     * @throws Exception\NoSuchEntityException
     */
    public function getById($purchaseOrderLineId): PurchaseOrderLineInterface;

    /**
     * Get all lines by Purchase order ID.
     *
     * @param $purchaseOrderId
     *
     * @return PurchaseOrderLineInterface[]
     * @throws Exception\NoSuchEntityException
     */
    public function getByPurchaseOrderId($purchaseOrderId): array;

    /**
     * Find PurchaseOrder Line by id.
     *
     * @param $purchaseOrderLineId
     * @return PurchaseOrderLineInterface|null
     */
    public function findById($purchaseOrderLineId): ?PurchaseOrderLineInterface;

    /**
     * Retrieve PurchaseOrder Lines matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Delete PurchaseOrder Line
     *
     * @param PurchaseOrderLineInterface $purchaseOrderLine
     *
     * @return bool
     * @throws Exception\CouldNotDeleteException
     */
    public function delete(PurchaseOrderLineInterface $purchaseOrderLine): bool;

    /**
     * Delete PurchaseOrder Line by ID.
     *
     * @param $purchaseOrderLineId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteById($purchaseOrderLineId): bool;

    /**
     * Delete all lines by PurchaseOrder ID.
     *
     * @param $purchaseOrderId
     *
     * @return bool
     * @throws Exception\NoSuchEntityException
     * @throws Exception\CouldNotDeleteException
     */
    public function deleteByPurchaseOrderId($purchaseOrderId): bool;
}
