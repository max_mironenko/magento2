<?php declare(strict_types=1);

namespace C38\ProductImport\Ui\Component\Listing\PurchaseOrder\Column;

use C38\ProductImport\Api\Data\PurchaseOrderInterface;
use C38\ProductImport\Api\Data\PurchaseOrderLineInterface;
use C38\ProductImport\Model\ResourceModel\PurchaseOrderLine\CollectionFactory as OrderLineCollectionFactory;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Items
 * Add new Item for the Grid Column
 */
class Items extends Column
{
    /**
     * @var OrderLineCollectionFactory
     */
    private $orderLineCollectionFactory;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * Items constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param OrderLineCollectionFactory $orderLineCollectionFactory
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderLineCollectionFactory $orderLineCollectionFactory,
        PriceCurrencyInterface $priceCurrency,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->orderLineCollectionFactory = $orderLineCollectionFactory;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                if (!empty($item[PurchaseOrderInterface::ID])) {
                    $item[$fieldName] = $this->renderItems($item);
                }
            }
        }

        return $dataSource;
    }

    /**
     * @param $item
     * @return string
     */
    private function renderItems($item): string
    {
        $orderLineCollection = $this->orderLineCollectionFactory->create()
            ->addFieldToFilter(PurchaseOrderLineInterface::ORDER_ID, $item[PurchaseOrderInterface::ID]);
        $class = '';
        $html = '';
        /** @var PurchaseOrderLineInterface $orderItem */
        foreach ($orderLineCollection as $orderItem) {
            $html .= sprintf(
                "<p class=%s>%s / %s / %s , Qty: %d , Cost: %s </p>",
                $class,
                $orderItem->getSku(),
                $orderItem->getSize(),
                $orderItem->getVendorColor(),
                (int)$orderItem->getQty(),
                $this->formatPrice($orderItem->getCost())
            );
        }
        return $html;
    }

    /**
     * @param $price
     * @return string
     */
    private function formatPrice($price): string
    {
        return "" . $this->priceCurrency->format(
            $price,
            false
        );
    }
}
