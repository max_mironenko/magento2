<?php declare(strict_types=1);

namespace C38\ProductImport\Cron;

use C38\ProductImport\Api\Data\StyleInterface;
use C38\ProductImport\Api\StyleRepositoryInterface;
use C38\ProductImport\Helper\ProductHelper;
use C38\ProductImport\Helper\StyleLogger as Logger;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Throwable;

/**
 * Class ImportStyles
 * Import styles (create products) from local ERP storage to the Magento
 */
class ImportStyles
{
    const LOGGER_NAME = 'ImportStyles';

    /**
     * @var array
     */
    private $allProducts;

    /**
     * @var array
     */
    private $allParentsSku;

    /**
     * @var StyleRepositoryInterface
     */
    private $styleRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ProductResource
     */
    protected $productResource;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var ProductHelper
     */
    private $productHelper;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * ImportStyles constructor.
     *
     * @param StyleRepositoryInterface $styleRepository
     * @param ProductRepositoryInterface $productRepository
     * @param ProductResource $productResource
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param ProductHelper $productHelper
     * @param Logger $logger
     */
    public function __construct(
        StyleRepositoryInterface $styleRepository,
        ProductRepositoryInterface $productRepository,
        ProductResource $productResource,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        ProductHelper $productHelper,
        Logger $logger
    ) {
        $this->styleRepository = $styleRepository;
        $this->productRepository = $productRepository;
        $this->productResource = $productResource;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->productHelper = $productHelper;
        $this->logger = $logger;
        $this->allParentsSku = [];
        $this->allProducts = [];
    }

    /**
     * ImportStyles
     */
    public function execute()
    {
        //start
        $this->logger->info("Import Styles started...", self::LOGGER_NAME);

        // get list of all unprocessed styles
        try {
            // load all styles from database and create attributes
            $styles = $this->loadStyles();
            if (empty($styles)) {
                $this->logger->info("Unprocessed styles not found.", self::LOGGER_NAME);
                $this->logger->info("Import Styles finished", self::LOGGER_NAME);
                return;
            }

            // load all attributes
            $this->productHelper->loadAttributes();
            // get list of configurable products from the style list
            $configurableProducts = $this->getStyles($styles);
            // simple products
            $simpleProducts = $this->getStyles($styles, false);

            $this->logger->info("Found " . count($styles) . " unprocessed styles. " .
                count($configurableProducts) . " configurable products. " .
                count($simpleProducts) . " simple products. ", self::LOGGER_NAME);
        } catch (Throwable $ex) {
            $this->logger->error($ex->getMessage(), $ex->getTraceAsString(), self::LOGGER_NAME);
            $this->logger->info("Import Styles finished", self::LOGGER_NAME);
            return;
        }

        // create/updates products in the Magento
        try {
            // reload attributes
            $this->productHelper->loadAttributes(true);

            // process configurable styles
            $configurableProcessed = $this->createProducts($configurableProducts, true);

            // process simple styles
            $simpleProcessed = $this->createProducts($simpleProducts, false);

            $this->logger->info("Successfully processed " . ($configurableProcessed + $simpleProcessed) .
                " of " . count($styles) . " products. " .
                $configurableProcessed . " configurable products. " .
                $simpleProcessed . " simple products. ", self::LOGGER_NAME);
        } catch (Throwable $ex) {
            $this->logger->error($ex->getMessage(), $ex->getTraceAsString(), self::LOGGER_NAME);
        }

        // finish
        $this->logger->info("Import Styles finished", self::LOGGER_NAME);
    }

    /**
     * Get List of all unprocessed styles
     *
     * @return array
     * @throws Throwable
     */
    private function loadStyles(): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder->setField(StyleInterface::ERROR)
                ->setConditionType('null')->setValue(true)->create(),
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();

        // load all styles
        $styles = [];
        $allProducts = [];
        foreach ($this->styleRepository->getList($searchCriteria)->getItems() as $style) {
            try {
                /* @var $style StyleInterface */
                // if sku not found
                if (empty($style->getInventorySKU())) {
                    $this->logger->error(
                        "SKU not found for line with id {$style->getId()}",
                        '',
                        self::LOGGER_NAME
                    );
                    continue;
                }
                // if style already processed
                if ($style->getIsProcessed() == 1) {
                    $this->styleRepository->markAsProcessed($style->getId(), -1);
                    continue;
                }

                // add style to the list
                $styles[] = $style;

                // add style sku to the list
                $allProducts[] = $style->getInventorySKU();
            } catch (Throwable $ex) {
                $this->logger->error(
                    $ex->getMessage(),
                    $ex->getTraceAsString(),
                    self::LOGGER_NAME,
                    $style->getInventorySKU()
                );
            }
        }

        // load all products
        $this->allProducts = $this->loadProductsBySKU($allProducts);

        // return list of all styles
        return $styles;
    }

    /**
     * Get Styles with selected type
     *
     * @param array $allStyles
     * @param bool $isConfigurable
     *
     * @return array
     * @throws Throwable
     */
    private function getStyles(array $allStyles, bool $isConfigurable = true): array
    {
        // find styles by type in the style list
        $styles = [];
        $parentsSku = [];
        foreach ($allStyles as $style) {
            try {
                /* @var $style StyleInterface */
                // if style = required type, add to result list
                if ($style->getIsConfigurable() == $isConfigurable) {
                    // add style to the product list
                    $styles[] = $style;
                }

                // create product attributes in the Magento
                $this->productHelper->createAttributes($style);

                // get parent product for simple product
                if (!$isConfigurable) {
                    if (!empty($style->getStyleBaseSkuWithColor())) {
                        if (!in_array($style->getStyleBaseSkuWithColor(), $parentsSku)) {
                            $parentsSku[] = $style->getStyleBaseSkuWithColor();
                        }
                    }
                }
            } catch (Throwable $ex) {
                $this->logger->error(
                    $ex->getMessage(),
                    $ex->getTraceAsString(),
                    self::LOGGER_NAME,
                    $style->getInventorySKU()
                );
            }
        }

        // load list of simple parents sku
        if (!$isConfigurable) {
            $this->allParentsSku = $this->loadProductsBySKU($parentsSku);
        }

        // return styles
        return $styles;
    }

    /**
     * Create / Update Configurable Products
     *
     * @param array $products
     * @param bool $isConfigurable
     *
     * @return int
     * @throws Throwable
     */
    private function createProducts(array $products, bool $isConfigurable): int
    {
        // check data
        if (empty($products)) {
            return 0;
        }
        $productType = "Configurable";
        if (!$isConfigurable) {
            $productType = "Simple";
        }

        // create / update products
        $processed = 0;
        foreach ($products as $product) {
            /* @var $product StyleInterface */
            try {
                // prepare product data
                $productData = $this->productHelper->prepareProduct($product);
                $updatedId = -1;

                // create / update product
                if (empty($this->allProducts[$product->getInventorySKU()])) {
                    // check for simple product before creation
                    if (!$isConfigurable) {
                        $this->checkSimpleBeforeCreation(
                            $product->getStyleBaseSkuWithColor(),
                            $product->getInventorySKU()
                        );
                    }

                    // create new product
                    $newProduct = $this->productHelper->createProduct($productData, $isConfigurable);
                    $this->logger->info(
                        "New {$productType} product was successfully created. SKU {$product->getInventorySKU()}",
                        self::LOGGER_NAME,
                        $product->getInventorySKU()
                    );

                    if ($isConfigurable) {
                        // add new product sku to list of all parent products
                        $this->allParentsSku[$product->getInventorySKU()] = $newProduct;
                        // assign product to the category
                        $this->productHelper->assignProductToCategory(
                            $product->getInventorySKU(),
                            $product->getStyleBaseStyleSkuDesigner(),
                            $product->getStyleDesignerDescription()
                        );
                    } else {
                        // assign simple product to configurable
                        $this->productHelper->assignSimpleToConfigurable(
                            "" . $newProduct->getId(),
                            $this->allParentsSku[$product->getStyleBaseSkuWithColor()]
                        );
                    }
                } else {
                    // update product
                    $updatedId = $this->productHelper->updateProduct(
                        $productData,
                        $this->allProducts[$product->getInventorySKU()],
                        $isConfigurable
                    );
                    $this->logger->info(
                        "{$productType} product was successfully updated. SKU {$product->getInventorySKU()}",
                        self::LOGGER_NAME,
                        $product->getInventorySKU()
                    );
                }

                // mark product as processed and move to archive
                $this->styleRepository->markAsProcessed($product->getId(), $updatedId);
                $processed++;
            } catch (Throwable $ex) {
                $this->logger->error(
                    $ex->getMessage(),
                    $ex->getTraceAsString(),
                    self::LOGGER_NAME,
                    $product->getInventorySKU()
                );
                try {
                    $product->setError($ex->getMessage());
                    $this->styleRepository->save($product);
                } catch (Throwable $ex) {
                    $this->logger->error(
                        $ex->getMessage(),
                        $ex->getTraceAsString(),
                        self::LOGGER_NAME,
                        $product->getInventorySKU()
                    );
                }
            }
        }

        // return count of created/updated products
        return $processed;
    }

    /**
     * Check if parent product exist in the Magento
     *
     * @param string|null $parentSku
     * @param string sku
     *
     * @throws Throwable
     */
    private function checkSimpleBeforeCreation(?string $parentSku, string $sku)
    {
        // check if parent product in the magento database
        if (empty($parentSku) || empty($this->allParentsSku[$parentSku])) {
            throw new LocalizedException(new Phrase(
                "Error creating Simple product $sku. Incorrect parent SKU ({$parentSku})."
            ));
        }
    }

    /**
     * Load list of products for update
     *
     * @param array $skus
     * @param bool $isLoadAllData
     *
     * @return array
     * @throws Throwable
     */
    private function loadProductsBySKU(array $skus, bool $isLoadAllData = true): array
    {
        // check values
        if (empty($skus)) {
            return [];
        }

        // prepare filters
        $filters = [
            $this->filterBuilder->setField('sku')
                ->setConditionType('IN')->setValue($skus)->create(),
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();

        // load list of all products
        $products = [];
        foreach ($this->productRepository->getList($searchCriteria)->getItems() as $product) {
            if ($isLoadAllData) {
                $products[$product->getSku()] = $product;
            } else {
                $products[] = $product->getSku();
            }
        }

        return $products;
    }
}
