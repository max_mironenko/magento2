<?php declare(strict_types=1);

namespace C38\ProductImport\Controller\Adminhtml\PurchaseOrder;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\Page;

/**
 * Class Index
 * Controller for Purchase order page in the Magento Admin Panel
 */
class Index extends Action
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'C38_ProductImport::purchase_orders';

    /**
     * @return Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('C38_ProductImport::purchase_orders');
        $resultPage->addBreadcrumb(__('Purchase Orders'), __('Purchase Orders'));
        $resultPage->getConfig()->getTitle()->prepend(__('Purchase Orders'));

        return $resultPage;
    }
}
