<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\URLRewriteInterface;
use C38\ProductImport\Model\ResourceModel\URLRewrite as ResourceURLRewrite;
use C38\ProductImport\Model\ResourceModel\URLRewrite\Collection as ResourceURLRewriteCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Style
 * Model for Style object (ERP product)
 *
 * @method ResourceURLRewrite getResource()
 * @method ResourceURLRewriteCollection getCollection()
 * @method ResourceURLRewriteCollection getResourceCollection()
 */
class URLRewrite extends AbstractModel implements URLRewriteInterface
{
    protected $_eventPrefix = 'c38_product_import_model_url_rewrite';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceURLRewrite::class);
    }

    /**
     * Get Id
     *
     * @return string | null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setId($value): URLRewriteInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get EntityType value
     *
     * @return string
     */
    public function getEntityType(): string
    {
        return $this->_getData(self::ENTITY_TYPE);
    }

    /**
     * Set EntityType value
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setEntityType($value): URLRewriteInterface
    {
        $this->setData(self::ENTITY_TYPE, $value);
        return $this;
    }

    /**
     * Get EntityId value
     *
     * @return string
     */
    public function getEntityId(): string
    {
        return $this->_getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId value
     *
     * @param $entityId
     * @return URLRewriteInterface
     */
    public function setEntityId($entityId): URLRewriteInterface
    {
        $this->setData(self::ENTITY_ID, $entityId);
        return $this;
    }

    /**
     * Get RequestPath value
     *
     * @return string
     */
    public function getRequestPath(): string
    {
        return $this->_getData(self::REQUEST_PATH);
    }

    /**
     * Set RequestPath value
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setRequestPath($value): URLRewriteInterface
    {
        $this->setData(self::REQUEST_PATH, $value);
        return $this;
    }

    /**
     * Get TargetPath value
     *
     * @return string
     */
    public function getTargetPath(): string
    {
        return $this->_getData(self::TARGET_PATH);
    }

    /**
     * Set TargetPath value
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setTargetPath($value): URLRewriteInterface
    {
        $this->setData(self::TARGET_PATH, $value);
        return $this;
    }

    /**
     * Get RedirectType value
     *
     * @return string
     */
    public function getRedirectType(): string
    {
        return $this->_getData(self::REDIRECT_TYPE);
    }

    /**
     * Set RedirectType value
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setRedirectType($value): URLRewriteInterface
    {
        $this->setData(self::REDIRECT_TYPE, $value);
        return $this;
    }

    /**
     * Get StoreId value
     *
     * @return string
     */
    public function getStoreId(): string
    {
        return $this->_getData(self::STORE_ID);
    }

    /**
     * Set StoreId value
     *
     * @param $value
     * @return URLRewriteInterface
     */
    public function setStoreId($value): URLRewriteInterface
    {
        $this->setData(self::STORE_ID, $value);
        return $this;
    }
}
