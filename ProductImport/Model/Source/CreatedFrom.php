<?php declare(strict_types=1);

namespace C38\ProductImport\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

/**
 * Class CreatedFrom
 * CreatedFrom Magento Model for Magento Admin Panel
 */
class CreatedFrom extends AbstractSource
{
    /**
     * Option values
     */
    const VALUE_NO = 0;
    const VALUE_YES = 1;
    const VALUE_YES_NOT_INDEXING = 2;
    const VALUE_ERP_NOT_INDEXING = 3;
    const VALUE_ERP = 4;
    const LABEL_NO = 'Manual';
    const LABEL_YES = 'Product Tools';
    const LABEL_YES_NOT_INDEXING = 'Product Tools (not indexed)';
    const LABEL_ERP_NOT_INDEXING = 'ERP (not indexed)';
    const LABEL_ERP = 'ERP';

    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions(): array
    {
        if (!$this->_options) {
            $this->_options = [];

            foreach ($this->getArray() as $value => $label) {
                $this->_options[] = [
                    'value' => $value,
                    'label' => __($label),
                ];
            }
        }

        return $this->_options;
    }

    /**
     * Retrieves the all options as $key -> $value array.
     *
     * @return array
     */
    private function getArray(): array
    {
        return [
            self::VALUE_NO => self::LABEL_NO,
            self::VALUE_YES => self::LABEL_YES,
            self::VALUE_YES_NOT_INDEXING => self::LABEL_YES_NOT_INDEXING,
            self::VALUE_ERP_NOT_INDEXING => self::LABEL_ERP_NOT_INDEXING,
            self::VALUE_ERP => self::LABEL_ERP,
        ];
    }
}
