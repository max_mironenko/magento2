<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\PurchaseOrderLoggerInterface;
use C38\ProductImport\Api\PurchaseOrderLoggerRepositoryInterface;
use C38\ProductImport\Model\ResourceModel\PurchaseOrderLogger;
use Magento\Framework\Exception\CouldNotSaveException;
use Throwable;

/**
 * Class PurchaseOrderLoggerRepository
 * Repository for Purchase Order Logger objects
 */
class PurchaseOrderLoggerRepository implements PurchaseOrderLoggerRepositoryInterface
{
    /**
     * @var PurchaseOrderLogger
     */
    private $resource;

    /**
     * PurchaseOrderLoggerRepository constructor.
     *
     * @param PurchaseOrderLogger $resource
     */
    public function __construct(PurchaseOrderLogger $resource)
    {
        $this->resource = $resource;
    }

    /**
     * Save Log Message
     *
     * @param PurchaseOrderLoggerInterface $logger
     *
     * @return PurchaseOrderLoggerInterface
     * @throws CouldNotSaveException
     * @noinspection PhpParamsInspection
     */
    public function save(PurchaseOrderLoggerInterface $logger): PurchaseOrderLoggerInterface
    {
        try {
            $this->resource->save($logger);
        } catch (Throwable $ex) {
            throw new CouldNotSaveException(__($ex->getMessage()));
        }

        return $logger;
    }
}
