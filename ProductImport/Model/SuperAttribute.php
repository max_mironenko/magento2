<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\SuperAttributeInterface;
use C38\ProductImport\Model\ResourceModel\SuperAttribute as ResourceSuperAttribute;
use Magento\Framework\Model\AbstractModel;

/**
 * Class SuperAttribute
 * Model of SuperAttribute
 *
 * @method ResourceSuperAttribute getResource()
 */
class SuperAttribute extends AbstractModel implements SuperAttributeInterface
{
    protected $_eventPrefix = 'c38_product_import_model_super_attribute';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceSuperAttribute::class);
    }

    /**
     * Get Id
     *
     * @return string | null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::PRODUCT_SUPER_ATTRIBUTE_ID);
    }

    /**
     * Set Id
     *
     * @param $value
     * @return SuperAttributeInterface
     */
    public function setId($value): SuperAttributeInterface
    {
        $this->setData(self::PRODUCT_SUPER_ATTRIBUTE_ID, $value);
        return $this;
    }

    /**
     * Get ProductSuperAttributeId value
     *
     * @return string | null
     */
    public function getProductSuperAttributeId(): ?string
    {
        return $this->_getData(self::PRODUCT_SUPER_ATTRIBUTE_ID);
    }

    /**
     * Set ProductSuperAttributeId value
     *
     * @param $value
     * @return SuperAttributeInterface
     */
    public function setProductSuperAttributeId($value): SuperAttributeInterface
    {
        $this->setData(self::PRODUCT_SUPER_ATTRIBUTE_ID, $value);
        return $this;
    }

    /**
     * Get ProductId value
     *
     * @return string
     */
    public function getProductId(): string
    {
        return $this->_getData(self::PRODUCT_ID);
    }

    /**
     * Set ProductId value
     *
     * @param $value
     * @return SuperAttributeInterface
     */
    public function setProductId($value): SuperAttributeInterface
    {
        $this->setData(self::PRODUCT_ID, $value);
        return $this;
    }

    /**
     * Get AttributeId value
     *
     * @return string
     */
    public function getAttributeId(): string
    {
        return $this->_getData(self::ATTRIBUTE_ID);
    }

    /**
     * Set AttributeId value
     *
     * @param $value
     * @return SuperAttributeInterface
     */
    public function setAttributeId($value): SuperAttributeInterface
    {
        $this->setData(self::ATTRIBUTE_ID, $value);
        return $this;
    }

    /**
     * Get Position value
     *
     * @return string
     */
    public function getPosition(): string
    {
        return $this->_getData(self::POSITION);
    }

    /**
     * Set Position value
     *
     * @param $value
     * @return SuperAttributeInterface
     */
    public function setPosition($value): SuperAttributeInterface
    {
        $this->setData(self::POSITION, $value);
        return $this;
    }
}
