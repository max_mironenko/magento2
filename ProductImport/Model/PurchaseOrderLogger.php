<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\PurchaseOrderLoggerInterface;
use C38\ProductImport\Model\ResourceModel\PurchaseOrderLogger as ResourcePurchaseOrderLogger;
use Magento\Framework\Model\AbstractModel;

/**
 * Class PurchaseOrderLogger
 * Model of Purchase Order Logger
 *
 * @method ResourcePurchaseOrderLogger getResource()
 */
class PurchaseOrderLogger extends AbstractModel implements PurchaseOrderLoggerInterface
{
    protected $_eventPrefix = 'c38_product_import_model_purchase_order_logger';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourcePurchaseOrderLogger::class);
    }

    /**
     * Get Id
     *
     * @return string | null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id
     *
     * @param $value
     * @return PurchaseOrderLogger
     */
    public function setId($value): PurchaseOrderLoggerInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get Message Type
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->_getData(self::TYPE);
    }

    /**
     * Set Message Type
     *
     * @param $value
     * @return PurchaseOrderLoggerInterface
     */
    public function setType($value): PurchaseOrderLoggerInterface
    {
        $this->setData(self::TYPE, $value);
        return $this;
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->_getData(self::MESSAGE);
    }

    /**
     * Set Message
     *
     * @param $value
     * @return PurchaseOrderLoggerInterface
     */
    public function setMessage($value): PurchaseOrderLoggerInterface
    {
        $this->setData(self::MESSAGE, $value);
        return $this;
    }

    /**
     * Get Error stack trace
     *
     * @return string
     */
    public function getStack(): string
    {
        return $this->_getData(self::STACK);
    }

    /**
     * Set Error stack trace
     *
     * @param $value
     * @return PurchaseOrderLoggerInterface
     */
    public function setStack($value): PurchaseOrderLoggerInterface
    {
        $this->setData(self::STACK, $value);
        return $this;
    }

    /**
     * Get Cron Name
     *
     * @return string
     */
    public function getCronName(): string
    {
        return $this->_getData(self::CRON_NAME);
    }

    /**
     * Set Cron Name
     *
     * @param $value
     * @return PurchaseOrderLoggerInterface
     */
    public function setCronName($value): PurchaseOrderLoggerInterface
    {
        $this->setData(self::CRON_NAME, $value);
        return $this;
    }

    /**
     * Get Created At
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set Created At
     *
     * @param $value
     * @return PurchaseOrderLoggerInterface
     */
    public function setCreatedAt($value): PurchaseOrderLoggerInterface
    {
        $this->setData(self::CREATED_AT, $value);
        return $this;
    }
}
