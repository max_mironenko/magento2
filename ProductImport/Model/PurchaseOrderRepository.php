<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\PurchaseOrderInterface;
use C38\ProductImport\Api\Data\PurchaseOrderInterfaceFactory;
use C38\ProductImport\Api\Data\PurchaseOrderLineInterfaceFactory;
use C38\ProductImport\Api\Data\PurchaseOrderSearchResultsInterface;
use C38\ProductImport\Api\Data\PurchaseOrderSearchResultsInterfaceFactory;
use C38\ProductImport\Api\PurchaseOrderLineRepositoryInterface;
use C38\ProductImport\Api\PurchaseOrderRepositoryInterface;
use C38\ProductImport\Helper\PurchaseOrderLogger as Logger;
use C38\ProductImport\Model\ResourceModel\PurchaseOrder;
use C38\ProductImport\Model\ResourceModel\PurchaseOrder\CollectionFactory;
use Exception;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class PurchaseOrderRepository
 * Repository for Purchase Order objects
 */
class PurchaseOrderRepository implements PurchaseOrderRepositoryInterface
{
    const LOGGER_NAME = 'UpdatePurchaseOrder';

    /**
     * @var PurchaseOrder
     */
    private $resource;

    /**
     * @var PurchaseOrderInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var PurchaseOrderSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var PurchaseOrderLineInterfaceFactory
     */
    private $purchaseOrderLineFactory;

    /**
     * @var PurchaseOrderLineRepositoryInterface
     */
    private $purchaseOrderLineRepository;

    /**
     * @var ProductCollection
     */
    private $productCollectionFactory;

    /**
     * @var ProductResource
     */
    private $productResource;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * PurchaseOrderRepository constructor.
     *
     * @param PurchaseOrder $resource
     * @param PurchaseOrderInterfaceFactory $purchaseOrderFactory
     * @param CollectionFactory $collectionFactory
     * @param PurchaseOrderSearchResultsInterfaceFactory $searchResultFactory
     * @param PurchaseOrderLineInterfaceFactory $purchaseOrderLineFactory
     * @param PurchaseOrderLineRepositoryInterface $purchaseOrderLineRepository
     * @param ProductCollection $productCollectionFactory
     * @param ProductResource $productResource
     * @param Json $json
     * @param Logger $logger
     */
    public function __construct(
        PurchaseOrder $resource,
        PurchaseOrderInterfaceFactory $purchaseOrderFactory,
        CollectionFactory $collectionFactory,
        PurchaseOrderSearchResultsInterfaceFactory $searchResultFactory,
        PurchaseOrderLineInterfaceFactory $purchaseOrderLineFactory,
        PurchaseOrderLineRepositoryInterface $purchaseOrderLineRepository,
        ProductCollection $productCollectionFactory,
        ProductResource $productResource,
        Json $json,
        Logger $logger
    ) {
        $this->resource = $resource;
        $this->entityFactory = $purchaseOrderFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->purchaseOrderLineFactory = $purchaseOrderLineFactory;
        $this->purchaseOrderLineRepository = $purchaseOrderLineRepository;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productResource = $productResource;
        $this->json = $json;
        $this->logger = $logger;
    }

    /**
     * Save PurchaseOrder
     *
     * @param PurchaseOrderInterface $purchaseOrder
     *
     * @return PurchaseOrderInterface
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function save(PurchaseOrderInterface $purchaseOrder): PurchaseOrderInterface
    {
        try {
            $this->resource->save($purchaseOrder);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $purchaseOrder;
    }

    /**
     * Get PurchaseOrder by id.
     *
     * @param $purchaseOrderId
     *
     * @return PurchaseOrderInterface
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function getById($purchaseOrderId): PurchaseOrderInterface
    {
        $purchaseOrder = $this->entityFactory->create();
        $this->resource->load($purchaseOrder, $purchaseOrderId);
        if (!$purchaseOrder->getId()) {
            throw new NoSuchEntityException(__('PurchaseOrder with id "%1" does not exist.', $purchaseOrderId));
        }

        return $purchaseOrder;
    }

    /**
     * Find PurchaseOrder by id.
     *
     * @param $purchaseOrderId
     * @return PurchaseOrderInterface|null
     * @noinspection PhpParamsInspection
     */
    public function findById($purchaseOrderId): ?PurchaseOrderInterface
    {
        $purchaseOrder = $this->entityFactory->create();
        $this->resource->load($purchaseOrder, $purchaseOrderId);
        if (!$purchaseOrder->getId()) {
            return null;
        }

        return $purchaseOrder;
    }

    /**
     * Get PurchaseOrder by PO number.
     *
     * @param $purchaseOrderNumber
     *
     * @return PurchaseOrderInterface
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function getByPurchaseOrderNumber($purchaseOrderNumber): PurchaseOrderInterface
    {
        $purchaseOrder = $this->entityFactory->create();
        $this->resource->load($purchaseOrder, $purchaseOrderNumber, PurchaseOrderInterface::PO_NUMBER);
        return $purchaseOrder;
    }

    /**
     * Retrieve PurchaseOrder matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|PurchaseOrderSearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete PurchaseOrder
     *
     * @param PurchaseOrderInterface $purchaseOrder
     *
     * @return bool
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function delete(PurchaseOrderInterface $purchaseOrder): bool
    {
        try {
            $this->resource->delete($purchaseOrder);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete PurchaseOrder by ID.
     *
     * @param $purchaseOrderId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById($purchaseOrderId): bool
    {
        return $this->delete($this->getById($purchaseOrderId));
    }

    /**
     * Update Purchase Order
     *
     * @param mixed $purchaseOrderData
     * @return array
     * @throws Exception
     */
    public function updatePurchaseOrder($purchaseOrderData): array
    {
        // start
        $lineUpdated = 0;
        $lineTotal = 0;
        $this->logger->info('Update Purchase Order started...', self::LOGGER_NAME);

        try {
            // check purchase order data
            if (empty($purchaseOrderData['po_number'])) {
                throw new CouldNotSaveException(__("PO Number not found."));
            }
            if (empty($purchaseOrderData['lines_number'])) {
                throw new CouldNotSaveException(__("PO lines number is unknown."));
            }
            if (empty($purchaseOrderData['vendor_name'])) {
                throw new CouldNotSaveException(__("Vendor Name value not found for order."));
            }

            // load purchase order (or create new if not found)
            $purchaseOrder = $this->getByPurchaseOrderNumber($purchaseOrderData['po_number']);

            // set values to the purchase order
            $purchaseOrder
                ->setLinesNumber($purchaseOrderData['lines_number'])
                ->setPoNumber($purchaseOrderData['po_number'])
                ->setVendorName($purchaseOrderData['vendor_name']);

            if (!empty($purchaseOrderData['po_date'])) {
                $purchaseOrder->setPoDate($purchaseOrderData['po_date']);
            }
            if (!empty($purchaseOrderData['po_cancel_date'])) {
                $purchaseOrder->setPoCancelDate($purchaseOrderData['po_cancel_date']);
            }
            if (!empty($purchaseOrderData['warehouse_name'])) {
                $purchaseOrder->setWarehouseName($purchaseOrderData['warehouse_name']);
            }
            if (!empty($purchaseOrderData['user_name'])) {
                $purchaseOrder->setUserName($purchaseOrderData['user_name']);
            }
            if (!empty($purchaseOrderData['user_email'])) {
                $purchaseOrder->setUserEmail($purchaseOrderData['user_email']);
            }

            // save (update / add new) purchase order
            $purchaseOrder = $this->save($purchaseOrder);

            // if information about items not found
            if (empty($purchaseOrderData['items'])) {
                $this->logger->info(
                    "Purchase Order# {$purchaseOrderData['po_number']} has been saved. No items updated.",
                    self::LOGGER_NAME
                );
                return ['success' => 'Purchase Order has been saved'];
            }

            // if items found then delete all old items
            $this->purchaseOrderLineRepository->deleteByPurchaseOrderId($purchaseOrderData['po_number']);

            // create new set of items
            $productList = [];
            $lineTotal = count($purchaseOrderData['items']);
            foreach ($purchaseOrderData['items'] as $line) {
                try {
                    if (empty($line['sku'])) {
                        $this->logger->error(
                            "SKU for Purchase Order Item not found.",
                            $this->json->serialize($line),
                            self::LOGGER_NAME
                        );
                        continue;
                    }
                    // create new lines
                    $purchaseOrderLine = $this->purchaseOrderLineFactory->create();

                    // set values to the new line
                    $purchaseOrderLine->setOrderId($purchaseOrder->getId());
                    $purchaseOrderLine->setSku($line['sku']);

                    if (!empty($line['size'])) {
                        $purchaseOrderLine->setSize($line['size']);
                    }
                    if (!empty($line['vendor_color'])) {
                        $purchaseOrderLine->setVendorColor($line['vendor_color']);
                    }
                    if (!empty($line['qty'])) {
                        $purchaseOrderLine->setQty(number_format((float)$line['qty'], 4, '.', ''));
                    }
                    if (!empty($line['cost'])) {
                        $purchaseOrderLine->setCost(number_format((float)$line['cost'], 4, '.', ''));
                    }
                    $purchaseOrderLine->setIdentifier(substr($line['sku'], 0, strrpos($line['sku'], "-")));

                    // save line
                    $this->purchaseOrderLineRepository->save($purchaseOrderLine);

                    // add SKU to the PO item list
                    $productList[] = $line['sku'];
                    $lineUpdated++;
                } catch (Exception $ex) {
                    $this->logger->error($ex->getMessage(), $ex->getTraceAsString(), self::LOGGER_NAME);
                }
            }

            // add purchase order number to magento products
            $this->addPurchaseOrderNumberToProducts($productList, $purchaseOrderData['po_number']);

            // return success result
            $response = ['success' => 'Purchase Order has been saved'];
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage(), $ex->getTraceAsString(), self::LOGGER_NAME);
            $response = ['error' => __('Purchase Order not saved. ' . $ex->getMessage())];
        }

        // finish
        $this->logger->info("Update Purchase Order# {$purchaseOrderData['po_number']} finished. " .
            "Successfully update {$lineUpdated} of {$lineTotal} items.", self::LOGGER_NAME);
        return $response;
    }

    /**
     * Add purchase order number to magento products
     *
     * @param array $productList
     * @param string $purchaseOrderNumber
     */
    private function addPurchaseOrderNumberToProducts(array $productList, string $purchaseOrderNumber)
    {
        try {
            // load list of products Magento products
            $productCollection = $this->productCollectionFactory
                ->create()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('sku', ['in' => $productList])
                ->load();

            // add po information to each product
            foreach ($productCollection as $product) {
                try {
                    // get list of purchase order numbers for product
                    $poNumbers = $product->getData('purchase_order_list');

                    // add po number
                    $poNumbersArray = [];
                    if (!empty($poNumbers)) {
                        $poNumbersArray = explode(' ', $poNumbers);
                    }
                    $poNumbersArray[] = $purchaseOrderNumber;

                    // save PO numbers as product attribute
                    $product->setData('purchase_order_list', implode(' ', array_unique($poNumbersArray)));
                    $product->getResource()->saveAttribute($product, 'purchase_order_list');
                } catch (Exception $ex) {
                    $this->logger->error($ex->getMessage(), $ex->getTraceAsString(), self::LOGGER_NAME);
                }
            }
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage(), $ex->getTraceAsString(), self::LOGGER_NAME);
        }
    }
}
