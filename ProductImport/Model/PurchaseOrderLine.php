<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\PurchaseOrderLineInterface;
use C38\ProductImport\Model\ResourceModel\PurchaseOrderLine as ResourcePurchaseOrderLine;
use C38\ProductImport\Model\ResourceModel\PurchaseOrderLine\Collection as ResourcePurchaseOrderLineCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class PurchaseOrderLine
 * Model of Purchase Order Line
 *
 * @method ResourcePurchaseOrderLine getResource()
 * @method ResourcePurchaseOrderLineCollection getCollection()
 * @method ResourcePurchaseOrderLineCollection getResourceCollection()
 */
class PurchaseOrderLine extends AbstractModel implements PurchaseOrderLineInterface
{
    protected $_eventPrefix = 'c38_product_import_model_purchase_order_line';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourcePurchaseOrderLine::class);
    }

    /**
     * Get Id
     *
     * @return string | null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setId($value): PurchaseOrderLineInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get Order Id
     *
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->_getData(self::ORDER_ID);
    }

    /**
     * Set Order Id
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setOrderId($value): PurchaseOrderLineInterface
    {
        $this->setData(self::ORDER_ID, $value);
        return $this;
    }

    /**
     * Get Product Sku
     *
     * @return string
     */
    public function getSku(): string
    {
        return $this->_getData(self::SKU);
    }

    /**
     * Set Product Sku
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setSku($value): PurchaseOrderLineInterface
    {
        $this->setData(self::SKU, $value);
        return $this;
    }

    /**
     * Get Product Size
     *
     * @return string
     */
    public function getSize(): string
    {
        return $this->_getData(self::SIZE);
    }

    /**
     * Set Product Size
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setSize($value): PurchaseOrderLineInterface
    {
        $this->setData(self::SIZE, $value);
        return $this;
    }

    /**
     * Get Vendor Color
     *
     * @return string
     */
    public function getVendorColor(): string
    {
        return $this->_getData(self::VENDOR_COLOR);
    }

    /**
     * Set Vendor Color
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setVendorColor($value): PurchaseOrderLineInterface
    {
        $this->setData(self::VENDOR_COLOR, $value);
        return $this;
    }

    /**
     * Get Qty
     *
     * @return float
     */
    public function getQty(): float
    {
        return $this->_getData(self::QTY);
    }

    /**
     * Set Qty
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setQty($value): PurchaseOrderLineInterface
    {
        $this->setData(self::QTY, $value);
        return $this;
    }

    /**
     * Get Cost
     *
     * @return float
     */
    public function getCost(): float
    {
        return $this->_getData(self::COST);
    }

    /**
     * Set Cost
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setCost($value): PurchaseOrderLineInterface
    {
        $this->setData(self::COST, $value);
        return $this;
    }

    /**
     * Get Identifier
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->_getData(self::IDENTIFIER);
    }

    /**
     * Set Identifier
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setIdentifier($value): PurchaseOrderLineInterface
    {
        $this->setData(self::IDENTIFIER, $value);
        return $this;
    }

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set Created Date
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setCreatedAt($value): PurchaseOrderLineInterface
    {
        $this->setData(self::CREATED_AT, $value);
        return $this;
    }

    /**
     * Get Manifest Id
     *
     * @return string
     */
    public function getManifestId(): string
    {
        return $this->_getData(self::MANIFEST_ID);
    }

    /**
     * Set Manifest Id
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setManifestId($value): PurchaseOrderLineInterface
    {
        $this->setData(self::MANIFEST_ID, $value);
        return $this;
    }

    /**
     * Get Updated Date
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->_getData(self::UPDATED_AT);
    }

    /**
     * Set Updated Date
     *
     * @param $value
     * @return PurchaseOrderLineInterface
     */
    public function setUpdatedAt($value): PurchaseOrderLineInterface
    {
        $this->setData(self::UPDATED_AT, $value);
        return $this;
    }
}
