<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\StyleLoggerInterface;
use C38\ProductImport\Api\StyleLoggerRepositoryInterface;
use C38\ProductImport\Model\ResourceModel\StyleLogger;
use Magento\Framework\Exception\CouldNotSaveException;
use Throwable;

/**
 * Class StyleLoggerRepository
 * Repository for StyleLogger objects
 */
class StyleLoggerRepository implements StyleLoggerRepositoryInterface
{
    /**
     * @var StyleLogger
     */
    private $resource;

    /**
     * StyleLoggerRepository constructor.
     *
     * @param StyleLogger $resource
     */
    public function __construct(StyleLogger $resource)
    {
        $this->resource = $resource;
    }

    /**
     * Save Log Message
     *
     * @param StyleLoggerInterface $logger
     *
     * @return StyleLoggerInterface
     * @throws CouldNotSaveException
     * @noinspection PhpParamsInspection
     */
    public function save(StyleLoggerInterface $logger): StyleLoggerInterface
    {
        try {
            $this->resource->save($logger);
        } catch (Throwable $ex) {
            throw new CouldNotSaveException(__($ex->getMessage()));
        }

        return $logger;
    }
}
