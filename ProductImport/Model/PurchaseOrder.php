<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\PurchaseOrderInterface;
use C38\ProductImport\Model\ResourceModel\PurchaseOrder as ResourcePurchaseOrder;
use C38\ProductImport\Model\ResourceModel\PurchaseOrder\Collection as ResourcePurchaseOrderCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class PurchaseOrder
 * Model of Purchase Order
 *
 * @method ResourcePurchaseOrder getResource()
 * @method ResourcePurchaseOrderCollection getCollection()
 * @method ResourcePurchaseOrderCollection getResourceCollection()
 */
class PurchaseOrder extends AbstractModel implements PurchaseOrderInterface
{
    protected $_eventPrefix = 'c38_product_import_model_purchase_order';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourcePurchaseOrder::class);
    }

    /**
     * Get PO Id
     *
     * @return string | null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set PO Id
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setId($value): PurchaseOrderInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get Order Id
     *
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->_getData(self::ORDER_ID);
    }

    /**
     * Set Order Id
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setOrderId($value): PurchaseOrderInterface
    {
        $this->setData(self::ORDER_ID, $value);
        return $this;
    }

    /**
     * Get PO Number
     *
     * @return string
     */
    public function getPoNumber(): string
    {
        return $this->_getData(self::PO_NUMBER);
    }

    /**
     * Set PO Number
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setPoNumber($value): PurchaseOrderInterface
    {
        $this->setData(self::PO_NUMBER, $value);
        return $this;
    }

    /**
     * Get PO Date
     *
     * @return string
     */
    public function getPoDate(): string
    {
        return $this->_getData(self::PO_DATE);
    }

    /**
     * Set PO Date
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setPoDate($value): PurchaseOrderInterface
    {
        $this->setData(self::PO_DATE, $value);
        return $this;
    }

    /**
     * Get Vendor Name
     *
     * @return string
     */
    public function getVendorName(): string
    {
        return $this->_getData(self::VENDOR_NAME);
    }

    /**
     * Set Vendor Name
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setVendorName($value): PurchaseOrderInterface
    {
        $this->setData(self::VENDOR_NAME, $value);
        return $this;
    }

    /**
     * Get Warehouse Name
     *
     * @return string
     */
    public function getWarehouseName(): string
    {
        return $this->_getData(self::WAREHOUSE_NAME);
    }

    /**
     * Set Warehouse Name
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setWarehouseName($value): PurchaseOrderInterface
    {
        $this->setData(self::WAREHOUSE_NAME, $value);
        return $this;
    }

    /**
     * Get User Name
     *
     * @return string
     */
    public function getUserName(): string
    {
        return $this->_getData(self::USER_NAME);
    }

    /**
     * Set User Name
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setUserName($value): PurchaseOrderInterface
    {
        $this->setData(self::USER_NAME, $value);
        return $this;
    }

    /**
     * Get User Email
     *
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->_getData(self::USER_EMAIL);
    }

    /**
     * Set User Email
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setUserEmail($value): PurchaseOrderInterface
    {
        $this->setData(self::USER_EMAIL, $value);
        return $this;
    }

    /**
     * Get Manifest Id
     *
     * @return string
     */
    public function getManifestId(): string
    {
        return $this->_getData(self::MANIFEST_ID);
    }

    /**
     * Set Manifest Id
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setManifestId($value): PurchaseOrderInterface
    {
        $this->setData(self::MANIFEST_ID, $value);
        return $this;
    }

    /**
     * Get Manifest Date
     *
     * @return string
     */
    public function getManifestDate(): string
    {
        return $this->_getData(self::MANIFEST_DATE);
    }

    /**
     * Set Manifest Date
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setManifestDate($value): PurchaseOrderInterface
    {
        $this->setData(self::MANIFEST_DATE, $value);
        return $this;
    }

    /**
     * Get Created Date
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set Created Date
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setCreatedAt($value): PurchaseOrderInterface
    {
        $this->setData(self::CREATED_AT, $value);
        return $this;
    }

    /**
     * Get Cancel Date
     *
     * @return string
     */
    public function getPoCancelDate(): string
    {
        return $this->_getData(self::PO_CANCEL_DATE);
    }

    /**
     * Set Cancel Date
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setPoCancelDate($value): PurchaseOrderInterface
    {
        $this->setData(self::PO_CANCEL_DATE, $value);
        return $this;
    }

    /**
     * Get Updated Date
     *
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->_getData(self::UPDATED_AT);
    }

    /**
     * Set Updated Date
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setUpdatedAt($value): PurchaseOrderInterface
    {
        $this->setData(self::UPDATED_AT, $value);
        return $this;
    }

    /**
     * Get Lines Number
     *
     * @return string
     */
    public function getLinesNumber(): string
    {
        return $this->_getData(self::LINES_NUMBER);
    }

    /**
     * Set Lines Number
     *
     * @param $value
     * @return PurchaseOrderInterface
     */
    public function setLinesNumber($value): PurchaseOrderInterface
    {
        $this->setData(self::LINES_NUMBER, $value);
        return $this;
    }
}
