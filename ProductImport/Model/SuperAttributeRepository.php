<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\SuperAttributeInterface;
use C38\ProductImport\Api\SuperAttributeRepositoryInterface;
use C38\ProductImport\Model\ResourceModel\SuperAttribute;
use Magento\Framework\Exception\CouldNotSaveException;
use Throwable;

/**
 * Class SuperAttributeRepository
 * Repository for Super Attributes
 */
class SuperAttributeRepository implements SuperAttributeRepositoryInterface
{
    /**
     * @var SuperAttribute
     */
    private $resource;

    /**
     * SuperAttributeRepository constructor.
     *
     * @param SuperAttribute $resource
     */
    public function __construct(SuperAttribute $resource)
    {
        $this->resource = $resource;
    }

    /**
     * Save Style
     *
     * @param SuperAttributeInterface $supperAttribute
     *
     * @return SuperAttributeInterface
     * @throws Throwable
     * @noinspection PhpParamsInspection
     */
    public function save(SuperAttributeInterface $supperAttribute): SuperAttributeInterface
    {
        try {
            $this->resource->save($supperAttribute);
        } catch (Throwable $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $supperAttribute;
    }
}
