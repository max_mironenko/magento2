<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

// phpcs:disable Magento2.SQL.RawQuery.FoundRawSql

use C38\ProductImport\Api\UPCRepositoryInterface;
use C38\Core\Model\MysqlQueryExecutor;
use Throwable;

/**
 * Class UPCRepository
 * Repository for UPC objects. Using for calculate UPC number for product
 */
class UPCRepository implements UPCRepositoryInterface
{
    /**
     * @var MysqlQueryExecutor
     */
    private $mysqlQueryExecutor;

    /**
     * UPCRepository constructor.
     *
     * @param MysqlQueryExecutor $mysqlQueryExecutor
     */
    public function __construct(
        MysqlQueryExecutor $mysqlQueryExecutor
    ) {
        $this->mysqlQueryExecutor = $mysqlQueryExecutor;
    }

    /**
     * Calculate UPC value for the product id
     *
     * @param string $productId
     *
     * @return string
     * @throws Throwable
     */
    public function calculateUPC(string $productId): string
    {
        // generate UPC in the Database
        $upcValues = $this->mysqlQueryExecutor->execute("SELECT func_calculate_upc({$productId})")->fetchAll();
        // return UPC code
        foreach ($upcValues[0] as $value) {
            return $value;
        }
        return "";
    }
}
