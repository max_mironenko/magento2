<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\PurchaseOrderLineInterface;
use C38\ProductImport\Api\Data\PurchaseOrderLineInterfaceFactory;
use C38\ProductImport\Api\Data\PurchaseOrderLineSearchResultsInterface;
use C38\ProductImport\Api\Data\PurchaseOrderLineSearchResultsInterfaceFactory;
use C38\ProductImport\Api\PurchaseOrderLineRepositoryInterface;
use C38\ProductImport\Helper\PurchaseOrderLogger as Logger;
use C38\ProductImport\Model\ResourceModel\PurchaseOrderLine;
use C38\ProductImport\Model\ResourceModel\PurchaseOrderLine\CollectionFactory;
use Exception;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class PurchaseOrderLineRepository
 * Repository for Purchase Order Line objects
 */
class PurchaseOrderLineRepository implements PurchaseOrderLineRepositoryInterface
{
    /**
     * @var PurchaseOrderLine
     */
    private $resource;

    /**
     * @var PurchaseOrderLineInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var PurchaseOrderLineSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * PurchaseOrderLineRepository constructor.
     *
     * @param PurchaseOrderLine $resource
     * @param PurchaseOrderLineInterfaceFactory $purchaseOrderFactory
     * @param CollectionFactory $collectionFactory
     * @param PurchaseOrderLineSearchResultsInterfaceFactory $searchResultFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param Logger $logger
     */
    public function __construct(
        PurchaseOrderLine $resource,
        PurchaseOrderLineInterfaceFactory $purchaseOrderFactory,
        CollectionFactory $collectionFactory,
        PurchaseOrderLineSearchResultsInterfaceFactory $searchResultFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        Logger $logger
    ) {
        $this->resource = $resource;
        $this->entityFactory = $purchaseOrderFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->logger = $logger;
    }

    /**
     * Save PurchaseOrder Line
     *
     * @param PurchaseOrderLineInterface $purchaseOrderLine
     *
     * @return PurchaseOrderLineInterface
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function save(PurchaseOrderLineInterface $purchaseOrderLine): PurchaseOrderLineInterface
    {
        try {
            $this->resource->save($purchaseOrderLine);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $purchaseOrderLine;
    }

    /**
     * Get PurchaseOrder Line by id.
     *
     * @param $purchaseOrderLineId
     *
     * @return PurchaseOrderLineInterface
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function getById($purchaseOrderLineId): PurchaseOrderLineInterface
    {
        $purchaseOrderLine = $this->entityFactory->create();
        $this->resource->load($purchaseOrderLine, $purchaseOrderLineId);
        if (!$purchaseOrderLine->getId()) {
            throw new NoSuchEntityException(__('PurchaseOrderLine with id "%1" does not exist.', $purchaseOrderLineId));
        }

        return $purchaseOrderLine;
    }

    /**
     * Get all lines by Purchase order ID.
     *
     * @param $purchaseOrderId
     *
     * @return PurchaseOrderLineInterface[]
     * @throws Exception
     */
    public function getByPurchaseOrderId($purchaseOrderId): array
    {
        // prepare filters
        $filters = [
            $this->filterBuilder->setField(PurchaseOrderLineInterface::ORDER_ID)
                ->setConditionType('eq')->setValue($purchaseOrderId)->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // return list of items
        return $this->getList($searchCriteria)->getItems();
    }

    /**
     * Find PurchaseOrder Line by id.
     *
     * @param $purchaseOrderLineId
     * @return PurchaseOrderLineInterface|null
     * @noinspection PhpParamsInspection
     */
    public function findById($purchaseOrderLineId): ?PurchaseOrderLineInterface
    {
        $purchaseOrderLine = $this->entityFactory->create();
        $this->resource->load($purchaseOrderLine, $purchaseOrderLineId);
        if (!$purchaseOrderLine->getId()) {
            return null;
        }

        return $purchaseOrderLine;
    }

    /**
     * Retrieve PurchaseOrder Lines matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface|PurchaseOrderLineSearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete PurchaseOrder Line
     *
     * @param PurchaseOrderLineInterface $purchaseOrderLine
     *
     * @return bool
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function delete(PurchaseOrderLineInterface $purchaseOrderLine): bool
    {
        try {
            $this->resource->delete($purchaseOrderLine);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete PurchaseOrder Line by ID.
     *
     * @param $purchaseOrderLineId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById($purchaseOrderLineId): bool
    {
        return $this->delete($this->getById($purchaseOrderLineId));
    }

    /**
     * Delete all lines by PurchaseOrder ID.
     *
     * @param $purchaseOrderId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteByPurchaseOrderId($purchaseOrderId): bool
    {
        // get list of all lines for this purchase order
        $lines = $this->getByPurchaseOrderId($purchaseOrderId);
        if (empty($lines)) {
            return false;
        }

        // delete each line
        foreach ($lines as $line) {
            $this->delete($line);
        }

        return true;
    }
}
