<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\StyleLoggerInterface;
use C38\ProductImport\Model\ResourceModel\StyleLogger as ResourceStyleLogger;
use Magento\Framework\Model\AbstractModel;

/**
 * Class StyleLogger
 * Model of Logger for Styles
 *
 * @method ResourceStyleLogger getResource()
 */
class StyleLogger extends AbstractModel implements StyleLoggerInterface
{
    protected $_eventPrefix = 'c38_product_import_model_style_logger';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceStyleLogger::class);
    }

    /**
     * Get Id
     *
     * @return string | null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setId($value): StyleLoggerInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get Message Type
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->_getData(self::TYPE);
    }

    /**
     * Set Message Type
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setType($value): StyleLoggerInterface
    {
        $this->setData(self::TYPE, $value);
        return $this;
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->_getData(self::MESSAGE);
    }

    /**
     * Set Message
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setMessage($value): StyleLoggerInterface
    {
        $this->setData(self::MESSAGE, $value);
        return $this;
    }

    /**
     * Get Full Error (stack trace)
     *
     * @return string
     */
    public function getFullError(): string
    {
        return $this->_getData(self::FULL_ERROR);
    }

    /**
     * Set Full Error (stack trace)
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setFullError($value): StyleLoggerInterface
    {
        $this->setData(self::FULL_ERROR, $value);
        return $this;
    }

    /**
     * Get Cron Name
     *
     * @return string
     */
    public function getCronName(): string
    {
        return $this->_getData(self::CRON_NAME);
    }

    /**
     * Set Cron Name
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setCronName($value): StyleLoggerInterface
    {
        $this->setData(self::CRON_NAME, $value);
        return $this;
    }

    /**
     * Get ProductId
     *
     * @return string
     */
    public function getProductId(): string
    {
        return $this->_getData(self::PRODUCT_ID);
    }

    /**
     * Set ProductId
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setProductId($value): StyleLoggerInterface
    {
        $this->setData(self::PRODUCT_ID, $value);
        return $this;
    }

    /**
     * Get Created At
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set Created At
     *
     * @param $value
     * @return StyleLoggerInterface
     */
    public function setCreatedAt($value): StyleLoggerInterface
    {
        $this->setData(self::CREATED_AT, $value);
        return $this;
    }
}
