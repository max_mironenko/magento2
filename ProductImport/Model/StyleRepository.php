<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\Core\Model\MysqlQueryExecutor;
use C38\ProductImport\Api\Data\StyleHistoryInterfaceFactory;
use C38\ProductImport\Api\Data\StyleInterface;
use C38\ProductImport\Api\Data\StyleInterfaceFactory;
use C38\ProductImport\Api\Data\StyleSearchResultsInterfaceFactory;
use C38\ProductImport\Api\StyleHistoryRepositoryInterface;
use C38\ProductImport\Api\StyleRepositoryInterface;
use C38\ProductImport\Helper\StyleLogger as Logger;
use C38\ProductImport\Model\ResourceModel\Style;
use C38\ProductImport\Model\ResourceModel\Style\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class StyleRepository
 * Repository for Style (ERP Products) objects
 */
class StyleRepository implements StyleRepositoryInterface
{
    const LOGGER_NAME = 'ERPStyleImport';

    /**
     * @var Style
     */
    private $resource;

    /**
     * @var StyleInterfaceFactory
     */
    private $entityFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var StyleSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var StyleHistoryInterfaceFactory
     */
    private $styleHistoryFactory;

    /**
     * @var StyleHistoryRepositoryInterface
     */
    private $styleHistoryRepository;

    /**
     * @var MysqlQueryExecutor
     */
    private $mysqlQueryExecutor;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * StyleRepository constructor.
     *
     * @param Style $resource
     * @param StyleInterfaceFactory $styleFactory
     * @param CollectionFactory $collectionFactory
     * @param StyleSearchResultsInterfaceFactory $searchResultFactory
     * @param StyleHistoryInterfaceFactory $styleHistoryFactory
     * @param StyleHistoryRepositoryInterface $styleHistoryRepository
     * @param MysqlQueryExecutor $mysqlQueryExecutor
     * @param Json $json
     * @param Logger $logger
     */
    public function __construct(
        Style $resource,
        StyleInterfaceFactory $styleFactory,
        CollectionFactory $collectionFactory,
        StyleSearchResultsInterfaceFactory $searchResultFactory,
        StyleHistoryInterfaceFactory $styleHistoryFactory,
        StyleHistoryRepositoryInterface $styleHistoryRepository,
        MysqlQueryExecutor $mysqlQueryExecutor,
        Json $json,
        Logger $logger
    ) {
        $this->resource = $resource;
        $this->entityFactory = $styleFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->styleHistoryFactory = $styleHistoryFactory;
        $this->styleHistoryRepository = $styleHistoryRepository;
        $this->mysqlQueryExecutor = $mysqlQueryExecutor;
        $this->json = $json;
        $this->logger = $logger;
    }

    /**
     * Save Style
     *
     * @param StyleInterface $style
     *
     * @return StyleInterface
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function save(StyleInterface $style): StyleInterface
    {
        try {
            $this->resource->save($style);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $style;
    }

    /**
     * Get Style by id.
     *
     * @param $styleId
     *
     * @return StyleInterface
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function getById($styleId): StyleInterface
    {
        $style = $this->entityFactory->create();
        $this->resource->load($style, $styleId);
        if (!$style->getId()) {
            throw new NoSuchEntityException(__('Style with id "%1" does not exist.', $styleId));
        }

        return $style;
    }

    /**
     * Find Style by id.
     *
     * @param $styleId
     * @return StyleInterface|null
     * @noinspection PhpParamsInspection
     */
    public function findById($styleId): ?StyleInterface
    {
        $style = $this->entityFactory->create();
        $this->resource->load($style, $styleId);

        if (!$style->getId()) {
            return null;
        }

        return $style;
    }

    /**
     * Get Style by SKU.
     *
     * @param $styleSku
     *
     * @return StyleInterface
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function getBySku($styleSku): StyleInterface
    {
        $style = $this->entityFactory->create();
        $this->resource->load($style, $styleSku, StyleInterface::INVENTORY_SKU);
        return $style;
    }

    /**
     * Retrieve Style matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Exception
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }

    /**
     * Delete Style
     *
     * @param StyleInterface $style
     *
     * @return bool
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function delete(StyleInterface $style): bool
    {
        try {
            $this->resource->delete($style);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Style by ID.
     *
     * @param $styleId
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById($styleId): bool
    {
        return $this->delete($this->getById($styleId));
    }

    /**
     * Mark style record as processed and save to the History Repository
     *
     * @param  $styleId
     * @param $productID
     *
     * @return bool
     * @throws Exception
     * @noinspection PhpUndefinedMethodInspection
     */
    public function markAsProcessed($styleId, $productID): bool
    {
        if ($productID != -1) {
            // update 'update_at' column
            $this->mysqlQueryExecutor->execute(
                "UPDATE " . " catalog_product_entity set `updated_at` = now() where entity_id = {$productID};"
            );
        }

        // load current record
        $style = $this->getById($styleId);
        // update this style record
        $style->setIsProcessed(1);
        // save record
        $this->save($style);

        // save line in the history table
        $styleHistory = $this->styleHistoryFactory->create();
        // set history data
        $styleHistory->setData($style->getData());
        $styleHistory->setId(null);
        // save history data
        $this->styleHistoryRepository->save($styleHistory);

        // delete record
        return $this->delete($style);
    }

    /**
     * Update style
     *
     * @param mixed $styleData
     * @return array
     * @throws Exception
     * @noinspection PhpUndefinedMethodInspection
     */
    public function updateStyle($styleData): array
    {
        $styleSku = '';
        try {
            // check if array
            if (!is_array($styleData)) {
                $this->logger->error(
                    'Update Style Error: Style Data is not an array',
                    $this->json->serialize($styleData),
                    self::LOGGER_NAME
                );
                return ['error' => 'Style Data is not an array'];
            }

            // get style SKU
            if (!empty($styleData[StyleInterface::INVENTORY_SKU])) {
                $styleSku = $styleData[StyleInterface::INVENTORY_SKU];
            } else {
                if (!empty($styleData[StyleInterface::STYLE_BASE_SKU_WITH_COLOR])) {
                    $styleSku = $styleData[StyleInterface::STYLE_BASE_SKU_WITH_COLOR];
                } else {
                    $this->logger->error(
                        'Update Style Error: SKU not found in the Style Data',
                        $this->json->serialize($styleData),
                        self::LOGGER_NAME
                    );
                    return ['error' => 'SKU not found in the Style Data'];
                }
            }

            // load or create new style from database
            $style = $this->getBySku($styleSku);

            // set style values
            foreach (array_keys($styleData) as $column) {
                $style->setData($column, "" . $styleData[$column]);
            }
            // set SKU
            $style->setInventorySKU($styleSku);

            // set type
            if (empty($styleData[StyleInterface::STYLE_BASE_SKU_WITH_COLOR]) ||
                $styleSku != $styleData[StyleInterface::STYLE_BASE_SKU_WITH_COLOR]) {
                // set simple
                $style->setIsConfigurable(0);
            } else {
                // configurable product
                $style->setIsConfigurable(1);
            }

            // remove previous error
            $style->setError(null);

            // save style
            $this->save($style);

            // finish
            $response = ['success' => 'Style data has been saved into database'];
            $this->logger->info(
                "Update Style: Style with SKU {$styleSku} has been saved into database",
                self::LOGGER_NAME,
                $styleSku
            );
        } catch (Exception $ex) {
            $this->logger->error(
                "Update Style Error: " . $ex->getMessage(),
                $ex->getTraceAsString(),
                self::LOGGER_NAME,
                $styleSku
            );
            $response = ['error' => 'Style data not saved. ' . $ex->getMessage()];
        }

        // return result
        return $response;
    }
}
