<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\SwatchAttributeInterface;
use C38\ProductImport\Api\Data\SwatchAttributeSearchResultsInterfaceFactory;
use C38\ProductImport\Api\SwatchAttributeRepositoryInterface;
use C38\ProductImport\Model\ResourceModel\SwatchAttribute;
use C38\ProductImport\Model\ResourceModel\SwatchAttribute\CollectionFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Throwable;

/**
 * Class SwatchAttributeRepository
 * Repository for SwatchAttribute objects
 */
class SwatchAttributeRepository implements SwatchAttributeRepositoryInterface
{
    /**
     * @var SwatchAttribute
     */
    private $resource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SwatchAttributeSearchResultsInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * SwatchAttributeRepository constructor.
     *
     * @param SwatchAttribute $resource
     * @param CollectionFactory $collectionFactory
     * @param SwatchAttributeSearchResultsInterfaceFactory $searchResultFactory
     */
    public function __construct(
        SwatchAttribute $resource,
        CollectionFactory $collectionFactory,
        SwatchAttributeSearchResultsInterfaceFactory $searchResultFactory
    ) {
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * Save Swatch Attribute
     *
     * @param SwatchAttributeInterface $swatchAttribute
     *
     * @return SwatchAttributeInterface
     * @throws Throwable
     * @noinspection PhpParamsInspection
     */
    public function save(SwatchAttributeInterface $swatchAttribute): SwatchAttributeInterface
    {
        try {
            $this->resource->save($swatchAttribute);
        } catch (Throwable $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $swatchAttribute;
    }

    /**
     * Retrieve Swatch Attributes matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws Throwable
     * @noinspection DuplicatedCode
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        $searchResult->setTotalCount($collection->getSize());
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() === SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $searchResult->setItems($collection->getItems());
    }
}
