<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\StyleHistoryInterface;
use C38\ProductImport\Api\StyleHistoryRepositoryInterface;
use C38\ProductImport\Model\ResourceModel\StyleHistory;
use Exception;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Class StyleHistoryRepository
 * Repository for StylesHistory (processed styles) object
 */
class StyleHistoryRepository implements StyleHistoryRepositoryInterface
{
    /**
     * @var StyleHistory
     */
    private $resource;

    /**
     * StyleHistoryRepository constructor.
     *
     * @param StyleHistory $resource
     */
    public function __construct(
        StyleHistory $resource
    ) {
        $this->resource = $resource;
    }

    /**
     * Save StyleHistory
     *
     * @param StyleHistoryInterface $style
     *
     * @return StyleHistoryInterface
     * @throws Exception
     * @noinspection PhpParamsInspection
     */
    public function save(StyleHistoryInterface $style): StyleHistoryInterface
    {
        try {
            $this->resource->save($style);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $style;
    }
}
