<?php declare(strict_types=1);

namespace C38\ProductImport\Model\ResourceModel\PurchaseOrder;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\ProductImport\Model\PurchaseOrder;
use C38\ProductImport\Model\ResourceModel\PurchaseOrder as ResourcePurchaseOrder;

/**
 * Class Collection
 * Resource collection of Purchase Orders
 *
 * @method ResourcePurchaseOrder getResource()
 * @method PurchaseOrder[] getItems()
 * @method PurchaseOrder[] getItemsByColumnValue($column, $value)
 * @method PurchaseOrder getFirstItem()
 * @method PurchaseOrder getLastItem()
 * @method PurchaseOrder getItemByColumnValue($column, $value)
 * @method PurchaseOrder getItemById($idValue)
 * @method PurchaseOrder getNewEmptyItem()
 * @method PurchaseOrder fetchItem()
 * @property PurchaseOrder[] _items
 * @property ResourcePurchaseOrder _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_product_import_model_purchase_order_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(PurchaseOrder::class, ResourcePurchaseOrder::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
