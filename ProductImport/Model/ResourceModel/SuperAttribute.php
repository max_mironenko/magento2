<?php declare(strict_types=1);

namespace C38\ProductImport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use C38\ProductImport\Api\Data\SuperAttributeInterface as DataInterface;

/**
 * Class SuperAttribute
 * Resource model of SuperAttribute
 */
class SuperAttribute extends AbstractDb
{
    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(DataInterface::TABLE_NAME, DataInterface::PRODUCT_SUPER_ATTRIBUTE_ID);
    }
}
