<?php declare(strict_types=1);

namespace C38\ProductImport\Model\ResourceModel\PurchaseOrderLine;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\ProductImport\Model\PurchaseOrderLine;
use C38\ProductImport\Model\ResourceModel\PurchaseOrderLine as ResourcePurchaseOrderLine;

/**
 * Class Collection
 * Resource collection of Purchase Order Lines (Items)
 *
 * @method ResourcePurchaseOrderLine getResource()
 * @method PurchaseOrderLine[] getItems()
 * @method PurchaseOrderLine[] getItemsByColumnValue($column, $value)
 * @method PurchaseOrderLine getFirstItem()
 * @method PurchaseOrderLine getLastItem()
 * @method PurchaseOrderLine getItemByColumnValue($column, $value)
 * @method PurchaseOrderLine getItemById($idValue)
 * @method PurchaseOrderLine getNewEmptyItem()
 * @method PurchaseOrderLine fetchItem()
 * @property PurchaseOrderLine[] _items
 * @property ResourcePurchaseOrderLine _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_product_import_model_purchase_order_line_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(PurchaseOrderLine::class, ResourcePurchaseOrderLine::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
