<?php declare(strict_types=1);

namespace C38\ProductImport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use C38\ProductImport\Api\Data\StyleHistoryInterface as DataInterface;

/**
 * Class StyleHistory
 * Resource model of Style History (processed in magento ERP object)
 */
class StyleHistory extends AbstractDb
{
    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(DataInterface::TABLE_NAME, DataInterface::ID);
    }
}
