<?php declare(strict_types=1);

namespace C38\ProductImport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use C38\ProductImport\Api\Data\PurchaseOrderLineInterface as DataInterface;

/**
 * Class PurchaseOrderLine
Resource model of Purchase Order Line
 */
class PurchaseOrderLine extends AbstractDb
{
    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(DataInterface::TABLE_NAME, DataInterface::ID);
    }
}
