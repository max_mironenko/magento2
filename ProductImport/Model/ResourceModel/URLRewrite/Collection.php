<?php declare(strict_types=1);

namespace C38\ProductImport\Model\ResourceModel\URLRewrite;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\ProductImport\Model\URLRewrite;
use C38\ProductImport\Model\ResourceModel\URLRewrite as ResourceURLRewrite;

/**
 * Class Collection
 * Resource collection of URL Rewrites
 *
 * @method ResourceURLRewrite getResource()
 * @method URLRewrite[] getItems()
 * @method URLRewrite[] getItemsByColumnValue($column, $value)
 * @method URLRewrite getFirstItem()
 * @method URLRewrite getLastItem()
 * @method URLRewrite getItemByColumnValue($column, $value)
 * @method URLRewrite getItemById($idValue)
 * @method URLRewrite getNewEmptyItem()
 * @method URLRewrite fetchItem()
 * @property URLRewrite[] _items
 * @property ResourceURLRewrite _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_product_import_url_rewrite_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(URLRewrite::class, ResourceURLRewrite::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
