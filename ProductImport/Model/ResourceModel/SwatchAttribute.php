<?php declare(strict_types=1);

namespace C38\ProductImport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use C38\ProductImport\Api\Data\SwatchAttributeInterface as DataInterface;

/**
 * Class SwatchAttribute
 * Resource model of SwatchAttribute object
 */
class SwatchAttribute extends AbstractDb
{
    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(DataInterface::TABLE_NAME, DataInterface::SWATCH_ID);
    }
}
