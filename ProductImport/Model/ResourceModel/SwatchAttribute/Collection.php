<?php declare(strict_types=1);

namespace C38\ProductImport\Model\ResourceModel\SwatchAttribute;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\ProductImport\Model\SwatchAttribute;
use C38\ProductImport\Model\ResourceModel\SwatchAttribute as ResourceSwatchAttribute;

/**
 * Class Collection
 * Resource collection of Swatch Attribute objects
 *
 * @method ResourceSwatchAttribute getResource()
 * @method SwatchAttribute[] getItems()
 * @method SwatchAttribute[] getItemsByColumnValue($column, $value)
 * @method SwatchAttribute getFirstItem()
 * @method SwatchAttribute getLastItem()
 * @method SwatchAttribute getItemByColumnValue($column, $value)
 * @method SwatchAttribute getItemById($idValue)
 * @method SwatchAttribute getNewEmptyItem()
 * @method SwatchAttribute fetchItem()
 * @property SwatchAttribute[] _items
 * @property ResourceSwatchAttribute _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_product_import_swatch_attribute_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(SwatchAttribute::class, ResourceSwatchAttribute::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
