<?php declare(strict_types=1);

namespace C38\ProductImport\Model\ResourceModel\Style;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use C38\ProductImport\Model\Style;
use C38\ProductImport\Model\ResourceModel\Style as ResourceStyle;

/**
 * Class Collection
 * Resource collection of Styles (ERP objects)
 *
 * @method ResourceStyle getResource()
 * @method Style[] getItems()
 * @method Style[] getItemsByColumnValue($column, $value)
 * @method Style getFirstItem()
 * @method Style getLastItem()
 * @method Style getItemByColumnValue($column, $value)
 * @method Style getItemById($idValue)
 * @method Style getNewEmptyItem()
 * @method Style fetchItem()
 * @property Style[] _items
 * @property ResourceStyle _resource
 */
class Collection extends AbstractCollection
{
    protected $_eventPrefix = 'c38_product_import_style_collection';

    protected $_eventObject = 'object';

    /**
     * Class constructor.
     *
     * @throws LocalizedException
     */
    protected function _construct()
    {
        $this->_init(Style::class, ResourceStyle::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }
}
