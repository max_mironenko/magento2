<?php declare(strict_types=1);

namespace C38\ProductImport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use C38\ProductImport\Api\Data\StyleInterface as DataInterface;

/**
 * Class Style
 * Resource model of Style (ERP Object)
 */
class Style extends AbstractDb
{
    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(DataInterface::TABLE_NAME, DataInterface::ID);
    }
}
