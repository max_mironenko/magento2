<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\StyleInterface;
use C38\ProductImport\Model\ResourceModel\Style as ResourceStyle;
use C38\ProductImport\Model\ResourceModel\Style\Collection as ResourceStyleCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Style
 * Model of Style (ERP Product data)
 *
 * @method ResourceStyle getResource()
 * @method ResourceStyleCollection getCollection()
 * @method ResourceStyleCollection getResourceCollection()
 */
class Style extends AbstractModel implements StyleInterface
{
    protected $_eventPrefix = 'c38_product_import_model_style';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceStyle::class);
    }

    /**
     * Get Id
     *
     * @return string | null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::ID);
    }

    /**
     * Set Id
     *
     * @param $value
     * @return StyleInterface
     */
    public function setId($value): StyleInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * Get StyleCategorySubDescription value
     *
     * @return string
     */
    public function getStyleCategorySubDescription(): string
    {
        return $this->_getData(self::STYLE_CATEGORY_SUB_DESCRIPTION);
    }

    /**
     * Set StyleCategorySubDescription value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleCategorySubDescription($value): StyleInterface
    {
        $this->setData(self::STYLE_CATEGORY_SUB_DESCRIPTION, $value);
        return $this;
    }

    /**
     * Get StyleSeasonsLabel value
     *
     * @return string
     */
    public function getStyleSeasonsLabel(): string
    {
        return $this->_getData(self::STYLE_SEASONS_LABEL);
    }

    /**
     * Set StyleSeasonsLabel value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleSeasonsLabel($value): StyleInterface
    {
        $this->setData(self::STYLE_SEASONS_LABEL, $value);
        return $this;
    }

    /**
     * Get StyleColorSecondaryName value
     *
     * @return string
     */
    public function getStyleColorSecondaryName(): string
    {
        return $this->_getData(self::STYLE_COLOR_SECONDARY_NAME);
    }

    /**
     * Set StyleColorSecondaryName value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorSecondaryName($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_SECONDARY_NAME, $value);
        return $this;
    }

    /**
     * Get StyleColorSecondaryAbbreviation value
     *
     * @return string
     */
    public function getStyleColorSecondaryAbbreviation(): string
    {
        return $this->_getData(self::STYLE_COLOR_SECONDARY_ABBREVIATION);
    }

    /**
     * Set StyleColorSecondaryAbbreviation value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorSecondaryAbbreviation($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_SECONDARY_ABBREVIATION, $value);
        return $this;
    }

    /**
     * Get StyleCategoryDescription value
     *
     * @return string
     */
    public function getStyleCategoryDescription(): string
    {
        return $this->_getData(self::STYLE_CATEGORY_DESCRIPTION);
    }

    /**
     * Set StyleCategoryDescription value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleCategoryDescription($value): StyleInterface
    {
        $this->setData(self::STYLE_CATEGORY_DESCRIPTION, $value);
        return $this;
    }

    /**
     * Get StyleColorVendorSku value
     *
     * @return string
     */
    public function getStyleColorVendorSku(): string
    {
        return $this->_getData(self::STYLE_COLOR_VENDOR_SKU);
    }

    /**
     * Set StyleColorVendorSku value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorVendorSku($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_VENDOR_SKU, $value);
        return $this;
    }

    /**
     * Get StyleColorVendorColor value
     *
     * @return string
     */
    public function getStyleColorVendorColor(): string
    {
        return $this->_getData(self::STYLE_COLOR_VENDOR_COLOR);
    }

    /**
     * Set StyleColorVendorColor value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorVendorColor($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_VENDOR_COLOR, $value);
        return $this;
    }

    /**
     * Get StyleColorLaunchDate value
     *
     * @return string
     */
    public function getStyleColorLaunchDate(): string
    {
        return $this->_getData(self::STYLE_COLOR_LAUNCH_DATE);
    }

    /**
     * Set StyleColorLaunchDate value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorLaunchDate($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_LAUNCH_DATE, $value);
        return $this;
    }

    /**
     * Get StyleColorStyleSkuColor value
     *
     * @return string
     */
    public function getStyleColorStyleSkuColor(): string
    {
        return $this->_getData(self::STYLE_COLOR_STYLE_SKU_COLOR);
    }

    /**
     * Set StyleColorStyleSkuColor value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorStyleSkuColor($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_STYLE_SKU_COLOR, $value);
        return $this;
    }

    /**
     * Get StyleSkuBase value
     *
     * @return string
     */
    public function getStyleSkuBase(): string
    {
        return $this->_getData(self::STYLE_SKU_BASE);
    }

    /**
     * Set StyleSkuBase value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleSkuBase($value): StyleInterface
    {
        $this->setData(self::STYLE_SKU_BASE, $value);
        return $this;
    }

    /**
     * Get StyleBaseName value
     *
     * @return string
     */
    public function getStyleBaseName(): string
    {
        return $this->_getData(self::STYLE_BASE_NAME);
    }

    /**
     * Set StyleBaseName value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseName($value): StyleInterface
    {
        $this->setData(self::STYLE_BASE_NAME, $value);
        return $this;
    }

    /**
     * Get StyleBaseMSRP value
     * @return string
     */
    public function getStyleBaseMSRP(): string
    {
        return $this->_getData(self::STYLE_BASE_MSRP);
    }

    /**
     * Set StyleBaseMSRP value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseMSRP($value): StyleInterface
    {
        $this->setData(self::STYLE_BASE_MSRP, $value);
        return $this;
    }

    /**
     * Get StyleBaseLaunchDate value
     *
     * @return string
     */
    public function getStyleBaseLaunchDate(): string
    {
        return $this->_getData(self::STYLE_BASE_LAUNCH_DATE);
    }

    /**
     * Set StyleBaseLaunchDate value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseLaunchDate($value): StyleInterface
    {
        $this->setData(self::STYLE_BASE_LAUNCH_DATE, $value);
        return $this;
    }

    /**
     * Get StyleBaseStyleSkuCode value
     *
     * @return string
     */
    public function getStyleBaseStyleSkuCode(): string
    {
        return $this->_getData(self::STYLE_BASE_STYLE_SKU_CODE);
    }

    /**
     * Set StyleBaseStyleSkuCode value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseStyleSkuCode($value): StyleInterface
    {
        $this->setData(self::STYLE_BASE_STYLE_SKU_CODE, $value);
        return $this;
    }

    /**
     * Get StyleBaseDefVendorCost value
     *
     * @return string
     */
    public function getStyleBaseDefVendorCost(): string
    {
        return $this->_getData(self::STYLE_BASE_DEF_VENDOR_COST);
    }

    /**
     * Set StyleBaseDefVendorCost value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseDefVendorCost($value): StyleInterface
    {
        $this->setData(self::STYLE_BASE_DEF_VENDOR_COST, $value);
        return $this;
    }

    /**
     * Get StyleBaseStyleSkuBase value
     *
     * @return string
     */
    public function getStyleBaseStyleSkuBase(): string
    {
        return $this->_getData(self::STYLE_BASE_STYLE_SKU_BASE);
    }

    /**
     * Set StyleBaseStyleSkuBase value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseStyleSkuBase($value): StyleInterface
    {
        $this->setData(self::STYLE_BASE_STYLE_SKU_BASE, $value);
        return $this;
    }

    /**
     * Get StyleBaseStyleSkuDesigner value
     *
     * @return string
     */
    public function getStyleBaseStyleSkuDesigner(): string
    {
        return $this->_getData(self::STYLE_BASE_STYLE_SKU_DESIGNER);
    }

    /**
     * Set StyleBaseStyleSkuDesigner value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseStyleSkuDesigner($value): StyleInterface
    {
        $this->setData(self::STYLE_BASE_STYLE_SKU_DESIGNER, $value);
        return $this;
    }

    /**
     * Get StyleCategoryParentDescription value
     *
     * @return string
     */
    public function getStyleCategoryParentDescription(): string
    {
        return $this->_getData(self::STYLE_CATEGORY_PARENT_DESCRIPTION);
    }

    /**
     * Set StyleCategoryParentDescription value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleCategoryParentDescription($value): StyleInterface
    {
        $this->setData(self::STYLE_CATEGORY_PARENT_DESCRIPTION, $value);
        return $this;
    }

    /**
     * Get InventorySKU value
     *
     * @return string
     */
    public function getInventorySKU(): string
    {
        return $this->_getData(self::INVENTORY_SKU);
    }

    /**
     * Set InventorySKU value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setInventorySKU($value): StyleInterface
    {
        $this->setData(self::INVENTORY_SKU, $value);
        return $this;
    }

    /**
     * Get InventorySize value
     *
     * @return string
     */
    public function getInventorySize(): string
    {
        return $this->_getData(self::INVENTORY_SIZE);
    }

    /**
     * Set InventorySize value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setInventorySize($value): StyleInterface
    {
        $this->setData(self::INVENTORY_SIZE, $value);
        return $this;
    }

    /**
     * Get StyleColorPrimaryName value
     *
     * @return string
     */
    public function getStyleColorPrimaryName(): string
    {
        return $this->_getData(self::STYLE_COLOR_PRIMARY_NAME);
    }

    /**
     * Set StyleColorPrimaryName value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorPrimaryName($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_PRIMARY_NAME, $value);
        return $this;
    }

    /**
     * Get StyleColorPrimaryAbbreviation value
     *
     * @return string
     */
    public function getStyleColorPrimaryAbbreviation(): string
    {
        return $this->_getData(self::STYLE_COLOR_PRIMARY_ABBREVIATION);
    }

    /**
     * Set StyleColorPrimaryAbbreviation value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorPrimaryAbbreviation($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_PRIMARY_ABBREVIATION, $value);
        return $this;
    }

    /**
     * Get StyleDesignerDescription value
     *
     * @return string
     */
    public function getStyleDesignerDescription(): string
    {
        return $this->_getData(self::STYLE_DESIGNER_DESCRIPTION);
    }

    /**
     * Set StyleDesignerDescription value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleDesignerDescription($value): StyleInterface
    {
        $this->setData(self::STYLE_DESIGNER_DESCRIPTION, $value);
        return $this;
    }

    /**
     * Get StyleColorPatternLabel value
     *
     * @return string
     */
    public function getStyleColorPatternLabel(): string
    {
        return $this->_getData(self::STYLE_COLOR_PATTERN_LABEL);
    }

    /**
     * Set StyleColorPatternLabel value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorPatternLabel($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_PATTERN_LABEL, $value);
        return $this;
    }

    /**
     * Get StyleBaseVendorDescription value
     *
     * @return string
     */
    public function getStyleBaseVendorDescription(): string
    {
        return $this->_getData(self::STYLE_BASE_VENDOR_DESCRIPTION);
    }

    /**
     * Set StyleBaseVendorDescription value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseVendorDescription($value): StyleInterface
    {
        $this->setData(self::STYLE_BASE_VENDOR_DESCRIPTION, $value);
        return $this;
    }

    /**
     * Get StyleColorVendorCost value
     *
     * @return string
     */
    public function getStyleColorVendorCost(): string
    {
        return $this->_getData(self::STYLE_COLOR_VENDOR_COST);
    }

    /**
     * Set StyleColorVendorCost value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorVendorCost($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_VENDOR_COST, $value);
        return $this;
    }

    /**
     * Get StyleCategorySubDefWeight value
     *
     * @return string
     */
    public function getStyleCategorySubDefWeight(): string
    {
        return $this->_getData(self::STYLE_CATEGORY_SUB_DEF_WEIGHT);
    }

    /**
     * Set StyleCategorySubDefWeight value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleCategorySubDefWeight($value): StyleInterface
    {
        $this->setData(self::STYLE_CATEGORY_SUB_DEF_WEIGHT, $value);
        return $this;
    }

    /**
     * Get StyleBaseSkuWithColor value
     *
     * @return string
     */
    public function getStyleBaseSkuWithColor(): string
    {
        return $this->_getData(self::STYLE_BASE_SKU_WITH_COLOR);
    }

    /**
     * Set StyleBaseSkuWithColor value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleBaseSkuWithColor($value): StyleInterface
    {
        $this->setData(self::STYLE_BASE_SKU_WITH_COLOR, $value);
        return $this;
    }

    /**
     * Get IsProcessed value
     *
     * @return string|null
     */
    public function getIsProcessed(): ?string
    {
        return $this->_getData(self::IS_PROCESSED);
    }

    /**
     * Set IsProcessed value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setIsProcessed($value): StyleInterface
    {
        $this->setData(self::IS_PROCESSED, $value);
        return $this;
    }

    /**
     * Get CreatedAt value
     *
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setCreatedAt($value): StyleInterface
    {
        $this->setData(self::CREATED_AT, $value);
        return $this;
    }

    /**
     * Get IsConfigurable value
     *
     * @return string
     */
    public function getIsConfigurable(): string
    {
        return $this->_getData(self::IS_CONFIGURABLE);
    }

    /**
     * Set IsConfigurable value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setIsConfigurable($value): StyleInterface
    {
        $this->setData(self::IS_CONFIGURABLE, $value);
        return $this;
    }

    /**
     * Get IsExclusive value
     *
     * @return string
     */
    public function getIsExclusive(): string
    {
        return $this->_getData(self::IS_EXCLUSIVE);
    }

    /**
     * Set IsExclusive value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setIsExclusive($value): StyleInterface
    {
        $this->setData(self::IS_EXCLUSIVE, $value);
        return $this;
    }

    /**
     * Get StyleColorCostAfterDiscount value
     *
     * @return string
     */
    public function getStyleColorCostAfterDiscount(): string
    {
        return $this->_getData(self::STYLE_COLOR_COST_AFTER_DISCOUNT);
    }

    /**
     * Set StyleColorCostAfterDiscount value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorCostAfterDiscount($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_COST_AFTER_DISCOUNT, $value);
        return $this;
    }

    /**
     * Get StyleColorStandardCost value
     *
     * @return string
     */
    public function getStyleColorStandardCost(): string
    {
        return $this->_getData(self::STYLE_COLOR_STANDARD_COST);
    }

    /**
     * Set StyleColorStandardCost value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setStyleColorStandardCost($value): StyleInterface
    {
        $this->setData(self::STYLE_COLOR_STANDARD_COST, $value);
        return $this;
    }

    /**
     * Get Collection value
     *
     * @return string
     */
    public function getCollectionName(): string
    {
        return $this->_getData(self::COLLECTION);
    }

    /**
     * Set Collection value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setCollectionName($value): StyleInterface
    {
        $this->setData(self::COLLECTION, $value);
        return $this;
    }

    /**
     * Get Error value
     *
     * @return string
     */
    public function getError(): string
    {
        return $this->_getData(self::ERROR);
    }

    /**
     * Set Error value
     *
     * @param $value
     * @return StyleInterface
     */
    public function setError($value): StyleInterface
    {
        $this->setData(self::ERROR, $value);
        return $this;
    }
}
