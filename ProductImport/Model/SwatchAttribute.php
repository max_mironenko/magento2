<?php declare(strict_types=1);

namespace C38\ProductImport\Model;

use C38\ProductImport\Api\Data\SwatchAttributeInterface;
use C38\ProductImport\Model\ResourceModel\SwatchAttribute as ResourceSwatchAttribute;
use Magento\Framework\Model\AbstractModel;

/**
 * Class SwatchAttribute
 * Model of SwatchAttribute objects
 *
 * @method ResourceSwatchAttribute getResource()
 */
class SwatchAttribute extends AbstractModel implements SwatchAttributeInterface
{
    protected $_eventPrefix = 'c38_product_import_model_swatch_attribute';

    /**
     * Class constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceSwatchAttribute::class);
    }

    /**
     * Get Id
     *
     * @return string | null
     */
    public function getId(): ?string
    {
        return $this->_getData(self::SWATCH_ID);
    }

    /**
     * Set Id
     *
     * @param $value
     * @return SwatchAttributeInterface
     */
    public function setId($value): SwatchAttributeInterface
    {
        $this->setData(self::SWATCH_ID, $value);
        return $this;
    }

    /**
     * Get Swatch value
     *
     * @return string | null
     */
    public function getSwatchId(): ?string
    {
        return $this->_getData(self::SWATCH_ID);
    }

    /**
     * Set Swatch value
     *
     * @param $value
     * @return SwatchAttributeInterface
     */
    public function setSwatchId($value): SwatchAttributeInterface
    {
        $this->setData(self::SWATCH_ID, $value);
        return $this;
    }

    /**
     * Get OptionId value
     *
     * @return string | null
     */
    public function getOptionId(): ?string
    {
        return $this->_getData(self::OPTION_ID);
    }

    /**
     * Set OptionId value
     *
     * @param $value
     * @return SwatchAttributeInterface
     */
    public function setOptionId($value): SwatchAttributeInterface
    {
        $this->setData(self::OPTION_ID, $value);
        return $this;
    }

    /**
     * Get StoreId value
     *
     * @return string
     */
    public function getStoreId(): string
    {
        return $this->_getData(self::STORE_ID);
    }

    /**
     * Set StoreId value
     *
     * @param $value
     * @return SwatchAttributeInterface
     */
    public function setStoreId($value): SwatchAttributeInterface
    {
        $this->setData(self::STORE_ID, $value);
        return $this;
    }

    /**
     * Get Type value
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->_getData(self::TYPE);
    }

    /**
     * Set Type value
     *
     * @param $value
     * @return SwatchAttributeInterface
     */
    public function setType($value): SwatchAttributeInterface
    {
        $this->setData(self::TYPE, $value);
        return $this;
    }

    /**
     * Get Value value
     *
     * @return string | null
     */
    public function getValue(): ?string
    {
        return $this->_getData(self::VALUE);
    }

    /**
     * Set Value value
     *
     * @param $value
     * @return SwatchAttributeInterface
     */
    public function setValue($value): SwatchAttributeInterface
    {
        $this->setData(self::VALUE, $value);
        return $this;
    }
}
