<?php declare(strict_types=1);

namespace C38\ProductImport\Helper;

use C38\ProductColorTree\Api\ColorPrimaryRepositoryInterface;
use C38\ProductColorTree\Api\ColorSecondaryRepositoryInterface;
use C38\ProductColorTree\Api\Data\ColorPrimaryInterfaceFactory;
use C38\ProductColorTree\Api\Data\ColorSecondaryInterfaceFactory;
use C38\ProductImport\Api\Data\StyleInterface;
use C38\ProductImport\Api\Data\SwatchAttributeInterface;
use C38\ProductImport\Api\Data\SwatchAttributeInterfaceFactory;
use C38\ProductImport\Api\SwatchAttributeRepositoryInterface;
use C38\Taxonomy\Model\ResourceModel\Category\CollectionFactory as TaxonomyCategoryCollection;
use C38\Taxonomy\Model\ResourceModel\Subcategory\CollectionFactory as TaxonomySubcategoryCollection;
use Magento\Catalog\Model\Product\Attribute\Repository as ProductAttributeRepository;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory as AttributeCollectionFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Throwable;

/**
 * Class AttributesHelper
 * Create / update product attributes before product creations
 */
class AttributesHelper
{
    /**
     * @var array
     */
    private $productAttributes;

    /**
     * @var array
     */
    private $productAttributeIds;

    /**
     * @var array
     */
    private $primaryColors;

    /**
     * @var array
     */
    private $secondaryColors;

    /**
     * @var array
     */
    private $taxonomyCategoryMappings;

    /**
     * @var array
     */
    private $taxonomySubCategoryMappings;

    /**
     * @var array
     */
    private $taxonomySubCategoryWeights;

    /**
     * @var array
     */
    private $taxonomySubCategoryAttributesMappings;

    /**
     * @var array
     */
    private $updatedAttributeCodes;

    /**
     * @var ProductAttributeRepository
     */
    private $productAttributeRepository;

    /**
     * @var AttributeCollectionFactory
     */
    private $attributeCollectionFactory;

    /**
     * @var TaxonomyCategoryCollection
     */
    private $taxonomyCategoryCollection;

    /**
     * @var TaxonomySubcategoryCollection
     */
    private $taxonomySubcategoryCollection;

    /**
     * @var ColorPrimaryInterfaceFactory
     */
    private $colorPrimaryInterfaceFactory;

    /**
     * @var ColorPrimaryRepositoryInterface
     */
    private $colorPrimaryRepository;

    /**
     * @var ColorSecondaryInterfaceFactory
     */
    private $colorSecondaryInterfaceFactory;

    /**
     * @var ColorSecondaryRepositoryInterface
     */
    private $colorSecondaryRepository;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var SwatchAttributeInterfaceFactory
     */
    private $swatchAttributeFactory;

    /**
     * @var SwatchAttributeRepositoryInterface
     */
    private $swatchAttributeRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * AttributesHelper constructor.
     *
     * @param ProductAttributeRepository $productAttributeRepository
     * @param AttributeCollectionFactory $attributeCollectionFactory
     * @param TaxonomyCategoryCollection $taxonomyCategoryCollection
     * @param TaxonomySubcategoryCollection $taxonomySubcategoryCollection
     * @param ColorPrimaryInterfaceFactory $colorPrimaryInterfaceFactory
     * @param ColorPrimaryRepositoryInterface $colorPrimaryRepository
     * @param ColorSecondaryInterfaceFactory $colorSecondaryInterfaceFactory
     * @param ColorSecondaryRepositoryInterface $colorSecondaryRepository
     * @param EavSetupFactory $eavSetupFactory
     * @param SwatchAttributeInterfaceFactory $swatchAttributeFactory
     * @param SwatchAttributeRepositoryInterface $swatchAttributeRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     */
    public function __construct(
        ProductAttributeRepository $productAttributeRepository,
        AttributeCollectionFactory $attributeCollectionFactory,
        TaxonomyCategoryCollection $taxonomyCategoryCollection,
        TaxonomySubcategoryCollection $taxonomySubcategoryCollection,
        ColorPrimaryInterfaceFactory $colorPrimaryInterfaceFactory,
        ColorPrimaryRepositoryInterface $colorPrimaryRepository,
        ColorSecondaryInterfaceFactory $colorSecondaryInterfaceFactory,
        ColorSecondaryRepositoryInterface $colorSecondaryRepository,
        EavSetupFactory $eavSetupFactory,
        SwatchAttributeInterfaceFactory $swatchAttributeFactory,
        SwatchAttributeRepositoryInterface $swatchAttributeRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder
    ) {
        $this->productAttributeRepository = $productAttributeRepository;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->taxonomyCategoryCollection = $taxonomyCategoryCollection;
        $this->taxonomySubcategoryCollection = $taxonomySubcategoryCollection;
        $this->colorPrimaryInterfaceFactory = $colorPrimaryInterfaceFactory;
        $this->colorPrimaryRepository = $colorPrimaryRepository;
        $this->colorSecondaryInterfaceFactory = $colorSecondaryInterfaceFactory;
        $this->colorSecondaryRepository = $colorSecondaryRepository;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->swatchAttributeFactory = $swatchAttributeFactory;
        $this->swatchAttributeRepository = $swatchAttributeRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        // set defaults
        $this->updatedAttributeCodes = [];
        $this->taxonomySubCategoryWeights = [];
    }

    /**
     * Load all attributes
     *
     * @throws Throwable
     */
    public function loadAttributes()
    {
        // load all attributes
        $this->loadProductAttributes();
        // load all colors
        $this->loadColors();
        // load taxonomy mapping
        $this->loadTaxonomyMapping();
    }

    /**
     * Reload attribute product's options attribute
     *
     * @throws Throwable
     */
    public function reloadAttributes()
    {
        // reload each attribute
        foreach ($this->updatedAttributeCodes as $updatedAttributeCode) {
            // check attribute code
            if (!isset($this->productAttributes[$updatedAttributeCode])) {
                continue;
            }
            if (!isset($this->productAttributeIds[$updatedAttributeCode])) {
                continue;
            }

            // get collection of all options for attribute
            $collection = $this->attributeCollectionFactory
                ->create()
                ->setAttributeFilter($this->productAttributeIds[$updatedAttributeCode])
                ->setStoreFilter(0)
                ->load();

            // prepare collection items
            $items = [];
            foreach ($collection->toOptionArray() as $item) {
                $items[] = [
                    'label' => $item['label'],
                    'value' => $item['value']
                ];
            }

            // set values to the new array
            $this->productAttributes[$updatedAttributeCode] = $items;
        }

        if (empty($this->productAttributes['size'])) {
            return;
        }
        // load swatch attributes
        $swatchAttributes = $this->loadSwatchAttributes();
        // check if every size has swatch attribute
        foreach ($this->productAttributes['size'] as $size) {
            if (!empty($size['value']) && !empty($size['label']) &&
            empty($this->getSwatchAttribute($size['label'], $size['value'], $swatchAttributes))) {
                $this->createSwatchAttribute($size['label'], $size['value']);
            }
        }
    }

    /**
     * Create all attributes in magento
     *
     * @param StyleInterface $product
     * @throws Throwable
     */
    public function createAttributes(StyleInterface $product)
    {
        // get color pattern
        $colorPattern = $this->getValueByLabel(
            $product->getStyleColorPatternLabel(),
            $this->productAttributes['c38_color_pattern']
        );

        // get primary color id
        $primaryColorId = $this->getColorId($product->getStyleColorPrimaryName(), $colorPattern);
        // if primary color not found then create new one
        if (empty($primaryColorId)) {
            $primaryColorId = $this->createPrimaryColor($product, $colorPattern);
        }

        // get secondary color id
        $secondaryColorId = $this->getColorId($product->getStyleColorSecondaryName(), $primaryColorId, false);
        // if secondary color not found then create new one
        if (empty($secondaryColorId)) {
            $this->createSecondaryColor($product, $primaryColorId);
        }

        // check if need to add collection name attribute
        if (!empty($product->getCollectionName())) {
            if (empty($this->getValueByLabel(
                $product->getCollectionName(),
                $this->productAttributes['collection_name']
            ))) {
                // create new collection name attribute
                $this->createAttribute('collection_name', $product->getCollectionName());
            }
        }

        // check if need to add manufacturer attribute
        if (!empty($product->getStyleDesignerDescription())) {
            if (empty($this->getValueByLabel(
                $product->getStyleDesignerDescription(),
                $this->productAttributes['manufacturer']
            ))) {
                // create new manufacturer attribute
                $this->createAttribute('manufacturer', $product->getStyleDesignerDescription());
            }
        }

        // check if need to create color attribute
        if (!empty($product->getStyleColorVendorColor())) {
            if (empty($this->getValueByLabel(
                $product->getStyleColorVendorColor(),
                $this->productAttributes['color']
            ))) {
                // create new color attribute
                $this->createAttribute('color', $product->getStyleColorVendorColor());
            }
        }

        // check if need to create size attribute
        if (!empty($product->getInventorySize())) {
            if (empty($this->getValueByLabel($product->getInventorySize(), $this->productAttributes['size']))) {
                // create new size attribute
                $this->createAttribute('size', $product->getInventorySize());
            }
        }
    }

    /**
     * Get Attribute Set ID by taxonomy category name
     *
     * @param string|null $taxonomyCategoryName
     *
     * @return string
     * @throws Throwable
     */
    public function getTaxonomyAttributeSetId(?string $taxonomyCategoryName): string
    {
        // check values
        if (empty($taxonomyCategoryName)) {
            return "";
        }
        if (empty($this->taxonomySubCategoryAttributesMappings[strtolower($taxonomyCategoryName)])) {
            return "";
        }

        // return attribute set id
        return $this->taxonomySubCategoryAttributesMappings[strtolower($taxonomyCategoryName)];
    }

    /**
     * Get taxonomy category (subcategory) id by name
     *
     * @param string|null $categoryName
     * @param string|null $parentCategory
     * @param bool $isCategory
     *
     * @return string
     * @throws Throwable
     */
    public function getTaxonomyCategoryId(?string $categoryName, ?string $parentCategory, bool $isCategory): string
    {
        // check values
        if (empty($categoryName)) {
            return "";
        }
        if (empty($parentCategory)) {
            return "";
        }

        if ($isCategory) {
            return $this->searchWithParent(
                $categoryName,
                $this->productAttributes['tx_category_id'],
                $this->taxonomyCategoryMappings,
                $parentCategory
            );
        }

        return $this->searchWithParent(
            $categoryName,
            $this->productAttributes['tx_sub_category_id'],
            $this->taxonomySubCategoryMappings,
            $parentCategory
        );
    }

    /**
     * Get attribute value for selected attribute code
     *
     * @param string|null $label
     * @param string $attributeCode
     *
     * @return string
     * @throws Throwable
     */
    public function getAttributeValue(?string $label, string $attributeCode): string
    {
        // check value
        if (empty($label)) {
            return "";
        }
        if (empty($this->productAttributes[$attributeCode])) {
            return "";
        }

        // get value
        return $this->getValueByLabel($label, $this->productAttributes[$attributeCode]);
    }

    /**
     * Get Primary / Secondary Color attribute ID by color name
     *
     * @param string|null $colorName
     * @param string $parentName
     * @param bool $isPrimaryColor
     *
     * @return string
     * @throws Throwable
     */
    public function getColorAttributeId(?string $colorName, string $parentName, bool $isPrimaryColor): string
    {
        // check value
        if (empty($colorName)) {
            return "";
        }
        if (empty($parentName)) {
            return "";
        }

        // get color id
        return $this->getColorId($colorName, $parentName, $isPrimaryColor);
    }

    /**
     * Get Attribute Label by ID
     *
     * @param string|null $attributeId
     * @param string|null $attributeCode
     *
     * @return string
     * @throws Throwable
     */
    public function getAttributeLabel(?string $attributeId, ?string $attributeCode): string
    {
        // check values
        if (empty($attributeId)) {
            return "";
        }
        if (empty($attributeCode)) {
            return "";
        }

        // get list
        if (empty($this->productAttributes[$attributeCode])) {
            return "";
        }
        // get
        foreach ($this->productAttributes[$attributeCode] as $attribute) {
            // check if label set
            if (empty($attribute['value']) || !is_string($attribute['value'])) {
                continue;
            }
            if ($attribute['value'] == $attributeId) {
                return $this->nullToString($attribute['label']);
            }
        }

        // nothing found
        return "";
    }

    /**
     * Get AttributeCode ID by name
     *
     * @param string $attributeCode
     * @return string
     */
    public function getAttributeCodeId(string $attributeCode): string
    {
        // check value
        if (empty($attributeCode)) {
            return '';
        }
        if (empty($this->productAttributeIds[$attributeCode])) {
            return '';
        }

        // return ID
        return $this->productAttributeIds[$attributeCode];
    }

    /**
     * Get default weight for Taxonomy SubCategory
     *
     * @param string $subCategoryId
     * @return string|null
     */
    public function getDefaultWeight(string $subCategoryId): ?string
    {
        if (empty($this->taxonomySubCategoryWeights[$subCategoryId])) {
            return null;
        }
        return $this->taxonomySubCategoryWeights[$subCategoryId];
    }

    /**
     * Create new attribute value in the Magento
     *
     * @param string $attributeCode
     * @param string $value
     *
     * @throws Throwable
     */
    private function createAttribute(string $attributeCode, string $value)
    {
        // check value
        if (empty($attributeCode)) {
            return;
        }
        if (empty($this->productAttributeIds[$attributeCode])) {
            return;
        }

        // create new attribute value
        try {
            // create new attribute
            $eavSetup = $this->eavSetupFactory->create();

            // set values
            $option['attribute_id'] = $this->productAttributeIds[$attributeCode];
            $option['value'][][0] = trim($value);

            // save attribute
            $eavSetup->addAttributeOption($option);

            $this->productAttributes[$attributeCode][] =
                [
                    'label' => $value,
                    'value' => "-1"
                ];

            // add attribute code to the update list
            $this->needUpdateAttributeCode($attributeCode);
        } catch (Throwable $ex) {
            throw new LocalizedException(new Phrase("Can't create attribute: '{$attributeCode}'; " .
                "Message: " . $ex->getMessage()), $ex);
        }
    }

    /**
     * Create new swatch attribute
     *
     * @param string $label
     * @param string $optionId
     *
     * @throws Throwable
     */
    private function createSwatchAttribute(string $label, string $optionId)
    {
        foreach ([0,1] as $storeId) {
            // create new swatch attribute factory
            $swatchAttribute = $this->swatchAttributeFactory->create();
            // set options
            $swatchAttribute->setOptionId($optionId);
            $swatchAttribute->setStoreId($storeId);
            $swatchAttribute->setType(0);
            $swatchAttribute->setValue($label);
            // save swatch attribute
            $this->swatchAttributeRepository->save($swatchAttribute);
        }
    }

    /**
     * Create New Primary Color
     *
     * @param StyleInterface $product
     * @param string $colorPattern
     *
     * @return string
     * @throws Throwable
     * @noinspection PhpUndefinedMethodInspection
     */
    private function createPrimaryColor(StyleInterface $product, string $colorPattern): string
    {
        try {
            // create new color
            $primaryColor = $this->colorPrimaryInterfaceFactory->create();

            // set parameters
            $primaryColor->setColorPatternId($colorPattern);
            $primaryColor->setPrimaryCpName($product->getStyleColorPrimaryName());
            if (!empty($product->getStyleColorStyleSkuColor())) {
                $primaryColor->setAbbreviation(substr($product->getStyleColorStyleSkuColor(), 0, 3));
            }

            // save
            $primaryColor = $this->colorPrimaryRepository->save($primaryColor);

            // add created color to the local list
            $this->primaryColors[] =
                [
                    'id' => $primaryColor->getId(),
                    'color_pattern_id' => $primaryColor->getColorPatternId(),
                    'label' => $primaryColor->getPrimaryCpName(),
                    'abbreviation' => $primaryColor->getAbbreviation()
                ];

            // return color id
            return "" . $primaryColor->getId();
        } catch (Throwable $ex) {
            throw new LocalizedException(
                new Phrase("Can't create primary color: " . $product->getStyleColorPrimaryName() .
                            "; Message: " . $ex->getMessage()),
                $ex
            );
        }
    }

    /**
     * Create New Secondary Color
     *
     * @param StyleInterface $product
     * @param string $primaryColorId
     *
     * @return string
     * @throws Throwable
     * @noinspection PhpUndefinedMethodInspection
     */
    private function createSecondaryColor(StyleInterface $product, string $primaryColorId): string
    {
        try {
            // create new color
            $secondaryColor = $this->colorSecondaryInterfaceFactory->create();

            // set parameters
            $secondaryColor->setPrimaryCpId($primaryColorId);
            $secondaryColor->setSecondaryCpName($product->getStyleColorSecondaryName());
            if (!empty($product->getStyleColorStyleSkuColor())) {
                $secondaryColor->setAbbreviation(substr($product->getStyleColorStyleSkuColor(), 3, 3));
            }
            $secondaryColor->setFullAbbreviation($product->getStyleColorStyleSkuColor());

            // save
            $secondaryColor = $this->colorSecondaryRepository->save($secondaryColor);

            // add created color to the local list
            $this->secondaryColors[] =
                [
                    'id' => $secondaryColor->getId(),
                    'primary_cp_id' => $secondaryColor->getPrimaryCpId(),
                    'label' => $secondaryColor->getSecondaryCpName(),
                    'abbreviation' => $secondaryColor->getAbbreviation(),
                    'full_abbreviation' => $secondaryColor->getFullAbbreviation()
                ];

            // return color id
            return "" . $secondaryColor->getId();
        } catch (Throwable $ex) {
            throw new LocalizedException(
                new Phrase(
                    "Can't create secondary color " . $product->getStyleColorSecondaryName() .
                    "; Message: " . $ex->getMessage()
                ),
                $ex
            );
        }
    }

    /**
     * Get values for All product's options attributes
     *
     * @throws Throwable
     */
    private function loadProductAttributes()
    {
        // prepare filters
        $filters = [
            $this->filterBuilder->setField('attribute_code')
                ->setConditionType('in')
                ->setValue([
                    'collection_name', 'manufacturer', 'color', 'c38_color_pattern', 'size',
                    'tx_parentcategory_id', 'tx_category_id', 'tx_sub_category_id'
                ])
                ->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();

        // load all options for attributes
        foreach ($this->productAttributeRepository->getList($searchCriteria)->getItems() as $attribute) {
            // add attribute id by code
            $this->productAttributeIds[$attribute->getAttributeCode()] = $attribute->getAttributeId();
            // add all options for this attribute
            $this->productAttributes[$attribute->getAttributeCode()] = $this->attributeOptionsToArray($attribute);
        }
    }

    /**
     * Load Colors
     *
     * @throws Throwable
     * @noinspection PhpPossiblePolymorphicInvocationInspection
     */
    private function loadColors()
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilters([])->create();

        // load primary colors
        $primaryColors = $this->colorPrimaryRepository->getList($searchCriteria)->getItems();
        // add all primary colors information to the list
        foreach ($primaryColors as $primaryColor) {
            $this->primaryColors[] =
                [
                    'id' => $primaryColor->getId(),
                    'color_pattern_id' => $primaryColor->getColorPatternId(),
                    'label' => $primaryColor->getPrimaryCpName(),
                    'abbreviation' => $primaryColor->getAbbreviation()
                ];
        }

        // load secondary colors
        $secondaryColors = $this->colorSecondaryRepository->getList($searchCriteria)->getItems();
        // add all secondary colors information to the list
        foreach ($secondaryColors as $secondaryColor) {
            $this->secondaryColors[] =
                [
                    'id' => $secondaryColor->getId(),
                    'primary_cp_id' => $secondaryColor->getPrimaryCpId(),
                    'label' => $secondaryColor->getSecondaryCpName(),
                    'abbreviation' => $secondaryColor->getAbbreviation(),
                    'full_abbreviation' => $secondaryColor->getFullAbbreviation()
                ];
        }
    }

    /**
     * Get Taxonomy Category Mapping
     *
     * @throws Throwable
     */
    private function loadTaxonomyMapping()
    {
        // get mapping for Taxonomy Category
        $this->taxonomyCategoryMappings = [];
        foreach ($this->taxonomyCategoryCollection->create() as $txCategory) {
            $this->taxonomyCategoryMappings[$txCategory->getTxCategoryId()] = $txCategory->getTxParentcategoryId();
        }

        // get mapping for Taxonomy Sub Category
        foreach ($this->taxonomySubcategoryCollection->create() as $txCategory) {
            // add sub-category mapping
            $this->taxonomySubCategoryMappings[$txCategory->getTxSubCategoryId()] = $txCategory->getTxCategoryId();
            // add sub-category attribute mapping
            $this->taxonomySubCategoryAttributesMappings[strtolower($txCategory->getTxSubCategoryName())] =
                $txCategory->getAttributeSetId();
            // add sub-category weight
            $this->taxonomySubCategoryWeights[$txCategory->getTxSubCategoryId()] = $txCategory->getWeight();
        }
    }

    /**
     * Load List of Swatch Attributes
     *
     * @return SwatchAttributeInterface[]
     * @throws Throwable
     */
    private function loadSwatchAttributes(): array
    {
        $swatchAttributeList = $this->swatchAttributeRepository->getList($this->searchCriteriaBuilder->create());
        // load list of all swatch attributes
        $swatchAttributes = [];
        foreach ($swatchAttributeList->getItems() as $swatchAttribute) {
            $swatchAttributes[] = $swatchAttribute;
        }
        // return result
        return $swatchAttributes;
    }

    /**
     * Get Color ID
     *
     * @param string $colorName
     * @param string $colorPatternId
     * @param bool $isPrimary
     * @return string
     */
    private function getColorId(string $colorName, string $colorPatternId, bool $isPrimary = true): string
    {
        // check value
        if (empty($colorName)) {
            return "";
        }
        // prepare value
        $colorName = trim(mb_strtolower($colorName));
        // name correction
        if ($colorName == 'tie-dye') {
            $colorName = 'tie- dye';
        }

        if ($isPrimary) {
            // primary settings
            $colorList = $this->primaryColors;
            $parentName = 'color_pattern_id';
        } else {
            // secondary settings
            $colorList = $this->secondaryColors;
            $parentName = 'primary_cp_id';
        }

        // looking color in the array
        foreach ($colorList as $color) {
            // check data
            if (empty($color['label']) || empty($color[$parentName]) || !is_string($color['label'])) {
                continue;
            }
            // if color and patternId equals
            if (trim(mb_strtolower($color['label'])) == $colorName &&
                $color[$parentName] == $colorPatternId) {
                return $this->nullToString($color['id']);
            }
        }

        return "";
    }

    /**
     * Search value in the array
     *
     * @param string $label
     * @param array $array
     * @return string
     */
    private function getValueByLabel(string $label, array $array): string
    {
        // check value
        if (empty($label)) {
            return "";
        }
        // prepare value
        $label = trim(mb_strtolower($label));

        // get all elements in the array
        foreach ($array as $value) {
            // check value
            if (empty($value['label']) || !is_string($value['label'])) {
                continue;
            }
            // check if value equals
            if (trim(mb_strtolower($value['label'])) == $label) {
                return $this->nullToString($value['value']);
            }
        }

        return "";
    }

    /**
     * Get Swatch Attribute by Value
     *
     * @param string|null $label
     * @param string|null $optionId
     * @param SwatchAttributeInterface[] $swatchAttributes
     * @return SwatchAttributeInterface|null
     */
    private function getSwatchAttribute(
        ?string $label,
        ?string $optionId,
        array $swatchAttributes
    ): ?SwatchAttributeInterface {
        // check data
        if (empty($label) || empty($optionId)) {
            return null;
        }
        // find swatch attribute with requested value and option id parameters
        foreach ($swatchAttributes as $swatchAttribute) {
            if (!empty($swatchAttribute->getValue()) && !empty($swatchAttribute->getOptionId()) &&
                $swatchAttribute->getValue() == $label && $swatchAttribute->getOptionId() == $optionId) {
                return $swatchAttribute;
            }
        }

        // nothing found
        return null;
    }

    /**
     * Select Attribute with Parent ID
     *
     * @param string $label
     * @param array $array
     * @param array $mapping
     * @param string $parentId
     *
     * @return string
     */
    private function searchWithParent(string $label, array $array, array $mapping, string $parentId): string
    {
        // check value
        if (empty($label)) {
            return "";
        }
        // prepare value
        $label = trim(mb_strtolower($label));

        // check all elements
        foreach ($array as $value) {
            // check if label set
            if (empty($value['label']) || !is_string($value['label'])) {
                continue;
            }
            // check if value equals
            if (trim(mb_strtolower($value['label'])) == $label) {
                if (empty($mapping[$value['value']])) {
                    continue;
                }
                if ($mapping[$value['value']] == $parentId) {
                    return $this->nullToString($value['value']);
                }
            }
        }

        return "";
    }

    /**
     * Format Attribute Option list to the array
     *
     * @param AttributeInterface $attribute
     *
     * @return array
     * @throws Throwable
     */
    private function attributeOptionsToArray(AttributeInterface $attribute): array
    {
        $options = [];
        // add each option to the array
        foreach ($attribute->getOptions() as $option) {
            $options[] = [
                'label' => $option->getLabel(),
                'value' => $option->getValue()
            ];
        }

        // return result
        return $options;
    }

    /**
     * Add attribute code to the update list
     *
     * @param string $attributeCode
     */
    private function needUpdateAttributeCode(string $attributeCode)
    {
        // check attribute code
        if (empty($attributeCode)) {
            return;
        }
        if (in_array($attributeCode, $this->updatedAttributeCodes)) {
            return;
        }
        // add to the update list
        $this->updatedAttributeCodes[] = $attributeCode;
    }

    /**
     * Convert NULL to "" (empty string)
     *
     * @param $value
     * @return string
     */
    private function nullToString($value): string
    {
        return empty($value) ? "" : $value;
    }
}
