<?php declare(strict_types=1);

namespace C38\ProductImport\Helper;

use C38\ProductImport\Api\Data\PurchaseOrderLoggerInterfaceFactory;
use C38\ProductImport\Api\PurchaseOrderLoggerRepositoryInterface;

/**
 * Class PurchaseOrderLogger
 * Logger Helper for Purchase Order process
 */
class PurchaseOrderLogger
{
    const PURCHASE_ORDER_LOG_ERROR = 'ERROR';

    const PURCHASE_ORDER_LOG_INFO = 'INFO';

    const PURCHASE_ORDER_LOG_WARNING = 'WARNING';

    /**
     * @var PurchaseOrderLoggerInterfaceFactory
     */
    private $loggerFactory;

    /**
     * @var PurchaseOrderLoggerRepositoryInterface
     */
    private $loggerRepository;

    /**
     * Logger constructor.
     *
     * @param PurchaseOrderLoggerInterfaceFactory $loggerFactory
     * @param PurchaseOrderLoggerRepositoryInterface $loggerRepository
     */
    public function __construct(
        PurchaseOrderLoggerInterfaceFactory $loggerFactory,
        PurchaseOrderLoggerRepositoryInterface $loggerRepository
    ) {
        $this->loggerFactory = $loggerFactory;
        $this->loggerRepository = $loggerRepository;
    }

    /**
     * Log Message in the Database
     *
     * @param string $type
     * @param string $message
     * @param string $stackTrace
     * @param string $cronName
     */
    public function log(string $type, string $message, string $stackTrace, string $cronName)
    {
        // create new log record
        $log = $this->loggerFactory->create();

        // add data to the record
        $log->setType($type);
        $log->setMessage($message);
        $log->setStack($stackTrace);
        $log->setCronName($cronName);

        // save record
        $this->loggerRepository->save($log);
    }

    /**
     * Log Error
     *
     * @param string $message
     * @param string $stackTrace
     * @param string $cronName
     */
    public function error(string $message, string $stackTrace, string $cronName)
    {
        $this->log(self::PURCHASE_ORDER_LOG_ERROR, $message, $stackTrace, $cronName);
    }

    /**
     * Log Warning
     *
     * @param string $message
     * @param string $stackTrace
     * @param string $cronName
     */
    public function warning(string $message, string $stackTrace, string $cronName)
    {
        $this->log(self::PURCHASE_ORDER_LOG_WARNING, $message, $stackTrace, $cronName);
    }

    /**
     * Log Info
     *
     * @param string $message
     * @param string $cronName
     */
    public function info(string $message, string $cronName)
    {
        $this->log(self::PURCHASE_ORDER_LOG_INFO, $message, '', $cronName);
    }
}
