<?php declare(strict_types=1);

namespace C38\ProductImport\Helper;

use C38\Catalog\Model\Product\AttributeValueUpdater;
use C38\ProductImport\Api\Data\StyleInterface;
use C38\ProductImport\Api\Data\SuperAttributeInterfaceFactory;
use C38\ProductImport\Api\Data\URLRewriteInterface;
use C38\ProductImport\Api\SuperAttributeRepositoryInterface;
use C38\ProductImport\Api\UPCRepositoryInterface;
use C38\ProductImport\Api\URLRewriteRepositoryInterface;
use C38\ProductImport\Model\Source\CreatedFrom;
use Exception;
use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Store\Model\StoreManagerInterface;
use Throwable;

/**
 * Class ProductHelper
 * Create magento product from ERP style object
 */
class ProductHelper
{
    const DEFAULT_WEBSITE_ID = 1;

    const DEFAULT_TAX_CLASS = 2;

    const DEFAULT_DESCRIPTION = '.';

    const DEFAULT_WEIGHT = '1.000000';

    const DEFAULT_ASSIGN_TO_CATEGORIES = ["810"];

    /**
     * @var AttributesHelper
     */
    private $attributesHelper;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var SuperAttributeInterfaceFactory
     */
    private $superAttributeFactory;

    /**
     * @var SuperAttributeRepositoryInterface
     */
    private $superAttributeRepository;

    /**
     * @var URLRewriteRepositoryInterface
     */
    private $urlRewriteRepository;

    /**
     * @var UPCRepositoryInterface
     */
    private $upcRepository;

    /**
     * @var AttributeValueUpdater
     */
    private $attributeValueUpdater;

    /**
     * @var CategoryCollectionFactory
     */
    private $categoryCollection;

    /**
     * @var CategoryLinkManagementInterface
     */
    private $categoryLinkManagement;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var array
     */
    private $magentoCategories;

    /**
     * ProductHelper constructor.
     *
     * @param AttributesHelper $attributesHelper
     * @param ProductFactory $productFactory
     * @param ProductRepositoryInterface $productRepository
     * @param SuperAttributeInterfaceFactory $superAttributeFactory
     * @param SuperAttributeRepositoryInterface $superAttributeRepository
     * @param URLRewriteRepositoryInterface $urlRewriteRepository
     * @param UPCRepositoryInterface $upcRepository
     * @param AttributeValueUpdater $attributeValueUpdater
     * @param CategoryCollectionFactory $categoryCollection
     * @param CategoryLinkManagementInterface $categoryLinkManagement
     * @param StoreManagerInterface $storeManager
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     */
    public function __construct(
        AttributesHelper $attributesHelper,
        ProductFactory $productFactory,
        ProductRepositoryInterface $productRepository,
        SuperAttributeInterfaceFactory $superAttributeFactory,
        SuperAttributeRepositoryInterface $superAttributeRepository,
        URLRewriteRepositoryInterface $urlRewriteRepository,
        UPCRepositoryInterface $upcRepository,
        AttributeValueUpdater $attributeValueUpdater,
        CategoryCollectionFactory $categoryCollection,
        CategoryLinkManagementInterface $categoryLinkManagement,
        StoreManagerInterface $storeManager,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder
    ) {
        $this->attributesHelper = $attributesHelper;
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->superAttributeFactory = $superAttributeFactory;
        $this->superAttributeRepository = $superAttributeRepository;
        $this->urlRewriteRepository = $urlRewriteRepository;
        $this->upcRepository = $upcRepository;
        $this->attributeValueUpdater = $attributeValueUpdater;
        $this->categoryCollection = $categoryCollection;
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->storeManager = $storeManager;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
    }

    /**
     * Load Attributes
     *
     * @param bool $isReload
     *
     * @throws Throwable
     */
    public function loadAttributes(bool $isReload = false)
    {
        if ($isReload) {
            $this->attributesHelper->reloadAttributes();
        } else {
            $this->attributesHelper->loadAttributes();
        }
    }

    /**
     * Create attributes for product
     *
     * @param StyleInterface $product
     * @throws Throwable
     */
    public function createAttributes(StyleInterface $product)
    {
        $this->attributesHelper->createAttributes($product);
    }

    /**
     * @param StyleInterface $product
     *
     * @return array
     * @throws Throwable
     */
    public function prepareProduct(StyleInterface $product): array
    {
        // create product data array
        $productData = [];

        // set attribute set id by taxonomy name
        $productData['attribute_set_id'] =
            $this->attributesHelper->getTaxonomyAttributeSetId($product->getStyleCategorySubDescription());

        // get taxonomy parent category id
        $productData['tx_parentcategory_id'] = $this->attributesHelper->getAttributeValue(
            $product->getStyleCategoryParentDescription(),
            'tx_parentcategory_id'
        );

        // set taxonomy category id
        if (!empty($productData['tx_parentcategory_id'])) {
            $productData['tx_category_id'] = $this->attributesHelper->getTaxonomyCategoryId(
                $product->getStyleCategoryDescription(),
                $productData['tx_parentcategory_id'],
                true
            );
        }

        // set taxonomy sub category
        if (!empty($productData['tx_category_id'])) {
            $productData['tx_sub_category_id'] = $this->attributesHelper->getTaxonomyCategoryId(
                $product->getStyleCategorySubDescription(),
                $productData['tx_category_id'],
                false
            );
        }

        // set products parameters
        if (!empty($product->getInventorySKU())) {
            $productData['sku'] = $product->getInventorySKU();
        }
        if (!empty($product->getStyleBaseName())) {
            $productData['name'] = $product->getStyleBaseName();
        }
        if (!empty($product->getStyleSkuBase())) {
            $productData['base_sku'] = $product->getStyleSkuBase();
        }
        if (!empty($product->getStyleColorVendorSku())) {
            $productData['vendor_style_id'] = $product->getStyleColorVendorSku();
        }
        if (!empty($product->getStyleSeasonsLabel())) {
            $productData['season'] = $product->getStyleSeasonsLabel();
        }
        if (!empty($product->getIsExclusive())) {
            $productData['is_exclusive'] = $product->getIsExclusive();
        }
        if (!empty($product->getStyleColorVendorColor())) {
            $productData['color_name'] = $product->getStyleColorVendorColor();
        }
        if (!empty($product->getStyleColorPrimaryName())) {
            $productData['c38_primary_color_name'] = $product->getStyleColorPrimaryName();
        }
        if (!empty($product->getStyleColorSecondaryName())) {
            $productData['c38_secondary_color_name'] = $product->getStyleColorSecondaryName();
        }
        if (!empty($product->getInventorySize())) {
            $productData['inventory_size_name'] = $product->getInventorySize();
        }
        if (!empty($product->getStyleCategorySubDefWeight())) {
            // get weight from ERP
            $productData['weight'] = $product->getStyleCategorySubDefWeight();
        } else {
            // get default weight for taxonomy subcategory
            $defaultWeight = $this->attributesHelper->getDefaultWeight($productData['tx_sub_category_id']);
            if (!empty($defaultWeight)) {
                $productData['weight'] = $defaultWeight;
            } else {
                $productData['weight'] = self::DEFAULT_WEIGHT;
            }
        }
        $productData['weight'] = number_format(floatval($productData['weight']), 6);

        // set product attributes id
        $productData['color'] = $this->attributesHelper->getAttributeValue(
            $product->getStyleColorVendorColor(),
            'color'
        );
        $productData['collection_name'] = $this->attributesHelper->getAttributeValue(
            $product->getCollectionName(),
            'collection_name'
        );
        $productData['size'] = $this->attributesHelper->getAttributeValue(
            $product->getInventorySize(),
            'size'
        );
        $productData['manufacturer'] = $this->attributesHelper->getAttributeValue(
            $product->getStyleDesignerDescription(),
            'manufacturer'
        );
        $productData['c38_color_pattern'] = $this->attributesHelper->getAttributeValue(
            $product->getStyleColorPatternLabel(),
            'c38_color_pattern'
        );

        // get primary color id
        $productData['c38_primary_color'] = $this->attributesHelper->getColorAttributeId(
            $product->getStyleColorPrimaryName(),
            $productData['c38_color_pattern'],
            true
        );

        // get secondary color
        $productData['c38_secondary_color'] = $this->attributesHelper->getColorAttributeId(
            $product->getStyleColorSecondaryName(),
            $productData['c38_primary_color'],
            false
        );

        // set cost
        $cost = 0;
        if (!empty($product->getStyleColorVendorCost())) {
            $cost = $product->getStyleColorVendorCost();
        }
        $productData['cost'] = number_format(floatval($cost), 6);

        // set standard cost
        if (!empty($product->getStyleColorStandardCost())) {
            $standard_cost = $product->getStyleColorStandardCost();
        } else {
            $standard_cost = $cost;
        }
        $productData['standard_cost'] = number_format(floatval($standard_cost), 6);

        // set cost after discount
        $cost_after_discount = 0;
        if (!empty($product->getStyleColorCostAfterDiscount())) {
            $cost_after_discount = $product->getStyleColorCostAfterDiscount();
        }
        $productData['cost_after_discount'] = number_format(floatval($cost_after_discount), 6);

        // set price
        $price = 0;
        if (!empty($product->getStyleBaseMSRP())) {
            $price = $product->getStyleBaseMSRP();
        }
        $productData['price'] = number_format(floatval($price), 6);

        // check product before
        $this->checkProduct($productData);

        // return result
        return $productData;
    }

    /**
     * Create new configurable product
     *
     * @param array $productData
     * @param bool $isConfigurable
     *
     * @return ProductInterface
     * @throws Throwable
     * @noinspection PhpUndefinedMethodInspection
     * @noinspection PhpPossiblePolymorphicInvocationInspection
     */
    public function createProduct(array $productData, bool $isConfigurable = true): ProductInterface
    {
        // set store id
        $this->storeManager->setCurrentStore(0);

        // initial check
        if (!$isConfigurable) {
            // check product size for simple product
            if (empty($productData['size'])) {
                throw new LocalizedException(
                    new Phrase("Simple product {$productData['sku']} doesn't have Product Size")
                );
            }
        }

        // create new product
        $product = $this->productFactory->create();

        // add attributes for all product types
        $product
            ->setStoreId(0)
            ->setWebsiteIds([self::DEFAULT_WEBSITE_ID])
            ->setAttributeSetId($productData['attribute_set_id'])
            ->setCreatedAt(date('Y-m-d H:i:s'))
            ->setSku($productData['sku'])
            ->setBaseSku($productData['base_sku'])
            ->setTaxClassId(self::DEFAULT_TAX_CLASS)
            ->setManufacturer($productData['manufacturer'])
            ->setTxParentcategoryId($productData['tx_parentcategory_id'])
            ->setTxCategoryId($productData['tx_category_id'])
            ->setTxSubCategoryId($productData['tx_sub_category_id'])
            ->setColor($productData['color'])
            ->setWeight($productData['weight'])
            ->setData('c38_color_pattern', $productData['c38_color_pattern'])
            ->setData('c38_primary_color', $productData['c38_primary_color'])
            ->setData('c38_secondary_color', $productData['c38_secondary_color'])
            ->setCost($productData['cost'])
            ->setStandardCost($productData['standard_cost'])
            ->setCostAfterDiscount($productData['cost_after_discount'])
            ->setPrice($productData['price'])
            ->setDescription(self::DEFAULT_DESCRIPTION)
            ->setShortDescription(self::DEFAULT_DESCRIPTION)
            ->setCategoryIds([])
            ->setCreatedFromProductTools(CreatedFrom::VALUE_ERP_NOT_INDEXING);

        // set is exclusive attribute
        if (!empty($productData['is_exclusive']) && $productData['is_exclusive'] == "1") {
            $product->setData('c38_exclusive', '1');
        } else {
            $product->setData('c38_exclusive', '0');
        }

        // set parameters for type
        if ($isConfigurable) {
            // for configurable
            $product
                ->setTypeId('configurable')
                ->setUrlKey($this->generateProductURL($productData))
                ->setName($productData['name'])
                ->setStatus(Status::STATUS_DISABLED)
                ->setVisibility(Visibility::VISIBILITY_BOTH);
            // set collection name
            if (!empty($productData['collection_name'])) {
                $product->setCollectionName($productData['collection_name']);
            }

            // add attributes to the product
            $sizeAttributeId = $this->attributesHelper->getAttributeCodeId('size');
            $product->getTypeInstance()->setUsedProductAttributes($product, [$sizeAttributeId]);
            $configurableAttributesData = $product->getTypeInstance()->getConfigurableAttributesAsArray($product);
            $product->setCanSaveConfigurableAttributes(true);
            $product->setConfigurableAttributesData($configurableAttributesData);
            $product->setConfigurableProductsData([]);

            $product->setAffectConfigurableProductAttributes($productData['attribute_set_id']);
            $product->setNewVariationsAttributeSetId($productData['attribute_set_id']);
            // add stock data
            $product->setData(
                'stock_data',
                [
                    'use_config_manage_stock' => 0,
                    'manage_stock' => 0,
                    'is_in_stock' => 1,
                    'qty' => 0
                ]
            );
        } else {
            // for simple
            $product
                ->setTypeId('simple')
                ->setName($productData['name'] . "-" . strtoupper($productData['inventory_size_name']))
                ->setUrlKey($this->generateProductURL($productData, false))
                ->setSize($productData['size'])
                ->setStatus(Status::STATUS_ENABLED)
                ->setVisibility(Visibility::VISIBILITY_NOT_VISIBLE);
            // add stock data
            $product->setData(
                'stock_data',
                [
                    'use_config_manage_stock' => 1,
                    'manage_stock' => 1,
                    'is_in_stock' => 0,
                    'qty' => 0
                ]
            );
        }

        // save product
        $product = $this->productRepository->save($product);

        // after save
        if ($isConfigurable) {
            // create supper attribute for configurable product
            $supperAttribute = $this->superAttributeFactory->create();
            $supperAttribute->setProductId($product->getId());
            $supperAttribute->setAttributeId($this->attributesHelper->getAttributeCodeId('size'));
            $supperAttribute->setPosition("0");
            // save
            $this->superAttributeRepository->save($supperAttribute);
        } else {
            // set UPC numbers for simple product
            $this->setUpc("" . $product->getId());
        }

        // return new product id
        return $product;
    }

    /**
     * Update Configurable Product
     *
     * @param array $productData
     * @param ProductInterface $product
     * @param bool $isConfigurable
     *
     * @return int
     * @throws Throwable
     * @noinspection PhpUndefinedMethodInspection
     */
    public function updateProduct(array $productData, ProductInterface $product, bool $isConfigurable = true): int
    {
        // set store id
        $this->storeManager->setCurrentStore(0);

        // prepare updated data for product
        $updateData = [
            'price' => $productData['price'],
            'standard_cost' => $productData['standard_cost'],
            'cost_after_discount' => $productData['cost_after_discount'],
            'manufacturer' => $productData['manufacturer']
        ];
        $updateData['cost'] = $productData['cost'];

        if (!empty($productData['color'])) {
            $updateData['color'] = $productData['color'];
        }

        // set attribute for type
        if ($isConfigurable) {
            // for configurable product
            $updateData['name'] = $productData['name'];
            if (!empty($productData['collection_name'])) {
                $updateData['collection_name'] = $productData['collection_name'];
            }
        } else {
            // for simple product
            $updateData['name'] = $productData['name'] . "-" . strtoupper($productData['inventory_size_name']);
            if (!empty($productData['size'])) {
                $updateData['size'] = $productData['size'];
            }
            if (!empty($productData['weight'])) {
                $updateData['weight'] = $productData['weight'];
            }
        }

        // compare if any data was changed
        $diff = array_diff_assoc($updateData, $product->getData());
        // update product
        if (!empty($diff)) {
            $productId = (int)$product->getId();
            // update product attributes
            $this->attributeValueUpdater->updateAttributes($productId, $diff, 0);
            // return product id
            return $productId;
        }

        // products wasn't update
        return -1;
    }

    /**
     * Check if all required attributes set
     *
     * @param array $productData
     * @throws Exception
     */
    private function checkProduct(array $productData)
    {
        // list of invalid attributes
        $invalidAttributes = [];

        // check product data attributes
        if (empty($productData['manufacturer'])) {
            $invalidAttributes[] = "Designer";
        }
        if (empty($productData['c38_color_pattern'])) {
            $invalidAttributes[] = "Color Pattern";
        }
        if (empty($productData['c38_primary_color'])) {
            $invalidAttributes[] = "Primary Color";
        }
        if (empty($productData['c38_secondary_color'])) {
            $invalidAttributes[] = "Secondary Color";
        }
        if (empty($productData['attribute_set_id'])) {
            $invalidAttributes[] = "Attribute Set";
        }
        if (empty($productData['tx_parentcategory_id'])) {
            $invalidAttributes[] = "Taxonomy Parent Category";
        }
        if (empty($productData['tx_category_id'])) {
            $invalidAttributes[] = "Taxonomy Category";
        }
        if (empty($productData['tx_sub_category_id'])) {
            $invalidAttributes[] = "Taxonomy Sub-Category";
        }

        // if error found
        if (!empty($invalidAttributes)) {
            throw new LocalizedException(new Phrase("Attributes for product {$productData['sku']} not set: " .
                implode(', ', $invalidAttributes)));
        }
    }

    /**
     * Generate Product URL
     *
     * @param array $productData
     * @param bool $isConfigurable
     *
     * @return string
     * @throws Throwable
     */
    private function generateProductURL(array $productData, bool $isConfigurable = true): string
    {
        // list of unique postfix (for unique URL)
        $uniquePostfix = [
            "", '1',
            $productData['c38_primary_color_name'], $productData['c38_secondary_color_name'], '2',
            $productData['c38_primary_color_name'] . "-1", $productData['c38_primary_color_name'] . "-2",
            $productData['c38_primary_color_name'] . "-" . $productData['c38_secondary_color_name'],
            $productData['c38_primary_color_name'] . "-" . $productData['c38_secondary_color_name'] . "-1",
            $productData['c38_primary_color_name'] . "-" . $productData['c38_secondary_color_name'] . "-2"
        ];

        // generate list of URLs with unique prefixes
        $urls = [];
        foreach ($uniquePostfix as $postfix) {
            $urls[] = "product/" . $this->generateUrlKey($productData, $postfix, $isConfigurable);
        }

        // load URLs from Database, and check if URLs are unique
        $filters = [
            $this->filterBuilder->setField(URLRewriteInterface::STORE_ID)
                ->setConditionType('eq')->setValue(self::DEFAULT_WEBSITE_ID)->create(),
            $this->filterBuilder->setField(URLRewriteInterface::ENTITY_TYPE)
                ->setConditionType('eq')->setValue('product')->create(),
            $this->filterBuilder->setField(URLRewriteInterface::REQUEST_PATH)
                ->setConditionType('in')->setValue($urls)->create()
        ];
        // create search criteria
        $searchCriteria = $this->searchCriteriaBuilder->addFilters($filters)->create();
        // if url found
        $foundUrls = [];
        foreach ($this->urlRewriteRepository->getList($searchCriteria)->getItems() as $urlRewrite) {
            /** @var URLRewriteInterface $urlRewrite */
            $foundUrls[] = $urlRewrite->getRequestPath();
        }

        // found url can be used
        $diff = array_diff($urls, $foundUrls);
        // nothing found, can't create url
        if (empty($diff)) {
            throw new LocalizedException(new Phrase("Can't create unique URL for product {$productData['sku']}"));
        }

        return str_replace("product/", "", array_shift($diff));
    }

    /**
     * Generate Product URL Key
     *
     * @param array $productData
     * @param string $uniquePostfix
     * @param bool $isConfigurable
     *
     * @return string
     */
    private function generateUrlKey(array $productData, string $uniquePostfix = '', bool $isConfigurable = true): string
    {
        // get color name
        $colorName = "";
        if (!empty($productData['color_name'])) {
            $colorName .= '-' . $productData['color_name'];
        } else {
            $colorName .= '-' . $productData['c38_primary_color'];
        }
        // add unique postfix
        if (!empty($uniquePostfix)) {
            $colorName .= '-' . $uniquePostfix;
        }

        // create url key
        if ($isConfigurable) {
            $url_key = $productData['name'] . $colorName;
        } else {
            $url_key = $productData['name'] . "-" . strtoupper($productData['size']) . $colorName;
        }
        $url_key = strtolower($url_key);
        $url_key = preg_replace('/[^a-z0-9]/', '-', $url_key);
        $url_key = preg_replace('/[\-]{2,}/', '-', $url_key);
        $url_key = preg_replace('/^[\-]/', '', $url_key);
        $url_key = preg_replace('/[\-]&/', '', $url_key);

        return $url_key;
    }

    /**
     * Set UPC number for the product
     *
     * @param string $productId
     * @throws Throwable
     */
    private function setUpc(string $productId)
    {
        // calculate UPC number for the product
        $productUpc = $this->upcRepository->calculateUPC($productId);
        if (!empty($productUpc)) {
            // set upc code for product
            $this->attributeValueUpdater->updateAttribute((int)$productId, 'upc', $productUpc);
        }
    }

    /**
     * Assign Simple Product to the Configurable (parent) product
     *
     * @param string $productId
     * @param ProductInterface $parentProduct
     *
     * @throws Throwable
     * @noinspection PhpUndefinedMethodInspection
     */
    public function assignSimpleToConfigurable(string $productId, ProductInterface $parentProduct)
    {
        $parentProduct = $this->productRepository->getById($parentProduct->getId());
        // get list of all child ids
        $allChildIds[] = $productId;

        // load all simple product for parent (configurable product)
        foreach ($parentProduct->getTypeInstance()->getUsedProducts($parentProduct) as $simple) {
            $allChildIds[] = $simple->getId();
        }

        // add list of associated products to the parent product
        $parentProduct->setAssociatedProductIds($allChildIds);
        $parentProduct->setCanSaveConfigurableAttributes(true);

        // save product
        $this->productRepository->save($parentProduct);
    }

    /**
     * Assign Product to the Category
     *
     * @param string $sku
     * @param string | null $designer
     * @param string | null $designerDescription
     * @throws Throwable
     */
    public function assignProductToCategory(string $sku, ?string $designer, ?string $designerDescription)
    {
        // check parameters
        if (empty($designer) && empty($designerDescription)) {
            return;
        }

        // load magento categories
        if (empty($this->magentoCategories)) {
            $this->magentoCategories = $this->getMagentoCategories();
        }

        // get magento category id
        $categoryIDs = self::DEFAULT_ASSIGN_TO_CATEGORIES;
        $categoryID = $this->getDesignerCategoryID($designer, $designerDescription);
        if (!empty($categoryID)) {
            $categoryIDs[] = $categoryID;
        }

        // assign product to the category
        try {
            // add product to categories
            $this->categoryLinkManagement->assignProductToCategories($sku, $categoryIDs);
        } catch (Throwable $ex) {
            throw new LocalizedException(
                new Phrase($ex->getMessage())
            );
        }
    }

    /**
     * Get Designer Category ID by Designer Name
     *
     * @param string | null  $designer
     * @param string | null  $designerDescription
     *
     * @return string | null
     * @throws Throwable
     */
    private function getDesignerCategoryID(?string $designer, ?string $designerDescription): ?string
    {
        // prepare parameters
        if (empty($designer)) {
            $designer = '';
        } else {
            $designer = strtolower(trim($designer));
        }
        if (empty($designerDescription)) {
            $designerDescription = '';
        } else {
            $designerDescription = strtolower(trim($designerDescription));
        }

        // search designers in the magento category list
        foreach ($this->magentoCategories as $category) {
            if (empty($category->getName())) {
                continue;
            }
            $categoryName = strtolower(trim($category->getName()));
            if ($categoryName == $designer || $categoryName == $designerDescription) {
                return $category->getId();
            }
        }

        // nothing found
        return null;
    }

    /**
     * Get List of all Magento Categories
     *
     * @return array
     * @throws Throwable
     */
    private function getMagentoCategories(): array
    {
        // get magento categories collection
        $magentoCategories = [];
        $categories = $this->categoryCollection->create()->addAttributeToSelect('*');

        // check all categories
        foreach ($categories as $category) {
            $magentoCategories[$category->getId()] = $category;
        }

        //return result
        return $magentoCategories;
    }
}
