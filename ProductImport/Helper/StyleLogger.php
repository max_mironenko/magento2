<?php declare(strict_types=1);

namespace C38\ProductImport\Helper;

use C38\ProductImport\Api\Data\StyleLoggerInterfaceFactory;
use C38\ProductImport\Api\StyleLoggerRepositoryInterface;

/**
 * Class StyleLogger
 * Logger for Import Style (create products) process
 */
class StyleLogger
{
    const STYLE_LOG_ERROR = 'ERROR';

    const STYLE_LOG_INFO = 'INFO';

    const STYLE_WARNING = 'WARNING';

    /**
     * @var StyleLoggerInterfaceFactory
     */
    private $loggerFactory;

    /**
     * @var StyleLoggerRepositoryInterface
     */
    private $loggerRepository;

    /**
     * Logger constructor.
     *
     * @param StyleLoggerInterfaceFactory $loggerFactory
     * @param StyleLoggerRepositoryInterface $loggerRepository
     */
    public function __construct(
        StyleLoggerInterfaceFactory $loggerFactory,
        StyleLoggerRepositoryInterface $loggerRepository
    ) {
        $this->loggerFactory = $loggerFactory;
        $this->loggerRepository = $loggerRepository;
    }

    /**
     * Log Message in the Database
     *
     * @param string $type
     * @param string $message
     * @param string $stackTrace
     * @param string $cronName
     * @param string $productName
     */
    public function log(string $type, string $message, string $stackTrace, string $cronName, string $productName)
    {
        // create new log record
        $log = $this->loggerFactory->create();

        // add data to the record
        $log->setType($type);
        $log->setMessage($message);
        $log->setFullError($stackTrace);
        $log->setCronName($cronName);
        $log->setProductId($productName);

        // save record
        $this->loggerRepository->save($log);
    }

    /**
     * Log Error
     *
     * @param string $message
     * @param string $stackTrace
     * @param string $cronName
     * @param string $productName
     */
    public function error(string $message, string $stackTrace, string $cronName, string $productName = '')
    {
        $this->log(self::STYLE_LOG_ERROR, $message, $stackTrace, $cronName, $productName);
    }

    /**
     * Log Warning
     *
     * @param string $message
     * @param string $stackTrace
     * @param string $cronName
     * @param string $productName
     */
    public function warning(string $message, string $stackTrace, string $cronName, string $productName = '')
    {
        $this->log(self::STYLE_WARNING, $message, $stackTrace, $cronName, $productName);
    }

    /**
     * Log Info
     *
     * @param string $message
     * @param string $cronName
     * @param string $productName
     */
    public function info(string $message, string $cronName, string $productName = '')
    {
        $this->log(self::STYLE_LOG_INFO, $message, '', $cronName, $productName);
    }
}
